<!DOCTYPE html>
<html dir="ltr" lang="en-US">

@include('templates.frontend.magenta.includes.head')

<body class="stretched no-transition">

<!-- Document Wrapper
============================================= -->
<div id="wrapper" class="clearfix">

    <!-- Header
    ============================================= -->
    @include('templates.frontend.magenta.includes.header')

    <!-- Content
    ============================================= -->
    <section id="content">
        <div class="content-wrap pt-3 pb-0">

            @include('templates.frontend.magenta.includes.alert')
            @include('templates.frontend.magenta.includes.alert_email')
            <div class="section nobg py-2" @if(!$home->slogan && !$home->introduksi) style="display: none;" @endif>
                <div class="container">
                    <div class="row">
                        <div class="col-md-7">
                            <h2 class="display-4 t600 ls--2">{{ $home->slogan }} <span class="text-rotater" data-separator="|" data-rotate="fadeIn" data-speed="2000">
									<span style="color:#1ABC9C;"></span>
								</span></h2>                               
                        </div>
                        <div class="col-md-5">
                            <p class="lead text-muted">
                            {{ $home->introduksi }}
                            </p>
                        </div>
                    </div>
                </div>
            </div>

            <div class="ohidden align-items-center" data-bottom-top="background-position:0px 200px;" data-top-bottom="background-position:0px -400px;" style="background-image: url({{ Str::replaceFirst('public/', 'storage/', $home->gambar_depan) }}); background-size: cover; height: 100%; width: auto;">
                <img src="{{ Str::replaceFirst('public/', 'storage/', $home->gambar_depan) }}" style="visibility: hidden;" />
                <div class="container">
                    <div class="row">
                        <div class="col-lg-6">
                            <h2 class="text-white">{{ $home->teks_gambar }}</h2>
                        </div>
                    </div>
                </div>
            </div>

            <div class="bg-theme-light">
                @php
                    $stepIsKosong = false;
                    if(!$home->step1_judul && !$home->step1_teks && !$home->step2_judul && !$home->step2_teks && !$home->step3_judul && !$home->step3_teks){
                        $stepIsKosong = true;
                    }
                @endphp
                <div class="shadow-sm hero-features bgcolor dark shadow" @if($stepIsKosong) style="display: none;" @endif>
                    <div class="row">
                        <div class="col-md-4 mt-3 mt-md-0">
                            <div class="feature-box fbox-plain fbox-small fbox-dark  mb-0">
                                <div class="fbox-icon">
                                    <i class="icon-line-circle-check"></i>
                                </div>
                                <h3 class="text-white">{{ $home->step1_judul }}</h3>
                                <p class="text-white mb-0">{{ $home->step1_teks }}</p>
                            </div>
                        </div>

                        <div class="col-md-4 mt-3 mt-md-0">
                            <div class="feature-box fbox-plain fbox-small fbox-dark mb-0">
                                <div class="fbox-icon">
                                    <i class="icon-line2-users"></i>
                                </div>
                                <h3 class="text-white">{{ $home->step2_judul }}</h3>
                                <p class="text-white mb-0">{{ $home->step2_teks }}</p>
                            </div>
                        </div>

                        <div class="col-md-4 mt-3 mt-md-0">
                            <div class="feature-box fbox-plain fbox-small fbox-dark mb-0">
                                <div class="fbox-icon">
                                    <i class="icon-line-mail"></i>
                                </div>
                                <h3 class="text-white">{{ $home->step3_judul }}</h3>
                                <p class="text-white mb-0">{{ $home->step3_teks }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            @yield('content')

{{--            @include('templates.frontend.magenta.includes.knowmore')--}}

{{--            @include('templates.frontend.magenta.includes.featurebox')--}}

{{--            @include('templates.frontend.magenta.includes.selectyourplan')--}}

{{--            @include('templates.frontend.magenta.includes.gallery')--}}

{{--            @include('templates.frontend.magenta.includes.testimonials')--}}

{{--            @include('templates.frontend.magenta.includes.youtubevid')--}}

            {{--<div class="line"></div>--}}

{{--            @include('templates.frontend.magenta.includes.events')--}}

{{--            @include('templates.frontend.magenta.includes.contact')--}}

        </div>

    </section><!-- #content end -->

    <!-- Footer
    ============================================= -->
    <footer id="footer" class="noborder" style="background-color: #155BBC; padding-top: 80px; padding-top: 0px;">

    {{--@include('templates.frontend.magenta.includes.footer_widgets')--}}

        <!-- Copyrights -->
        @include('templates.frontend.magenta.includes.footer_copyright')
        <!-- copyrights end -->

    </footer><!-- #footer end -->

</div><!-- #wrapper end -->

<!-- Go To Top
============================================= -->
{{--<div id="gotoTop" class="icon-angle-up"></div>--}}

<!-- External JavaScripts
============================================= -->
<script src="{{url('/')}}/templates/frontend/magenta/js/jquery.js"></script>
<script src="{{url('/')}}/templates/frontend/magenta/js/plugins.js"></script>
<script src="{{url('/')}}/vendor/select2-4.0.13/dist/js/select2.full.min.js"></script>

<!-- Footer Scripts
============================================= -->
@include('templates.frontend.magenta.includes.metronic_scripts')
<script src="{{url('/')}}/templates/frontend/magenta/js/functions.js"></script>

@yield('pageScript')

</body>
</html>