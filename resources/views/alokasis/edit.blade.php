@extends((Request::ajax()) ? 'templates.layout-blank' : 'templates.'.config('genelator.template').'.layout')

@section('subheaderToolbar')
    <a href="#" class="btn invisible">
    </a>
@endsection

@section('content')
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">{{ $title }}</h5>
                <button id="closeBtn" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                @if($errors->isNotEmpty())
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-warning" role="alert">
                                <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                <div class="alert-text">
                                    <ul>
                                        @foreach($errors->all() as $message)
                                            <li>{{ $message }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-lg-12">
                        <div class="kt-portlet">
                            <!--begin::Form-->
                            <form action="{{ route('alokasi.update', $model->id) }}" id="editForm" method="post"
                                  class="kt-form" autocomplete="off" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="PUT">
                                <div class="kt-portlet__body">
                                    <div class="form-group row">
                                        <div class="col-lg-8">
                                            <label class="form-control-label">* Unit:</label>
                                            <select name="id_unit" id="id_unit" class="form-control select2ajax"
                                                    required data-ajax--url="{{ url('https://simanis.bumn.go.id/api/v1/select2/struktural_only') }}"
                                                    data-placeholder="Pilih" data-allow-clear="false">
                                                <option value="{{ $model->id_unit }}">{{ $model->unit }}
                                                </option>
                                            </select>
                                            <input type="hidden" id="unit" name="unit" value="{{ $model->unit }}">
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="col-lg-4">
                                            <label class="form-control-label">* Kuota:</label>
                                            <input type="number" name="kuota" id="kuota" class="form-control"
                                                   value="{{ $model->kuota }}" required>
                                            <span class="form-text text-muted">Jumlah kuota tersedia</span>
                                        </div>
                                        <div class="w-100"></div>
                                        <br>
                                    </div>
                                </div>
                                <div class="kt-portlet__foot">
                                    <div class="kt-form__actions">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <button type="submit" class="btn btn-warning">Submit</button>
                                                {{-- <a href="{{ url('alokasi/') }}" class="btn btn btn-secondary">Cancel</a> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!--end::Form-->
                        </div>
                        <!--end::Portlet-->
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection


@section('pageScripts')
    {!! $validator->selector('#editForm') !!}
    <script>
        jQuery(document).ready(function () {
            initSelect2();
            initDatepicker();
            if (!(typeof datatable === "undefined")) {
                //pass instance datatable (optional) agar di-reload ketika submit formnya sukses
                initAjaxForm('#editForm', datatable); //datatable dapat dari index.blade.php
            }

            $('#id_unit').change(function(){
                let data = $('#id_unit').select2('data');
                let namaUnit = data[0].text
                $('#unit').val(namaUnit);
            });
        });
    </script>
@endsection


{{-- For Modal --}}
@if(Request::ajax())
    @yield('content')
    @yield('pageScripts')
@endif