@extends('templates.frontend.magenta.layout_nonhome')

@section('content')
<div class="container">

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="nobottommargin">
                <div class="card-body" style="">
                    <form id="login-form" name="login-form" class="nobottommargin" action="{{ route('login') }}" method="POST">
                        @csrf

                        <h3>Login to your Account</h3>

                        <div class="col_full">
                            <label for="email"><span class="text-danger">*</span> Email:</label>
                            <input id="email" type="email" name="email" value="{{ old('email') }}" class="form-control @error('email') is-invalid @enderror" required autocomplete="email" autofocus>
                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col_full">
                            <label for="login-form-password"><span class="text-danger">*</span> Password:</label>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password"/>
                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>


                        <div class="col_full nobottommargin">
                            <button type="submit" class="button button-3d button-black nomargin" id="login-form-submit" name="login-form-submit" value="login">Login</button>
                            @if (Route::has('password.request'))
                                <a href="{{ route('password.request') }}" class="fright">Forgot Password?</a>
                            @endif
                        </div>

                    </form>
                    <br><br><br>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
