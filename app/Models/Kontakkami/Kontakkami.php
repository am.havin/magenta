<?php

namespace App\Kontakkami;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Kontakkami extends Model
{
    protected $table = 'kontakkamis';
    protected $guarded = ['id'];

    public function doDelete()
    {
        $status = true;

        DB::beginTransaction();
        try{
            $this->delete();
            DB::commit();
        }catch(\Exception $e) {
            DB::rollback();
            $status = false;
        }

        return $status;
    }
}
