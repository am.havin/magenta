<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Rolemenu extends Model
{
    protected $table = 'role_has_menus';
    protected $guarded = ['id'];
}
