<?php

namespace App\Http\Controllers\Auth;

use App\Pendaftar\Pendaftar;
use App\Sekolah\Sekolah;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Auth\Events\Registered;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest:pendaftar');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\Http\Response
     */
    public function showRegistrationForm()
    {
        $title = 'Register';
        $activeMenus = ['menuRegister'];
        $sekolahs = Sekolah::where('is_approved', 1)->get();
        $viewData = compact('title', 'activeMenus', 'sekolahs');

        return view('auth.register', $viewData);
    }

    /**
     * Handle a registration request for the application.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function register(Request $request)
    {
        $this->validator($request->all())->validate();

        event(new Registered($pendaftar = $this->create($request->all())));

        $this->guard()->login($pendaftar);

        $request->session()->flash('frontAlert', 'Pendaftaran berhasil diinput, silahkan cek email anda untuk melakukan verifikasi');

        return $this->registered($request, $pendaftar)
            ?: redirect($this->redirectPath());
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'nama_lengkap' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:pendaftars'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'no_hp' => ['required', 'string', 'min:10', 'unique:pendaftars'],
            'id_sekolah' => ['required_without:sekolah'],
            'sekolah' => ['required_if:id_sekolah,0'],
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $sekolah = $pendaftar = null;

        #get sekolah
        if(isset($data['id_sekolah'])){
            $sekolah = Sekolah::find($data['id_sekolah']);
        }

        #create dirinya sendiri
        $pendaftar = Pendaftar::create([
            'nama_lengkap' => $data['nama_lengkap'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'no_hp' => $data['no_hp'],
            'id_sekolah' => $data['id_sekolah'],
            'sekolah' => ($sekolah) ? $sekolah->nama : $data['sekolah'],
        ]);

        #create new sekolah
        if(isset($data['sekolah'])){
            $sekolah = Sekolah::updateOrCreate(['nama' => $data['sekolah']],[
                'nama' => $data['sekolah'],
                'id_pendaftar' => $pendaftar->id,
                'is_universitas' => $data['is_universitas']
            ]);
        }

        return $pendaftar;
    }

    /**
     * Get the post register / login redirect path.
     *
     * @return string
     */
    public function redirectPath()
    {
        if (method_exists($this, 'redirectTo')) {
            return $this->redirectTo();
        }

        return property_exists($this, 'redirectTo') ? $this->redirectTo : '/home';
    }

    /**
     * Get the guard to be used during registration.
     *
     * @return \Illuminate\Contracts\Auth\StatefulGuard
     */
    protected function guard()
    {
        return Auth::guard('pendaftar');
    }
}
