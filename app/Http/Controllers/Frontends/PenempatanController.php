<?php

namespace App\Http\Controllers\Frontends;

use App\Http\Controllers\Controller;
use App\Kontakkami\Kontakkami;
use App\Penempatan\Penempatan;

class PenempatanController extends Controller
{
    public function index()
    {
        $title = 'Penempatan';
        $activeMenus = ['menuInfoPenempatan'];
        $penempatan = Penempatan::first();
        $viewData = compact('title', 'activeMenus', 'penempatan');

        return view('frontends.penempatan.index', $viewData);


    }
}
