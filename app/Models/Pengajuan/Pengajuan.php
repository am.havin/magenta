<?php

namespace App\Pengajuan;

use App\Notifications\Approval;
use App\Notifications\SubmitPengajuanPendaftar;
use App\Notifications\SubmitPengajuanSdm;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Notification;

class Pengajuan extends Model
{
    protected $table = 'pengajuans';
    protected $guarded = ['id'];

    public static $maxPengajuanPeserta = 4;

    public function magangs()
    {
        return $this->hasMany('App\Magang\Magang', 'id_pengajuan');
    }

    public function pendaftar()
    {
        return $this->belongsTo('App\Pendaftar\Pendaftar', 'id_pendaftar');
    }

    public function getNomorPengajuanAttribute()
    {
        return '#'.sprintf("%06d", $this->id);
    }

    public static function getAprrovalBadgeQty()
    {
        $qty = DB::table('pengajuans')
            ->select('pengajuans.id as id')
            ->rightJoin('magangs', 'pengajuans.id', '=', 'magangs.id_pengajuan')
            ->where('pengajuans.is_submitted', true)
            ->whereNull('magangs.is_accepted')
            ->distinct('pengajuans.id')
            ->count();
        return $qty;
    }

    public function isMagangAllRejected()
    {
        $magangs = $this->magangs;
        $qty = $magangs->count();
        $rejectedQty = $magangs->where('is_accepted', false)->count();
        return ($qty == $rejectedQty) ? true : false;
    }

    public function hasAtLeast1AcceptedMagang()
    {
        $magangs = $this->magangs;
        $acceptedQty = $magangs->where('is_accepted', true)->count();
        return ($acceptedQty) ? true : false;
    }

    public function hasAtLeast1UnapprovedMagangBersyarat(){
        $magangs = $this->magangs;
        $qty = $magangs->where('is_accepted', true)
            ->where('is_need_pendaftar_approval', true)
            ->where('is_approved_by_pendaftar', null)
            ->count();
        return ($qty) ? true : false;
    }

    public function hasAtLeast1CheckedMagang()
    {
        $checkedQty = $this->magangs()->whereNotNull('is_accepted')->count();
        return ($checkedQty) ? true : false;
    }

    public function hasExpiredMagang()
    {
        $magangs = $this->magangs;
        $expiredQty = $magangs->where('is_expired', true)->count();
        return ($expiredQty) ? true : false;
    }

    public function sendEmailAfterSubmit()
    {
        Notification::route('mail', $this->pendaftar->email)
            ->notify(new SubmitPengajuanPendaftar($this));
        Notification::route('mail', config('mail.from.address'))
            ->notify(new SubmitPengajuanSdm($this));
    }

    public function sendEmailAfterApproval()
    {
        $unchekedMagangs = $this->magangs()->whereNull('is_accepted')->get();
        if($unchekedMagangs->isEmpty()){ //klo udah nggak ada yang null artinya sudah semua dicek!
            Notification::route('mail', $this->pendaftar->email)
                ->notify(new Approval($this));
        }
    }


    public function doDelete()
    {
        $status = true;

        DB::beginTransaction();
        try{
            $this->magangs->each(function($magang, $key){
                $magang->doDelete();
            });
            $this->delete();
            DB::commit();
        }catch(\Exception $e) {
            DB::rollback();
            $status = false;
        }

        return $status;
    }
}
