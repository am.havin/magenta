@extends((Request::ajax()) ? 'templates.layout-blank' : 'templates.'.config('genelator.template').'.layout')

@section('subheaderToolbar')
    <a href="#" class="btn invisible">
    </a>
@endsection

@section('content')
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title">{{ $title }}</h5>
                <button id="closeBtn" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="kt-portlet">
                            <div class="kt-portlet__body">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <th width="200px">No Pengajuan</th>
                                        <td>{{ $model->nomorPengajuan }}</td>
                                    </tr>
                                    <tr>
                                        <th>Tanggal Pengajuan</th>
                                        <td>{{ \Carbon\Carbon::create($model->submitted_at)->format('d F Y') }}</td>
                                    </tr>
                                    <tr>
                                        <th>Akun Pendaftar</th>
                                        <td>{{ $model->pendaftar->nama_lengkap }}</td>
                                    </tr>
                                    <tr>
                                        <th>Pejabat Berwenang</th>
                                        <td>{{ $model->nama_pejabat_berwenang }}</td>
                                    </tr>
                                    <tr>
                                        <th>Jabatan</th>
                                        <td>{{ $model->jabatan }}</td>
                                    </tr>
                                    <tr>
                                        <th>Alamat Instansi</th>
                                        <td>{!! nl2br($model->alamat_instansi) !!}</td>
                                    </tr>
                                    <tr>
                                        <th>Surat Pengantar Resmi</th>
                                        <td>
                                            <a href="{{ url("filedownload?p=$model->file_surat_pengantar_resmi") }}">
                                                <i class="la la-file"></i> {{ basename($model->file_surat_pengantar_resmi) }}
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <th>Proposal</th>
                                        <td>
                                            @if($model->file_proposal)
                                                <a href="{{ url("filedownload?p=$model->file_proposal") }}">
                                                    <i class="la la-file"></i> {{ basename($model->file_proposal) }}
                                                </a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!--end::Portlet-->
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection


@section('pageScripts')
@endsection


{{-- For Modal --}}
@if(Request::ajax())
    @yield('content')
    @yield('pageScripts')
@endif