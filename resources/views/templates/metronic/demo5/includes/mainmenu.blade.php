<button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
<div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
    <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile ">
        <ul class="kt-menu__nav ">

            @foreach($vcMainmenuItems as $menu)
                @php
                    if(!App\MainmenuHelper::isHavingThisMenu(array_values($menu)[0][2], $vcUserRoleMenus)){
                        continue;
                    }
                    $isActive = (in_array( array_values($menu)[0][2], $activeMenus)) ? 'kt-menu__item--open kt-menu__item--here' : '';
                    $url = (array_values($menu)[0][1]) ? url(array_values($menu)[0][1]) : 'javascript:;';
                @endphp
                <li class="kt-menu__item  {{$isActive}} kt-menu__item--submenu kt-menu__item--rel" @if(isset($menu['child']))data-ktmenu-submenu-toggle="hover" aria-haspopup="true" @endif>
                    <a href="{{ $url }}" class="kt-menu__link @if(isset($menu['child'])) kt-menu__toggle @endif">
                        <span class="kt-menu__link-text">{{ array_keys($menu)[0] }}</span>
                        @if(isset($menu['child']))
                            {{--<i class="kt-menu__ver-arrow la la-angle-right"></i>--}}
                        @endif
                    </a>
                    @php recursive($menu, 'left', $activeMenus, $vcUserRoleMenus) //$activeMenus didapat dari controller @endphp
                </li>
            @endforeach

        </ul>
    </div>
    <div class="kt-header-toolbar">
        @include('templates.metronic.demo5.includes.generatormenu')
    </div>
</div>



<?php function recursive($menu, $menuDirection, $activeMenus, $vcUserRoleMenus){ ?>

@if(isset($menu['child']))
    <div class="kt-menu__submenu kt-menu__submenu--classic kt-menu__submenu--{{$menuDirection}}">
        <ul class="kt-menu__subnav">
            <?php foreach($menu['child'] as $menu): ?>
            @php
                if(!App\MainmenuHelper::isHavingThisMenu(array_values($menu)[0][2], $vcUserRoleMenus)){
                    continue;
                }
                $isActive = (in_array( array_values($menu)[0][2], $activeMenus)) ? 'kt-menu__item--active' : '';
                $url = (array_values($menu)[0][1]) ? url(array_values($menu)[0][1]) : 'javascript:;';
            @endphp

            <li class="kt-menu__item  {{$isActive}} kt-menu__item--submenu" @if(isset($menu['child']))data-ktmenu-submenu-toggle="hover" aria-haspopup="true" @endif>
                <a href="{{$url }}" class="kt-menu__link ">
                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                        <span></span>
                    </i>
                    <span class="kt-menu__link-text">{{ array_keys($menu)[0] }}</span>
                    @if(isset($menu['child']))
                        <i class="kt-menu__hor-arrow la la-angle-right"></i>
                        <i class="kt-menu__ver-arrow la la-angle-right"></i>
                    @endif
                </a>
                @php recursive($menu, 'right', $activeMenus, $vcUserRoleMenus) @endphp
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
@endif

<?php } ?>
