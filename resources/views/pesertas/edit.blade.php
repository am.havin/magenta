@extends((Request::ajax()) ? 'templates.layout-blank' : 'templates.'.config('genelator.template').'.layout')

@section('subheaderToolbar')
    <a href="#" class="btn invisible">
    </a>
@endsection

@section('content')
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title">{{ $title }}</h5>
                <button id="closeBtn" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                @if($errors->isNotEmpty())
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-warning" role="alert">
                                <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                <div class="alert-text">
                                    <ul>
                                        @foreach($errors->all() as $message)
                                            <li>{{ $message }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-lg-12">
                        <div class="kt-portlet">

                            <!--begin::Form-->
                            <form action="{{ route('peserta.update', $model->id) }}" id="editForm" method="post" class="kt-form" autocomplete="off" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="PUT">

                                <div class="kt-portlet__body">
                                    <div class="form-group row">
                                                                    <div class="col-lg-6">
                                <label class="form-control-label">* Pendaftar:</label>
                                <select name="id_pendaftar" id="id_pendaftar" class="form-control select2ajax" required   data-ajax--url="{{ url('select2/pendaftar') }}" data-placeholder="Pilih" data-allow-clear="true">
                                    <option value="{{ $model->id_pendaftar }}">Please manually set this text</option>
                                </select>
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-6">
                                <label class="form-control-label">* Nama:</label>
                                <input type="text" name="nama" id="nama"  class="form-control" value="{{ $model->nama }}" required >
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="w-100"></div><br>
                            <div class="col-lg-6">
                                <label class="form-control-label">* KTP:</label>
                                <input type="text" name="no_ktp" id="no_ktp"  class="form-control" value="{{ $model->no_ktp }}" required >
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-6">
                                <label class="form-control-label">* NIM/NIS:</label>
                                <input type="text" name="nim_nis" id="nim_nis"  class="form-control" value="{{ $model->nim_nis }}" required >
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="w-100"></div><br>
                            <div class="col-lg-6">
                                <label class="form-control-label">* Email:</label>
                                <input type="text" name="email" id="email"  class="form-control" value="{{ $model->email }}" required >
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-6">
                                <label class="form-control-label">* id_sekolah:</label>
                                <select name="id_sekolah" id="id_sekolah" class="form-control select2ajax" required   data-ajax--url="{{ url('select2/sekolah') }}" data-placeholder="Pilih" data-allow-clear="true">
                                    <option value="{{ $model->id_sekolah }}">Please manually set this text</option>
                                </select>
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="w-100"></div><br>
                            <div class="col-lg-6">
                                <label class="form-control-label">* Sekolah:</label>
                                <input type="text" name="sekolah" id="sekolah"  class="form-control" value="{{ $model->sekolah }}" required >
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-6">
                                <label class="form-control-label">* id_programstudi:</label>
                                <select name="id_programstudi" id="id_programstudi" class="form-control select2ajax" required   data-ajax--url="{{ url('select2/programstudi') }}" data-placeholder="Pilih" data-allow-clear="true">
                                    <option value="{{ $model->id_programstudi }}">Please manually set this text</option>
                                </select>
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="w-100"></div><br>
                            <div class="col-lg-6">
                                <label class="form-control-label">* Program Studi:</label>
                                <input type="text" name="programstudi" id="programstudi"  class="form-control" value="{{ $model->programstudi }}" required >
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-6">
                                <label> File CV:</label>
                                <input type="file" class="form-control-file" name="file_cv" id="file_cv"   >
                                <span class="form-text text-muted"></span>
                                
            <p><a href="{{ url('filedownload?p='.$model->file_cv) }}"><b><i class="la la-file"></i> {{ basename($model->file_cv) }}</b></a></p>
        
                            </div>
                            <div class="w-100"></div><br>

                                    </div>
                                </div>
                                <div class="kt-portlet__foot">
                                    <div class="kt-form__actions">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <button type="submit" class="btn btn-warning">Submit</button>
                                                {{-- <a href="{{ url('peserta/') }}" class="btn btn btn-secondary">Cancel</a> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!--end::Form-->
                        </div>
                        <!--end::Portlet-->
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection


@section('pageScripts')
    {!! $validator->selector('#editForm') !!}
    <script>
        jQuery(document).ready(function () {
            initSelect2();
            initDatepicker();
            if (!(typeof datatable === "undefined")) {
                //pass instance datatable (optional) agar di-reload ketika submit formnya sukses
                initAjaxForm('#editForm', datatable); //datatable dapat dari index.blade.php
            }
        });
    </script>
@endsection


{{-- For Modal --}}
@if(Request::ajax())
    @yield('content')
    @yield('pageScripts')
@endif