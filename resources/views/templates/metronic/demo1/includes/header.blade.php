<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed ">

    <!-- begin:: Header Menu -->

    <!-- Uncomment this to display the close button of the panel
    <button class="kt-header-menu-wrapper-close" id="kt_header_menu_mobile_close_btn"><i class="la la-close"></i></button>
    -->
    <div class="kt-header-menu-wrapper" id="kt_header_menu_wrapper">
        <div id="kt_header_menu" class="kt-header-menu kt-header-menu-mobile  kt-header-menu--layout-default ">
            <h3 class="kt-subheader__title" id="genelator-main_title">{{ $title }}</h3>
        </div>
    </div>

    <!-- end:: Header Menu -->

    <!-- begin:: Header Topbar -->
    <div class="kt-header__topbar">

        <!--begin: Search -->
        {{--@include('templates.metronic.demo1.includes.header_search')--}}
        <!--end: Search -->

        <!--begin: Notifications -->
        {{--@include('templates.metronic.demo1.includes.header_notifications')--}}
        <!--end: Notifications -->

        <!--begin: Quick Actions -->
        {{--@include('templates.metronic.demo1.includes.header_quickaction')--}}
        <!--end: Quick Actions -->

        <!--begin: My Cart -->
        {{--@include('templates.metronic.demo1.includes.header_mycart')--}}
        <!--end: My Cart -->

        <!--begin: Quick panel toggler -->
        {{--@include('templates.metronic.demo1.includes.header_quickpanel')--}}
        <!--end: Quick panel toggler -->

        <!--begin: Language bar -->
        {{--@include('templates.metronic.demo1.includes.header_languagebar')--}}
        <!--end: Language bar -->

        <!--begin: User Bar -->
        @include('templates.metronic.demo1.includes.header_userbar')
        <!--end: User Bar -->
    </div>

    <!-- end:: Header Topbar -->
</div>