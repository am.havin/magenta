<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SertifikatRequest extends FormRequest
{
    public $rules = [
        'nilai' => 'required|numeric|min:0|max:100|',
    ];

    public function getCreateRules()
    {
        return $this->rules;
    }

    public function getEditRules()
    {
        return  $this->rules;
    }

    public function getCetakRules()
    {
        return  [
            'id_nama_tt_sertifikat' => 'required',
            'id_jabatan_tt_sertifikat' => 'required'
        ];
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch (request()->route()->getActionMethod()) {
            case 'index':
                return auth('web')->user()->hasAnyPermission(['Sertifikat - index']);
                break;
            case 'index10hari':
                return auth('web')->user()->hasAnyPermission(['Sertifikat - index']);
                break;
            case 'edit':
                return auth('web')->user()->hasAnyPermission(['Sertifikat - edit']);
                break;
            case 'update':
                return auth('web')->user()->hasAnyPermission(['Sertifikat - edit']);
                break;
            case 'datatable':
                return auth('web')->user()->hasAnyPermission(['Sertifikat - index']);
                break;
            case 'datatable10hari':
                return auth('web')->user()->hasAnyPermission(['Sertifikat - index']);
                break;
            case 'formCetak':
                return auth('web')->user()->hasAnyPermission(['Sertifikat - edit']);
                break;
            case 'cetak':
                return auth('web')->user()->hasAnyPermission(['Sertifikat - edit']);
                break;
            default:
                return false;
                break;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $controllerMethod = request()->route()->getActionMethod();
        if ($controllerMethod == 'update') {
            return $this->getEditRules();
        }
        if ($controllerMethod == 'cetak') {
            return $this->getCetakRules();
        }
        return [];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id_nama_tt_sertifikat.required' => 'Nama penanda tangan harus dipilih',
            'id_jabatan_tt_sertifikat.required' => 'Jabatannya harus dipilih',
        ];
    }
}
