@extends((Request::ajax()) ? 'templates.layout-blank' : 'templates.'.config('genelator.template').'.layout')

@section('subheaderToolbar')
@endsection

@section('vendorPluginForCustomPagesStyles')
    @include('templates.metronic.demo5.includes.datatable_css')
@endsection

@section('content')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-list-2"></i>
            </span>
                <h3 class="kt-portlet__head-title">
                    List data FAQ
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        @if(auth()->user()->can('Faq - create'))
                            <a href="{{ url('faq/create') }}"
                               class="btn btn-sm btn-warning btn-elevate btn-icon-sm btnAdd">
                                <i class="la la-plus"></i>
                                Tambah {{ $title }}
                            </a>
                        @endif
                        <button type="button" class="btn btn-sm btn-success btn-elevate btn-icon" id="btnToggleFilter"
                                data-toggle="kt-tooltip" data-placement="top" title=""
                                data-original-title="Search / Filter list data"
                                data-skin="brand">
                            <i class="la la-search"></i>
                        </button>
                        <button type="button" class="btn btn-sm btn-info btn-elevate btn-icon" id="btnReloadDt"
                                data-toggle="kt-tooltip" data-placement="top" title=""
                                data-original-title="Refresh list data"
                                data-skin="brand">
                            <i class="la la-refresh"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Filter for datatable-->
            <div id="sectionFilter" class="kt-section" style="display: none;">
                <div class="kt-section__content kt-section__content--solid">
                    <form id="filterForm" autocomplete="off">
                        <h5>Filter:</h5>
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label class="form-control-label"> Judul FAQ:</label>
                                <input type="text" name="nama" id="filter_nama" class="form-control" value="">
                                <span class="form-text text-muted">Judul FAQ</span>
                            </div>
                            <div class="col-lg-3">
                                <label class="form-control-label"> Konten:</label>
                                <textarea name="konten" id="filter_konten" class="form-control"></textarea>
                                <span class="form-text text-muted">Konten FAQ</span>
                            </div>
                            <div class="col-lg-3">
                                <label class="form-control-label"> Status:</label>
                                <select name="is_aktif" id="filter_is_aktif" class="form-control">
                                    <option value=''>Pilih</option>
                                    <option value='1'>Aktifkan</option>
                                    <option value='0'>Non Aktifkan</option>

                                </select>
                                <span class="form-text text-muted"></span>
                            </div>

                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-12 text-right">
                                <button type="reset" class="btn btn-wide btn-sm btn-clean">Clear</button>
                                <button type="submit" class="btn btn-wide btn-sm btn-outline-success">Filter</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!--end: Filter for datatable-->

            <!--begin: Datatable -->
            <table class="table table-striped- table-hover table-checkable" id="datatable">
                <thead>
                <tr>
                    <th>Pertanyaan</th>
                    <th>Jawaban</th>
                    <th>Aktif?</th>
                    <th>Urutan</th>

                    <th width="100px;">Action</th>
                </tr>
                </thead>
            </table>
            <!--end: Datatable -->

        </div>
    </div>

    <!-- Modal Container-->
    <div class="modal fade" id="modal" tabindex="-1"></div>
    <!--end: Modal Container-->
@endsection

@section('vendorPluginForCustomPagesScripts')
    @include('templates.metronic.demo5.includes.datatable_js')
@endsection

@section('pageScripts')
    <script>
        jQuery(document).ready(function () {
            datatable = $('#datatable').DataTable({
                dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url: "{{ url('faq/datatable') }}",
                    data: function (d) {
                        d.filter_nama = $('#filter_nama').val();
                        d.filter_konten = $('#filter_konten').val();
                        d.filter_is_aktif = $('#filter_is_aktif').val();

                    }
                },
                columns: [
                    {data: 'nama', name: 'nama'},
                    {data: 'konten', name: 'konten'},
                    {data: 'is_aktif', name: 'is_aktif'},
                    {data: 'urutan', name: 'urutan'},

                    {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'},
                ],
                order: [[ 3, "asc" ]],
                drawCallback: function (settings) {
                    KTApp.initTooltip($('[data-toggle="kt-tooltip"]'));
                }
            });

            //INIT DATATABLE'S BUTTONS
            initDatatableFilterAndRefreshButton(datatable); //check function in custom.js
            initDeleteButton(datatable); //check function in custom.js

            //INIT MODAL
            initAjaxModalButton('a.btnAdd, a.btnEdit');
        });
    </script>
@endsection