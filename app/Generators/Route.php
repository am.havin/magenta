<?php

namespace App\Generators;

use Carbon\Carbon;
use Illuminate\Http\File;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Symfony\Component\Yaml\Yaml;

class Route
{
    private $stubString;
    private $routeUrl;
    private $controllerName;

    /**
     * Model constructor.
     * @param $routeUrl
     * @param $controllerName
     */
    public function __construct($routeUrl, $controllerName)
    {
        $stubFileLoc                = resource_path('stubs/route.stub');
        $this->stubString           = file_get_contents($stubFileLoc);
        $this->routeUrl             = $routeUrl;
        $this->controllerName       = $controllerName;
    }

    /**
     * @param $tableName
     * @param $modelData
     * @return string
     */
    public function generate()
    {
        $string = $this->replaceStubString($this->routeUrl , $this->controllerName );
        return $this->updateFile($string);
    }

    /**
     * @param $routeUrl
     * @param $controller
     * @return mixed
     */
    private function replaceStubString($routeUrl, $controller)
    {
        $replacedString = str_replace(
            [
                '{{routeUrl}}',
                '{{controller}}',
            ],
            [
                $routeUrl,
                $controller,
            ],
            $this->stubString
        );
        return $replacedString;
    }

    /**
     * @param $string
     * @return string
     */
    private function updateFile($string)
    {
        try{
            $handle = fopen(base_path('routes/web.php'), 'a');
            fwrite($handle, "\n" . $string . "\n");
            return $string;
        }catch (\Exception $e){
            return '';
        }
    }
}
