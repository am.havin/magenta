<?php

namespace App\Magang;

use App\IndexNilai\IndexNilai;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Carbon\Carbon;

class Magang extends Model
{
    protected $table = 'magangs';
    protected $guarded = ['id', 'is_accepted', 'is_expired'];

    public function pengajuan()
    {
        return $this->belongsTo('App\Pengajuan\Pengajuan', 'id_pengajuan');
    }

    public function modelPeserta()
    {
        return $this->belongsTo('App\Peserta\Peserta', 'id_peserta');
    }

    public function scopeGetAcceptedStatus($query, $stat)
    {
        return $query->where('is_accepted' ,'=', $stat);
    }

    public function scopeGetMahasiswa($query)
    {
        //$time_now = \Carbon\Carbon::now();
        return $query->where('tgl_akhir' ,'>=', Carbon::now());
    }

    public function scopeMagangAktif($query)
    {
        return $query->where('is_accepted' ,'=', true)
            ->whereRaw('CASE WHEN is_need_pendaftar_approval = true THEN is_approved_by_pendaftar = true ELSE 1=1 END');
    }

    public function scopeGetMahasiswaSaatIni($query)
    {
        //$time_now = \Carbon\Carbon::now();
        return $query->where('tgl_akhir' ,'>=', Carbon::now())->where('tgl_awal', '<=', Carbon::now());
    }

    public function getStartMagangAttribute()
    {
        return Carbon::create($this->tgl_awal)->format('d/m/Y');
    }

    public function getEndMagangAttribute()
    {
        return Carbon::create($this->tgl_akhir)->format('d/m/Y');
    }

    public function getCarbonTglAwalAttribute()
    {
        if($this->tgl_awal){
            return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->tgl_awal);
        }
        return null;
    }

    public function getCarbonOldTglAwalAttribute()
    {
        if($this->old_tgl_awal){
            return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->old_tgl_awal);
        }
        return null;
    }

    public function getCarbonTglAkhirAttribute()
    {
        if($this->tgl_akhir){
            return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->tgl_akhir);
        }
        return null;
    }

    public function getCarbonOldTglAkhirAttribute()
    {
        if($this->old_tgl_akhir){
            return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->old_tgl_akhir);
        }
        return null;
    }

    public function getLamaHariAttribute()
    {
        if($this->carbon_tgl_awal && $this->carbon_tgl_akhir){
            return $this->carbon_tgl_awal->diffInDays($this->carbon_tgl_akhir);
        }
        return null;
    }

    public function getOldLamaHariAttribute()
    {
        if($this->carbon_old_tgl_awal && $this->carbon_old_tgl_akhir){
            return $this->carbon_old_tgl_awal->diffInDays($this->carbon_old_tgl_akhir);
        }
        return null;
    }

    public function getCarbonAcceptedAtAttribute()
    {
        if($this->accepted_at){
            return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->accepted_at);
        }
        return null;
    }

    public function getTextStatusForEmailAttribute()
    {
        $text = '';
        if ($this->is_accepted === true) {
            if ($this->is_need_pendaftar_approval && $this->is_approved_by_pendaftar === null) {
                $text = 'Disetujui Bersyarat';
            } elseif ($this->is_need_pendaftar_approval && $this->is_approved_by_pendaftar) {
                $text = 'Telah Anda Setujui';
            } elseif ($this->is_need_pendaftar_approval && !$this->is_approved_by_pendaftar) {
                $text = 'Telah Anda Tolak';
            } else {
                $text = 'Disetujui';
            }
        }

        // TOLAK
        if($this->is_accepted === false) {
            if ($this->is_need_pendaftar_approval && $this->is_approved_by_pendaftar === false) {
                $text = 'Telah Anda Tolak';
            }
            else {
                $text = 'Tidak disetujui';
            }
        }

        // RETURN
        return $text;
    }

    public function getNilaiPredikatAttribute()
    {
        $nilai = ($this->nilai != null) ? $this->nilai : 0;
        $index = IndexNilai::where('nilai_awal', '<=', $nilai)
            ->where('nilai_akhir', '>=', $nilai)
            ->first();
        return $index->standar_kualitas;
    }

    public static function getPengajuanSama($id_pengajuan)
    {
        $accepted_magang = Magang::getAcceptedStatus(true)
                            ->where('id_pengajuan', $id_pengajuan);
        $accepted_magang_with_doc = Magang::getAcceptedStatus(true)
                            ->where('id_pengajuan', $id_pengajuan)
                            ->whereNotNull('file_surat_persetujuan_magang');
        if( $accepted_magang->get()->count() == $accepted_magang_with_doc->get()->count() )
        {
            return $accepted_magang_with_doc;
        }

        return false;
    }

    public function purgeExpired()
    {
        $this->is_expired = false;
        $this->save();
    }

    public function doDelete()
    {
        $status = true;

        DB::beginTransaction();
        try{
            $this->delete();
            DB::commit();
        }catch(\Exception $e) {
            DB::rollback();
            $status = false;
        }

        return $status;
    }
}
