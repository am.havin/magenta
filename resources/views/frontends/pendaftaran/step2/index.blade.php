@extends('templates.frontend.magenta.layout_nonhome')

@section('pageCss')
@endsection

@section('content')
    <div class="container">
        @if($errors->isNotEmpty())
            <div class="row justify-content-center">
                <div class="col-md-8">
                    <div class="style-msg2 errormsg">
                        <div class="msgtitle">Terjadi kesalahan pengisian form:</div>
                        <div class="sb-msg">
                            <ul>
                                @foreach($errors->all() as $message)
                                    <li>{{ $message }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        {{--STEPS UI--}}
        <div class="col-md-10 offset-1">
            <ul class="process-steps pengajuan-steps bottommargin clearfix">
                <li class="">
                    <a href="" class="i-circled i-bordered i-alt divcenter">1</a>
                    <h5>Pilih Peserta</h5>
                </li>
                <li class="active">
                    <a href="" class="i-circled i-bordered i-alt divcenter">2</a>
                    <h5>Kirim Pengajuan</h5>
                </li>
            </ul>
        </div>

        <form method="POST" action="{{ route('pendaftaran.step2.update', $pengajuan->id) }}" id="editForm" autocomplete="off" enctype="multipart/form-data">
            @csrf
            @method('PUT')

            <div class="row">
                <div class="col-md-6 offset-3">
                    <h4 class="text-center">
                        <em>
                            Untuk menyelesaikan pengajuan magang ini, mohon melengkapi
                            data penanggung jawab instansi beserta surat pengantar resminya dibawah ini.
                        </em>
                    </h4>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 offset-3">
                    <div class="form-group">
                        <label for="nama_pejabat_berwenang" style="display: block;"><span class="text-danger">*</span> Nama pejabat berwenang</label>
                        <input id="nama_pejabat_berwenang" type="text"
                               class="form-control"
                               name="nama_pejabat_berwenang" value="{{ $pengajuan->nama_pejabat_berwenang }}" required placeholder="">
                        <small id="" class="form-text text-muted"></small>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 offset-3">
                    <div class="form-group">
                        <label for="jabatan" style="display: block;"><span class="text-danger">*</span> Jabatan</label>
                        <input id="jabatan" type="text"
                               class="form-control"
                               name="jabatan" value="{{ $pengajuan->jabatan }}" required placeholder="">
                        <small id="" class="form-text text-muted"></small>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 offset-3">
                    <div class="form-group">
                        <label for="alamat_instansi" style="display: block;"><span class="text-danger">*</span> Alamat Instansi</label>
                        <textarea name="alamat_instansi" id="alamat_instansi" cols="30" rows="3" class="form-control">{{ $pengajuan->alamat_instansi }}</textarea>
                        <small id="" class="form-text text-muted"></small>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 offset-3">
                    <hr>
                    <div class="form-group">
                        <label for="file_surat_pengantar_resmi" style="display: block;"><span class="text-danger">*</span> Surat pengantar resmi sekolah / universitas</label>
                        <input id="file_surat_pengantar_resmi" type="file"
                               class="form-control-file"
                               name="file_surat_pengantar_resmi" value="" required placeholder="">
                        <small id="" class="form-text text-muted">File .pdf berukuran maksimal 512 Kb</small>
                        @if($pengajuan->file_surat_pengantar_resmi)
                            <a href="{{ route('pendaftaran.step2.surat_pengantar_resmi', $pengajuan->id) }}">
                                > <i class="icon-file"></i>
                                {{ basename($pengajuan->file_surat_pengantar_resmi) }}
                            </a>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 offset-3">
                    <hr>
                    <div class="form-group">
                        <label for="file_proposal" style="display: block;">Proposal (optional)</label>
                        <input id="file_proposal" type="file"
                               class="form-control-file"
                               name="file_proposal" value="" required placeholder="">
                        <small id="" class="form-text text-muted">File .pdf berukuran maksimal 512 Kb</small>
                        @if($pengajuan->file_proposal)
                            <a href="{{ route('pendaftaran.step2.file_proposal', $pengajuan->id) }}">
                                > <i class="icon-file"></i>
                                {{ basename($pengajuan->file_proposal) }}
                            </a>
                        @endif
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6 offset-3">
                    <hr>
                    <div class="form-group">
                        <div class="form-check">
                            <input class="form-check-input" type="checkbox" value="1" id="agreement" name="agreement" @if($pengajuan->is_submitted) checked @endif>
                            <label class="form-check-label" for="agreement">
                                <span class="text-danger">*</span> Data-data yang diberikan telah sesuai dengan yang sebenarnya
                            </label>
                        </div>
                    </div>
                </div>
            </div>

            <br>
            <div class="col-md-10 offset-1">
                <div style="" class="kt-portlet__head-actions text-center">
                    <a href="{{ route('pendaftaran.step1.edit', $pengajuan->id) }}"
                       class="button button-3d button-small button-rounded button-leaf button-light">
                        <i class="icon-arrow-left"></i>
                        Kembali
                    </a>
                    <button class="button button-3d button-small button-rounded button-amber button-light">
                        Kirim <i class="icon-check"></i>
                    </button>
                </div>
            </div>
        </form>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true"></div>
@endsection


@section('pageScript')
    {!! $validator->selector('#editForm') !!}

@endsection