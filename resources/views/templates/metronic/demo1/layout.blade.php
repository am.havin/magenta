<!DOCTYPE html>
<html lang="en">

<!-- begin::Head -->
<head>
    <base href="">
    <meta charset="utf-8"/>
    <title>{{ $title }}</title>
    <meta name="description" content="Basic datatables examples">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!--begin::Fonts -->
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700|Roboto:300,400,500,600,700">
    <!--end::Fonts -->

    <!--begin::Page Vendors Styles(used by this page) -->
    <link href="{{ url('/') }}/templates/metronic/demo1/assets/plugins/custom/datatables/datatables.bundle.css" rel="stylesheet" type="text/css"/>
    <!--end::Page Vendors Styles -->

    <!--begin::Global Theme Styles(used by all pages) -->
    <link href="{{ url('/') }}/templates/metronic/demo1/assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css"/>
    <link href="{{ url('/') }}/templates/metronic/demo1/assets/css/style.bundle.css" rel="stylesheet" type="text/css"/>
    <!--end::Global Theme Styles -->

    <!--begin::Layout Skins(used by all pages) -->
    <link href="{{ url('/') }}/templates/metronic/demo1/assets/css/skins/header/base/light.css" rel="stylesheet" type="text/css"/>
    <link href="{{ url('/') }}/templates/metronic/demo1/assets/css/skins/header/menu/light.css" rel="stylesheet" type="text/css"/>
    <link href="{{ url('/') }}/templates/metronic/demo1/assets/css/skins/brand/dark.css" rel="stylesheet" type="text/css"/>
    <link href="{{ url('/') }}/templates/metronic/demo1/assets/css/skins/aside/dark.css" rel="stylesheet" type="text/css"/>
    <!--end::Layout Skins -->

    <link href="{{ url('/') }}/templates/metronic/demo1/assets/css/custom.css" rel="stylesheet" type="text/css"/>
    <link rel="shortcut icon" href="{{ url('/') }}/icon.ico"/>
</head>
<!-- end::Head -->

<!-- begin::Body -->
<body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

<!-- begin:: Page -->

<!-- begin:: Header Mobile -->
@include('templates.metronic.demo1.includes.headermobile')
<!-- end:: Header Mobile -->

<div class="kt-grid kt-grid--hor kt-grid--root">
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">

        <!-- begin:: Aside -->
        @include('templates.metronic.demo1.includes.aside')
        <!-- end:: Aside -->

        <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">

            <!-- begin:: Header -->
            @include('templates.metronic.demo1.includes.header')
            <!-- end:: Header -->

            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
                <!-- begin:: Subheader -->
                @include('templates.metronic.demo1.includes.subheader')
                <!-- end:: Subheader -->

                <!-- begin:: Content -->
                <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">
                    @include('templates.metronic.demo1.includes.alert')
                    @yield('content')
                </div>
                <!-- end:: Content -->
            </div>

            <!-- begin:: Footer -->
            @include('templates.metronic.demo1.includes.footer')
            <!-- end:: Footer -->
        </div>
    </div>
</div>

<!-- end:: Page -->

<!-- begin::Quick Panel -->
@include('templates.metronic.demo1.includes.quickpanel')
<!-- end::Quick Panel -->

<!-- begin::Scrolltop -->
<div id="kt_scrolltop" class="kt-scrolltop">
    <i class="fa fa-arrow-up"></i>
</div>

<!-- end::Scrolltop -->

<!-- begin::Sticky Toolbar -->
{{--@include('templates.metronic.demo1.includes.stickytoolbar')--}}
<!-- end::Sticky Toolbar -->

<!-- begin::Demo Panel -->
@include('templates.metronic.demo1.includes.demopanel')
<!-- end::Demo Panel -->

<!--Begin:: Chat-->
@include('templates.metronic.demo1.includes.chat')
<!--ENd:: Chat-->

<!-- begin::Global Config(global config for global JS sciprts) -->
<script>
    var KTAppOptions = {
        "colors": {
            "state": {
                "brand": "#5d78ff",
                "dark": "#282a3c",
                "light": "#ffffff",
                "primary": "#5867dd",
                "success": "#34bfa3",
                "info": "#36a3f7",
                "warning": "#ffb822",
                "danger": "#fd3995"
            },
            "base": {
                "label": [
                    "#c5cbe3",
                    "#a1a8c3",
                    "#3d4465",
                    "#3e4466"
                ],
                "shape": [
                    "#f0f3ff",
                    "#d9dffa",
                    "#afb4d4",
                    "#646c9a"
                ]
            }
        }
    };
</script>

<!-- end::Global Config -->

<!--begin::Global Theme Bundle(used by all pages) -->
<script src="{{ url('/') }}/templates/metronic/demo1/assets/plugins/global/plugins.bundle.js" type="text/javascript"></script>
<script src="{{ url('/') }}/templates/metronic/demo1/assets/js/scripts.bundle.js" type="text/javascript"></script>
<script src="{{ url('/js/custom.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
<script>
    jQuery(document).ready(function() {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "10",
            "hideDuration": "10",
            "timeOut": "10000",
            "extendedTimeOut": "10",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
        };

        @if(session('toastr'))
        toastr.{{ session('toastr')->type }}("{{ session('toastr')->text }}");
        @endif
    });
</script>
<!--end::Global Theme Bundle -->

<!--begin::Page Vendors(used by this page) -->
<script src="{{ url('/') }}/templates/metronic/demo1/assets/plugins/custom/datatables/datatables.bundle.js" type="text/javascript"></script>
<!--end::Page Vendors -->

<!--begin::Page Scripts(used by this page) -->
<script src="{{ url('/') }}/templates/metronic/demo1/assets/js/pages/crud/datatables/basic/basic.js" type="text/javascript"></script>
@yield('pageScripts')
<!--end::Page Scripts -->
</body>

<!-- end::Body -->
</html>