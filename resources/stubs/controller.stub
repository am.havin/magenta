<?php

namespace {{namespace}};

use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

use App\Alert;
use App\Http\Controllers\Controller;
use {{requestNamespace}}\{{requestClassName}};
use {{modelNamespace}}\{{modelClassName}};
use App\Toastr;


class {{className}} extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param {{requestClassName}} $request
     * @return \Illuminate\Http\Response
     */
    public function index({{requestClassName}} $request)
    {
        $title = '{{title}}';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            '{{title}}' => route('{{route}}index'),
        ];
        $viewData = compact('title', 'activeMenus', 'breadcrumbs');

        return view('{{viewFileDir}}index', $viewData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param {{requestClassName}} $request
     * @return \Illuminate\Http\Response
     */
    public function create({{requestClassName}} $request)
    {
        $title = 'Tambah {{title}}';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            '{{title}}' => route('{{route}}index'),
            'Tambah' => route('{{route}}create'),
        ];
        $validator = \JsValidator::make($request->getCreateRules(), $request->messages());

        $viewData = compact('title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('{{viewFileDir}}create', $viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param {{requestClassName}} $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store({{requestClassName}} $request)
    {
        try{
            $model = {{modelClassName}}::create($request->all());
            $this->storeFiles($request, $model); //if there is any
            $json = ['status' => true, 'message' => 'Data berhasil disimpan!'];
        }catch(\Exception $e){
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data baru berhasil disimpan!');
        //return redirect(route('{{route}}index'))->with('alert', $alert);
        $toastr = new Toastr('success', 'Data baru berhasil disimpan!');
        return redirect(route('{{route}}index'))->with('toastr', $toastr);
    }

    /**
     * Display the specified resource.
     *
     * @param {{requestClassName}} $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show({{requestClassName}} $request, $id)
    {
        $model = {{modelClassName}}::findOrFail($id);
        $title = 'Detail {{title}}';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            '{{title}}' => route('{{route}}index'),
            'Detail' => route('{{route}}show', $model->id),
        ];
        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs');

        return view('{{viewFileDir}}show', $viewData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param {{requestClassName}} $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit({{requestClassName}} $request, $id)
    {
        $model = {{modelClassName}}::findOrFail($id);
        $title = 'Edit {{title}}';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            '{{title}}' => route('{{route}}index'),
            'Edit' => route('{{route}}edit', $model->id),
        ];
        $validator = \JsValidator::make($request->getEditRules(), $request->messages());

        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('{{viewFileDir}}edit', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param {{requestClassName}} $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update({{requestClassName}} $request, $id)
    {
        try{
            $model =  {{modelClassName}}::findOrFail($id);
            $model->update($request->all());
            $this->storeFiles($request, $model); //if there is any
            $json = ['status' => true, 'message' => 'Data berhasil diupdate!'];
        }catch(\Exception $e){
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data berhasil diupdate!');
        //return redirect()->back()->with('alert', $alert);
        $toastr = new Toastr('success', 'Data berhasil diupdate!');
        return redirect()->back()->with('toastr', $toastr);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param {{requestClassName}} $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy({{requestClassName}} $request, $id)
    {
        $model = {{modelClassName}}::findOrFail($id);
        return response()->json([
            'status' => $model->doDelete()
        ]);
    }

    /**
     * Handle Datatable request.
     *
     * @param {{requestClassName}} $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function datatable({{requestClassName}} $request)
    {
        return Datatables::of({{modelClassName}}::query())
            ->filter(function ($query) use ($request) {
                {{filterCodes}}
            })
            ->addColumn('action', function ($data) {
                $show = $edit = $delete = '';
                if(auth()->user()->hasAnyPermission(['{{modelClassName}} - show'])){
                    $show = '<a href="'.route('{{route}}show', $data->id).'" class="btn btn-outline-hover-warning btn-icon btn-circle btn-sm" data-toggle="kt-tooltip" title="Show detail" data-skin="brand"><i class="la la-eye kt-font-warning"></i></a>';
                }
                if(auth()->user()->hasAnyPermission(['{{modelClassName}} - edit'])){
                    $edit = '<a href="'.route('{{route}}edit', $data->id).'"class="btn btn-outline-hover-info btn-icon btn-circle btn-sm btnEdit" data-toggle="kt-tooltip" title="Edit" data-skin="brand"><i class="la la-pencil kt-font-info"></i></a>';
                }
                if(auth()->user()->hasAnyPermission(['{{modelClassName}} - delete'])){
                    $delete = '<button type="button" class="btn btn-outline-hover-danger btn-icon btn-circle btn-sm btn-dt-delete" data-toggle="kt-tooltip" title="Delete" data-skin="brand" data-url="'.route('{{route}}destroy', $data->id).'" data-csrf="'.csrf_token().'"><i class="la la-close kt-font-danger"></i></button>';
                }
                return $show.$edit.$delete;
            })->make(true);
    }

    /**
     * Handle file store
     *
     * @param $request
     * @param $model
     */
    private function storeFiles($request, $model)
    {
        foreach($request->files as $key => $file){
            if ($request->file($key)->isValid()) {
                Storage::delete($model->$key);
                $model->$key = $request->file($key)->storeAs("{{modelClassName}}/$key/$model->id", $file->getClientOriginalName(),'local');
                $model->save();
            }
        }
    }
}
