@extends('templates.frontend.magenta.layout_nonhome')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-1">
                {!! nl2br($penempatan->konten) !!}

                <br><br>
            </div>
        </div>
    </div>
@endsection