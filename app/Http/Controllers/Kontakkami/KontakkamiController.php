<?php

namespace App\Http\Controllers\Kontakkami;

use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

use App\Alert;
use App\Http\Controllers\Controller;
use App\Http\Requests\KontakkamiRequest;
use App\Kontakkami\Kontakkami;
use App\Toastr;


class KontakkamiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param KontakkamiRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(KontakkamiRequest $request)
    {
        $kontakkami = Kontakkami::where('is_aktif', 1)->first();
        return redirect(route('kontakkami.edit', $kontakkami->id));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param KontakkamiRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(KontakkamiRequest $request)
    {
        $title = 'Tambah Kontakkami';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Kontakkami' => route('kontakkami.index'),
            'Tambah' => route('kontakkami.create'),
        ];
        $validator = \JsValidator::make($request->getCreateRules(), $request->messages());

        $viewData = compact('title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('kontakkami.create', $viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param KontakkamiRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(KontakkamiRequest $request)
    {
        try{
            $model = Kontakkami::create($request->all());
            $this->storeFiles($request, $model); //if there is any
            $json = ['status' => true, 'message' => 'Data berhasil disimpan!'];
        }catch(\Exception $e){
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data baru berhasil disimpan!');
        //return redirect(route('kontakkami.index'))->with('alert', $alert);
        $toastr = new Toastr('success', 'Data baru berhasil disimpan!');
        return redirect(route('kontakkami.index'))->with('toastr', $toastr);
    }

    /**
     * Display the specified resource.
     *
     * @param KontakkamiRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(KontakkamiRequest $request, $id)
    {
        $model = Kontakkami::findOrFail($id);
        $title = 'Detail Kontakkami';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Kontakkami' => route('kontakkami.index'),
            'Detail' => route('kontakkami.show', $model->id),
        ];
        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs');

        return view('kontakkami.show', $viewData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param KontakkamiRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(KontakkamiRequest $request, $id)
    {
        $model = Kontakkami::findOrFail($id);
        $title = 'Edit Kontak Kami';
        $activeMenus = ['menuKontakKami'];
        $breadcrumbs = [
            'Kontakkami' => route('kontakkami.index'),
            'Edit' => route('kontakkami.edit', $model->id),
        ];
        $validator = \JsValidator::make($request->getEditRules(), $request->messages());

        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('kontakkami.edit', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param KontakkamiRequest $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(KontakkamiRequest $request, $id)
    {
        try{
            $model =  Kontakkami::findOrFail($id);
            $this->storeFiles($request, $model); //if there is any
            $model->update($request->all());
            $json = ['status' => true, 'message' => 'Data berhasil diupdate!'];
        }catch(\Exception $e){
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data berhasil diupdate!');
        //return redirect()->back()->with('alert', $alert);
        $toastr = new Toastr('success', 'Data berhasil diupdate!');
        return redirect()->back()->with('toastr', $toastr);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param KontakkamiRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(KontakkamiRequest $request, $id)
    {
        $model = Kontakkami::findOrFail($id);
        return response()->json([
            'status' => $model->doDelete()
        ]);
    }

    /**
     * Handle Datatable request.
     *
     * @param KontakkamiRequest $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function datatable(KontakkamiRequest $request)
    {
        return Datatables::of(Kontakkami::query())
            ->filter(function ($query) use ($request) {
                
                if (!empty($request->input('filter_judul'))) {
                    $query->where('judul', 'ilike', "%{$request->input('filter_judul')}%");
                }
            

                if (!empty($request->input('filter_konten'))) {
                    $query->where('konten', 'ilike', "%{$request->input('filter_konten')}%");
                }
            

                if (!empty($request->input('filter_is_aktif'))) {
                    $query->where('is_aktif', 'ilike', "%{$request->input('filter_is_aktif')}%");
                }
            

            })
            ->addColumn('action', function ($data) {
                $show = $edit = $delete = '';
                if(auth()->user()->hasAnyPermission(['Kontakkami - show'])){
                    $show = '<a href="'.route('kontakkami.show', $data->id).'" class="btn btn-outline-hover-warning btn-icon btn-circle btn-sm" data-toggle="kt-tooltip" title="Show detail" data-skin="brand"><i class="la la-eye kt-font-warning"></i></a>';
                }
                if(auth()->user()->hasAnyPermission(['Kontakkami - edit'])){
                    $edit = '<a href="'.route('kontakkami.edit', $data->id).'"class="btn btn-outline-hover-info btn-icon btn-circle btn-sm btnEdit" data-toggle="kt-tooltip" title="Edit" data-skin="brand"><i class="la la-pencil kt-font-info"></i></a>';
                }
                if(auth()->user()->hasAnyPermission(['Kontakkami - delete'])){
                    $delete = '<button type="button" class="btn btn-outline-hover-danger btn-icon btn-circle btn-sm btn-dt-delete" data-toggle="kt-tooltip" title="Delete" data-skin="brand" data-url="'.route('kontakkami.destroy', $data->id).'" data-csrf="'.csrf_token().'"><i class="la la-close kt-font-danger"></i></button>';
                }
                return $show.$edit.$delete;
            })->make(true);
    }

    /**
     * Handle file store
     *
     * @param $request
     * @param $model
     */
    private function storeFiles($request, $model)
    {
        foreach($request->files as $key => $file){
            if ($request->file($key)->isValid()) {
                Storage::delete($model->$key);
                $model->$key = $request->file($key)->storeAs("Kontakkami/$key/$model->id", $file->getClientOriginalName(),'local');
                $model->save();
            }
        }
    }
}
