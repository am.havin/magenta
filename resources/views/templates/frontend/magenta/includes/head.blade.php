<head>

    <meta http-equiv="content-type" content="text/html; charset=utf-8" />
    <meta name="author" content="Kementerian Badan Usaha Milik Negara" />

    <!-- Stylesheets
    ============================================= -->
    <link href="https://fonts.googleapis.com/css?family=Poppins:300,400,400i,500,600,700" rel="stylesheet" type="text/css" />
    <link rel="stylesheet" href="{{url('/')}}/templates/frontend/magenta/css/bootstrap.css" type="text/css" />
    <link rel="stylesheet" href="{{url('/')}}/templates/frontend/magenta/style.css" type="text/css" />
{{--    <link rel="stylesheet" href="{{url('/')}}/templates/frontend/magenta/css/dark.css" type="text/css" />--}}

    <link rel="stylesheet" href="{{url('/')}}/templates/frontend/magenta/css/font-icons.css" type="text/css" />
{{--    <link rel="stylesheet" href="{{url('/')}}/templates/frontend/magenta/css/animate.css" type="text/css" />--}}
    <link rel="stylesheet" href="{{url('/')}}/templates/frontend/magenta/css/magnific-popup.css" type="text/css" />

    <link rel="stylesheet" href="{{url('/')}}/templates/frontend/magenta/css/responsive.css" type="text/css" />
    <meta name="viewport" content="width=device-width, initial-scale=1" />

    <!-- Workspace Demo Specific Stylesheet -->
    <link rel="stylesheet" href="{{url('/')}}/templates/frontend/magenta/css/colors.php?color=1ABC9C" type="text/css" /> <!-- Theme Color -->
    <link rel="stylesheet" href="{{url('/')}}/templates/frontend/magenta/demos/coworking/css/fonts.css" type="text/css" /> <!-- Theme Font -->
    <link rel="stylesheet" href="{{url('/')}}/templates/frontend/magenta/demos/coworking/coworking.css" type="text/css" /> <!-- Theme CSS -->
    <!-- / -->

    <!-- custom css -->
    <link href="{{ url('vendor/select2-4.0.13/dist/css/select2.min.css') }}" rel="stylesheet" />
    <link href="{{ url('vendor/sweetalert2-9.7.1/dist/sweetalert2.min.css') }}" rel="stylesheet" />
    @yield('pageCss')
    <link rel="stylesheet" href="{{url('/')}}/templates/frontend/magenta/css/custom.css" type="text/css" />
    <!-- /end custom -->

    <!-- Document Title
    ============================================= -->
    <title>{{ $title }} | Kementerian BUMN</title>

    <link rel="shortcut icon" href="{{ url('/') }}/icon.ico"/>
</head>