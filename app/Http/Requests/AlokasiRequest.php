<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AlokasiRequest extends FormRequest
{
    public $rules = [
        'id_unit' => 'required|unique:alokasis',
        'unit' => 'required|min:0',
        'kuota' => 'required|min:0',
    ];

    public function getCreateRules()
    {
        return $this->rules;
    }

    public function getEditRules()
    {
        $this->rules['id_unit'] = [
            'required',
            Rule::unique('alokasis')->ignore(array_values(request()->route()->originalParameters())[0]),
        ];
        return $this->rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch (request()->route()->getActionMethod()) {
            case 'index':
                return auth('web')->user()->hasAnyPermission(['Alokasi - index']);
                break;
            case 'create':
                return auth('web')->user()->hasAnyPermission(['Alokasi - create']);
                break;
            case 'store':
                return auth('web')->user()->hasAnyPermission(['Alokasi - create']);
                break;
            case 'show':
                return auth('web')->user()->hasAnyPermission(['Alokasi - show']);
                break;
            case 'edit':
                return auth('web')->user()->hasAnyPermission(['Alokasi - edit']);
                break;
            case 'update':
                return auth('web')->user()->hasAnyPermission(['Alokasi - edit']);
                break;
            case 'destroy':
                return auth('web')->user()->hasAnyPermission(['Alokasi - delete']);
                break;
            case 'datatable':
                return auth('web')->user()->hasAnyPermission(['Alokasi - index']);
                break;
            default:
                return false;
                break;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $controllerMethod = request()->route()->getActionMethod();
        if ($controllerMethod == 'store') {
            return $this->getCreateRules();
        }
        if ($controllerMethod == 'update') {
            return $this->getEditRules();
        }
        return [];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //'title.required' => 'A title is required',
            //'body.required'  => 'A message is required',
        ];
    }
}
