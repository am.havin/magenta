@extends('templates.frontend.magenta.layout_nonhome')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-1">
                <h4>Pertanyaan yang sering diajukan beserta jawabannya: <small></small></h4>
                @foreach($faqs as $faq)
                    <div class="toggle toggle-bg">
                        <div class="togglet"><i class="toggle-closed icon-question-circle"></i><i class="toggle-open icon-remove-circle"></i>{{ $faq->nama }}</div>
                        <div class="togglec">{!! nl2br($faq->konten) !!}</div>
                    </div>
                @endforeach
            </div>
        </div>
    </div>



@endsection