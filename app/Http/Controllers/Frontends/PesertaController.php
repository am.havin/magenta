<?php

namespace App\Http\Controllers\Frontends;

use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\PesertaRequest;
use App\Peserta\Peserta;
use App\Sekolah\Sekolah;
use App\SekolahProgramStudi\SekolahProgramStudi;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;


class PesertaController extends Controller
{
    protected $pendaftar;

    public function __construct()
    {
        //harus begini karena auth()->user() nge-return null kalau belum ngelewatin middleware
        $this->middleware(function ($request, $next) {
            $this->pendaftar= auth('pendaftar')->user();
            return $next($request);
        });
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        $title = 'Administrasi Peserta Magang';
        $activeMenus = ['menuPeserta'];
        $viewData = compact('title', 'activeMenus');

        return view('frontends.peserta.index', $viewData);
    }

    /**
     * @param PesertaRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create(PesertaRequest $request)
    {
        $title = 'Tambah Data Peserta';
        $activeMenus = [''];
        $validator = \JsValidator::make($request->getCreateRules(), $request->messages());
        $viewData = compact('title', 'activeMenus', 'validator');
        return view('frontends.peserta.create', $viewData);
    }

    /**
     * @param PesertaRequest $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function store(PesertaRequest $request)
    {
        try{
            //create new sekolah (kalau input lain2)
            $sekolah = $this->createSekolah($request);

            //create program studi (kalau input lain2)
            $prodi = $this->createProdi($request, $sekolah);

            //create new peserta
            $data = $request->all();
            $data['sekolah'] = $sekolah->nama;
            $data['programstudi'] = $prodi->nama;
            $model = $this->pendaftar->pesertas()->create($data);

            $this->storeFiles($request, $model); //if there is any
            $json = ['status' => true, 'message' => 'SUKSES! Data peserta magang berhasil disimpan!'];
        }catch(\Exception $e){
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data baru berhasil disimpan!');
        //return redirect(route('pendaftar.index'))->with('alert', $alert);
        $toastr = new Toastr('success', 'Data baru berhasil disimpan!');
        return redirect(route('pendaftar.index'))->with('toastr', $toastr);
    }

    /**
     * @param PesertaRequest $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(PesertaRequest $request, $pesertaId)
    {
        $title = 'Detail Data Peserta';
        $activeMenus = [''];
        $validator = \JsValidator::make($request->getEditRules(), $request->messages());

        $peserta = Peserta::where('id', $pesertaId)->where('id_pendaftar', $this->pendaftar->id)->first();
        $sekolah = ($peserta->id_sekolah) ? Sekolah::find($peserta->id_sekolah) : null;
        $programstudi = ($peserta->id_programstudi) ? SekolahProgramStudi::find($peserta->id_programstudi) : null;
        if($peserta){
            $viewData = compact('title', 'activeMenus', 'validator', 'peserta', 'sekolah', 'programstudi');
            return view('frontends.peserta.edit', $viewData);
        }
        return abort(404);
    }

    /**
     * @param PesertaRequest $request
     * @param $pesertaId
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function update(PesertaRequest $request, $pesertaId)
    {
        try{
            //get model
            $model = Peserta::where('id', $pesertaId)->where('id_pendaftar', $this->pendaftar->id)->first();

            if($model){
                //create new sekolah (kalau input lain2)
                $sekolah = $this->createSekolah($request);

                //create program studi (kalau input lain2)
                $prodi = $this->createProdi($request, $sekolah);

                //update peserta
                $data = $request->all();
                $data['sekolah'] = $sekolah->nama;
                $data['programstudi'] = $prodi->nama;
                $model->update($data);

                $this->storeFiles($request, $model); //if there is any
            }

            $json = ['status' => true, 'message' => 'SUKSES! Data peserta magang berhasil diupdate!'];
        }catch(\Exception $e){
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data baru berhasil disimpan!');
        //return redirect(route('pendaftar.index'))->with('alert', $alert);
        $toastr = new Toastr('success', 'Data baru berhasil disimpan!');
        return redirect(route('peserta_magang.index'))->with('toastr', $toastr);
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function foto($id)
    {
        $model = Peserta::where('id', $id)->where('id_pendaftar', $this->pendaftar->id)->first();
        return response()->file(storage_path('app/'.$model->foto));
    }

    /**
     * @param $pesertaId
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function cv($pesertaId)
    {
        $peserta = Peserta::where('id', $pesertaId)->where('id_pendaftar', $this->pendaftar->id)->first();

        if($peserta){
            if (Storage::disk('local')->exists($peserta->file_cv)) {
                return Storage::download($peserta->file_cv);
            }
            abort(404);
        }
    }

    /**
     * @param $pesertaId
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function skck($pesertaId)
    {
        $peserta = Peserta::where('id', $pesertaId)->where('id_pendaftar', $this->pendaftar->id)->first();

        if($peserta){
            if (Storage::disk('local')->exists($peserta->file_skck)) {
                return Storage::download($peserta->file_skck);
            }
            abort(404);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $pesertaId
     * @return \Illuminate\Http\Response
     */
    public function destroy($pesertaId)
    {
        $status = false;
        $peserta = Peserta::where('id', $pesertaId)->where('id_pendaftar', $this->pendaftar->id)->first();

        if($peserta){
            $status = $peserta->doDelete();
        }

        return response()->json([
            'status' => $status
        ]);
    }

    /**
     * Handle Datatable request.
     *
     * @param Request $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function datatable(PesertaRequest $request)
    {
        $datatable =  Datatables::of(Peserta::where('id_pendaftar', $this->pendaftar->id))
            ->filter(function ($query) use ($request) {
                if (!empty($request->input('username'))) {
                    $query->where('username', 'ilike', "%{$request->input('username')}%");
                }
                if (!empty($request->input('pegawai_id'))) {
                    $query->where('pegawai_id', 'ilike', "%{$request->input('pegawai_id')}%");
                }
                if (!empty($request->input('pegawai_nama'))) {
                    $query->where('pegawai_nama', 'ilike', "%{$request->input('pegawai_nama')}%");
                }
                if (!empty($request->input('roles'))) {
                    $query->whereRaw("users.id IN (SELECT model_id FROM model_has_roles WHERE role_id = {$request->input('roles')})");
                }
            })
            ->editColumn('nama', function($data){
                return '<b>' . $data->nama . '</b><br>' . $data->email;
            })
            ->editColumn('no_ktp', function($data){
                //return 'KTP: ' . $data->no_ktp . '<br>NIS/NIM: ' . $data->nim_nis;
                return $data->no_ktp . ' (KTP)<br>' . $data->nim_nis . ' (NIS/NIM)';
            })
            ->editColumn('sekolah', function($data){
                return '<b>'. $data->sekolah . '</b><br>' . $data->programstudi;
            })
            ->editColumn('foto', function($data){
                if($data->foto){
                    return '<img src="'.route('peserta_magang.foto', $data->id).'?rand='.rand(10,1000).'">';
                }
                return '';
            })
            ->editColumn('file_cv', function($data){
                if($data->file_cv){
                    return '<a class="btn btn-success btn-sm" href="'.route('peserta_magang.cv', $data->id).'" data-toggle="tooltip" title="'.basename($data->file_cv).'"><i class="icon-download"></i></a>';
                }
                return '';
            })
            ->addColumn('action', function ($data) {
                $actions = '';
                $actions .= '<a href="' . route('peserta_magang.edit', $data->id) . '"class="btn btn-info btn-sm btnEdit" data-toggle="tooltip" title="Detail"><i class="icon-search"></i></a> ';
                $actions .= '<button type="button" class="btn btn-danger btn-sm btn-dt-delete" data-toggle="tooltip" title="Delete" data-url="' . route('peserta_magang.destroy', $data->id) . '" data-csrf="' . csrf_token() . '"><i class="icon-remove"></i></button>';
                return $actions;
            })
            ->rawColumns(['foto', 'action', 'nama', 'no_ktp', 'sekolah', 'file_cv']);

        return $datatable->make(true);
    }

    /**
     * Handle file store
     *
     * @param $request
     * @param $model
     */
    private function storeFiles($request, $model)
    {
        foreach($request->files as $key => $file){
            if ($request->file($key)->isValid()) {
                Storage::delete($model->$key);
                $model->$key = $request->file($key)->storeAs("Peserta/$key/$model->id", $file->getClientOriginalName(),'local');
                $model->save();
            }
        }
    }

    private function createSekolah($request)
    {
        $sekolah = Sekolah::find($request->id_sekolah);
        if($request->sekolah){
            $sekolah = Sekolah::updateOrCreate(
                ['nama' => $request->sekolah],
                ['id_pendaftar' => $this->pendaftar->id, 'nama' => $request->sekolah, 'is_universitas' => $request->is_universitas]
            );
        }
        return $sekolah;
    }

    private function createProdi($request, $sekolah)
    {
        $prodi = SekolahProgramStudi::find($request->id_programstudi);
        if($request->programstudi){
            $prodi = SekolahProgramStudi::updateOrCreate(
                ['id_sekolah' => $sekolah->id, 'nama' => $request->programstudi],
                ['id_pendaftar' => $this->pendaftar->id, 'id_sekolah' => $sekolah->id,'nama' => $request->programstudi]
            );
        }
        return $prodi;
    }
}
