<script>
    jQuery(document).ready(function() {
        toastr.options = {
            "closeButton": true,
            "debug": false,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-top-right",
            "preventDuplicates": false,
            "onclick": null,
            "showDuration": "10",
            "hideDuration": "10",
            "timeOut": "10000",
            "extendedTimeOut": "10",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut",
        };

        @if(session('toastr'))
        toastr.{{ session('toastr')->type }}("{{ session('toastr')->text }}");
        @endif
    });
</script>