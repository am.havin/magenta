@extends((Request::ajax()) ? 'templates.layout-blank' : 'templates.'.config('genelator.template').'.layout')

@section('subheaderToolbar')
    <a href="#" class="btn invisible">
    </a>
@endsection

@section('content')
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title">{{ $title }}</h5>
                <button id="closeBtn" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                @if($errors->isNotEmpty())
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-warning" role="alert">
                                <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                <div class="alert-text">
                                    <ul>
                                        @foreach($errors->all() as $message)
                                            <li>{{ $message }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-lg-12">
                        <div class="kt-portlet">

                            <!--begin::Form-->
                            <form action="{{ url('halamandepan/') }}" id="createForm" method="post" class="kt-form" autocomplete="off" enctype="multipart/form-data">
                                @csrf

                                <div class="kt-portlet__body">
                                    <div class="form-group row">
                                                                    <div class="col-lg-6">
                                <label class="form-control-label">* Slogan:</label>
                                <input type="text" name="slogan" id="slogan"  class="form-control" value="" required >
                                <span class="form-text text-muted">Slogan</span>
                            </div>
                            <div class="col-lg-6">
                                <label class="form-control-label">* Introduksi:</label>
                                <input type="text" name="introduksi" id="introduksi"  class="form-control" value="" required >
                                <span class="form-text text-muted">Introduksi</span>
                            </div>
                            <div class="w-100"></div><br>
                            <div class="col-lg-6">
                                <label class="form-control-label">* Teks Gambar:</label>
                                <input type="text" name="teks_gambar" id="teks_gambar"  class="form-control" value="" required >
                                <span class="form-text text-muted">Teks Gambar</span>
                            </div>
                            <div class="col-lg-6">
                                <label class="form-control-label">* Step1 Judul:</label>
                                <input type="text" name="step1_judul" id="step1_judul"  class="form-control" value="" required >
                                <span class="form-text text-muted">Step1 Judul</span>
                            </div>
                            <div class="w-100"></div><br>
                            <div class="col-lg-6">
                                <label class="form-control-label">* Step1 Teks:</label>
                                <textarea name="step1_teks" id="step1_teks" class="form-control" required ></textarea>
                                <span class="form-text text-muted">Step1 Teks</span>
                            </div>
                            <div class="col-lg-6">
                                <label class="form-control-label">* Step2 Judul:</label>
                                <input type="text" name="step2_judul" id="step2_judul"  class="form-control" value="" required >
                                <span class="form-text text-muted">Step2 Judul</span>
                            </div>
                            <div class="w-100"></div><br>
                            <div class="col-lg-6">
                                <label class="form-control-label">* Step2 Teks:</label>
                                <textarea name="step2_teks" id="step2_teks" class="form-control" required ></textarea>
                                <span class="form-text text-muted">Step2 Teks</span>
                            </div>
                            <div class="col-lg-6">
                                <label class="form-control-label">* Step3 Judul:</label>
                                <input type="text" name="step3_judul" id="step3_judul"  class="form-control" value="" required >
                                <span class="form-text text-muted">Step3 Judul</span>
                            </div>
                            <div class="w-100"></div><br>
                            <div class="col-lg-6">
                                <label class="form-control-label">* Step3 Teks:</label>
                                <textarea name="step3_teks" id="step3_teks" class="form-control" required ></textarea>
                                <span class="form-text text-muted">Step3 Teks</span>
                            </div>
                            <div class="col-lg-6">
                                <label> Gambar Depan:</label>
                                <input type="file" class="form-control-file" name="gambar_depan" id="gambar_depan">
                                <span class="form-text text-muted"></span>
                                
                            </div>
                            <div class="w-100"></div><br>

                                    </div>
                                </div>
                                <div class="kt-portlet__foot">
                                    <div class="kt-form__actions">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <button type="submit" class="btn btn-warning">Submit</button>
                                                {{-- <a href="{{ url('halamandepan/') }}" class="btn btn btn-secondary">Cancel</a> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!--end::Form-->
                        </div>
                        <!--end::Portlet-->
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('pageScripts')
    {!! $validator->selector('#createForm') !!}
    <script>
            jQuery(document).ready(function () {
                initSelect2();
                initDatepicker();
                if (!(typeof datatable === "undefined")) {
                    //pass instance datatable (optional) agar di-reload ketika submit formnya sukses
                    initAjaxForm('#createForm', datatable); //datatable dapat dari index.blade.php
                }
            });
        </script>
@endsection

{{-- For Modal --}}
@if(Request::ajax())
    @yield('content')
    @yield('pageScripts')
@endif