@extends('templates.metronic.demo1.layout-cetak')

@section('body')
    <div style="background-image: url('images/cert_bg3.png'); background-repeat: no-repeat; background-size: 100%; background-position: 0 0; padding: 100px 80px;">
        <p class="text-center" style="margin: 0px 0px 20px;">
            <img src="{{ url('images/logo_kbumn.jpg') }}" alt="" style="height: 110px; width: auto;">
        </p>
        <h1 class="text-center" style="font-size: 42px; margin: 0px 0px 20px;">SERTIFIKAT</h1>
        <h1 class="text-center" style="font-size: 28px; margin: 0px 0px 0px;">{{ @$model->peserta }}</h1>
        <hr style="width: 80%; text-align: center; margin: 0px 0px 10px;">
        <h3 class="text-center" style="margin: 0px 0px 0px;">NIM/NIS {{ @$model->modelPeserta->nim_nis }} | {{ @$model->modelPeserta->programstudi }}</h3>
        <h2 class="text-center" style="margin: 0px 0px 0px;">{{ @$model->modelPeserta->sekolah }}</h2>
        <br><br>
        <p class="text-center" style="margin: 0px 0px 0px; font-weight: bold; font-size: 16px;">
            @php
                $tglAwal = $model->carbonTglAwal->locale('id_ID');
                $tglAkhir = $model->carbonTglAkhir->locale('id_ID');
            @endphp
            Telah melaksanakan Magang sejak tanggal {{ $tglAwal->day }} {{ $tglAwal->monthName }} {{ $tglAwal->year }}
            sampai dengan {{ $tglAkhir->day }} {{ $tglAkhir->monthName }} {{ $tglAkhir->year }} <br>
            di Kementerian Badan Usaha Milik Negara, dengan predikat: <br>

            <span style="font-size: 22px; text-transform: uppercase;">
                {{ $model->nilaiPredikat }}
            </span>

            <br><br>
            @php $date = \Carbon\Carbon::today()->locale('id_ID'); @endphp
            Jakarta, {{ $date->day }} {{ $date->monthName }} {{ $date->year }}
            <br>
            {!! @$model->jabatan_tt_sertifikat !!}
            <br><br><br><br><br>
            {{ @$model->nama_tt_sertifikat }} <br>
            NIP {{ @$model->nip_tt_sertifikat }}
        </p>
    </div>
@endsection