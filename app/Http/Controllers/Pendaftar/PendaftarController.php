<?php

namespace App\Http\Controllers\Pendaftar;

use App\Sekolah\Sekolah;
use Carbon\Carbon;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

use App\Alert;
use App\Http\Controllers\Controller;
use App\Http\Requests\PendaftarRequest;
use App\Pendaftar\Pendaftar;
use App\Toastr;


class PendaftarController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param PendaftarRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(PendaftarRequest $request)
    {
        $title = 'Akun Pendaftar';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Akun Pendaftar' => route('pendaftar.index'),
        ];
        $viewData = compact('title', 'activeMenus', 'breadcrumbs');

        return view('pendaftars.index', $viewData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param PendaftarRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(PendaftarRequest $request)
    {
        $title = 'Tambah Akun Pendaftar';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Akun Pendaftar' => route('pendaftar.index'),
            'Tambah' => route('pendaftar.create'),
        ];
        $validator = \JsValidator::make($request->getCreateRules(), $request->messages());

        $viewData = compact('title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('pendaftars.create', $viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PendaftarRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PendaftarRequest $request)
    {
        try {
            $data = $request->all();

            #get sekolah
            if(isset($data['id_sekolah'])){
                $sekolah = Sekolah::find($data['id_sekolah']);
            }

            #create dirinya sendiri
            $pendaftar = Pendaftar::create([
                'nama_lengkap' => $data['nama_lengkap'],
                'email' => $data['email'],
                'password' => Hash::make($data['password']),
                'no_hp' => $data['no_hp'],
                'id_sekolah' => $data['id_sekolah'],
                'sekolah' => $sekolah->nama,
            ]);
            $this->storeFiles($request, $pendaftar); //if there is any
            $json = ['status' => true, 'message' => 'Data berhasil disimpan!'];
        } catch (\Exception $e) {
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data baru berhasil disimpan!');
        //return redirect(route('pendaftar.index'))->with('alert', $alert);
        $toastr = new Toastr('success', 'Data baru berhasil disimpan!');
        return redirect(route('pendaftar.index'))->with('toastr', $toastr);
    }

    /**
     * Display the specified resource.
     *
     * @param PendaftarRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(PendaftarRequest $request, $id)
    {
        $model = Pendaftar::findOrFail($id);
        $title = 'Detail Akun Pendaftar';
        $activeMenus = ['menuPendaftar'];
        $breadcrumbs = [
            'Akun Pendaftar' => route('pendaftar.index'),
            'Detail' => route('pendaftar.show', $model->id),
        ];
        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs');

        return view('pendaftars.show', $viewData);
    }

    public function foto($id)
    {
        $model = Pendaftar::findOrFail($id);
        return response()->file(storage_path('app/'.$model->pic));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param PendaftarRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PendaftarRequest $request, $id)
    {
        $model = Pendaftar::findOrFail($id);
        $title = 'Edit Akun Pendaftar';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Akun Pendaftar' => route('pendaftar.index'),
            'Edit' => route('pendaftar.edit', $model->id),
        ];
        $validator = \JsValidator::make($request->getEditRules(), $request->messages());

        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('pendaftars.edit', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PendaftarRequest $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(PendaftarRequest $request, $id)
    {
        try {
            $model = Pendaftar::findOrFail($id);
            $this->storeFiles($request, $model); //if there is any
            $data = $request->all();
            $data['password'] = ($request->password) ? Hash::make($request->password) : $model->password;
            $data['sekolah'] = ($data['id_sekolah']) ? Sekolah::find($data['id_sekolah'])->nama : null;
            $data['is_banned'] = ($request->has('is_banned')) ? true : false;
            $data['email_verified_at'] = ($request->has('email_verified_at')) ? Carbon::now() : null;

            $model->update($data);

            $json = ['status' => true, 'message' => 'Data berhasil diupdate!'];
        } catch (\Exception $e) {
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data berhasil diupdate!');
        //return redirect()->back()->with('alert', $alert);
        $toastr = new Toastr('success', 'Data berhasil diupdate!');
        return redirect()->back()->with('toastr', $toastr);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param PendaftarRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PendaftarRequest $request, $id)
    {
        $model = Pendaftar::findOrFail($id);
        return response()->json([
            'status' => $model->doDelete()
        ]);
    }

    /**
     * Handle Datatable request.
     *
     * @param PendaftarRequest $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function datatable(PendaftarRequest $request)
    {
        return Datatables::of(Pendaftar::query()->where('is_deleted', false))
            ->filter(function ($query) use ($request) {
                if (!empty($request->input('filter_email'))) {
                    $query->where('email', 'ilike', "%{$request->input('filter_email')}%");
                }
                if (!empty($request->input('filter_nama_lengkap'))) {
                    $query->where('nama_lengkap', 'ilike', "%{$request->input('filter_nama_lengkap')}%");
                }
                if (!empty($request->input('filter_no_hp'))) {
                    $query->where('no_hp', 'ilike', "%{$request->input('filter_no_hp')}%");
                }
                if (!empty($request->input('filter_id_sekolah'))) {
                    $query->where('id_sekolah', 'ilike', "%{$request->input('filter_id_sekolah')}%");
                }
            })
            ->editColumn('is_banned', function ($data) {
                return ($data->is_banned) ? '<span class="kt-badge kt-badge--danger kt-badge--inline"><i class="fa fa-check"></i> &nbsp; Diblokir</span>' : '';
            })
            ->editColumn('email_terverifikasi', function ($data) {
                return ($data->email_verified_at) ? '<span class="kt-badge kt-badge--warning kt-badge--inline"><i class="fa fa-check"></i> &nbsp; Sudah</span>' : '<span class="kt-badge kt-badge--dark kt-badge--inline">Belum</span>';
            })
            ->addColumn('action', function ($data) {
                $show = $edit = $delete = '';
                if (auth()->user()->hasAnyPermission(['Pendaftar - show'])) {
                    //$show = '<a href="'.route('pendaftar.show', $data->id).'" class="btn btn-outline-hover-warning btn-icon btn-circle btn-sm" data-toggle="kt-tooltip" title="Show detail" data-skin="brand"><i class="la la-eye kt-font-warning"></i></a>';
                }
                if (auth()->user()->hasAnyPermission(['Pendaftar - edit'])) {
                    $edit = '<a href="' . route('pendaftar.edit', $data->id) . '"class="btn btn-outline-hover-info btn-icon btn-circle btn-sm btnEdit" data-toggle="kt-tooltip" title="Edit" data-skin="brand"><i class="la la-pencil kt-font-info"></i></a>';
                }
                if (auth()->user()->hasAnyPermission(['Pendaftar - delete'])) {
                    $delete = '<button type="button" class="btn btn-outline-hover-danger btn-icon btn-circle btn-sm btn-dt-delete" data-toggle="kt-tooltip" title="Delete" data-skin="brand" data-url="' . route('pendaftar.destroy', $data->id) . '" data-csrf="' . csrf_token() . '"><i class="la la-close kt-font-danger"></i></button>';
                }
                return $show . $edit . $delete;
            })
            ->rawColumns(['is_banned', 'email_terverifikasi', 'action'])
            ->make(true);
    }

    /**
     * Handle file store
     *
     * @param $request
     * @param $model
     */
    private function storeFiles($request, $model)
    {
        foreach ($request->files as $key => $file) {
            if ($request->file($key)->isValid()) {
                Storage::delete($model->$key);
                $model->$key = $request->file($key)->storeAs("Pendaftar/$key/$model->id", $file->getClientOriginalName(), 'local');
                $model->save();
            }
        }
    }
}
