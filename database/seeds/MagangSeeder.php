<?php

use Illuminate\Database\Seeder;

class MagangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
//            [
//                'id_pengajuan' => 0,
//                'id_peserta' => 0,
//                'peserta' => 'john doe',
//                'email' => 'john.doe@gmail.com',
//                'no_ktp' => '321321123123',
//                'nim_nis' => '3212231',
//                'id_sekolah' => 0,
//                'sekolah' => 'Universitas Komputer Indonesia',
//                'id_programstudi' => 0,
//                'programstudi' => 'S1 Sistem Informasi',
//                'id_unit' => 615,
//                'unit' => 'Data dan Teknologi Informasi',
//                'tgl_awal' => '2019-12-01',
//                'tgl_akhir' => '2019-12-28',
//                'is_accepted' => null,
//                'file_surat_persetujuan_magang' => null,
//                'nilai' => null,
//                'catatan_nilai' => null,
//                'created_by' => 'admin.adla',
//                'created_at' => \Carbon\Carbon::now()
//            ],
//            [
//                'id_pengajuan' => -1,
//                'id_peserta' => -1,
//                'peserta' => 'Nampo Simambue',
//                'email' => 'nampo.simambue@gmail.com',
//                'no_ktp' => '22233112314123',
//                'nim_nis' => '2231222',
//                'id_sekolah' => 0,
//                'sekolah' => 'Universitas Komputer Indonesia',
//                'id_programstudi' => 0,
//                'programstudi' => 'S1 Teknik Informasi',
//                'id_unit' => 615,
//                'unit' => 'Data dan Teknologi Informasi',
//                'tgl_awal' => '2019-12-04',
//                'tgl_akhir' => '2020-01-03',
//                'is_accepted' => null,
//                'file_surat_persetujuan_magang' => null,
//                'nilai' => null,
//                'catatan_nilai' => null,
//                'created_by' => 'admin.adla',
//                'created_at' => \Carbon\Carbon::now()
//            ],
        ];

        foreach($data as $val){
            \App\Magang\Magang::updateOrCreate($val, $val);
        }
    }
}
