@extends((Request::ajax()) ? 'templates.layout-blank' : 'templates.'.config('genelator.template').'.layout')

@section('subheaderToolbar')
@endsection

@section('vendorPluginForCustomPagesStyles')
    @include('templates.metronic.demo5.includes.datatable_css')
@endsection

@section('content')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-list-2"></i>
            </span>
                <h3 class="kt-portlet__head-title">
                    List data Peserta Magang
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        @if(auth()->user()->can('Peserta - create'))
                            <a href="{{ url('peserta/create') }}"
                               class="btn btn-sm btn-warning btn-elevate btn-icon-sm btnAdd">
                                <i class="la la-plus"></i>
                                Tambah {{ $title }}
                            </a>
                        @endif
                        <button type="button" class="btn btn-sm btn-success btn-elevate btn-icon" id="btnToggleFilter"
                                data-toggle="kt-tooltip" data-placement="top" title=""
                                data-original-title="Search / Filter list data"
                                data-skin="brand">
                            <i class="la la-search"></i>
                        </button>
                        <button type="button" class="btn btn-sm btn-info btn-elevate btn-icon" id="btnReloadDt"
                                data-toggle="kt-tooltip" data-placement="top" title=""
                                data-original-title="Refresh list data"
                                data-skin="brand">
                            <i class="la la-refresh"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Filter for datatable-->
            <div id="sectionFilter" class="kt-section">
                <div class="kt-section__content kt-section__content--solid">
                    <form id="filterForm" autocomplete="off">
                        <h5>Filter:</h5>
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label class="form-control-label"> Pendaftar:</label>
                                <select name="id_pendaftar" id="filter_id_pendaftar" class="form-control select2ajax"
                                        data-ajax--url="{{ url('select2/pendaftar') }}" data-placeholder="Pilih"
                                        data-allow-clear="true">
                                    <option></option>
                                </select>
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-3">
                                <label class="form-control-label"> Nama:</label>
                                <input type="text" name="nama" id="filter_nama" class="form-control" value="">
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-3">
                                <label class="form-control-label"> KTP:</label>
                                <input type="text" name="no_ktp" id="filter_no_ktp" class="form-control" value="">
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-3">
                                <label class="form-control-label"> NIM/NIS:</label>
                                <input type="text" name="nim_nis" id="filter_nim_nis" class="form-control" value="">
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="w-100"></div>
                            <br>
                            <div class="col-lg-3">
                                <label class="form-control-label"> Email:</label>
                                <input type="text" name="email" id="filter_email" class="form-control" value="">
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-3">
                                <label class="form-control-label"> id_sekolah:</label>
                                <select name="id_sekolah" id="filter_id_sekolah" class="form-control select2ajax"
                                        data-ajax--url="{{ url('select2/sekolah') }}" data-placeholder="Pilih"
                                        data-allow-clear="true">
                                    <option></option>
                                </select>
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-3">
                                <label class="form-control-label"> Sekolah:</label>
                                <input type="text" name="sekolah" id="filter_sekolah" class="form-control" value="">
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-3">
                                <label class="form-control-label"> id_programstudi:</label>
                                <select name="id_programstudi" id="filter_id_programstudi"
                                        class="form-control select2ajax"
                                        data-ajax--url="{{ url('select2/programstudi') }}" data-placeholder="Pilih"
                                        data-allow-clear="true">
                                    <option></option>
                                </select>
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="w-100"></div>
                            <br>
                            <div class="col-lg-3">
                                <label class="form-control-label"> Program Studi:</label>
                                <input type="text" name="programstudi" id="filter_programstudi" class="form-control"
                                       value="">
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-3">
                                <label class="form-control-label"> File CV:</label>
                                <input type="text" name="file_cv" id="filter_file_cv" class="form-control" value="">
                                <span class="form-text text-muted"></span>
                            </div>

                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-12 text-right">
                                <button type="reset" class="btn btn-wide btn-sm btn-clean">Clear</button>
                                <button type="submit" class="btn btn-wide btn-sm btn-outline-success">Filter</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!--end: Filter for datatable-->

            <!--begin: Datatable -->
            <table class="table table-striped- table-hover table-checkable" id="datatable">
                <thead>
                <tr>
                    <th>Pendaftar</th>
                    <th>Nama</th>
                    <th>KTP</th>
                    <th>NIM/NIS</th>
                    <th>Email</th>
                    <th>id_sekolah</th>
                    <th>Sekolah</th>
                    <th>id_programstudi</th>
                    <th>Program Studi</th>
                    <th>File CV</th>

                    <th width="100px;">Action</th>
                </tr>
                </thead>
            </table>
            <!--end: Datatable -->

        </div>
    </div>

    <!-- Modal Container-->
    <div class="modal fade" id="modal" tabindex="-1"></div>
    <!--end: Modal Container-->
@endsection

@section('vendorPluginForCustomPagesScripts')
    @include('templates.metronic.demo5.includes.datatable_js')
@endsection

@section('pageScripts')
    <script>
        jQuery(document).ready(function () {
            datatable = $('#datatable').DataTable({
                dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url: "{{ url('peserta/datatable') }}",
                    data: function (d) {
                        d.filter_id_pendaftar = $('#filter_id_pendaftar').val();
                        d.filter_nama = $('#filter_nama').val();
                        d.filter_no_ktp = $('#filter_no_ktp').val();
                        d.filter_nim_nis = $('#filter_nim_nis').val();
                        d.filter_email = $('#filter_email').val();
                        d.filter_id_sekolah = $('#filter_id_sekolah').val();
                        d.filter_sekolah = $('#filter_sekolah').val();
                        d.filter_id_programstudi = $('#filter_id_programstudi').val();
                        d.filter_programstudi = $('#filter_programstudi').val();
                        d.filter_file_cv = $('#filter_file_cv').val();

                    }
                },
                columns: [
                    {data: 'id_pendaftar', name: 'id_pendaftar'},
                    {data: 'nama', name: 'nama'},
                    {data: 'no_ktp', name: 'no_ktp'},
                    {data: 'nim_nis', name: 'nim_nis'},
                    {data: 'email', name: 'email'},
                    {data: 'id_sekolah', name: 'id_sekolah'},
                    {data: 'sekolah', name: 'sekolah'},
                    {data: 'id_programstudi', name: 'id_programstudi'},
                    {data: 'programstudi', name: 'programstudi'},
                    {data: 'file_cv', name: 'file_cv'},

                    {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'},
                ],
                drawCallback: function (settings) {
                    KTApp.initTooltip($('[data-toggle="kt-tooltip"]'));
                }
            });

            //INIT DATATABLE'S BUTTONS
            initDatatableFilterAndRefreshButton(datatable); //check function in custom.js
            initDeleteButton(datatable); //check function in custom.js

            //INIT MODAL
            initAjaxModalButton('a.btnAdd, a.btnEdit');
        });
    </script>
@endsection