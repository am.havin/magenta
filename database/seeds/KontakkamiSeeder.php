<?php

use Illuminate\Database\Seeder;

class KontakkamiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'judul'     => 'Kementerian Badan Usaha Milik Negara',
                'konten'    => "
Email: magang@bumn.go.id
Nomor Telepon: 021-29935678 \n
Jalan Medan Merdeka Selatan No. 13 Jakarta 10110 Indonesia
Telepon. 021-29935678 Fax. 021-29935740
                ",
                'is_aktif' => 1,
            ],
        ];

        foreach($data as $val){
            \App\Kontakkami\Kontakkami::updateOrCreate($val, $val);
        }
    }
}
