<div class="section nobg clearfix" style="padding: 80px 0">
    <div class="container">
        <div class="row justify-content-between align-items-center">
            <div class="col-lg-8 col-md-10 offset-lg-2 offset-md-1 bottommargin-lg">
                <a href="https://www.youtube.com/watch?v=P3Huse9K6Xs" data-lightbox="iframe" class="play-video position-relative"><img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/video-bg.jpg" alt="Video Image" class="rounded-lg shadow-lg">
                    <i class="icon-play"></i>
                </a>
            </div>
            <div class="col-lg-5 col-md-6 mt-0 mt-md-4">
                <div class="feature-box fbox-plain fbox-small">
                    <div class="fbox-icon">
                        <i class="icon-line2-magic-wand"></i>
                    </div>
                    <h3>Easily Manage Your Works</h3>
                    <p>Quickly impact virtual testing procedures vis-a-vis viral e-commerce. Completely myocardinate seamless.</p>
                </div>

                <div class="feature-box fbox-plain fbox-small">
                    <div class="fbox-icon">
                        <i class="icon-line2-clock"></i>
                    </div>
                    <h3>24x7 Access</h3>
                    <p>Authoritatively create one-to-one relationships for B2B "outside the box" thinking. Dramatically seize.</p>
                </div>

                <div class="feature-box fbox-plain fbox-small mb-0">
                    <div class="fbox-icon">
                        <i class="icon-line2-speech"></i>
                    </div>
                    <h3>Great Community</h3>
                    <p>Proactively evolve user-centric paradigms rather than exceptional core. Authoritatively brand unique.</p>
                </div>
            </div>

            <div class="col-lg-5 col-md-6 mt-5 mt-md-0">
                <h2 class="ls0">Get One Day Free Trial.</h2>
                <p class="text-muted mb-5" style="font-size: 18px;">Enthusiastically embrace diverse e-markets after sustainable applications. Collaboratively impact intermandated systems vis-a-vis progressive information.</p>
                <div class="d-flex">
                    <a href="#" class="button button-rounded button-xlarge d-block flex-fill m-0 center nott t600 ls0">Sign Up</a>
                    <a href="#" class="button button-rounded button-xlarge d-block flex-fill button-border m-0 ml-3 center nott t600 ls0">Contact Us</a>
                </div>
            </div>
        </div>
    </div>
</div>
