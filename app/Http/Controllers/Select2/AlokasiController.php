<?php

namespace App\Http\Controllers\Select2;

use App\Alokasi\Alokasi;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;


class AlokasiController extends Controller
{
    public function unitkerjas(Request $request)
    {
        $return = [];
        $data = Alokasi::where('unit', 'ilike', "%$request->q%")->where('kuota', '>', '0')->pluck('unit', 'id_unit');
        foreach($data as $id => $text){
            $return[] = ['id' => $id, 'text' => $text ];
        }

        return response()->json(['results' => $return]);
    }
}
