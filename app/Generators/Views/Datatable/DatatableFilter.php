<?php

namespace App\Generators\Views\Datatable;

class DatatableFilter
{
    private $stubString;
    private $fieldsData;

    /**
     * Create constructor.
     */
    public function __construct($fieldsData)
    {
        $stubFileLoc        = resource_path('stubs/views/datatables/datatable_filter.stub');
        $this->stubString   = file_get_contents($stubFileLoc);
        $this->fieldsData   = $fieldsData;
    }

    public function generate()
    {
        $fieldsString = $this->generateFields($this->fieldsData);
        return $this->replaceStubString($fieldsString);
    }

    private function generateFields($fields)
    {
        $datatableFilterFields = new DatatableFilterFields($fields);
        return $datatableFilterFields->generate();
    }

    /**
     * @param $fieldsString
     * @return mixed
     */
    private function replaceStubString($fieldsString)
    {
        $replacedString = str_replace(
            [
                '{{fields}}',
            ],
            [
                $fieldsString,
            ],
            $this->stubString
        );
        return $replacedString;
    }
}
