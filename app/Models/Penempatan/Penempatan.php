<?php

namespace App\Penempatan;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Penempatan extends Model
{
    protected $table = 'penempatans';
    protected $guarded = ['id'];

    public function doDelete()
    {
        $status = true;

        DB::beginTransaction();
        try{
            $this->delete();
            DB::commit();
        }catch(\Exception $e) {
            DB::rollback();
            $status = false;
        }

        return $status;
    }
}
