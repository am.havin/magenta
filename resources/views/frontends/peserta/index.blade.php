@extends('templates.frontend.magenta.layout_nonhome')


@section('pageCss')
    <link rel="stylesheet" href="{{ url('templates/frontend/magenta/css/components/bs-datatable.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{url('templates/frontend/magenta/css/components/datepicker.css')}}" type="text/css" />
@endsection


@section('content')
    <div class="container">
        <div class="row">

            <div class="col-md-10 offset-1">
                <div style="" class="kt-portlet__head-actions">
                    <a href="{{ route('peserta_magang.create') }}" class="button button-3d button-small button-rounded button-lime button-light btnAdd">
                        <i class="icon-plus"></i>
                        Tambah data peserta
                    </a>
                    <br>
                    <br>
                    <br>
                </div>
            </div>

            <div class="col-md-10 offset-1">
                <div class="table-responsive">
                    <table class="table" id="datatable">
                        <thead>
                        <tr>
                            <th width="50px">Foto</th>
                            <th width="200px">Nama</th>
                            <th width="175px">No Indentitas</th>
                            <th>SMA-SMK / Universitas</th>
                            <th width="30px" class="text-center">CV</th>
                            <th width="75px" class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody></tbody>
                    </table>
                </div>

                <!-- Modal -->
                <div class="modal fade" id="modal" role="dialog" aria-labelledby="" aria-hidden="true"></div>
            </div>
        </div>
    </div>
@endsection


@section('pageScript')
    <script src="{{ url('templates/frontend/magenta/js/components/bs-datatable.js') }}"></script>
    <script>
        $(document).ready(function(){
            datatable = $('#datatable').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url: "{{ url('peserta_magang/datatable') }}",
                },
                columns: [
                    {data: 'foto', name: 'foto', orderable: false},
                    {data: 'nama', name: 'nama'},
                    {data: 'no_ktp', name: 'no_ktp'},
                    {data: 'sekolah', name: 'sekolah'},
                    {data: 'file_cv', name: 'file_cv', orderable: false, className: 'text-center', visible: false},
                    {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'},
                ],
                order: [[1, 'asc']],
                drawCallback: function (settings) {
                    $('[data-toggle="tooltip"]').tooltip()
                }
            });

            //INIT DATATABLE'S BUTTONS
            initDeleteButton(datatable); //check function in custom.js

            //INIT MODAL
            initAjaxModalButton('a.btnAdd, a.btnEdit');
        });
    </script>
@endsection