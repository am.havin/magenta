<?php

return [
	'mode'                  => 'utf-8',
	'format'                => 'A4',
	'author'                => '',
	'subject'               => '',
	'keywords'              => '',
	'creator'               => 'Kementerian Badan Usaha Milik Negara',
	'display_mode'          => 'fullpage',
	'tempDir'               => base_path('temp/')
];
