<?php

namespace App;

class PermissionsHelper
{
    public static function isChecked($rolesPermissions, $permissionId, $roleId)
    {
        $res = $rolesPermissions->where('permission_id', $permissionId)->where('role_id', $roleId);
        return ($res->count()) ? true : false;
    }
}
