@extends('templates.frontend.magenta.layout_nonhome')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-1">
                <h3>{{ $kontakkami->judul }} </h3>
                <p>
                {!! nl2br($kontakkami->konten) !!}
                </p>                     
                <br><br>
            </div>
        </div>
    </div>
@endsection