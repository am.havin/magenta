<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RolemenuRequest extends FormRequest
{
    public $rules = [];

    public function getCreateRules()
    {
        return $this->rules;
    }

    public function getEditRules()
    {
        return  $this->rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch (request()->route()->getActionMethod()) {
            case 'index':
                return auth('web')->user()->hasAnyPermission(['menu - manage akses menu']);
                break;
            case 'create':
                return false;
                break;
            case 'store':
                return auth('web')->user()->hasAnyPermission(['menu - manage akses menu']);
                break;
            case 'show':
                return false;
                break;
            case 'edit':
                return false;
                break;
            case 'update':
                return false;
                break;
            case 'destroy':
                return false;
                break;
            case 'datatable':
                return false;
                break;
            default:
                return false;
                break;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $controllerMethod = request()->route()->getActionMethod();
        if ($controllerMethod == 'store') {
            return $this->getCreateRules();
        }
        if ($controllerMethod == 'update') {
            return $this->getEditRules();
        }
        return [];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //'title.required' => 'A title is required',
            //'body.required'  => 'A message is required',
        ];
    }
}
