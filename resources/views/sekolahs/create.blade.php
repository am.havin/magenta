@extends((Request::ajax()) ? 'templates.layout-blank' : 'templates.'.config('genelator.template').'.layout')

@section('subheaderToolbar')
    <a href="#" class="btn invisible">
    </a>
@endsection

@section('content')
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title">{{ $title }}</h5>
                <button id="closeBtn" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                @if($errors->isNotEmpty())
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-warning" role="alert">
                                <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                <div class="alert-text">
                                    <ul>
                                        @foreach($errors->all() as $message)
                                            <li>{{ $message }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-lg-12">
                        <div class="kt-portlet">

                            <!--begin::Form-->
                            <form action="{{ url('sekolah/') }}" id="createForm" method="post" class="kt-form"
                                  autocomplete="off" enctype="multipart/form-data">
                                @csrf

                                <div class="kt-portlet__body">
                                    <div class="form-group row">
                                        <div class="col-lg-6">
                                            <label class="form-control-label">* Nama Sekolah:</label>
                                            <input type="text" name="nama" id="nama" class="form-control" value=""
                                                   required>
                                            <span class="form-text text-muted">nama universitas atau sekolah</span>
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="" style="display: block;"><span class="text-danger">*</span> Tingkat Sekolah</label>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input is_universitas_cb" type="radio" name="is_universitas" id="is_universitas_true" value="1">
                                                <label class="form-check-label" for="is_universitas_true">
                                                    Universitas
                                                </label>
                                            </div>
                                            <div class="form-check form-check-inline">
                                                <input class="form-check-input is_universitas_cb" type="radio" name="is_universitas" id="is_universitas_false" value="0">
                                                <label class="form-check-label" for="is_universitas_false">
                                                    SMA/SMK
                                                </label>
                                            </div>
                                        </div>
                                        <div class="w-100"></div>
                                        <br>
                                        <div class="col-lg-6">
                                            <label class="form-control-label"> Status Approval:</label>
                                            <select name="is_approved" id="is_approved" class="form-control">
                                                <option value="">Pilih</option>
                                                <option value="0">Reject</option>
                                                <option value="1">Approve</option>
                                            </select>
                                            <span class="form-text text-muted">approval referensi universitas atau sekolah</span>
                                        </div>

                                    </div>
                                </div>
                                <div class="kt-portlet__foot">
                                    <div class="kt-form__actions">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <button type="submit" class="btn btn-warning">Submit</button>
                                                {{-- <a href="{{ url('sekolah/') }}" class="btn btn btn-secondary">Cancel</a> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!--end::Form-->
                        </div>
                        <!--end::Portlet-->
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('pageScripts')
    {!! $validator->selector('#createForm') !!}
    <script>
        jQuery(document).ready(function () {
            initSelect2();
            initDatepicker();
            if (!(typeof datatable === "undefined")) {
                //pass instance datatable (optional) agar di-reload ketika submit formnya sukses
                initAjaxForm('#createForm', datatable); //datatable dapat dari index.blade.php
            }
        });
    </script>
@endsection

{{-- For Modal --}}
@if(Request::ajax())
    @yield('content')
    @yield('pageScripts')
@endif