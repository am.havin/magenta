<?php

namespace App\Sekolah;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Sekolah extends Model
{
    protected $table = 'sekolahs';
    protected $guarded = ['id'];

    public function pendaftar()
    {
        return $this->belongsTo('App\Pendaftar\Pendaftar', 'id_pendaftar');
    }

    public function sekolahprogramstudis()
    {
        return $this->hasMany('App\SekolahProgramStudi\SekolahProgramStudi', 'id_sekolah');
    }

    public function scopeGetNamaSekolahForAlokasi($query,$id_unit){
        return $query->where('id', '=', $id_unit);
    }

    public function scopeApprovalCaption($query, $is_approved){
        $status_caption = '';

        if($is_approved === 0)
        {
            $status_caption = 'Rejected';
        }
        else if($is_approved === 1)
        {
            $status_caption = 'Approved';
        }
        else
        {
            $status_caption = '-';
        }
        return $status_caption;
    }

    public function scopeNeedApproval($query)
    {
        return $query->whereNull('is_approved');
    }

    public function doDelete()
    {
        $status = true;

        DB::beginTransaction();
        try{
            $this->delete();
            DB::commit();
        }catch(\Exception $e) {
            DB::rollback();
            $status = false;
        }

        return $status;
    }
}
