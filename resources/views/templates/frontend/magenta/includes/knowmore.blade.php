<div class="section mt-0 pt-md-5 pt-0">
    <div class="container clearfix">
        <div class="row align-items-md-center mb-5">
            <div class="col-md-6 pr-5">
                <div class="heading-block mb-4 nobottomborder">
                    <div class="before-heading">Know More</div>
                    <h2 class="nott t600">Work Together, Build Together.</h2>
                </div>
                <div class="row">
                    <div class="col-6 col-sm-6 mb-4">
                        <div class="counter color t600"><span data-from="1" data-to="3" data-refresh-interval="2" data-speed="600"></span>+</div>
                        <h5 class="mt-0 t500">Branches</h5>
                    </div>

                    <div class="col-6 col-sm-6 mb-4">
                        <div class="counter color t600"><span data-from="1" data-to="37" data-refresh-interval="11" data-speed="900"></span>+</div>
                        <h5 class="mt-0 t500">Single Studios</h5>
                    </div>

                    <div class="col-6 col-sm-6 mb-4">
                        <div class="counter color t600"><span data-from="1" data-to="21" data-refresh-interval="3" data-speed="1000"></span>+</div>
                        <h5 class="mt-0 t500">Events per Month</h5>
                    </div>

                    <div class="col-6 col-sm-6 mb-4">
                        <div class="counter color t600"><span data-from="100" data-to="4500" data-refresh-interval="100" data-speed="1500"></span>+</div>
                        <h5 class="mt-0 t500">Active Members</h5>
                    </div>
                </div>
                <p class="text-muted">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda praesentium eius, similique reprehenderit ab voluptatibus eaque.</p>
            </div>
            <div class="col-md-3 col-sm-6">
                <div class="card shadow-sm border-light">
                    <img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/features/3.jpg" alt="Featured image 1" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title t600 color">Meeting Rooms</h5>
                        <p class="card-text">Some quick example text to build on the card title.</p>
                    </div>
                </div>
                <div class="card shadow-sm border-light mt-4">
                    <img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/features/2.jpg" alt="Featured image 2" class="card-img-top">
                    <div class="card-body">
                        <h5 class="card-title t600 color">Private Rooms</h5>
                        <p class="card-text">Some quick example text. similique reprehenderit ab voluptatibus eaque.</p>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-sm-6 pl-sm-3 mt-3 mt-sm-0">
                <div class="card shadow-sm border-light">
                    <img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/features/1.jpg" alt="Featured image 3" class="card-img-top">
                    <div class="card-body">
                        <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
