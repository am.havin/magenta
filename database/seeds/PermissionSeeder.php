<?php

use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            //USER
            ['group' => 'User', 'name' => 'user - index', 'guard_name' => 'web'],
            ['group' => 'User', 'name' => 'user - create', 'guard_name' => 'web'],
            ['group' => 'User', 'name' => 'user - show', 'guard_name' => 'web'],
            ['group' => 'User', 'name' => 'user - edit', 'guard_name' => 'web'],
            ['group' => 'User', 'name' => 'user - delete', 'guard_name' => 'web'],

            //ROLES
            ['group' => 'Roles', 'name' => 'roles - index', 'guard_name' => 'web'],
            ['group' => 'Roles', 'name' => 'roles - create', 'guard_name' => 'web'],
            ['group' => 'Roles', 'name' => 'roles - edit', 'guard_name' => 'web'],
            ['group' => 'Roles', 'name' => 'roles - delete', 'guard_name' => 'web'],

            //MENU
            ['group' => 'Menu', 'name' => 'menu - manage akses menu', 'guard_name' => 'web'],

            //PERMISSION
            ['group' => 'Permission', 'name' => 'permission - manage permission', 'guard_name' => 'web'],

            //GENERATOR
            ['group' => 'Generator', 'name' => 'generator - akses menu', 'guard_name' => 'web'],
        ];

        foreach($data as $val){
            \Spatie\Permission\Models\Permission::updateOrCreate($val, $val);
        }
    }
}
