@extends('templates.layout-blank')


@section('content')
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-body">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="">{{ $title }}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-sm">
                                <tbody>
                                    <tr>
                                        <td width="175px;">No Pengajuan</td>
                                        <td width="1px">:</td>
                                        <td>{{ $pengajuan->nomorPengajuan }}</td>
                                    </tr>
                                    <tr>
                                        <td>Alamat Instansi</td>
                                        <td>:</td>
                                        <td>{{ $pengajuan->alamat_instansi }}</td>
                                    </tr>
                                    <tr>
                                        <td>Pejabat Berwenang</td>
                                        <td>:</td>
                                        <td>{{ $pengajuan->nama_pejabat_berwenang }}</td>
                                    </tr>
                                    <tr>
                                        <td>Jabatan</td>
                                        <td>:</td>
                                        <td>{{ $pengajuan->jabatan }}</td>
                                    </tr>
                                    <tr>
                                        <td>Surat Pengantar Resmi</td>
                                        <td>:</td>
                                        <td>
                                            <a href="{{ route('pendaftaran.surat_pengantar_resmi', $pengajuan->id) }}">
                                                <i class="icon-download"></i> {{ basename($pengajuan->file_surat_pengantar_resmi) }}
                                            </a>
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Proposal</td>
                                        <td>:</td>
                                        <td>
                                            @if($pengajuan->file_proposal)
                                                <a href="{{ route('pendaftaran.proposal', $pengajuan->id) }}">
                                                    <i class="icon-download"></i> {{ basename($pengajuan->file_proposal) }}
                                                </a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    

                    <br>
                    <div class="row">
                        <div class="col-md-12">
                            <table class="table table-sm table-bordered">
                                <thead>
                                <tr>
                                    <th width="50px">Foto</th>
                                    <th width="">Peserta Magang</th>
                                    <th width="250px">Unit Kerja Tujuan</th>
                                    <th width="75px">Status</th>
                                    <th width="150px">Surat Persetujuan Magang</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($pengajuan->magangs->sortBy('peserta') as $magang)
                                    <tr>
                                        <td>
                                            <img src="{{ route('peserta_magang.foto', $magang->id_peserta) }}?rand={{ rand(10,1000) }}" alt="">
                                        </td>
                                        <td>{{ $magang->peserta }}</td>
                                        <td>
                                            {{--UNIT--}}
                                            @if($magang->old_unit)
                                                <s>{{ $magang->old_unit }}</s><br>
                                            @endif
                                            {{ $magang->unit }}<br>

                                            {{--TANGGAL--}}
                                            @if($magang->old_tgl_awal && $magang->old_tgl_akhir)
                                                <small class="text-muted">
                                                    <s>
                                                        {{ $magang->carbon_old_tgl_awal->format('d M Y') }} -
                                                        {{ $magang->carbon_old_tgl_akhir->format('d M Y') }}
                                                        ({{ $magang->old_lama_hari }} hari)
                                                    </s>
                                                </small><br>
                                            @endif
                                            <small class="text-muted">
                                                @if($magang->carbon_tgl_awal && $magang->carbon_tgl_akhir)
                                                    {{ $magang->carbon_tgl_awal->format('d M Y') }} -
                                                    {{ $magang->carbon_tgl_akhir->format('d M Y') }}
                                                    ({{ $magang->lama_hari }} hari)
                                                @endif
                                            </small>
                                        </td>
                                        <td>
                                            @if(!$pengajuan->is_submitted)
                                                @if($magang->is_expired)
                                                    <span class="badge badge-pill badge-danger">Expired</span>
                                                @else
                                                    <span class="badge badge-pill badge-dark">Belum disubmit</span>
                                                @endif
                                            @else
                                                {{--BELUM APA2--}}
                                                @if($magang->is_accepted === null) <span class="badge badge-primary">Belum disetujui</span> @endif

                                                {{--SETUJU--}}
                                                @if($magang->is_accepted === true)
                                                    @if($magang->is_need_pendaftar_approval && $magang->is_approved_by_pendaftar === null)
                                                        <span class="badge badge-warning"> <i class="icon-exclamation"></i> &nbsp; Disetujui Bersyarat</span>
                                                    @elseif($magang->is_need_pendaftar_approval && $magang->is_approved_by_pendaftar)
                                                        <span class="badge badge-warning"> <i class="icon-check"></i> &nbsp; Telah Anda Setujui</span>
                                                    @elseif($magang->is_need_pendaftar_approval && !$magang->is_approved_by_pendaftar)
                                                        <span class="badge badge-dark">Telah Anda Tolak</span>
                                                    @else
                                                        <span class="badge badge-warning"> <i class="icon-check"></i> &nbsp; Disetujui</span>
                                                        @endif
                                                @endif

                                                {{--TOLAK--}}
                                                @if($magang->is_accepted === false)
                                                    @if($magang->is_need_pendaftar_approval && $magang->is_approved_by_pendaftar === false)
                                                        <span class="badge badge-dark">Telah Anda Tolak</span>
                                                    @else
                                                        <span class="badge badge-danger">Tidak disetujui</span>
                                                    @endif
                                                @endif
                                            @endif
                                        </td>
                                        <td>
                                            @if($magang->is_accepted !== false)
                                                @if($magang->file_surat_persetujuan_magang)
                                                    <a href="{{ route('pendaftaran.proposal', $magang->id) }}">
                                                        <i class="icon-file"></i> {{ basename($magang->file_surat_persetujuan_magang) }}
                                                    </a>
                                                @else
                                                    <i>Belum ada</i>
                                                @endif
                                            @else
                                                -
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pageScript')
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
        });
    </script>
@endsection


{{-- For Modal --}}
@if(Request::ajax())
    @yield('content')
    @yield('pageScripts')
@endif
