<?php

use Illuminate\Database\Seeder;

class AlokasiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id_unit' => 615,
                'unit' => 'Bidang Teknologi Informasi',
                'kuota' => 2
            ],
        ];

        foreach($data as $val){
            \App\Alokasi\Alokasi::updateOrCreate($val, $val);
        }
    }
}
