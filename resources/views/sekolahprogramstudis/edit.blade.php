@extends((Request::ajax()) ? 'templates.layout-blank' : 'templates.'.config('genelator.template').'.layout')

@section('subheaderToolbar')
    <a href="#" class="btn invisible">
    </a>
@endsection

@section('content')
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title">{{ $title }}</h5>
                <button id="closeBtn" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                @if($errors->isNotEmpty())
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-warning" role="alert">
                                <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                <div class="alert-text">
                                    <ul>
                                        @foreach($errors->all() as $message)
                                            <li>{{ $message }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-lg-12">
                        <div class="kt-portlet">

                            <!--begin::Form-->
                            <form action="{{ route('sekolahprogramstudi.update', $model->id) }}" id="editForm"
                                  method="post" class="kt-form" autocomplete="off" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="PUT">

                                <div class="kt-portlet__body">
                                    <div class="form-group row">
                                        <div class="col-lg-6">
                                            <label class="form-control-label">* Nama Program Studi:</label>
                                            <input type="text" name="nama" id="nama" class="form-control"
                                                   value="{{ $model->nama }}" required>
                                            <span class="form-text text-muted">nama program studi</span>
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="form-control-label">* Sekolah:</label>
                                            <input type="text" class="form-control" value="{{ $model->sekolah->nama }}" disabled>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="w-100"></div>
                                        <br>
                                        <div class="col-lg-6">
                                            <label class="form-control-label"> Pendaftar:</label>
                                            <input type="text" class="form-control" value="{{ $model->pendaftar->nama_lengkap }}" disabled>
                                            <span class="form-text text-muted">identitas pendaftar</span>
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="form-control-label"> Status Approval:</label>
                                            <select name="is_approved" id="is_approved" class="form-control">
                                                <option value="">Pilih</option>
                                                <option value="0" @if($model->is_approved === 0) selected @endif >
                                                    Reject
                                                </option>
                                                <option value="1" @if($model->is_approved === 1) selected @endif >
                                                    Approve
                                                </option>
                                            </select>
                                            <span class="form-text text-muted">approval referensi program studi</span>
                                        </div>
                                        <div class="w-100"></div>
                                        <br>

                                    </div>
                                </div>
                                <div class="kt-portlet__foot">
                                    <div class="kt-form__actions">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <button type="submit" class="btn btn-warning">Submit</button>
                                                {{-- <a href="{{ url('sekolahprogramstudi/') }}" class="btn btn btn-secondary">Cancel</a> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!--end::Form-->
                        </div>
                        <!--end::Portlet-->
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection


@section('pageScripts')
    {!! $validator->selector('#editForm') !!}
    <script>
        jQuery(document).ready(function () {
            initSelect2();
            initDatepicker();

            formEle = '#editForm';
            $(formEle).on('submit', function (event) {
                event.preventDefault();
                if ($(this).valid()) {
                    ajaxFormSubmit({
                        form: $(this),
                        initialFunction: function () {
                            // FUNCTION YANG DIJALANKAN SAAT SI FORM DISUBMIT
                            KTApp.block(formEle, {
                                overlayColor: '#000000',
                                type: 'v2',
                                state: 'primary',
                                message: ''
                            });
                        },
                        successFunction: function () {
                            // FUNCTION YANG DIJALANKAN KETIKA RETURN DARI CONTROLLER STATUS = TRUE
                            KTApp.unblock(formEle);
                            $('#modal').modal('hide'); //#modal ada di layout.blade.php
                            window.location.href = '{{ route('sekolahprogramstudi.index') }}';
                        },
                        failFunction: function () {
                            // FUNCTION YANG DIJALANKAN KETIKA RETURN DARI CONTROLLER STATUS = FALSE
                            KTApp.unblock(formEle);
                        },
                        errorFunction: function () {
                            // FUNCTION YANG DI JALANKAN KETIKA ADA ERROR DILUAR RETURN YANG DARI CONTROLLER
                            KTApp.unblock(formEle);
                        }
                    });
                }
            });
        });
    </script>
@endsection


{{-- For Modal --}}
@if(Request::ajax())
    @yield('content')
    @yield('pageScripts')
@endif