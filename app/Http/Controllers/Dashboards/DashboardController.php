<?php

namespace App\Http\Controllers\Dashboards;

use App\Toastr;
use App\Helpers\Helper;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Pkpt\PkptController;
use App\Models\Pkpt\PkptPeriode;
use App\Magang\Magang;
use App\Pengajuan\Pengajuan;
use App\Sekolah\Sekolah;
use App\Sertifikat\Sertifikat;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use phpCAS;
use PDF;


class DashboardController extends Controller
{

    public function __construct()
    {
    }

    /**
     * Display a listing of the resource.
     *
     * return \Illuminate\Http\Response
     */
    public function index()
    {
        //$magang_new = Pengajuan::getSubmittedStatus(false)->count();
        $magang_new = Pengajuan::getAprrovalBadgeQty();
        $cetak_sertifikat = Sertifikat::getBadgeQty();
        $sekolah_new = Sekolah::needApproval()->count();
        $tanggal_now = date("Y-m-d", time()); 
        $magang_aktif = Magang::getMahasiswa()->magangAktif()->get();
        $magang_aktif_array = array();
        $magang_saat_ini_aktif = Magang::getMahasiswaSaatIni()->magangAktif()->get();
        $magang_aktif_count = $magang_saat_ini_aktif->count();
        $magang_saat_ini_aktif_nama = array();

        foreach($magang_aktif as $key => $value)
        {
            $start = explode(" ", $value['tgl_awal']);
            $start_tstmp = strtotime($start[0]);
            $start_magang = date("d-m-Y", $start_tstmp);
            $start_magang = str_replace("-","/", $start_magang);
            $end =  explode(" ", $value['tgl_akhir']);
            $end_tstmp = strtotime($end[0]);
            $end_magang = date("d-m-Y", $end_tstmp);
            $end_magang = str_replace("-","/", $end_magang);
            $magang_aktif_array[$key]['name'] = ucwords($value['peserta']) ." - ". $value['unit'];
            $magang_aktif_array[$key]['details'] =  "Unit : " .$value['unit'] ."<br>Peserta Magang : ".$value['peserta'] . "<br>Periode Magang : " . $start_magang . " sampai " . $end_magang;
            $magang_aktif_array[$key]['start'] = $start[0];
            $magang_aktif_array[$key]['end'] = $end[0];
            $magang_aktif_array[$key]['color'] = '#4285F4';
        }

        foreach ($magang_saat_ini_aktif as $key => $value)
        {
            array_push($magang_saat_ini_aktif_nama, $value['peserta']);
        }

        $magang_aktif_json = json_encode($magang_aktif_array);
        $title = 'Homepage';
        $activeMenus = ['menuHomepage'];
        $breadcrumbs = [
            'Homepage' => url('/'),
        ];

        $viewData = compact('magang_new', 'magang_aktif', 'magang_saat_ini_aktif', 'magang_aktif_count', 'cetak_sertifikat', 'sekolah_new', 'tanggal_now', 'magang_aktif_json', 'magang_saat_ini_aktif_nama', 'title', 'activeMenus', 'breadcrumbs');

        $userRoles = auth('web')->user()->getRoleNames();
        if(count($userRoles) == 1 && $userRoles[0] == 'user'){
            return view('dashboards.index_user', $viewData);
        }
        return view('dashboards.index', $viewData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * return \Illuminate\Http\Response
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(Request $request)
    {
        abort(404);
    }

    /**
     * Display the specified resource.
     *
     * param  int  $id
     * return \Illuminate\Http\Response
     */
    public function show($id)
    {
        abort(404);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * param  int  $id
     * return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(Request $request, $id)
    {
        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * param  int  $id
     * return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        abort(404);
    }
}
