<?php

namespace App\Generators;

use Carbon\Carbon;
use Illuminate\Http\File;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Symfony\Component\Yaml\Yaml;

class Request
{
    private $stubString;

    /**
     * Migration constructor.
     */
    public function __construct()
    {
        $stubFileLoc        = resource_path('stubs/request.stub');
        $this->stubString   = file_get_contents($stubFileLoc);
    }

    /**
     * @param $requestData
     * @param $fieldsData
     * @param $modelData
     * @return string
     */
    public function generate($requestData, $fieldsData, $modelData)
    {
        $rulesString = $this->generateRules($fieldsData);
        $requestString = $this->replaceStubString($requestData['class_name'], $requestData['namespace'], $rulesString, $modelData['class_name']);
        return $this->createFile($requestData['class_name'], $requestData['file_dir'], $requestString);
    }

    private function generateRules($fields)
    {
        $return = '';
        foreach($fields as $key => $value){
            $return .= "
                '$key' => '".$value['validation_rules']."',
            ";
        }
        return $return;
    }

    /**
     * @param $className
     * @param $namespace
     * @param $rulesString
     * @param $modelName
     * @return mixed
     */
    private function replaceStubString($className, $namespace, $rulesString, $modelName)
    {
        $replacedString = str_replace(
            [
                '{{namespace}}',
                '{{className}}',
                '{{rules}}',
                '{{modelName}}'
            ],
            [
                $namespace,
                $className,
                $rulesString,
                $modelName
            ],
            $this->stubString
        );
        return $replacedString;
    }

    /**
     * @param $className
     * @param $file_dir
     * @param $requestString
     * @return string
     */
    private function createFile($className, $file_dir, $requestString)
    {
        $fileDir = 'Http/Requests/'.$file_dir;
        if(!is_dir(app_path($fileDir))){ //bikin direktorinya  kalau belum ada
            mkdir(app_path($fileDir), 0755, true);
        }

        $filepath = Str::finish($fileDir , '/') . "$className.php";
        file_put_contents(app_path($filepath), $requestString);

        return $filepath;
    }
}
