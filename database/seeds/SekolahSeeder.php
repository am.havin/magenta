<?php

use Illuminate\Database\Seeder;

class SekolahSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            //['nama' => 'Universitas Komputer Indonesia', 'id_pendaftar' => null, 'is_approved' => 1, 'is_universitas' => true],
        ];

        foreach($data as $val){
            \App\Sekolah\Sekolah::updateOrCreate($val, $val);
        }
    }
}
