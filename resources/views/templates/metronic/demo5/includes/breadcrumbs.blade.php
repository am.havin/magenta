<div class="kt-subheader__breadcrumbs">
    <a href="{{ url('') }}" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>

    @foreach($breadcrumbs as $name => $url)
        {{--<span class="kt-subheader__breadcrumbs-separator"></span>--}}
        <i class="flaticon2-right-arrow custom-breadcrumb-separator"></i> &nbsp;
        <a href="{{ $url }}" class="kt-link kt-subheader__breadcrumbs-link">
            {{ $name }}
        </a>
    @endforeach
</div>
