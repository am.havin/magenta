<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePesertasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pesertas', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pendaftar');
            $table->string('nama');
            $table->string('tempat_lahir');
            $table->timestamp('tgl_lahir');
            $table->string('no_ktp');
            $table->string('nim_nis');
            $table->string('email');
            $table->boolean('is_universitas')->default(0);
            $table->float('ipk')->nullable();
            $table->integer('id_sekolah')->nullable();
            $table->string('sekolah')->nullable();
            $table->integer('id_programstudi')->nullable();
            $table->string('programstudi')->nullable();
            $table->string('file_cv')->nullable();
            $table->string('file_skck')->nullable();
            $table->string('foto');

            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pesertas');
    }
}
