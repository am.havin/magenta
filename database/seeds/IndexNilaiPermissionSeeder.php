<?php

use Illuminate\Database\Seeder;

class IndexNilaiPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['group' => 'IndexNilai', 'name' => 'IndexNilai - index', 'guard_name' => 'web'],
            ['group' => 'IndexNilai', 'name' => 'IndexNilai - create', 'guard_name' => 'web'],
            ['group' => 'IndexNilai', 'name' => 'IndexNilai - show', 'guard_name' => 'web'],
            ['group' => 'IndexNilai', 'name' => 'IndexNilai - edit', 'guard_name' => 'web'],
            ['group' => 'IndexNilai', 'name' => 'IndexNilai - delete', 'guard_name' => 'web'],
        ];

        foreach($data as $val){
            \Spatie\Permission\Models\Permission::updateOrCreate($val, $val);
        }
    }
}
