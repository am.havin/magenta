<?php

namespace App\Http\Controllers\Frontends\Pendaftaran;

use App\Alokasi\Alokasi;
use App\Helper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Step1Request;
use App\Magang\Magang;
use App\Pengajuan\Pengajuan;
use App\Peserta\Peserta;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use PHPUnit\TextUI\Help;
use Yajra\DataTables\DataTables;


class PendaftaranController extends Controller
{
    protected $pendaftar;

    public function __construct()
    {
        Helper::findThenHandleExpired(); //untuk check pengajuan magang yg udah expired

        //harus begini karena auth()->user() nge-return null kalau belum ngelewatin middleware
        $this->middleware(function ($request, $next) {
            $this->pendaftar = auth('pendaftar')->user();
            return $next($request);
        });
    }

    public function index()
    {
        $title = 'Pendaftaran Magang';
        $activeMenus = ['menuPendaftaran'];
        $this->pendaftar->clearDraftPengajuans();
        $pengajuans = $this->pendaftar->pengajuans()
            ->where('is_draft', false)
            ->where('is_deleted', false)
            ->with('magangs')
            ->orderBy('id', 'desc')
            ->get();
        $viewData = compact('title', 'activeMenus', 'pengajuans');
        Helper::findThenHandleExpired();

        return view('frontends.pendaftaran.index', $viewData);
    }

    public function show(Request $request, $pengajuanId)
    {
        $pengajuan = Pengajuan::where('id', $pengajuanId)->where('id_pendaftar', $this->pendaftar->id)->first();

        if ($pengajuan) {
            $title = 'Detail pengajuan';
            $activeMenus = [''];
            $viewData = compact('title', 'activeMenus', 'pengajuan');

            return view('frontends.pendaftaran.show', $viewData);
        }
        return abort(404);
    }

    public function suratPengantarResmi(Request $request, $pengajuanId)
    {
        $pengajuan = Pengajuan::where('id', $pengajuanId)->where('id_pendaftar', $this->pendaftar->id)->first();

        if ($pengajuan) {
            if (Storage::disk('local')->exists($pengajuan->file_surat_pengantar_resmi)) {
                return Storage::download($pengajuan->file_surat_pengantar_resmi);
            }
            abort(404);
        }
    }

    public function proposal(Request $request, $pengajuanId)
    {
        $pengajuan = Pengajuan::where('id', $pengajuanId)->where('id_pendaftar', $this->pendaftar->id)->first();

        if ($pengajuan) {
            if (Storage::disk('local')->exists($pengajuan->file_proposal)) {
                return Storage::download($pengajuan->file_proposal);
            }
            abort(404);
        }
    }

    public function approve(Request $request, $magangId)
    {
        $status = false;
        $magang = Magang::findOrFail($magangId);
        $pengajuan = Pengajuan::where('id', $magang->id_pengajuan)->where('id_pendaftar', $this->pendaftar->id)->first();
        if($pengajuan && $magang->is_approved_by_pendaftar === null && $magang->is_need_pendaftar_approval && $request->has('approval')) {
            $magang->is_approved_by_pendaftar = $request->approval;
            $magang->is_accepted = ($request->approval) ? true : false;
            $magang->accepted_at = Carbon::now();
            $magang->save();
            $status = true;
        }

        return response()->json([
            'status' => $status
        ]);
    }

    public function destroy(Request $request, $pengajuanId)
    {
        $status = false;
        $pengajuan = Pengajuan::where('id', $pengajuanId)->where('id_pendaftar', $this->pendaftar->id)->first();

        if ($pengajuan && !$pengajuan->hasAtLeast1CheckedMagang()) {
            $pengajuan->delete();
            DB::table('magangs')->where('id_pengajuan', $pengajuan->id)->delete();
            $status = true;
        }

        return response()->json([
            'status' => $status
        ]);
    }
}
