<?php

use Illuminate\Database\Seeder;

class HalamandepanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'slogan'        => 'Magang terbaik untuk Pengalaman kamu',
                'introduksi'    => 'Magang Generasi Bertalenta (MAGENTA) Kementerian BUMN. Jangan lewatkan kesempatan untuk memperoleh pengalaman kerja disini!',
                'teks_gambar'   => 'Kementerian BUMN bangga menjadi salah satu lembaga pemerintahan terdepan. Profesionalitas adalah nilai utama kami.',
                'step1_judul'   => '1. Register',
                'step1_teks'    => 'Langkah awal yang harus dilakukan adalah dengan melakukan registrasi.',
                'step2_judul'   => '2. Isi data calon peserta magang',
                'step2_teks'    => 'Input di sistem data-data calon peserta magang yang akan diajukan.',
                'step3_judul'   => '3. Pengajuan Magang',
                'step3_teks'    => 'Lakukan pengajuan magang, lalu tinggal tunggu pengumuman dari kami.',
                'gambar_depan'  => '',
                'created_by'    => 'admin.adla',
            ],
        ];

        foreach($data as $val){
            \App\Halamandepan\Halamandepan::updateOrCreate($val, $val);
        }
    }
}
