<?php

namespace App\Http\Controllers\IndexNilai;

use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

use App\Alert;
use App\Http\Controllers\Controller;
use App\Http\Requests\IndexNilaiRequest;
use App\IndexNilai\IndexNilai;
use App\Toastr;


class IndexNilaiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param IndexNilaiRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(IndexNilaiRequest $request)
    {
        $title = 'Indeks Penilaian';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Indeks Nilai' => route('indexnilai.index'),
        ];
        $viewData = compact('title', 'activeMenus', 'breadcrumbs');

        return view('indexnilai.index', $viewData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param IndexNilaiRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(IndexNilaiRequest $request)
    {
        $title = 'Tambah Indeks Nilai';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Indeks Nilai' => route('indexnilai.index'),
            'Tambah' => route('indexnilai.create'),
        ];
        $validator = \JsValidator::make($request->getCreateRules(), $request->messages());

        $viewData = compact('title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('indexnilai.create', $viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param IndexNilaiRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(IndexNilaiRequest $request)
    {
        try {
            $model = IndexNilai::create($request->all());
            $this->storeFiles($request, $model); //if there is any
            $json = ['status' => true, 'message' => 'Data berhasil disimpan!'];
        } catch (\Exception $e) {
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data baru berhasil disimpan!');
        //return redirect(route('indexnilai.index'))->with('alert', $alert);
        $toastr = new Toastr('success', 'Data baru berhasil disimpan!');
        return redirect(route('indexnilai.index'))->with('toastr', $toastr);
    }

    /**
     * Display the specified resource.
     *
     * @param IndexNilaiRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(IndexNilaiRequest $request, $id)
    {
        $model = IndexNilai::findOrFail($id);
        $title = 'Detail Indeks Nilai';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Indeks Nilai' => route('indexnilai.index'),
            'Detail' => route('indexnilai.show', $model->id),
        ];
        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs');

        return view('indexnilai.show', $viewData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param IndexNilaiRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(IndexNilaiRequest $request, $id)
    {
        $model = IndexNilai::findOrFail($id);
        $title = 'Edit Indeks Nilai';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Indeks Nilai' => route('indexnilai.index'),
            'Edit' => route('indexnilai.edit', $model->id),
        ];
        $validator = \JsValidator::make($request->getEditRules(), $request->messages());

        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('indexnilai.edit', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param IndexNilaiRequest $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(IndexNilaiRequest $request, $id)
    {
        try {
            $model = IndexNilai::findOrFail($id);
            $model->update($request->all());
            $this->storeFiles($request, $model); //if there is any
            $json = ['status' => true, 'message' => 'Data berhasil diupdate!'];
        } catch (\Exception $e) {
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data berhasil diupdate!');
        //return redirect()->back()->with('alert', $alert);
        $toastr = new Toastr('success', 'Data berhasil diupdate!');
        return redirect()->back()->with('toastr', $toastr);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param IndexNilaiRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(IndexNilaiRequest $request, $id)
    {
        $model = IndexNilai::findOrFail($id);
        return response()->json([
            'status' => $model->doDelete()
        ]);
    }

    /**
     * Handle Datatable request.
     *
     * @param IndexNilaiRequest $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function datatable(IndexNilaiRequest $request)
    {
        return Datatables::of(IndexNilai::query())
            ->filter(function ($query) use ($request) {
                if (!empty($request->input('filter_nilai_mutu'))) {
                    $query->where('nilai_mutu', 'ilike', "%{$request->input('filter_nilai_mutu')}%");
                }
                if (!empty($request->input('filter_nilai_awal'))) {
                    $query->where('nilai_awal', 'ilike', "%{$request->input('filter_nilai_awal')}%");
                }
                if (!empty($request->input('filter_nilai_akhir'))) {
                    $query->where('nilai_akhir', 'ilike', "%{$request->input('filter_nilai_akhir')}%");
                }
                if (!empty($request->input('filter_standar_kualitas'))) {
                    $query->where('standar_kualitas', 'ilike', "%{$request->input('filter_standar_kualitas')}%");
                }
            })
            ->addColumn('action', function ($data) {
                $show = $edit = $delete = '';
                if (auth()->user()->hasAnyPermission(['IndexNilai - show'])) {
                    //$show = '<a href="' . route('indexnilai.show', $data->id) . '" class="btn btn-outline-hover-warning btn-icon btn-circle btn-sm" data-toggle="kt-tooltip" title="Show detail" data-skin="brand"><i class="la la-eye kt-font-warning"></i></a>';
                }
                if (auth()->user()->hasAnyPermission(['IndexNilai - edit'])) {
                    $edit = '<a href="' . route('indexnilai.edit', $data->id) . '"class="btn btn-outline-hover-info btn-icon btn-circle btn-sm btnEdit" data-toggle="kt-tooltip" title="Edit" data-skin="brand"><i class="la la-pencil kt-font-info"></i></a>';
                }
                if (auth()->user()->hasAnyPermission(['IndexNilai - delete'])) {
                    $delete = '<button type="button" class="btn btn-outline-hover-danger btn-icon btn-circle btn-sm btn-dt-delete" data-toggle="kt-tooltip" title="Delete" data-skin="brand" data-url="' . route('indexnilai.destroy', $data->id) . '" data-csrf="' . csrf_token() . '"><i class="la la-close kt-font-danger"></i></button>';
                }
                return $show . $edit . $delete;
            })->make(true);
    }

    /**
     * Handle file store
     *
     * @param $request
     * @param $model
     */
    private function storeFiles($request, $model)
    {
        foreach ($request->files as $key => $file) {
            if ($request->file($key)->isValid()) {
                Storage::delete($model->$key);
                $model->$key = $request->file($key)->storeAs("IndexNilai/$key/$model->id", $file->getClientOriginalName(), 'local');
                $model->save();
            }
        }
    }
}
