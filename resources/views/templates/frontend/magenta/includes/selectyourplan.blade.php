<div class="section" style="padding: 80px 0">
    <div class="container">
        <div class="heading-block nobottomborder mb-0">
            <div class="before-heading">Select your Plan</div>
            <h2 class="nott t600 mb-0">Membership</h2>
        </div>
    </div>

    <div class="container">
        <div class="row">
            <div class="col-12">
                <div id="price-carousel" class="owl-carousel carousel-widget" data-margin="30" data-nav="false" data-pagi="true" data-items-xs="1" data-items-sm="1" data-items-md="2" data-items-lg="3" data-items-xl="3">

                    <div class="price-list shadow-sm card border-0 rounded">
                        <div class="position-relative">
                            <img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/membership/1-day.jpg" alt="Featured image 1" class="card-img-top rounded-top">
                            <div class="card-img-overlay dark d-flex justify-content-center flex-column p-0 center">
                                <h3 class="card-title mb-0 text-white">Day Pass</h3>
                                <p class="card-text mb-0">Give it a Try</p>
                            </div>
                        </div>
                        <div class="card-body">

                            <div class="price-title pb-3">$19<small>Per Day</small></div>

                            <ul class="list-group list-group-flush mb-4">
                                <li class="list-group-item pl-0"><i class="icon-line-check pr-3 color"></i> Day Access Only</li>
                                <li class="list-group-item pl-0"><i class="icon-line-check pr-3 color"></i> Unlimited Superfast Wi-Fi</li>
                                <li class="list-group-item pl-0 text-black-50"><i class="icon-line-check pr-3 color"></i> <del>No lockers</del></li>
                            </ul>

                            <a href="#" class="button button-rounded button-large btn-block m-0 center t500">Sign Up</a>

                        </div>
                    </div>

                    <div class="price-list shadow-sm card border-0 rounded">
                        <div class="position-relative">
                            <img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/membership/share.jpg" alt="Featured image 1" class="card-img-top rounded-top">
                            <div class="card-img-overlay dark d-flex justify-content-center flex-column p-0 center">
                                <h3 class="card-title mb-0 text-white">Shared Desk</h3>
                                <p class="card-text mb-0">Economical but Flexible</p>
                            </div>
                        </div>
                        <div class="card-body">

                            <div class="price-title pb-3">$99<small>Monthly</small></div>

                            <ul class="list-group list-group-flush mb-4">
                                <li class="list-group-item pl-0"><i class="icon-line-check pr-3 color"></i> 8am to 8pm Access</li>
                                <li class="list-group-item pl-0"><i class="icon-line-check pr-3 color"></i> 100 hours of Superfast Wi-Fi</li>
                                <li class="list-group-item pl-0"><i class="icon-line-check pr-3 color"></i> Lockers Access</li>
                                <li class="list-group-item pl-0 text-black-50"><i class="icon-line-check pr-3 color"></i> <del>No Printers</del></li>
                            </ul>

                            <a href="#" class="button button-rounded button-large btn-block m-0 center t500">Sign Up</a>

                        </div>
                    </div>

                    <div class="price-list shadow-sm card border-0 rounded">
                        <div class="position-relative">
                            <img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/membership/flexible.jpg" alt="Featured image 1" class="card-img-top rounded-top">
                            <div class="card-img-overlay dark d-flex justify-content-center flex-column p-0 center">
                                <h3 class="card-title mb-0 text-white">Flexible Desk</h3>
                                <p class="card-text mb-0">Any Desk, Any Chair</p>
                            </div>
                        </div>
                        <div class="card-body">

                            <div class="price-title pb-3">$149<small>Monthly</small></div>

                            <ul class="list-group list-group-flush mb-4">
                                <li class="list-group-item pl-0"><i class="icon-line-check pr-3 color"></i> 200 hours of WIFI per Month</li>
                                <li class="list-group-item pl-0"><i class="icon-line-check pr-3 color"></i> Lockers Access</li>
                                <li class="list-group-item pl-0"><i class="icon-line-check pr-3 color"></i> 50 pages Printers</li>
                                <li class="list-group-item pl-0"><i class="icon-line-check pr-3 color"></i> 5hrs Skype Booth</li>
                                <li class="list-group-item pl-0"><i class="icon-line-check pr-3 color"></i> Mailing Address</li>
                            </ul>

                            <a href="#" class="button button-rounded button-large btn-block m-0 center t500">Sign Up</a>

                        </div>
                    </div>

                    <div class="price-list shadow-sm card border-0 rounded">
                        <div class="position-relative">
                            <img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/membership/private.jpg" alt="Featured image 1" class="card-img-top rounded-top">
                            <div class="card-img-overlay dark d-flex justify-content-center flex-column p-0 center">
                                <h3 class="card-title mb-0 text-white">Private Desk</h3>
                                <p class="card-text mb-0">Its all Yours!</p>
                            </div>
                        </div>
                        <div class="card-body">

                            <div class="price-title pb-3">$249<small>Monthly</small></div>

                            <ul class="list-group list-group-flush mb-4">
                                <li class="list-group-item pl-0"><i class="icon-line-check pr-3 color"></i> Private Desk + Locker</li>
                                <li class="list-group-item pl-0"><i class="icon-line-check pr-3 color"></i> 500 hours of WIFI per Month</li>
                                <li class="list-group-item pl-0"><i class="icon-line-check pr-3 color"></i> Meeting Room Access</li>
                                <li class="list-group-item pl-0"><i class="icon-line-check pr-3 color"></i> Unlimited Printers</li>
                                <li class="list-group-item pl-0"><i class="icon-line-check pr-3 color"></i> Unlimited Skype Booth</li>
                                <li class="list-group-item pl-0"><i class="icon-line-check pr-3 color"></i> Mailing Address</li>
                            </ul>

                            <a href="#" class="button button-rounded button-large btn-block m-0 center t500">Sign Up</a>

                        </div>
                    </div>

                </div>
            </div>
            <div class="col-md-11 mt-5 offset-md-1">
                <h3>Terms &amp; Conditions</h3>
                <ul class="pl-4 mb-0">
                    <li class="mb-1">Hours indicated are access to the space NOT the WIFI or Internet Connection.</li>
                    <li class="mb-1">Payment is upfront. We accept Cash and VISA/Master Cards, 2% Credit Card surcharge applies.</li>
                    <li class="mb-1">Monthly Plan starts from the point of signup for 30 days.</li>
                    <li class="mb-1">Sorry NO Pausing Memberships and NO Cancellations.</li>
                    <li class="mb-1">Sorry NO Upgrading Plans you need to use the hours in the plan.</li>
                    <li class="mb-1">If you run out of hours you can buy additional hours at the rate above, through our website or front desk.</li>
                    <li class="mb-1">If you wish to change plan once you’re on a plan this is done only at the end of the month or once you use your hours.</li>
                    <li class="mb-1">Additional Printing is charged at &dollar;0.30 for Black and White A4 & A3 and &dollar;2 for Colour A4 & A3.</li>
                    <li class="mb-1">Skype Booths must be booked before 1 day &amp; the keys can be collected at the frontdesk. Additional Skype Booth usage will be charged at &dollar;5 per hour.</li>
                    <li class="mb-1">Storage Lockers are first come first serve and are subject to availability.</li>
                    <li class="mb-1">Additional Charge for Mail Delivery.</li>
                    <li class="mb-1">Pets are not allowed.</li>
                </ul>
            </div>
        </div>
    </div>
</div>
