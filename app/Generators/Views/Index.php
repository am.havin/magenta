<?php

namespace App\Generators\Views;

use App\Generators\Views\Datatable\Datatable;
use App\Generators\Views\Datatable\DatatableFilter;
use App\Generators\Views\Datatable\DatatableScript;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Symfony\Component\Yaml\Yaml;

class Index
{

    private $stubString;

    /**
     * Create constructor.
     */
    public function __construct()
    {
        $stubFileLoc        = resource_path('stubs/views/index.stub');
        $this->stubString   = file_get_contents($stubFileLoc);
    }

    /**
     * @param $routeData
     * @param $viewsData
     * @param $fieldsData
     * @return string
     */
    public function generate($routeData, $viewsData, $fieldsData, $modelData)
    {
        $datatableFilterString  = $this->generateDatatableFilter($fieldsData);
        $datatableString        = $this->generateDatatable($fieldsData);
        $datatableScriptString  = $this->generateDatatableScript($fieldsData, $routeData);
        $indexString            = $this->replaceStubString(
            $routeData['url'],
            $viewsData['title'],
            $datatableFilterString,
            $datatableString,
            $datatableScriptString,
            $modelData['class_name']
        );
        return $this->createFile($viewsData['file_dir'], $indexString);
    }

    private function generateDatatableFilter($fields)
    {
        $datatableFilter = new DatatableFilter($fields);
        return $datatableFilter->generate();
    }

    private function generateDatatable($fields)
    {
        $datatable = new Datatable($fields);
        return $datatable->generate();
    }

    private function generateDatatableScript($fields, $routeData)
    {
        $datatableScript = new DatatableScript($fields,$routeData);
        return $datatableScript->generate();
    }

    /**
     * @param $routeUrl
     * @param $title
     * @param $datatableFilter
     * @param $datatable
     * @param $datatableScript
     * @return mixed
     */
    private function replaceStubString($routeUrl, $title, $datatableFilter, $datatable, $datatableScript, $modelName)
    {
        $replacedString = str_replace(
            [
                '{{routeUrl}}',
                '{{title}}',
                '{{datatableFilter}}',
                '{{datatable}}',
                '{{datatableScript}}',
                '{{modelName}}',
            ],
            [
                Str::finish($routeUrl, '/'),
                $title,
                $datatableFilter,
                $datatable,
                $datatableScript,
                $modelName
            ],
            $this->stubString
        );
        return $replacedString;
    }

    /**
     * @param $file_dir
     * @param $string
     * @return string
     */
    private function createFile($file_dir, $string)
    {
        $fileDir = 'views/'.$file_dir;
        if(!is_dir(resource_path($fileDir))){ //bikin direktorinya  kalau belum ada
            mkdir(resource_path($fileDir), 0755, true);
        }

        $filepath = Str::finish($fileDir , '/') . "index.blade.php";
        file_put_contents(resource_path($filepath), $string);

        return $filepath;
    }
}
