<?php

namespace App;

use App\Alokasi\Alokasi;
use App\Magang\Magang;
use App\Pendaftar\Pendaftar;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Support\Facades\DB;

class Helper
{
    public static function getUnavailableDates($idUnit, $bulan, $tahun, $exceptMagangId = null, $checkIsOwnUnapprovedMagang = true)
    {
        $occurrence = collect([]);
        $alokasi = Alokasi::where('id_unit', $idUnit)->first();

        //1. ngambil data magang di antara 2 tanggal
        $baseDate = Carbon::create($tahun, $bulan, 1);
        $startDate = $baseDate->copy()->subWeek();
        $endDate = $baseDate->copy()->addYears(1);
        $magangs = Magang::query()
            ->whereDate('tgl_awal', '>=', $startDate)
            ->whereDate('tgl_akhir', '<=', $endDate)
            ->where('id_unit', $idUnit)
            ->where(function ($query) {
                $query
                    ->whereNull('is_accepted')
                    ->orWhere('is_accepted', true);
            });

        //klo $checkIsOwnUnapprovedMagang = true, berarti yang ngeceknya adalah dari sisi pendaftar (soalnya kalau admin gk perlu)
        if(
            ($exceptMagangId && $checkIsOwnUnapprovedMagang && Helper::checkIsOwnUnapprovedMagang($exceptMagangId)) ||
            ($exceptMagangId && !$checkIsOwnUnapprovedMagang)
        ){
            $magangs->where('id', '<>', $exceptMagangId);
        }
        $magangs->get();

        //2. kumpulin tanggal2 magang yang ditemukan. untuk nanti dilihat jumlah occurrence-nya
        $magangs->each(function($magang, $key) use ($occurrence){
            $period = CarbonPeriod::create($magang->tgl_awal, '1 day', $magang->tgl_akhir);
            foreach($period as $key => $value){
                $occurrence->push($value->format('Y-m-d'));
            }
        });

        //3. filter occurrence yang >= kuota
        $kuota = (isset($alokasi)) ? $alokasi->kuota : 0;
        $filtered = $occurrence->countBy()->filter(function ($value, $key) use ($kuota) {
            return $value >= $kuota;
        });

        return $filtered->keys();
    }

    public static function checkIsOwnUnapprovedMagang($idMagang)
    {
        $pendaftar = auth('pendaftar')->user();
        $pengajuans = $pendaftar->pengajuans;
        $magang = Magang::whereIn('id_pengajuan', $pengajuans->pluck('id'))
            ->whereNull('is_accepted')
            ->where('id', $idMagang)
            ->first();
        return ($magang) ? true : false;
    }

    public static function findThenHandleExpired()
    {
        $yesterday = Carbon::now()->subHours(24);
        return DB::table('magangs')
            ->select('magangs.updated_at')
            ->leftJoin('pengajuans', 'pengajuans.id', '=', 'magangs.id_pengajuan')
            ->where('pengajuans.is_submitted', false)
            ->whereDate('magangs.updated_at', '<', $yesterday)
            ->update([
                'is_expired' => true,
                'tgl_awal' => null,
                'tgl_akhir' => null,
                'updated_at' => Carbon::now(),
            ]);
    }
}