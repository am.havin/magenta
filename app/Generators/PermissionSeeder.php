<?php

namespace App\Generators;

use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Symfony\Component\Yaml\Yaml;

class PermissionSeeder
{
    private $stubString;

    /**
     * Migration constructor.
     */
    public function __construct()
    {
        $stubFileLoc        = resource_path('stubs/permission_seeder.stub');
        $this->stubString   = file_get_contents($stubFileLoc);
    }

    /**
     * @param $modelData
     * @return string
     */
    public function generate($modelData)
    {
        $permissionSeederString = $this->replaceStubString($modelData['class_name']);
        return $this->createFile($modelData['class_name'], $permissionSeederString);
    }

    /**
     * @param $modelName
     * @return mixed
     */
    private function replaceStubString($modelName)
    {
        $replacedString = str_replace(
            [
                '{{modelName}}',
            ],
            [
                $modelName,
            ],
            $this->stubString
        );
        return $replacedString;
    }

    /**
     * @param $string
     * @return string
     */
    private function createFile($modelName, $string)
    {
        $filepath = "seeds/{$modelName}PermissionSeeder.php";
        file_put_contents(database_path($filepath), $string);

        return $filepath;
    }
}
