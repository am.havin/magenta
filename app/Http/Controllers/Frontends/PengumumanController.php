<?php

namespace App\Http\Controllers\Frontends;

use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

use App\Http\Controllers\Controller;
use App\Http\Requests\PengumumanRequest;
use App\Pengumuman\Pengumuman;
use App\Toastr;


class PengumumanController extends Controller
{
    public function gambar($idPengumuman)
    {
        $model = Pengumuman::where('is_active', true)->where('id', $idPengumuman)->first();
        if($model){
            return response()->file(storage_path('app/'.$model->gambar));
        }
        abort(404);
    }
}
