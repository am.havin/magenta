<?php

use Illuminate\Database\Seeder;

class FaqPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['group' => 'Faq', 'name' => 'Faq - index', 'guard_name' => 'web'],
            ['group' => 'Faq', 'name' => 'Faq - create', 'guard_name' => 'web'],
            ['group' => 'Faq', 'name' => 'Faq - show', 'guard_name' => 'web'],
            ['group' => 'Faq', 'name' => 'Faq - edit', 'guard_name' => 'web'],
            ['group' => 'Faq', 'name' => 'Faq - delete', 'guard_name' => 'web'],
        ];

        foreach($data as $val){
            \Spatie\Permission\Models\Permission::updateOrCreate($val, $val);
        }
    }
}
