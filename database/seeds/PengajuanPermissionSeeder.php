<?php

use Illuminate\Database\Seeder;

class PengajuanPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['group' => 'Pengajuan', 'name' => 'Pengajuan - index', 'guard_name' => 'web'],
            ['group' => 'Pengajuan', 'name' => 'Pengajuan - create', 'guard_name' => 'web'],
            ['group' => 'Pengajuan', 'name' => 'Pengajuan - show', 'guard_name' => 'web'],
            ['group' => 'Pengajuan', 'name' => 'Pengajuan - edit', 'guard_name' => 'web'],
            ['group' => 'Pengajuan', 'name' => 'Pengajuan - delete', 'guard_name' => 'web'],
        ];

        foreach($data as $val){
            \Spatie\Permission\Models\Permission::updateOrCreate($val, $val);
        }
    }
}
