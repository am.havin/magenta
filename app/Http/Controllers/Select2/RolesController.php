<?php

namespace App\Http\Controllers\Select2;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

use App\Http\Controllers\Controller;


class RolesController extends Controller
{
    public function index(Request $request)
    {
        $return = [];
        $data = Role::where('name', 'ilike', "%$request->q%");
        if(!auth('web')->user()->hasAnyPermission(['roles - index'])){
            $data->where('name', '<>', 'superadmin');
        }

        foreach($data->pluck('name', 'id') as $id => $text){
            $return[] = ['id' => $id, 'text' => $text ];
        }

        return response()->json(['results' => $return]);
    }
}
