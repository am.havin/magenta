<?php

namespace App\Http\Requests\Frontend;

use App\Peserta\Peserta;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class AkunRequest extends FormRequest
{
    public $rules = [
        'nama_lengkap' => ['required', 'string', 'max:255'],
        'email' => ['required', 'string', 'email', 'max:255', 'unique:pendaftars'],
        'password' => ['required', 'string', 'min:8', 'confirmed'],
        'no_hp' => ['required', 'numeric', 'min:10', 'unique:pendaftars'],
        'id_sekolah' => ['required_without:sekolah'],
        'sekolah' => ['required_if:id_sekolah,0'],
        'pic' => 'max:512|mimes:jpg,png,jpeg',
    ];

    public function getEditRules()
    {
        $pendaftar = auth('pendaftar')->user();
        $uniqueRule = Rule::unique('pendaftars')->ignore($pendaftar->id);
        $this->rules['email'] = 'in:'.$pendaftar->email;
        $this->rules['no_hp'] = ['required', 'numeric', 'min:10', $uniqueRule];
        $this->rules['password'] = '';
        return $this->rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $controllerMethod = request()->route()->getActionMethod();
        if ($controllerMethod == 'updatedata') {
            return $this->getEditRules();
        }
        return [];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'email.required' => 'Email harus diisi!',
            'email.email' => 'Penulisan email harus benar!',
            'no_hp.min' => 'Nomor hp minimal 10 angka',
            'email.in' => 'Email tidak dapat diganti!',
            'sekolah.required_if' => 'Nama Sekolah harus diisi jika anda meilih Lain-lain',
            'id_sekolah.required_without' => 'Sekolah harus dipilih'
        ];
    }
}
