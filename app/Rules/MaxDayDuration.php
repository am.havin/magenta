<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class MaxDayDuration implements Rule
{
    private $maxDuration;

    /**
     * Create a new rule instance.
     *
     * @param $maxDuration
     */
    public function __construct($maxDuration)
    {
        $this->maxDuration = $maxDuration;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $tglAwal = Carbon::createFromFormat('Y-m-d', request()->tgl_awal);
        $tglAkhir = Carbon::createFromFormat('Y-m-d', request()->tgl_akhir);
        if($tglAwal->diffInDays($tglAkhir) <= $this->maxDuration){
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Maximal durasi magang adalah ' . $this->maxDuration . ' hari!';
    }
}
