<?php

use Illuminate\Database\Seeder;

class PenempatanPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['group' => 'Penempatan', 'name' => 'Penempatan - index', 'guard_name' => 'web'],
            ['group' => 'Penempatan', 'name' => 'Penempatan - create', 'guard_name' => 'web'],
            ['group' => 'Penempatan', 'name' => 'Penempatan - show', 'guard_name' => 'web'],
            ['group' => 'Penempatan', 'name' => 'Penempatan - edit', 'guard_name' => 'web'],
            ['group' => 'Penempatan', 'name' => 'Penempatan - delete', 'guard_name' => 'web'],
        ];

        foreach($data as $val){
            \Spatie\Permission\Models\Permission::updateOrCreate($val, $val);
        }
    }
}
