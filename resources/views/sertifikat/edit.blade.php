@extends((Request::ajax()) ? 'templates.layout-blank' : 'templates.'.config('genelator.template').'.layout')

@section('subheaderToolbar')
    <a href="#" class="btn invisible">
    </a>
@endsection

@section('content')
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title">{{ $title }}</h5>
                <button id="closeBtn" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                @if($errors->isNotEmpty())
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-warning" role="alert">
                                <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                <div class="alert-text">
                                    <ul>
                                        @foreach($errors->all() as $message)
                                            <li>{{ $message }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-lg-12">
                        <div class="kt-portlet">

                            <!--begin::Form-->
                            <form action="{{ route('sertifikat.update', $model->id) }}" id="editForm"
                                  method="post"
                                  class="kt-form" autocomplete="off" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="PUT">

                                <div class="kt-portlet__body">
                                    <div class="form-group row">
                                        {{--NAMA PESERTA--}}
                                        <div class="col-lg-12">
                                            <label>Nama Peserta:</label>
                                            <input type="text" class="form-control" value="{{ $model->peserta }}" disabled>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="w-100"></div>
                                        <br>

                                        {{--TOTAL NILAI--}}
                                        <div class="col-lg-6">
                                            <label>Total Nilai:</label>
                                            <input type="number" min="0" max="100" id="nilai" name="nilai" class="form-control" value="{{ $model->nilai }}">
                                            <span class="form-text text-muted"></span>
                                        </div>

                                        {{--Standar Kualitas--}}
                                        <div class="offset-lg-2 col-lg-4">
                                            <label>Standar Kualitas:</label>
                                            <input type="text" id="nilaiHuruf" name="" class="form-control" disabled value="{{ $model->nilaiHuruf }}">
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="w-100"></div>
                                        <br>

                                        {{--Catatan--}}
                                        <div class="col-lg-12">
                                            <label>Catatan:</label>
                                            <textarea class="form-control" name="catatan_nilai" id="catatan_nilai" cols="30" rows="5">{!! $model->catatan_nilai !!}</textarea>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="w-100"></div>
                                        <br>
                                    </div>
                                </div>
                                <div class="kt-portlet__foot">
                                    <div class="kt-form__actions">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <button type="submit" class="btn btn-warning">Submit</button>
                                                {{-- <a href="{{ url('pengajuan/') }}" class="btn btn btn-secondary">Cancel</a> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!--end::Form-->
                        </div>
                        <!--end::Portlet-->
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection


@section('pageScripts')
    {!! $validator->selector('#editForm') !!}

    <script>
        jQuery(document).ready(function () {
            $('#nilai').keyup(function(e){
                nilai = $(this).val();
                if(nilai != ''){
                    $.ajax({
                        url: '{{ url('sertifikat/get_nilai') }}' + '/' + nilai,
                        type: 'GET',
                        success: function (data) {
                            $('#nilaiHuruf').val(data.nilai_mutu);
                        },
                        error: function (error) {
                            //console.log(error);
                        },
                    });
                }

            });
        });
    </script>

    <script>
        jQuery(document).ready(function () {
            formEle = '#editForm';
            $(formEle).on('submit', function(event){
                event.preventDefault();
                if($(this).valid()){
                    ajaxFormSubmit({
                        form: $(this),
                        initialFunction: function(){
                            // FUNCTION YANG DIJALANKAN SAAT SI FORM DISUBMIT
                            KTApp.block(formEle, {
                                overlayColor: '#000000',
                                type: 'v2',
                                state: 'primary',
                                message: ''
                            });
                        },
                        successFunction: function () {
                            // FUNCTION YANG DIJALANKAN KETIKA RETURN DARI CONTROLLER STATUS = TRUE
                            KTApp.unblock(formEle);
                            $('#modal').modal('hide'); //#modal ada di layout.blade.php
                            window.location.href = '{{ route('sertifikat.index') }}';
                        },
                        failFunction: function(){
                            // FUNCTION YANG DIJALANKAN KETIKA RETURN DARI CONTROLLER STATUS = FALSE
                            KTApp.unblock(formEle);
                        },
                        errorFunction: function(){
                            // FUNCTION YANG DI JALANKAN KETIKA ADA ERROR DILUAR RETURN YANG DARI CONTROLLER
                            KTApp.unblock(formEle);
                        }
                    });
                }
            });
        });
    </script>
@endsection


{{-- For Modal --}}
@if(Request::ajax())
    @yield('content')
    @yield('pageScripts')
@endif