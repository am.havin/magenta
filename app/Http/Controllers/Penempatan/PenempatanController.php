<?php

namespace App\Http\Controllers\Penempatan;

use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

use App\Alert;
use App\Http\Controllers\Controller;
use App\Http\Requests\PenempatanRequest;
use App\Penempatan\Penempatan;
use App\Toastr;


class PenempatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param PenempatanRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(PenempatanRequest $request)
    {
        $penempatan = Penempatan::first();
        return redirect(route('penempatan.edit', $penempatan->id));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param PenempatanRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(PenempatanRequest $request)
    {
        $title = 'Tambah Penempatan';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Penempatan' => route('penempatan.index'),
            'Tambah' => route('penempatan.create'),
        ];
        $validator = \JsValidator::make($request->getCreateRules(), $request->messages());

        $viewData = compact('title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('penempatans.create', $viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PenempatanRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PenempatanRequest $request)
    {
        try {
            $model = Penempatan::create($request->all());
            $this->storeFiles($request, $model); //if there is any
            $json = ['status' => true, 'message' => 'Data berhasil disimpan!'];
        } catch (\Exception $e) {
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data baru berhasil disimpan!');
        //return redirect(route('penempatan.index'))->with('alert', $alert);
        $toastr = new Toastr('success', 'Data baru berhasil disimpan!');
        return redirect(route('penempatan.index'))->with('toastr', $toastr);
    }

    /**
     * Display the specified resource.
     *
     * @param PenempatanRequest $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show(PenempatanRequest $request, $id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param PenempatanRequest $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PenempatanRequest $request, $id)
    {
        $model = Penempatan::findOrFail($id);
        $title = 'Edit Penempatan';
        $activeMenus = ['menuPenempatan'];
        $breadcrumbs = [
            'Penempatan' => route('penempatan.index'),
            'Edit' => route('penempatan.edit', $model->id),
        ];
        $validator = \JsValidator::make($request->getEditRules(), $request->messages());

        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('penempatans.edit', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PenempatanRequest $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(PenempatanRequest $request, $id)
    {
        try {
            $model = Penempatan::findOrFail($id);
            $model->konten = $request->konten;
            $model->save();
            $this->storeFiles($request, $model); //if there is any
            $json = ['status' => true, 'message' => 'Data berhasil diupdate!'];
        } catch (\Exception $e) {
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data berhasil diupdate!');
        //return redirect()->back()->with('alert', $alert);
        $toastr = new Toastr('success', 'Data berhasil diupdate!');
        return redirect()->back()->with('toastr', $toastr);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param PenempatanRequest $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PenempatanRequest $request, $id)
    {
        $model = Penempatan::findOrFail($id);
        return response()->json([
            'status' => $model->doDelete()
        ]);
    }

    /**
     * Handle Datatable request.
     *
     * @param PenempatanRequest $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function datatable(PenempatanRequest $request)
    {
        return Datatables::of(Penempatan::query())
            ->filter(function ($query) use ($request) {

                if (!empty($request->input('filter_konten'))) {
                    $query->where('konten', 'ilike', "%{$request->input('filter_konten')}%");
                }


            })
            ->addColumn('action', function ($data) {
                $show = $edit = $delete = '';
                if (auth()->user()->hasAnyPermission(['Penempatan - show'])) {
                    $show = '<a href="' . route('penempatan.show', $data->id) . '" class="btn btn-outline-hover-warning btn-icon btn-circle btn-sm" data-toggle="kt-tooltip" title="Show detail" data-skin="brand"><i class="la la-eye kt-font-warning"></i></a>';
                }
                if (auth()->user()->hasAnyPermission(['Penempatan - edit'])) {
                    $edit = '<a href="' . route('penempatan.edit', $data->id) . '"class="btn btn-outline-hover-info btn-icon btn-circle btn-sm btnEdit" data-toggle="kt-tooltip" title="Edit" data-skin="brand"><i class="la la-pencil kt-font-info"></i></a>';
                }
                if (auth()->user()->hasAnyPermission(['Penempatan - delete'])) {
                    $delete = '<button type="button" class="btn btn-outline-hover-danger btn-icon btn-circle btn-sm btn-dt-delete" data-toggle="kt-tooltip" title="Delete" data-skin="brand" data-url="' . route('penempatan.destroy', $data->id) . '" data-csrf="' . csrf_token() . '"><i class="la la-close kt-font-danger"></i></button>';
                }
                return $show . $edit . $delete;
            })->make(true);
    }

    /**
     * Handle file store
     *
     * @param $request
     * @param $model
     */
    private function storeFiles($request, $model)
    {
        foreach ($request->files as $key => $file) {
            if ($request->file($key)->isValid()) {
                Storage::delete($model->$key);
                $model->$key = $request->file($key)->storeAs("Penempatan/$key/$model->id", $file->getClientOriginalName(), 'local');
                $model->save();
            }
        }
    }
}
