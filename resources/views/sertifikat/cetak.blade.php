@extends('templates.layout-blank')

@section('subheaderToolbar')
    <a href="#" class="btn invisible">
    </a>
@endsection

@section('content')
    <div class="modal-dialog modal-md" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title">{{ $title }}</h5>
                <button id="closeBtn" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                @if($errors->isNotEmpty())
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-warning" role="alert">
                                <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                <div class="alert-text">
                                    <ul>
                                        @foreach($errors->all() as $message)
                                            <li>{{ $message }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-lg-12">
                        <div class="kt-portlet">

                            <!--begin::Form-->
                            <form action="{{ route('sertifikat.cetak', $model->id) }}" id="createForm"
                                  target="_blank"
                                  method="post"
                                  class="kt-form" autocomplete="off" enctype="multipart/form-data">
                                {{ csrf_field() }}

                                <div class="kt-portlet__body">
                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <label>Nama Penanda Tangan:</label>
                                            <select name="id_nama_tt_sertifikat"
                                                    id="id_nama_tt_sertifikat"
                                                    class="form-control" required min="6">
                                                @if($model->id_nama_tt_sertifikat)
                                                    <option value="{{ $model->id_nama_tt_sertifikat }}">{{ $model->nama_tt_sertifikat }}</option>
                                                @endif
                                            </select>
                                            <input type="hidden" id="nama_tt_sertifikat" name="nama_tt_sertifikat" value="@if($model->id_nama_tt_sertifikat){{ $model->nama_tt_sertifikat }} @endif">
                                            <input type="hidden" id="nip_tt_sertifikat" name="nip_tt_sertifikat" value="@if($model->id_nama_tt_sertifikat){{ $model->nip_tt_sertifikat }} @endif">
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="w-100"></div>
                                        <br>
                                        <div class="col-lg-12">
                                            <label>Jabatan:</label>
                                            <select name="id_jabatan_tt_sertifikat"
                                                    id="id_jabatan_tt_sertifikat"
                                                    class="form-control"
                                                    @if($model->id_jabatan_tt_sertifikat === null) disabled @endif
                                                    required min="10">
                                                @if($model->id_jabatan_tt_sertifikat)
                                                    <option value="{{ $model->id_jabatan_tt_sertifikat }}">{{ $model->jabatan_tt_sertifikat }}</option>
                                                @endif
                                            </select>
                                            <input type="hidden" id="jabatan_tt_sertifikat" name="jabatan_tt_sertifikat" value="@if($model->id_jabatan_tt_sertifikat){{ $model->jabatan_tt_sertifikat }} @endif">
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="w-100"></div>
                                        <br>
                                    </div>
                                </div>
                                <div class="kt-portlet__foot">
                                    <div class="kt-form__actions">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <button type="submit" class="btn btn-warning">Cetak</button>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!--end::Form-->
                        </div>
                        <!--end::Portlet-->
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection


@section('pageScripts')
    {!! $validator->selector('#createForm') !!}
    <script>
        $(document).ready(function(){

            @if($model->id_jabatan_tt_sertifikat)
                //SELECT 2 JABATAN (kalau udah dibuat sebelumnya)
                initSelect2Jabatan({{ $model->id_nama_tt_sertifikat }}, {{ $model->id_jabatan_tt_sertifikat }});
            @endif

            //SELECT2 - PENILAI
            $('#id_nama_tt_sertifikat').select2({
                placeholder: 'Pilih',
                allowClear: false,
                minimumInputLength: 2,
                ajax: {
                    url: 'https://simanis.bumn.go.id/api/v1/pegawai_by_status/2,3',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data, params) {
                        return {
                            results: data.item,
                        };
                    },
                },
            }).on('select2:select', function (event) {
                idPegawai = $('#id_nama_tt_sertifikat').select2('data')[0].id;
                initSelect2Jabatan(idPegawai);

                namaPegawai = $('#id_nama_tt_sertifikat').select2('data')[0].text;
                nipPegawai = $('#id_nama_tt_sertifikat').select2('data')[0].nip;
                $('#nama_tt_sertifikat').val(namaPegawai);
                $('#nip_tt_sertifikat').val(nipPegawai);
            });
        });

        function initSelect2Jabatan(idPegawai, idJabatan = null){
            if(idJabatan == null){
                $('#jabatan_tt_sertifikat').val('');
                $('#id_jabatan_tt_sertifikat').val('').prop('disabled', false)
            }

            $('#id_jabatan_tt_sertifikat').select2({
                placeholder: 'pilih',
                allowClear: false,
                ajax: {
                    url: 'https://simanis.bumn.go.id/api/v1/riwayat_jabatan_struktural_pelaksana_pegawai/' + idPegawai,
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data, params) {
                        return {
                            results: data.item,
                        };
                    },
                },
                escapeMarkup: function(markup) {
                    return markup;
                }
            }).on('select2:select', function (event) {
                namaJabatan = $('#id_jabatan_tt_sertifikat').select2('data')[0].text;
                $('#jabatan_tt_sertifikat').val(namaJabatan);
            });
        }
    </script>
@endsection


{{-- For Modal --}}
@if(Request::ajax())
    @yield('content')
    @yield('pageScripts')
@endif