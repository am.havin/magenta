<?php

namespace App\Http\Middleware;

use App\Pengajuan\Pengajuan;
use App\Toastr;
use Closure;

class MustCompleteStep1
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $idPengajuan = array_values(request()->route()->originalParameters())[0];
        $pengajuan = Pengajuan::where('id', $idPengajuan)
            ->with('magangs')
            ->where('id_pendaftar', auth('pendaftar')->user()->id)
            ->first();

        //kalau pengajuannya bukan milik dia
        if(!$pengajuan){
            return abort(404);
        }

        //cek apakah punya peserta magangs?
        if($pengajuan->magangs->isEmpty()){
            $toastr = new Toastr('error', 'Anda BELUM mimilih peserta magang!');
            return redirect(route('pendaftaran.step1.edit', $pengajuan->id))->with('toastr', $toastr);
        }

        //cek apakah ada yng expired?
        if($pengajuan->hasExpiredMagang()){
            $toastr = new Toastr('error', 'Jadwal peserta magang anda ada yang EXPIRED!!');
            return redirect(route('pendaftaran.step1.edit', $pengajuan->id))->with('toastr', $toastr);
        }

        return $next($request);
    }
}
