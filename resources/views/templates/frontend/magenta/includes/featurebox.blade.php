<div class="container">
    <div class="row justify-content-between">

        <!-- Feature Box
        ============================================= -->
        <div class="col-lg-4 col-md-6 my-4">
            <i class="icon-wifi-full color ml-0 mb-4 i-plain d-block fnone"></i>
            <h4 class="mb-3">Fiber Optic WiFi</h4>
            <p class="text-muted">Add any Field to your Custom Form. Canvas's inbuilt Forms Processor will handle the rest. You will never have to touch any PHP Codes.</p>
        </div>

        <!-- Feature Box
        ============================================= -->
        <div class="col-lg-4 col-md-6 my-4">
            <i class="icon-line2-cup color ml-0 mb-3 i-plain d-block fnone"></i>
            <h4 class="mb-3">Cafeteria</h4>
            <p class="text-muted">Add any Field to your Custom Form. Canvas's inbuilt Forms Processor will handle the rest. You will never have to touch any PHP Codes.</p>
        </div>

        <!-- Feature Box
        ============================================= -->
        <div class="col-lg-4 col-md-6 my-4">
            <i class="icon-line2-screen-desktop color ml-0 mb-3 i-plain d-block fnone"></i>
            <h4 class="mb-3">Flexible Desks</h4>
            <p class="text-muted">Add any Field to your Custom Form. Canvas's inbuilt Forms Processor will handle the rest. You will never have to touch any PHP Codes.</p>
        </div>

        <!-- Feature Box
        ============================================= -->
        <div class="col-lg-4 col-md-6 my-4">
            <i class="icon-line2-lock color ml-0 mb-3 i-plain d-block fnone"></i>
            <h4 class="mb-3">Lockers</h4>
            <p class="text-muted">Add any Field to your Custom Form. Canvas's inbuilt Forms Processor will handle the rest. You will never have to touch any PHP Codes.</p>
        </div>

        <!-- Feature Box
        ============================================= -->
        <div class="col-lg-4 col-md-6 my-4">
            <i class="icon-fingerprint color ml-0 mb-3 i-plain d-block fnone"></i>
            <h4 class="mb-3">100% Secure</h4>
            <p class="text-muted">Add any Field to your Custom Form. Canvas's inbuilt Forms Processor will handle the rest. You will never have to touch any PHP Codes.</p>
        </div>

        <!-- Feature Box
        ============================================= -->
        <div class="col-lg-4 col-md-6 my-4">
            <i class="icon-line2-printer color ml-0 mb-3 i-plain d-block fnone"></i>
            <h4 class="mb-3">Printers &amp; Fax</h4>
            <p class="text-muted">Add any Field to your Custom Form. Canvas's inbuilt Forms Processor will handle the rest. You will never have to touch any PHP Codes.</p>
        </div>

        <!-- Feature Box
        ============================================= -->
        <div class="col-lg-4 col-md-6 my-4">
            <i class="icon-couch color ml-0 mb-3 i-plain d-block fnone"></i>
            <h4 class="mb-3">Common Area</h4>
            <p class="text-muted">Add any Field to your Custom Form. Canvas's inbuilt Forms Processor will handle the rest. You will never have to touch any PHP Codes.</p>
        </div>

        <!-- Feature Box
        ============================================= -->
        <div class="col-lg-4 col-md-6 my-4">
            <i class="icon-line-clock color ml-0 mb-3 i-plain d-block fnone"></i>
            <h4 class="mb-3">24x7 Access</h4>
            <p class="text-muted">Add any Field to your Custom Form. Canvas's inbuilt Forms Processor will handle the rest. You will never have to touch any PHP Codes.</p>
        </div>

        <!-- Feature Box
        ============================================= -->
        <div class="col-lg-4 col-md-6 my-4">
            <i class="icon-playstation color ml-0 mb-3 i-plain d-block fnone"></i>
            <h4 class="mb-3">Other Activities</h4>
            <p class="text-muted">Add any Field to your Custom Form. Canvas's inbuilt Forms Processor will handle the rest. You will never have to touch any PHP Codes.</p>
        </div>
    </div>
</div>