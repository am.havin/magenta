<?php

namespace App\Http\Controllers\Frontends;

use App\Http\Controllers\Controller;
use App\Halamandepan\Halamandepan;
use App\Pengumuman\Pengumuman;

class HomeController extends Controller
{
    public function index()
    {
        $title = 'Magenta';
        $activeMenus = ['menuHome'];
        $home = Halamandepan::findOrFail(1);
        $pengumuman = Pengumuman::where('is_active', true)->first();
        $viewData = compact('title', 'activeMenus', 'home', 'pengumuman');
        return view('frontends.home.index', $viewData);
    }
}
