<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;

class ManualLogin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!auth('web')->check()){
            if($request->session()->has('cas_user') && $request->session()->has('simanis_user')){
                auth('web')->login($request->session()->get('simanis_user'));
            }
        }

        if(auth('web')->check()) {
            return $next($request);
        }
        return redirect()->url('adminlogout');
    }
}
