<?php

namespace App\Http\Controllers\Select2;

use Illuminate\Http\Request;
use App\Sekolah\Sekolah;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Cache;


class SekolahController extends Controller
{
    public function all(Request $request)
    {
        $return = [];
        $data = Sekolah::where('nama', 'ilike', "%$request->q%")
            ->where('is_approved', '=', '1')
            ->pluck('nama', 'id');
        foreach($data as $id => $text){
            $return[] = ['id' => $id, 'text' => $text ];
        }

        return response()->json(['results' => $return]);
    }

    public function index(Request $request, $isUniversitas = 0)
    {
        $return = [];
        $data = Sekolah::where('nama', 'ilike', "%$request->q%")
            ->where('is_approved', '=', '1')
            ->where('is_universitas', $isUniversitas)
            ->pluck('nama', 'id');
        foreach($data as $id => $text){
            $return[] = ['id' => $id, 'text' => $text ];
        }

        return response()->json(['results' => $return]);
    }

    public function sekolah_with_lain_lain(Request $request, $isUniversitas = 0)
    {
        $return[] = ['id' => 0, 'text' => 'Lain-lain' ];
        $data = Sekolah::where('nama', 'ilike', "%$request->q%")
            ->where('is_approved', '=', '1')
            ->where('is_universitas', $isUniversitas)
            ->pluck('nama', 'id');
        foreach($data as $id => $text){
            $return[] = ['id' => $id, 'text' => $text ];
        }

        return response()->json(['results' => $return]);
    }
}
