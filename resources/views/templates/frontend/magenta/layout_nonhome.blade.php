<!DOCTYPE html>
<html dir="ltr" lang="en-US">

@include('templates.frontend.magenta.includes.head')

<body class="stretched no-transition">

<!-- Document Wrapper
============================================= -->
<div id="wrapper" class="clearfix"> 

    <!-- Header
    ============================================= -->
    @include('templates.frontend.magenta.includes.header')

    <!-- Content
    ============================================= -->
    <section id="content">

        @include('templates.frontend.magenta.includes.alert')
        @include('templates.frontend.magenta.includes.alert_email')

        <div class="section frontend-section bg-theme-light clearfix">
            <div class="container">
                <div class="row">
                    <div class="col-md-10 offset-1">
                        <h3 style="margin: 0;">{{ $title }}</h3>
                    </div>
                </div>
            </div>
        </div>

        @yield('content')
        <br><br><br>
    </section><!-- #content end -->

    <!-- Footer
    ============================================= -->
    <footer id="footer" class="noborder" style="background-color: #155BBC; padding-top: 80px; padding-top: 0px;">

    {{--@include('templates.frontend.magenta.includes.footer_widgets')--}}

        <!-- Copyrights -->
        @include('templates.frontend.magenta.includes.footer_copyright')
        <!-- copyrights end -->

    </footer><!-- #footer end -->

</div><!-- #wrapper end -->

<!-- Go To Top
============================================= -->
{{--<div id="gotoTop" class="icon-angle-up"></div>--}}

<!-- External JavaScripts
============================================= -->
<script src="{{url('/')}}/templates/frontend/magenta/js/jquery.js"></script>
<script src="{{url('/')}}/templates/frontend/magenta/js/plugins.js"></script>
<script src="{{url('/')}}/vendor/select2-4.0.13/dist/js/select2.full.min.js"></script>
<script src="{{url('/')}}/vendor/bootstrap-datepicker-1.9.0-dist/js/bootstrap-datepicker.min.js"></script>
<script src="{{url('/')}}/vendor/blockUI/jquery.blockUI.js"></script>
<script src="{{ url('/js/fr_custom.js') }}" type="text/javascript"></script>
<script src="{{ url('/vendor/moment/moment.min.js') }}" type="text/javascript"></script>


<!-- Footer Scripts
============================================= -->
@include('templates.frontend.magenta.includes.metronic_scripts')
<script src="{{url('/')}}/templates/frontend/magenta/js/functions.js"></script>
<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
<script type="text/javascript" src="{{ asset('vendor/sweetalert2-9.7.1/dist/sweetalert2.min.js')}}"></script>



@yield('pageScript')

</body>
</html>