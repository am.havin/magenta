<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

use App\Alert;
use App\Http\Controllers\Controller;
use App\Http\Requests\RoleRequest;
use App\Toastr;


class RoleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param RoleRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(RoleRequest $request)
    {
        $title = 'Role';
        $activeMenus = ['menuSettings', 'menuRoles'];
        $breadcrumbs = [
            'Settings' => 'javascript:;',
            'Role' => route('roles.index'),
        ];
        $viewData = compact('title', 'activeMenus', 'breadcrumbs');

        return view('roles.index', $viewData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param RoleRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(RoleRequest $request)
    {
        $title = 'Tambah Role';
        $activeMenus = ['menuSettings', 'menuRoles'];
        $breadcrumbs = [
            'Settings' => 'javascript:;',
            'Role' => route('roles.index'),
            'Tambah' => route('roles.create'),
        ];
        $validator = \JsValidator::make($request->getCreateRules(), $request->messages());

        $viewData = compact('title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('roles.create', $viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RoleRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(RoleRequest $request)
    {
        # STORE LOGIC HERE
        try{
            $model = Role::create($request->all());
            $this->storeFiles($request, $model); //if there is any

            $json = ['status' => true, 'message' => 'Data berhasil disimpan!'];
        }catch(\Exception $e){
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if($request->ajax()){
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data baru berhasil disimpan!');
        //return redirect(route('roles.index'))->with('alert', $alert);
        $toastr = new Toastr('success', 'Data baru berhasil disimpan!');
        return redirect(route('roles.index'))->with('toastr', $toastr);
    }

    /**
     * Display the specified resource.
     *
     * @param RoleRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(RoleRequest $request, $id)
    {
        $model = Role::findOrFail($id);
        $title = 'Detail Role';
        $activeMenus = ['menuSettings', 'menuRoles'];
        $breadcrumbs = [
            'Settings' => 'javascript:;',
            'Role' => route('roles.index'),
            'Detail' => route('roles.show', $model->id),
        ];
        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs');

        return view('roles.show', $viewData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param RoleRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(RoleRequest $request, $id)
    {
        $model = Role::findOrFail($id);
        $title = 'Edit Role';
        $activeMenus = ['menuSettings', 'menuRoles'];
        $breadcrumbs = [
            'Settings' => 'javascript:;',
            'Role' => route('roles.index'),
            'Edit' => route('roles.edit', $model->id),
        ];
        $validator = \JsValidator::make($request->getEditRules(), $request->messages());
        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs', 'validator');

        return view('roles.edit', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param RoleRequest $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(RoleRequest $request, $id)
    {
        try{
            $model = Role::findOrFail($id);
            $this->storeFiles($request, $model); //if there is any
            $model->update($request->all());
            $json = ['status' => true, 'message' => 'Data berhasil diupdate!'];
        }catch(\Exception $e){
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if($request->ajax()){
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data baru berhasil disimpan!');
        //return redirect(route('roles.index'))->with('alert', $alert);
        $toastr = new Toastr('success', 'Data baru berhasil disimpan!');
        return redirect(route('roles.index'))->with('toastr', $toastr);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param RoleRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(RoleRequest $request, $id)
    {
        $model = Role::findOrFail($id);
        return response()->json([
            'status' => $model->doDelete()
        ]);
    }

    /**
     * Handle Datatable request.
     *
     * @param RoleRequest $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function datatable(RoleRequest $request)
    {
        return Datatables::of(Role::query())
            ->filter(function ($query) use ($request) {
                if (!empty($request->input('name'))) {
                    $query->where('name', 'ilike', "%{$request->input('name')}%");
                }
                if (!empty($request->input('guard_name'))) {
                    $query->where('guard_name', 'ilike', "%{$request->input('guard_name')}%");
                }
            })
            ->addColumn('action', function ($data) {
                $actions = '';
                $actions .= '<a href="' . route('roles.edit', $data->id) . '"class="btn btn-outline-hover-info btn-icon btn-circle btn-sm btnEdit" data-toggle="kt-tooltip" title="Edit" data-skin="brand"><i class="la la-pencil kt-font-info"></i></a>';
                $actions .= '<button type="button" class="btn btn-outline-hover-danger btn-icon btn-circle btn-sm btn-dt-delete" data-toggle="kt-tooltip" title="Delete" data-skin="brand" data-url="' . route('roles.destroy', $data->id) . '" data-csrf="' . csrf_token() . '"><i class="la la-close kt-font-danger"></i></button>';
                return $actions;
            })->make(true);
    }

    /**
     * Handle file store
     *
     * @param $request
     * @param $model
     */
    private function storeFiles($request, $model)
    {
        foreach($request->files as $key => $file){
            if ($request->file($key)->isValid()) {
                Storage::delete($model->$key);
                $model->$key = $request->file($key)->storeAs("Reservation/$key/$model->id", $file->getClientOriginalName(),'local');
                $model->save();
            }
        }
    }
}
