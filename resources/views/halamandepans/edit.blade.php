@extends((Request::ajax()) ? 'templates.layout-blank' : 'templates.'.config('genelator.template').'.layout')

@section('subheaderToolbar')
    <a href="#" class="btn invisible">
    </a>
@endsection

@section('content')
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title">{{ $title }}</h5>
                <button id="closeBtn" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                @if($errors->isNotEmpty())
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-warning" role="alert">
                                <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                <div class="alert-text">
                                    <ul>
                                        @foreach($errors->all() as $message)
                                            <li>{{ $message }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-lg-12">
                        <div class="kt-portlet">

                            <!--begin::Form-->
                            <form action="{{ route('halamandepan.update', $model->id) }}" id="editForm" method="post"
                                  class="kt-form" autocomplete="off" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="PUT">

                                <div class="kt-portlet__body">
                                    <div class="form-group row">

                                        {{--ROW 1--}}
                                        <div class="col-lg-6">
                                            <label class="form-control-label">Slogan:</label>
                                            <textarea name="slogan" id="slogan" class="form-control" rows="4" required>{{ $model->slogan }}</textarea>
                                            <span class="form-text text-muted">Slogan di bagian kiri atas</span>
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="form-control-label">Introduksi:</label>
                                            <textarea name="introduksi" id="introduksi" class="form-control" rows="4" required>{{ $model->introduksi }}</textarea>
                                            <span class="form-text text-muted">Introduksi disebelah kanan slogan</span>
                                        </div>
                                        <div class="w-100"></div>
                                        <br>

                                        {{--ROW2--}}
                                        <div class="col-lg-6">
                                            <label> Gambar Depan:</label>
                                            <input type="file" class="form-control-file" name="gambar_depan"
                                                   id="gambar_depan">
                                            <span class="form-text text-muted">
                                                Upload file baru untuk mengganti gambar yang lama. <br>
                                                Ukuran maksimal 500KB, dalam format .jpg atau .png.
                                            </span>
                                            <p>
                                                <img src="{{ url( Str::replaceFirst('public/', 'storage/', $model->gambar_depan) ) }}" alt="" style="height: 50px; width: auto;">
                                                <a href="{{ url('filedownload?p='.$model->gambar_depan) }}">
                                                    <b>
                                                        <i class="la la-file"></i>
                                                        {{ basename($model->gambar_depan) }}
                                                    </b>
                                                </a>
                                            </p>
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="form-control-label">Teks Gambar:</label>
                                            <textarea name="teks_gambar" id="teks_gambar" class="form-control" rows="4" required>{{ $model->teks_gambar }}</textarea>
                                            <span class="form-text text-muted">Teks diatas gambar</span>
                                        </div>
                                        <div class="w-100">
                                            <br>
                                            <hr>
                                            <br>
                                        </div>
                                        <br><br>

                                        {{--STEP1--}}
                                        <div class="col-lg-6">
                                            <label class="form-control-label">Step 1 Judul:</label>
                                            <input type="text" name="step1_judul" id="step1_judul" class="form-control"
                                                   value="{{ $model->step1_judul }}" required>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="form-control-label">Step 1 Teks:</label>
                                            <textarea name="step1_teks" id="step1_teks" class="form-control"
                                                      required rows="3">{{ $model->step1_teks }}</textarea>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="w-100"></div>
                                        <br>

                                        {{--STEP2--}}
                                        <div class="col-lg-6">
                                            <label class="form-control-label">Step 2 Judul:</label>
                                            <input type="text" name="step2_judul" id="step2_judul" class="form-control"
                                                   value="{{ $model->step2_judul }}" required>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="form-control-label">Step 2 Teks:</label>
                                            <textarea name="step2_teks" id="step2_teks" class="form-control"
                                                      required rows="3">{{ $model->step2_teks }}</textarea>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="w-100"></div>
                                        <br>

                                        {{--STEP3--}}
                                        <div class="col-lg-6">
                                            <label class="form-control-label">Step 3 Judul:</label>
                                            <input type="text" name="step3_judul" id="step3_judul" class="form-control"
                                                   value="{{ $model->step3_judul }}" required>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="form-control-label">Step 3 Teks:</label>
                                            <textarea name="step3_teks" id="step3_teks" class="form-control"
                                                      required rows="3">{{ $model->step3_teks }}</textarea>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="w-100"></div>
                                        <br>

                                    </div>
                                </div>
                                <div class="kt-portlet__foot">
                                    <div class="kt-form__actions">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <button type="submit" class="btn btn-warning">Submit</button>
                                                {{-- <a href="{{ url('halamandepan/') }}" class="btn btn btn-secondary">Cancel</a> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!--end::Form-->
                        </div>
                        <!--end::Portlet-->
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection


@section('pageScripts')
    {!! $validator->selector('#editForm') !!}
    <script>
        jQuery(document).ready(function () {
            initSelect2();
            initDatepicker();
            if (!(typeof datatable === "undefined")) {
                //pass instance datatable (optional) agar di-reload ketika submit formnya sukses
                initAjaxForm('#editForm', datatable); //datatable dapat dari index.blade.php
            }
        });
    </script>
@endsection


{{-- For Modal --}}
@if(Request::ajax())
    @yield('content')
    @yield('pageScripts')
@endif