<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Permission;
use App\Role;

use App\Alert;
use App\Http\Controllers\Controller;
use App\Http\Requests\PermissionRequest;
use App\Toastr;


class PermissionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param PermissionRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(PermissionRequest $request)
    {
        $title = 'Permission';
        $activeMenus = ['menuSettings', 'menuPermissions'];
        $breadcrumbs = [
            'Settings' => 'javascript:;',
            'Permission' => route('permission.index'),
        ];
        $roles = Role::all();
        $permissions = Permission::orderBy('group')->get();
        $rolesPermissions = DB::table('role_has_permissions')->get();
        $viewData = compact('title', 'activeMenus', 'breadcrumbs', 'permissions', 'roles', 'rolesPermissions');

        return view('permissions.index', $viewData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PermissionRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PermissionRequest $request)
    {
        try{
            DB::table('role_has_permissions')->truncate();
            if($request->has('inputs')){
                $data = [];
                foreach($request->input('inputs') as $input){
                    $data[] = [
                        'permission_id' => $input['name'],
                        'role_id' => $input['value']
                    ];
                }
                DB::table('role_has_permissions')->insert($data);
            }
            app()->make(\Spatie\Permission\PermissionRegistrar::class)->forgetCachedPermissions();
            $status = true;
        }catch(\Exception $e){
            dd($e->getMessage());
            $status = false;
        }

        return response()->json([
            'status' => $status
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @return void
     */
    public function show()
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param PermissionRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PermissionRequest $request, $id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PermissionRequest $request
     * @param $id
     * @return void
     */
    public function update(PermissionRequest $request, $id)
    {
        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return void
     */
    public function destroy()
    {
        abort(404);
    }
}
