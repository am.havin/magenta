@extends('templates.frontend.magenta.layout_nonhome')

@section('pageCss')
    <link rel="stylesheet" href="{{ url('templates/frontend/magenta/css/components/bs-datatable.css') }}" type="text/css" />
    <link rel="stylesheet" href="{{url('templates/frontend/magenta/css/components/datepicker.css')}}" type="text/css" />
    <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/daterangepicker/daterangepicker.css" />
@endsection

@section('content')
    <div class="container">
        {{--STEPS UI--}}
        <div class="col-md-10 offset-1">
            <ul class="process-steps pengajuan-steps bottommargin clearfix">
                <li class="active">
                    <a href="" class="i-circled i-bordered i-alt divcenter">1</a>
                    <h5>Pilih Peserta</h5>
                </li>
                <li>
                    <a href="#ptab2" class="i-circled i-bordered i-alt divcenter">2</a>
                    <h5>Kirim Pengajuan</h5>
                </li>
            </ul>
        </div>

        {{--CREATE BUTTON--}}
        <div class="col-md-10 offset-1">
            <div style="" class="kt-portlet__head-actions">
                <a href="{{ route('pendaftaran.step1.magang.create', $pengajuan->id) }}" id="btnAdd" class="btn btn-warning btnAdd">
                    <i class="icon-plus"></i>
                    Pilih peserta
                </a> &nbsp;
                <small class="text-muted"><i>*Maksimal 1x pengajuan adalah {{ \App\Pengajuan\Pengajuan::$maxPengajuanPeserta }} peserta</i></small>
                <br>
                <br>
            </div>
        </div>

        {{--TABLE--}}
        <div class="col-md-10 offset-1">
            <div style="" class="kt-portlet__head-actions">
                <div class="table-responsive">
                    <table class="table" id="tablePeserta">
                        <thead>
                        <th width="30px">Foto</th>
                        <th width="20%">Nama</th>
                        <th width="20%">Unit Kerja tujuan</th>
                        <th width="100px">Tanggal Mulai</th>
                        <th width="100px">Tanggal Akhir</th>
                        <th width="50px">Lama</th>
                        <th width="100px">Action</th>
                        </thead>
                        <tbody>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>

        {{--BATAL BUTTON--}}
        <div class="col-md-10 offset-1">
            <div style="" class="kt-portlet__head-actions text-center">
                <a href="{{ route('pendaftaran') }}"
                   class="button button-3d button-small button-rounded button-leaf button-light">
                    <i class="icon-arrow-left"></i>
                    Kembali
                </a>
                <a href="{{ route('pendaftaran.step2.index', $pengajuan->id) }}"
                   class="button button-3d button-small button-rounded button-amber button-light">
                    Lanjut <i class="icon-arrow-right"></i>
                </a>
                <br>
            </div>
        </div>
    </div>

    <!-- Modal -->
    <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true"></div>
@endsection


@section('pageScript')
    <script src="{{ url('templates/frontend/magenta/js/components/bs-datatable.js') }}"></script>
    <script>
        $(document).ready(function(){
            datatable = $('#tablePeserta').DataTable({
                processing: true,
                serverSide: true,
                responsive: true,
                paging: false,
                searching: false,
                info : false,
                ajax: {
                    url: "{{ route('pendaftaran.step1.magang.datatable', $pengajuan->id) }}",
                },
                columns: [
                    {data: 'foto', name: 'foto', orderable: false},
                    {data: 'peserta', name: 'peserta', orderable: false},
                    {data: 'unit', name: 'unit', orderable: false},
                    {data: 'tgl_awal', name: 'tgl_awal', orderable: false},
                    {data: 'tgl_akhir', name: 'tgl_akhir', orderable: false},
                    {data: 'lama', name: 'lama', orderable: false},
                    {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'},
                ],
                order: [[0, 'asc']],
                drawCallback: function (settings) {
                    $('[data-toggle="tooltip"]').tooltip()
                    disableCreateButtonIfMaximumPesertaReached();
                }
            });

            //INIT DATATABLE'S BUTTONS
            initDeleteButton(datatable); //check function in custom.js

            //INIT MODAL
            initAjaxModalButton('a.btnAdd, a.btnEdit');
        });
    </script>
    <script>
        function disableCreateButtonIfMaximumPesertaReached(){
            if(datatable.data().count() >= 4){
                $('#btnAdd').addClass('disabled');
            }else{
                $('#btnAdd').removeClass('disabled');
            }
        }
    </script>
@endsection