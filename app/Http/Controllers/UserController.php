<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

use App\Alert;
use App\Http\Controllers\Controller;
use App\Http\Requests\UserRequest;
use App\User;
use App\Toastr;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param UserRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(UserRequest $request)
    {
        $title = 'User';
        $activeMenus = ['menuSettings', 'menuUsers'];
        $breadcrumbs = [
            'Settings' => 'javascript:;',
            'User' => route('users.index'),
        ];
        $viewData = compact('title', 'activeMenus', 'breadcrumbs');

        return view('users.index', $viewData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param UserRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(UserRequest $request)
    {
        $title = 'Tambah Users';
        $activeMenus = ['menuSettings', 'menuUsers'];
        $breadcrumbs = [
            'Settings' => 'javascript:;',
            'User' => route('users.index'),
            'Tambah' => route('users.create'),
        ];
        $validator = \JsValidator::make($request->getCreateRules(), $request->messages());

        $viewData = compact('title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('users.create', $viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param UserRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(UserRequest $request)
    {
        # STORE LOGIC HERE
        try{
            $model = User::create($request->all());
            $model->syncRoles($request->roles);
            $this->storeFiles($request, $model); //if there is any

            $json = ['status' => true, 'message' => 'Data baru berhasil disimpan!'];
        }catch(\Exception $e){
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if($request->ajax()){
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data baru berhasil disimpan!');
        //return redirect(route('users.index'))->with('alert', $alert);
        $toastr = new Toastr('success', 'Data baru berhasil disimpan!');
        return redirect(route('users.index'))->with('toastr', $toastr);
    }

    /**
     * Display the specified resource.
     *
     * @param UserRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(UserRequest $request, $id)
    {
        $model = User::findOrFail($id);
        $title = 'Detail Users';
        $activeMenus = ['menuSettings', 'menuUsers'];
        $breadcrumbs = [
            'Settings' => 'javascript:;',
            'User' => route('users.index'),
            'Detail' => route('users.show', $model->id),
        ];
        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs');

        return view('users.show', $viewData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param UserRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(UserRequest $request, $id)
    {
        $model = User::findOrFail($id);
        $title = 'Edit Users';
        $activeMenus = ['menuSettings', 'menuUsers'];
        $breadcrumbs = [
            'Settings' => 'javascript:;',
            'User' => route('users.index'),
            'Edit' => route('users.edit', $model->id),
        ];
        $validator = \JsValidator::make($request->getEditRules(), $request->messages());

        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('users.edit', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param UserRequest $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(UserRequest $request, $id)
    {
        try{
            $user = User::findOrFail($id);
            $this->storeFiles($request, $user); //if there is any
            $user->update($request->all());
            $user->syncRoles($request->roles);

            $json = ['status' => true, 'message' => 'Data berhasil diupdate!'];
        }catch(\Exception $e){
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if($request->ajax()){
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data baru berhasil disimpan!');
        //return redirect(route('roles.index'))->with('alert', $alert);
        $toastr = new Toastr('success', 'Data berhasil di-update!');
        return redirect()->back()->with('toastr', $toastr);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param UserRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(UserRequest $request, $id)
    {
        $model = User::findOrFail($id);
        return response()->json([
            'status' => $model->doDelete()
        ]);
    }

    /**
     * Handle Datatable request.
     *
     * @param UserRequest $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function datatable(UserRequest $request)
    {
        $query = DB::table('users')->select(DB::raw('
                users.id as id,
                users.username as username,
                users.pegawai_id as pegawai_id,
                users.pegawai_nama as pegawai_nama,
                string_agg(roles.name, \', \') as roles
            '))
            ->leftJoin('model_has_roles', 'users.id', '=', 'model_has_roles.model_id')
            ->leftJoin('roles', 'roles.id', '=', 'model_has_roles.role_id')
            ->groupBy('users.id');

        $datatable =  Datatables::of($query)
            ->filter(function ($query) use ($request) {
                if (!empty($request->input('username'))) {
                    $query->where('username', 'ilike', "%{$request->input('username')}%");
                }
                if (!empty($request->input('pegawai_id'))) {
                    $query->where('pegawai_id', 'ilike', "%{$request->input('pegawai_id')}%");
                }
                if (!empty($request->input('pegawai_nama'))) {
                    $query->where('pegawai_nama', 'ilike', "%{$request->input('pegawai_nama')}%");
                }
                if (!empty($request->input('roles'))) {
                    $query->whereRaw("users.id IN (SELECT model_id FROM model_has_roles WHERE role_id = {$request->input('roles')})");
                }
            })
            ->addColumn('action', function ($data) {
                $show = $edit = $delete= '';

                // SHOW
                //$show = '<a href="' . route('users.show', $data->id) . '" class="btn btn-outline-hover-warning btn-icon btn-circle btn-sm" data-toggle="kt-tooltip" title="Show detail" data-skin="brand"><i class="la la-eye kt-font-warning"></i></a>';

                //EDIT
                $edit = '<a href="' . route('users.edit', $data->id) . '"class="btn btn-outline-hover-info btn-icon btn-circle btn-sm btnEdit" data-toggle="kt-tooltip" title="Edit" data-skin="brand"><i class="la la-pencil kt-font-info"></i></a>';
                if(!auth('web')->user()->hasAnyPermission(['roles - index']) && Str::contains($data->roles, 'superadmin')){
                    $edit = '';
                }

                //DELETE
                $delete = '<button type="button" class="btn btn-outline-hover-danger btn-icon btn-circle btn-sm btn-dt-delete" data-toggle="kt-tooltip" title="Delete" data-skin="brand" data-url="' . route('users.destroy', $data->id) . '" data-csrf="' . csrf_token() . '"><i class="la la-close kt-font-danger"></i></button>';
                if(Str::contains($data->roles, 'superadmin')){
                    $delete = '';
                }

                return $show . $edit . $delete;
            });

        return $datatable->make(true);
    }

    /**
     * Handle file store
     *
     * @param $request
     * @param $model
     */
    private function storeFiles($request, $model)
    {
        foreach($request->files as $key => $file){
            if ($request->file($key)->isValid()) {
                Storage::delete($model->$key);
                $model->$key = $request->file($key)->storeAs("Reservation/$key/$model->id", $file->getClientOriginalName(),'local');
                $model->save();
            }
        }
    }
}
