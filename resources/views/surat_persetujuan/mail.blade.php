@component('mail::message')
<div class="fluid">Selamat siswa yang anda daftarkan telah diterima magang pada Kementerian Badan Usaha Milik Negara.
Berikut nama siswa yang diterima : </div><br/>

@foreach($data_pengajuan as $key => $value)
{{ $key + 1 . '. ' . $data_pengajuan[$key]->peserta . ' - ' . $data_pengajuan[$key]->nim_nis . ' (Periode '. $data_pengajuan[$key]->start_magang . ' - ' . $data_pengajuan[$key]->end_magang .  ')'}} <br/>
@endforeach
<br/>
<div class="fluid">Surat persetujuan siswa yang bersangkutan ada pada <i>attachment</i> pada email ini.
Terima Kasih.</div>
<br/>
Regards,<br/>
KBUMN - Kementerian Badan Usaha Milik Negara
@endcomponent