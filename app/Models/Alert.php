<?php

namespace App;

class Alert
{
    public $icon    = '';
    public $text    = '';
    public $type    = '';

    public static $types = ['success', 'danger', 'warning', 'info', 'light', 'dark'];
    private $icons  = [
        'success'   => 'flaticon-like',
        'danger'    => 'flaticon-cancel',
        'warning'   => 'flaticon-warning-sign',
        'info'      => 'flaticon-warning-sign',
        'light'     => 'flaticon-like',
        'dark'      => 'flaticon-warning-sign',
    ];

    public function __construct($type, $text, $icon = null)
    {
        $this->type = $type;
        $this->icon = ($icon) ? $icon : $this->icons[$type];
        $this->text = $text;
    }
}
