<?php

namespace App\Generators;

use App\Generators\Views\Create;
use App\Generators\Views\Edit;
use App\Generators\Views\Index;
use App\Generators\Views\Show;
use DatabaseSeeder;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Str;
use Symfony\Component\Yaml\Yaml;

class Generator
{
    public static $availableTypes = [
        'int',
        'integer',
        'string',
        'text',
        'date',
        'select',
        //'select2',
        'select2ajax',
        'file',
    ];

    private $yamlFileName;
    private $yamlData;

    public function __construct($yamlFileName)
    {
        $this->yamlFileName = $yamlFileName;
        $yamlFileLoc    = app_path("Yamls/generators/$yamlFileName.yaml");
        $this->yamlData = Yaml::parse(file_get_contents($yamlFileLoc));
    }

    public function generate()
    {
        if($this->hasAlreadyGenerated()){
            return "> this yaml has already been generated before!";
        }

        \App\Models\Generator::create([
            'yaml_filename'     => $this->yamlFileName,
            'route_url'         => $this->yamlData['route']['url'],
            'file_migration'    => $this->generateMigrations(),
            'file_permission_seeder'       => $this->generatePermissionSeeder(),
            'route_text'        => $this->generateRoute(),
            'file_model'        => $this->generateModel(),
            'file_request'      => $this->generateRequest(),
            'file_controller'   => $this->generateController(),
            'file_view_index'   => $this->generateViewIndex(),
            'file_view_create'  => $this->generateViewCreate(),
            'file_view_edit'    => $this->generateViewEdit(),
            'file_view_show'    => $this->generateViewShow(),
        ]);

        Artisan::call('dump-autoload');
        Artisan::call('migrate');
        $this->insertPermission($this->yamlData['model']['class_name']);
        return '> ' . $this->yamlFileName . ".yaml successfully executed \n> (composer dump-autoload and php artisan:migrate are also executed at the end of the process)";
    }

    private function generateMigrations()
    {
        $migrationGenerator = new Migration();
        return $migrationGenerator->generate($this->yamlData['table'], $this->yamlData['fields']);
    }

    private function generatePermissionSeeder()
    {
        $permissionSeederGenerator = new PermissionSeeder();
        return $permissionSeederGenerator->generate($this->yamlData['model']);
    }

    private function generateModel()
    {
        $modelGenerator = new Model();
        return $modelGenerator->generate($this->yamlData['table'], $this->yamlData['model']);
    }

    private function generateRequest()
    {
        $requestGenerator = new Request();
        return $requestGenerator->generate($this->yamlData['request'], $this->yamlData['fields'], $this->yamlData['model']);
    }

    private function generateController()
    {
        $controllerGenerator = new Controller();
        return $controllerGenerator->generate($this->yamlData['controller'], $this->yamlData['request'], $this->yamlData['model'], $this->yamlData['views'], $this->yamlData['route'], $this->yamlData['fields']);
    }

    private function generateViewIndex()
    {
        $indexGenerator = new Index();
        return $indexGenerator->generate($this->yamlData['route'], $this->yamlData['views'], $this->yamlData['fields'], $this->yamlData['model']);
    }

    private function generateViewCreate()
    {
        $createGenerator = new Create();
        return $createGenerator->generate($this->yamlData['fields'], $this->yamlData['route'], $this->yamlData['views'], $this->yamlData['request']);
    }

    private function generateViewEdit()
    {
        $createGenerator = new Edit();
        return $createGenerator->generate($this->yamlData['fields'], $this->yamlData['route'], $this->yamlData['views'], $this->yamlData['request']);
    }

    private function generateViewShow()
    {
        $createGenerator = new Show();
        return $createGenerator->generate($this->yamlData['fields'], $this->yamlData['route'], $this->yamlData['views']);
    }

    private function generateRoute()
    {
        $createGenerator = new Route($this->yamlData['route']['url'], $this->yamlData['route']['controller']);
        return $createGenerator->generate();
    }

    private function hasAlreadyGenerated()
    {
        $data = \App\Models\Generator::where('yaml_filename', $this->yamlFileName)->first();
        return ($data) ? true : false;
    }

    private function insertPermission($modelName)
    {
        $data = [
            ['group' => $modelName, 'name' => $modelName.' - index', 'guard_name' => 'web'],
            ['group' => $modelName, 'name' => $modelName.' - create', 'guard_name' => 'web'],
            ['group' => $modelName, 'name' => $modelName.' - show', 'guard_name' => 'web'],
            ['group' => $modelName, 'name' => $modelName.' - edit', 'guard_name' => 'web'],
            ['group' => $modelName, 'name' => $modelName.' - delete', 'guard_name' => 'web'],
        ];

        foreach($data as $val){
            \Spatie\Permission\Models\Permission::updateOrCreate($val, $val);
        }
    }
}
