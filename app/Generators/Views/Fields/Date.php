<?php

namespace App\Generators\Views\Fields;

use App\Generators\Views\Fields\Properties\Property;

class Date
{
    private $stubString;
    private $name;
    private $options;
    private $colSize;
    private $overrideRequired;
    private $withValue;
    private $elementId;

    public function __construct($name, $options, $colSize = 6, $overrideRequired = null, $withValue = false, $elementId = null)
    {
        $this->stubString  = file_get_contents(resource_path('stubs/views/fields/date.stub'));
        $this->name = $name;
        $this->options = $options;
        $this->colSize = $colSize;
        $this->overrideRequired = $overrideRequired;
        $this->withValue = $withValue;
        $this->elementId = $elementId;
    }

    /**
     * @return string
     */
    public function generate()
    {
        $required = Property::getRequired($this->options['nullable'], $this->overrideRequired);

        return $replacedString = str_replace(
            [
                '{{colSize}}',
                '{{label}}',
                '{{name}}',
                '{{asterisk}}',
                '{{required}}',
                '{{attributes}}',
                '{{helptext}}',
                '{{value}}',
                '{{elementId}}'
            ],
            [
                $this->colSize,
                $this->options['label'],
                $this->name,
                ($required) ? '*' : '',
                ($required) ? 'required' : '',
                isset($this->options['html_attributes']) ? $this->options['html_attributes'] : '',
                isset($this->options['helptext']) ? $this->options['helptext'] : '',
                ($this->withValue) ? $this->generateValue() : '',
                ($this->elementId) ? $this->elementId : $this->name,
            ],
            $this->stubString
        );
    }

    private function generateValue()
    {
        return '{{ \Carbon\Carbon::createFromFormat(\'Y-m-d H:i:s\', $model->'.$this->name.')->format(\'Y-m-d\') }}';
    }
}
