<?php

namespace App\Generators\Views\Datatable;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Symfony\Component\Yaml\Yaml;

class DatatableScript
{
    private $stubString;
    private $fieldsData;
    private $routeData;

    /**
     * Create constructor.
     * @param $fieldsData
     * @param $routeData
     */
    public function __construct($fieldsData, $routeData)
    {
        $stubFileLoc        = resource_path('stubs/views/datatables/datatable_script.stub');
        $this->stubString   = file_get_contents($stubFileLoc);
        $this->fieldsData   = $fieldsData;
        $this->routeData    = $routeData;
    }

    /**
     * @return string
     */
    public function generate()
    {
        $columnString = $this->generateColumns($this->fieldsData);
        $filterColumnString = $this->generateFilterColumns($this->fieldsData);
        return $this->replaceStubString($this->routeData['url'], $columnString, $filterColumnString);
    }

    private function generateColumns($fields)
    {
        $return = '';
        foreach($fields as $key => $value){
            $return .= "            {data: '$key', name: '$key'},\r\n";
        }
        return $return;
    }

    private function generateFilterColumns($fields)
    {
        $return = '';
        foreach($fields as $key => $value){
            $return .= "            d.filter_$key = $('#filter_$key').val();\r\n";
        }
        return $return;
    }

    /**
     * @param $routeUrl
     * @param $columnString
     * @param $filterColumnString
     * @return mixed
     */
    private function replaceStubString($routeUrl, $columnString, $filterColumnString)
    {
        $route = Str::finish($routeUrl, '/');
        $replacedString = str_replace(
            [
                '{{route}}',
                '{{columns}}',
                '{{filterColumns}}',
            ],
            [
                $route,
                $columnString,
                $filterColumnString,
            ],
            $this->stubString
        );
        return $replacedString;
    }
}
