<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePengajuansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pengajuans', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pendaftar');
            $table->string('file_surat_pengantar_resmi')->nullable();
            $table->string('file_proposal')->nullable();
            $table->string('file_persetujuan_magang')->nullable();
            $table->boolean('is_new')->default(0);
            $table->boolean('is_draft')->default(1);
            $table->boolean('is_submitted')->default(0);
            $table->boolean('is_deleted')->default(0);
            $table->string('nama_pejabat_berwenang')->nullable();
            $table->string('jabatan')->nullable();
            $table->text('alamat_instansi')->nullable();


            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamp('submitted_at')->nullable();
            $table->timestamp('deleted_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pengajuans');
    }
}
