<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateMagangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('magangs', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_pengajuan');
            $table->integer('id_peserta');
            $table->string('peserta');
            $table->string('email');
            $table->string('no_ktp');
            $table->string('nim_nis');
            $table->boolean('is_universitas')->default(0);
            $table->integer('id_sekolah');
            $table->string('sekolah');
            $table->integer('id_programstudi');
            $table->string('programstudi');
            $table->float('ipk')->nullable();
            $table->integer('id_unit');
            $table->integer('old_id_unit')->nullable();
            $table->string('unit');
            $table->string('old_unit')->nullable();
            $table->timestamp('tgl_awal')->nullable();
            $table->timestamp('tgl_akhir')->nullable();
            $table->timestamp('old_tgl_awal')->nullable();
            $table->timestamp('old_tgl_akhir')->nullable();
            $table->boolean('is_need_pendaftar_approval')->default(false);
            $table->boolean('is_approved_by_pendaftar')->nullable();
            $table->boolean('is_accepted')->nullable();
            $table->timestamp('accepted_at')->nullable();
            $table->boolean('is_expired')->default(0);
            $table->string('file_surat_persetujuan_magang')->nullable();
            $table->integer('nilai')->nullable();
            $table->text('catatan_nilai')->nullable();
            $table->integer('id_nama_tt_sertifikat')->nullable();
            $table->string('nama_tt_sertifikat')->nullable();
            $table->string('nip_tt_sertifikat')->nullable();
            $table->integer('id_jabatan_tt_sertifikat')->nullable();
            $table->string('jabatan_tt_sertifikat')->nullable();
            $table->boolean('is_deleted')->default(0);

            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('magangs');
    }
}
