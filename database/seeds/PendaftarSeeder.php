<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class PendaftarSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'email' => 'adla.mohammad@bumn.go.id',
            'password' => Hash::make('Kbumn123...'),
            'nama_lengkap' => 'Adla Mohammad Havin',
            'no_hp' => '081910629867',
            'id_sekolah' => 1,
            'sekolah' => 'UNIKOM',
            'created_by' => 'admin.adla',
            'email_verified_at' => \Carbon\Carbon::now(),
        ];

        \App\Pendaftar\Pendaftar::updateOrCreate(['email' => 'admin.adla@bumn.go.id'], $data);
    }
}
