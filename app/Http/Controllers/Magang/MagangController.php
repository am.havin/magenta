<?php

namespace App\Http\Controllers\Magang;

use App\Alokasi\Alokasi;
use App\Helper;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

use App\Alert;
use App\Http\Controllers\Controller;
use App\Http\Requests\MagangRequest;
use App\Magang\Magang;
use App\Toastr;


class MagangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param MagangRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(MagangRequest $request)
    {
        $title = 'Data Magang';
        $activeMenus = ['menuDataMagang'];
        $breadcrumbs = [
            'Data Magang' => route('magang.index'),
        ];
        $viewData = compact('title', 'activeMenus', 'breadcrumbs');

        return view('magangs.index', $viewData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param MagangRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(MagangRequest $request)
    {
        return abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param MagangRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(MagangRequest $request)
    {
        return abort(404);
    }

    /**
     * Display the specified resource.
     *
     * @param MagangRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(MagangRequest $request, $id)
    {
        $model = Magang::findOrFail($id);
        $title = 'Detail Data Magang';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Data Magang' => route('magang.index'),
            'Detail' => route('magang.show', $model->id),
        ];
        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs');

        return view('magangs.show', $viewData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param MagangRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(MagangRequest $request, $id)
    {
        $model = Magang::findOrFail($id);
        $title = 'Edit Data Magang';
        $activeMenus = ['menuApproval'];
        $breadcrumbs = [
            'Data Magang' => route('magang.index'),
            'Edit' => route('magang.edit', $model->id),
        ];
        $validator = \JsValidator::make($request->getEditRules(), $request->messages());

        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('magangs.edit', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param MagangRequest $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(MagangRequest $request, $id)
    {
        try {
            $model = Magang::findOrFail($id);
            $alokasi = Alokasi::where('id_unit', $request->id_unit)->first();
            $data = $request->all();
            $data['unit'] = $alokasi->unit;

            //kalau tgl yang diinput berbeda dengan tanggal yang lagi aktif sekarang
            if($data['tgl_awal'] != $model->carbonTglAwal->format('Y-m-d') ||
                $data['tgl_akhir'] != $model->carbonTglAkhir->format('Y-m-d'))
            {
                //klo kosong berarti baru pertama kali, maka simpan tgl originalnya tersebut!
                if($model->old_tgl_awal == null && $model->old_tgl_akhir == null){
                    $data['old_tgl_awal'] = $model->carbonTglAwal->format('Y-m-d');
                    $data['old_tgl_akhir'] = $model->carbonTglAkhir->format('Y-m-d');
                }

                //seandainya tgl yang dinput kembali lagi ke tgl yg original maka:
                if(
                    ($model->carbonOldTglAwal != null && $data['tgl_awal'] == $model->carbonOldTglAwal->format('Y-m-d')) &&
                    ($model->carbonOldTglAkhir != null && $data['tgl_akhir'] == $model->carbonOldTglAkhir->format('Y-m-d'))
                ){
                    $data['old_tgl_awal'] = null;
                    $data['old_tgl_akhir'] = null;
                }
            }

            //Kalau yang diinput beda unit yang dipilih
            if($data['id_unit'] != $model->id_unit){
                //klo kosong berarti baru pertama kali, maka simpan id_unit originalnya tersebut!
                if($model->old_id_unit == null && $model->old_unit == null){
                    $data['old_id_unit'] = $model->id_unit;
                    $data['old_unit'] = $model->unit;
                }

                //seandainya unit yang dinput kembali lagi ke unit yg original maka:
                if(
                    ($model->old_id_unit != null && $alokasi->id_unit == $model->old_id_unit) &&
                    ($model->old_unit != null && $alokasi->unit == $model->old_unit)
                ){
                    $data['old_id_unit'] = null;
                    $data['old_unit'] = null;
                }
            }

            //save ke DB
            $model->update($data);

            //siapin apakah butuh approval pendaftar / tidak & Reset status acceptednya setiap ganti tanggal
            $model = Magang::findOrFail($id);
            $model->is_need_pendaftar_approval =
                ($model->old_id_unit || $model->old_unit || $model->old_tgl_awal || $model->old_tgl_akhir)
                ? true : false;
            $model->is_accepted = null;
            $model->accepted_at = null;
            $model->is_approved_by_pendaftar = null;
            $model->save();

            $this->storeFiles($request, $model); //if there is any
            $json = ['status' => true, 'message' => 'Data berhasil diupdate!'];
        } catch (\Exception $e) {
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data berhasil diupdate!');
        //return redirect()->back()->with('alert', $alert);
        $toastr = new Toastr('success', 'Data berhasil diupdate!');
        return redirect()->back()->with('toastr', $toastr);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param MagangRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(MagangRequest $request, $id)
    {
        $model = Magang::findOrFail($id);
        return response()->json([
            'status' => $model->doDelete()
        ]);
    }

    /**
     * @param Request $request
     * @param $idUnit
     * @param $bulan
     * @param $tahun
     * @param null $exceptMagangId
     * @return \Illuminate\Http\JsonResponse
     */
    public function unavailableDates(Request $request, $idUnit, $bulan, $tahun, $exceptMagangId)
    {
        $dates = Helper::getUnavailableDates($idUnit, $bulan, $tahun, $exceptMagangId, false);
        return response()->json($dates);
    }

    /**
     * Handle Datatable request.
     *
     * @param MagangRequest $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function datatable(MagangRequest $request)
    {
        $queryBuilder = Magang::query()
            ->with('pengajuan.pendaftar')
            ->whereHas('pengajuan', function($q){
                $q->where('is_draft', false)->where('is_submitted', true)->where('is_deleted', false);
            })
            ->where('is_accepted', true)
            ->orderBy('id_pengajuan', 'desc');

        return Datatables::of($queryBuilder)
            ->filter(function ($query) use ($request) {
                if ($request->has('filter_pendaftar')) {
                    $query->whereHas('pengajuan.pendaftar', function($q) use($request){
                        $q->where('nama_lengkap', 'ilike', "%{$request->input('filter_pendaftar')}%");
                    });
                }
                if ($request->has('filter_peserta')) {
                    $query->where('peserta', 'ilike', "%{$request->input('filter_peserta')}%");
                }
                if ($request->has('filter_unit')) {
                    $query->where('unit', 'ilike', "%{$request->input('filter_unit')}%");
                }
            })
            ->editColumn('id_pendaftar', function($data){
                return $data->pengajuan->id_pendaftar;
            })
            ->editColumn('no_pengajuan', function($data){
                return '<a href="'.route('pengajuan.show', $data->id_pengajuan).'" class="btnShow">' . $data->pengajuan->nomorPengajuan . '</a>';
            })
            ->editColumn('pendaftar', function($data){
                return '<a href="'.route('pendaftar.show', $data->pengajuan->id_pendaftar).'" class="btnShow">' . $data->pengajuan->pendaftar->nama_lengkap . '</a>';
            })
            ->editColumn('id_magang', function($data){
                return $data->id;
            })
            ->editColumn('peserta', function($data){
                return '<a href="'.route('magang.show', $data->id).'" class="btnShow">' . $data->peserta . '</a>';
            })
            ->editColumn('unit', function($data){
                return $data->unit;
            })
            ->editColumn('waktu_magang', function($data){
                return "{$data->carbonTglAwal->format('d M Y')} s.d. {$data->carbonTglAkhir->format('d M Y')} <br>({$data->lamaHari} hari)";
            })
            ->editColumn('status', function($data){
                $return = '';
                if($data->is_accepted === null){
                    $return = '<span class="kt-badge kt-badge--brand kt-badge--inline">new</span>';
                }
                if($data->is_accepted === true){
                    $return = '<span class="kt-badge kt-badge--warning kt-badge--inline">disetujui</span>';
                }
                if($data->is_accepted === false){
                    $return = '<span class="kt-badge kt-badge--dark kt-badge--inline">ditolak</span>';
                }
                return $return;
            })
            ->editColumn('tgl_pengajuan', function($data){
                return Carbon::createFromFormat('Y-m-d H:i:s', $data->pengajuan->submitted_at)->format('d M Y');
            })
            ->rawColumns(['action', 'waktu_magang', 'status', 'no_pengajuan', 'pendaftar', 'peserta'])
            ->make(true);
    }

    /**
     * Handle file store
     *
     * @param $request
     * @param $model
     */
    private function storeFiles($request, $model)
    {
        foreach ($request->files as $key => $file) {
            if ($request->file($key)->isValid()) {
                Storage::delete($model->$key);
                $model->$key = $request->file($key)->storeAs("Magang/$key/$model->id", $file->getClientOriginalName(), 'local');
                $model->save();
            }
        }
    }
}
