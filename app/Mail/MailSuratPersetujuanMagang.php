<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;


class MailSuratPersetujuanMagang extends Mailable
{
    use Queueable, SerializesModels;
    public $data_pengajuan;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($data_pengajuan)
    {
       $this->data_pengajuan = $data_pengajuan;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        $attachments = array();
        $data_pengajuan = $this->data_pengajuan;
        foreach($data_pengajuan as $key => $magangs)
        {
            array_push($attachments, storage_path("app/" . $data_pengajuan[$key]->file_surat_persetujuan_magang) );
        }

        $email =
                $this->markdown('surat_persetujuan.mail')
                ->from(config('mail.from.address'), config('mail.from.name'))
                ->subject('Notifikasi - Surat Persetujuan Magang');

        foreach($attachments as $filePath)
        {
            $email->attach($filePath);
        }
        return $email;

    }
}
