<?php

use Illuminate\Database\Seeder;

class KontakkamiPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['group' => 'Kontakkami', 'name' => 'Kontakkami - index', 'guard_name' => 'web'],
            ['group' => 'Kontakkami', 'name' => 'Kontakkami - create', 'guard_name' => 'web'],
            ['group' => 'Kontakkami', 'name' => 'Kontakkami - show', 'guard_name' => 'web'],
            ['group' => 'Kontakkami', 'name' => 'Kontakkami - edit', 'guard_name' => 'web'],
            ['group' => 'Kontakkami', 'name' => 'Kontakkami - delete', 'guard_name' => 'web'],
        ];

        foreach($data as $val){
            \Spatie\Permission\Models\Permission::updateOrCreate($val, $val);
        }
    }
}
