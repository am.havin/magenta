<header id="header" class="clearfix static-sticky border-bottom-0">

    <div id="header-wrap">

        <div class="container clearfix">

            <div id="primary-menu-trigger"><i class="icon-reorder"></i></div>

            <!-- Logo
            ============================================= -->
            <div id="logo">
                <a href="{{ url('/') }}" class="standard-logo" data-sticky-logo="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/logo-sticky.png"><img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/logo.png" alt="Canvas Logo"></a>
                <a href="{{ url('/') }}" class="retina-logo" data-sticky-logo="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/logo-sticky@2x.png"><img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/logo@2x.png" alt="Canvas Logo"></a>
            </div><!-- #logo end -->

            @include('templates.frontend.magenta.includes.header_navigation')

        </div>

    </div>

</header><!-- #header end -->
