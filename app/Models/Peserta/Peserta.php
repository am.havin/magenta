<?php

namespace App\Peserta;

use App\Magang\Magang;
use App\Pendaftar\Pendaftar;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;

class Peserta extends Model
{
    protected $table = 'pesertas';
    protected $guarded = ['id'];

    public function getCarbonTglLahirAttribute()
    {
        return \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $this->tgl_lahir);
    }

    public static function getCanBeSubmitted($keyword = '', $idPendaftar = null)
    {
        $pendaftar = auth('pendaftar')->user();
        if($idPendaftar){
            $pendaftar = Pendaftar::findOrFail($idPendaftar);
        }
        $idPengajuans = $pendaftar->pengajuans->pluck('id');

        //yang nggak bisa dipilih adalah peserta milik sendiri yang:
        //1. lagi diajukan magang
        //2. pengajuan magangnya di terima
        $submittedMagangs = Magang::whereIn('id_pengajuan', $idPengajuans)
            ->where('is_accepted', true)
            ->orWhereNull('is_accepted')
            ->get();

        $data = Peserta::where('id_pendaftar', $pendaftar->id)
            ->whereNotIn('id', $submittedMagangs->pluck('id_peserta'))
            ->where('nama', 'ilike', "%$keyword%")
            ->get();

        return $data;
    }

    public function doDelete()
    {
        $status = true;

        DB::beginTransaction();
        try{
            //delete file CV
            Storage::delete($this->file_cv);

            //delete
            $this->delete();

            //commit query
            DB::commit();
        }catch(\Exception $e) {
            DB::rollback();
            $status = false;
        }

        return $status;
    }
}
