<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateGeneratorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('generators', function (Blueprint $table) {
            $table->increments('id');
            $table->string('yaml_filename')->unique();
            $table->string('route_url')->nullable();
            $table->text('route_text')->nullable();
            $table->string('file_migration')->nullable();
            $table->string('file_permission_seeder')->nullable();
            $table->string('file_model')->nullable();
            $table->string('file_request')->nullable();
            $table->string('file_form_request')->nullable();
            $table->string('file_controller')->nullable();
            $table->string('file_view_index')->nullable();
            $table->string('file_view_index_datatable')->nullable();
            $table->string('file_view_create')->nullable();
            $table->string('file_view_edit')->nullable();
            $table->string('file_view_show')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('generators');
    }
}
