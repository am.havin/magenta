<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PendaftarRequest extends FormRequest
{
    public $rules = [
        'nama_lengkap' => ['required', 'string', 'max:255'],
        'email' => ['required', 'string', 'email', 'max:255', 'unique:pendaftars'],
        'password' => ['required', 'string', 'min:8'],
        'no_hp' => ['required', 'numeric', 'min:10', 'unique:pendaftars'],
        'id_sekolah' => ['required'],
        'pic' => 'max:512|mimes:jpg,png,jpeg',
    ];

    public function getCreateRules()
    {
        return $this->rules;
    }

    public function getEditRules()
    {
        //different example rule
        $this->rules['email'] = [
            'string', 'email',
            'max:255',
            'unique:pendaftars',
            Rule::unique('pendaftars')->ignore(array_values(request()->route()->originalParameters())[0]),
        ];
        $this->rules['no_hp'] = [
            'required',
            'numeric',
            'min:10',
            Rule::unique('pendaftars')->ignore(array_values(request()->route()->originalParameters())[0]),
        ];
        $this->rules['password'] = ($this->input('password')) ? ['string', 'min:8'] : '';
        return $this->rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch (request()->route()->getActionMethod()) {
            case 'index':
                return auth('web')->user()->hasAnyPermission(['Pendaftar - index']);
                break;
            case 'create':
                return auth('web')->user()->hasAnyPermission(['Pendaftar - create']);
                break;
            case 'store':
                return auth('web')->user()->hasAnyPermission(['Pendaftar - create']);
                break;
            case 'show':
                return auth('web')->user()->hasAnyPermission(['Pendaftar - show']);
                break;
            case 'edit':
                return auth('web')->user()->hasAnyPermission(['Pendaftar - edit']);
                break;
            case 'update':
                return auth('web')->user()->hasAnyPermission(['Pendaftar - edit']);
                break;
            case 'destroy':
                return auth('web')->user()->hasAnyPermission(['Pendaftar - delete']);
                break;
            case 'datatable':
                return auth('web')->user()->hasAnyPermission(['Pendaftar - index']);
                break;
            default:
                return false;
                break;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $controllerMethod = request()->route()->getActionMethod();
        if ($controllerMethod == 'store') {
            return $this->getCreateRules();
        }
        if ($controllerMethod == 'update') {
            return $this->getEditRules();
        }
        return [];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //'title.required' => 'A title is required',
            //'body.required'  => 'A message is required',
        ];
    }
}
