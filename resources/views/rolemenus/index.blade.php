@extends((Request::ajax()) ? 'templates.layout-blank' : 'templates.'.config('genelator.template').'.layout')


@section('subheaderToolbar')
@endsection

@section('vendorPluginForCustomPagesStyles')
    @include('templates.metronic.demo5.includes.datatable_css')
@endsection

@section('content')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-list-2"></i>
            </span>
                <h3 class="kt-portlet__head-title">
                    Akses Menu
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">

            <form action="{{ url('rolemenus') }}" id="form" method="post" class="kt-form" autocomplete="off">
                @csrf

                <!--begin: Table -->
                <table class="table table-bordered- table-striped- table-hover table-checkable" id="menuTable">
                    <thead>
                    <tr>
                        <th width="">Menu</th>
                        @foreach($roles as $role)
                            <th width="2rem">{{ ucfirst($role->name) }}</th>
                        @endforeach
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($menus as $menu)
                        <tr>
                            <td>{{ array_keys($menu)[0] }}</td>
                            @foreach($roles as $role)
                                <td class="text-center">
                                    <label class="kt-checkbox kt-checkbox--solid">
                                        <input type="checkbox" name="inputs[{{ array_values($menu)[0][2] }}][]" value="{{ $role->id }}"
                                               @if(\App\RolemenuHelper::isChecked($rolesMenus, array_values($menu)[0][2], $role->id)) checked @endif>
                                        <span style="top: -3px;"></span>
                                    </label>
                                </td>
                            @endforeach
                        </tr>
                        @php trRecursive($menu, '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', $roles, $rolesMenus) @endphp
                    @endforeach
                    </tbody>
                </table>
                <!--end: Table -->

                <div class="row">
                    <div class="col-lg-12 text-right">
                        <br>
                        <button type="submit" class="btn btn-warning btn-elevate btn-widest">
                            <i class="la la-check"></i> Simpan
                        </button>
                    </div>
                </div>
            </form>

        </div>
    </div>
@endsection

@section('vendorPluginForCustomPagesScripts')
    @include('templates.metronic.demo5.includes.datatable_js')
@endsection

@section('pageScripts')
    <script>
        jQuery(document).ready(function () {
            // SUBMIT EVENT
            $('#rolemenuSubmit').click(function () {
                let data = table.$('input').serializeArray();
                let url = $(this).attr('data-url');
                let csrf = $(this).attr('data-csrf');
                doSubmit(data, url, csrf);
            });
        });
    </script>
@endsection




<?php function trRecursive($menu, $nbsps, $roles, $rolesMenus){ ?>

@if(isset($menu['child']))
    <?php foreach($menu['child'] as $menu): ?>
        <tr>
            <td>{!! $nbsps !!} - {{ array_keys($menu)[0] }}</td>
            <?php foreach($roles as $role): ?>
                <td class="text-center">
                    <label class="kt-checkbox kt-checkbox--solid">
                        <input type="checkbox" name="inputs[{{ array_values($menu)[0][2] }}][]" value="{{ $role->id }}"
                               @if(\App\RolemenuHelper::isChecked($rolesMenus, array_values($menu)[0][2], $role->id)) checked @endif>
                        <span style="top: -3px;"></span>
                    </label>
                </td>
            <?php endforeach; ?>
        </tr>
        @php trRecursive($menu, $nbsps.'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;', $roles, $rolesMenus) @endphp
    <?php endforeach; ?>
@endif

<?php } ?>
