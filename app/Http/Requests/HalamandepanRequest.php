<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class HalamandepanRequest extends FormRequest
{
    public $rules = [
        'slogan' => '',
        'introduksi' => '',
        'teks_gambar' => '',
        'step1_judul' => '',
        'step1_teks' => '',
        'step2_judul' => '',
        'step2_teks' => '',
        'step3_judul' => '',
        'step3_teks' => '',
        'gambar_depan' => 'required|max:512|mimes:jpg,png,jpeg',
    ];

    public function getCreateRules()
    {
        return $this->rules;
    }

    public function getEditRules()
    {
        //different example rule
        $this->rules['gambar_depan'] = 'max:10240|mimes:jpg,png,jpeg';
        return $this->rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch (request()->route()->getActionMethod()) {
            case 'index':
                return auth('web')->user()->hasAnyPermission(['Halamandepan - index']);
                break;
            case 'create':
                return auth('web')->user()->hasAnyPermission(['Halamandepan - create']);
                break;
            case 'store':
                return auth('web')->user()->hasAnyPermission(['Halamandepan - create']);
                break;
            case 'show':
                return auth('web')->user()->hasAnyPermission(['Halamandepan - show']);
                break;
            case 'edit':
                return auth('web')->user()->hasAnyPermission(['Halamandepan - edit']);
                break;
            case 'update':
                return auth('web')->user()->hasAnyPermission(['Halamandepan - edit']);
                break;
            case 'destroy':
                return auth('web')->user()->hasAnyPermission(['Halamandepan - delete']);
                break;
            case 'datatable':
                return auth('web')->user()->hasAnyPermission(['Halamandepan - index']);
                break;
            default:
                return false;
                break;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $controllerMethod = request()->route()->getActionMethod();
        if ($controllerMethod == 'store') {
            return $this->getCreateRules();
        }
        if ($controllerMethod == 'update') {
            return $this->getEditRules();
        }
        return [];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //'title.required' => 'A title is required',
            //'body.required'  => 'A message is required',
        ];
    }
}
