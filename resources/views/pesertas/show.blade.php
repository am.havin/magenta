@extends((Request::ajax()) ? 'templates.layout-blank' : 'templates.'.config('genelator.template').'.layout')

@section('subheaderToolbar')
    <a href="#" class="btn invisible">
    </a>
@endsection

@section('content')
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title">{{ $title }}</h5>
                <button id="closeBtn" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="kt-portlet">
                            <div class="kt-portlet__body">
                                <img src="{{ route('peserta.foto', $model->id) }}?rand={{ rand(10,1000) }}" style="width: 150px; height: auto; top:10px; right: 36px; position: absolute; padding: 8px; background-color: #fff;">
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <th width="150px">Nama</th>
                                        <td>{{ $model->nama }}</td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td>{{ $model->email }}</td>
                                    </tr>
                                    <tr>
                                        <th>No. KTP</th>
                                        <td>{{ $model->no_ktp }}</td>
                                    </tr>
                                    <tr>
                                        <th>NIM / NIS</th>
                                        <td>{{ $model->nim_nis }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ ($model->is_universitas) ? 'Universitas' : 'SMA/SMK' }}</th>
                                        <td>{{ $model->sekolah }}</td>
                                    </tr>
                                    <tr>
                                        <th>Program Studi</th>
                                        <td>{{ $model->programstudi }}</td>
                                    </tr>
                                    <tr>
                                        <th>CV</th>
                                        <td>
                                            @if($model->file_cv)
                                                <a href="{{ url("filedownload?p=$model->file_cv") }}">
                                                    <i class="la la-file"></i> {{ basename($model->file_cv) }}
                                                </a>
                                            @else
                                                -
                                            @endif
                                        </td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!--end::Portlet-->
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection


@section('pageScripts')
@endsection


{{-- For Modal --}}
@if(Request::ajax())
    @yield('content')
    @yield('pageScripts')
@endif