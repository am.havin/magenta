<?php

namespace App\Http\Controllers\Select2;

use App\Alokasi\Alokasi;
use App\Magang\Magang;
use App\Peserta\Peserta;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;


class MagangController extends Controller
{
    public function unitkerja_magang_yg_diterima(Request $request)
    {
        $return = [];
        $data = Magang::select('unit')->where('is_accepted', true)->where('unit', 'ilike', "%{$request->q}%")->distinct()->get();
        foreach($data as $magang){
            $return[] = ['id' => $magang->unit, 'text' => $magang->unit ];
        }

        return response()->json(['results' => $return]);
    }
}
