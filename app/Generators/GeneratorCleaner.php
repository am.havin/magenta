<?php

namespace App\Generators;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Spatie\Permission\Models\Permission;
use Symfony\Component\Yaml\Yaml;

class GeneratorCleaner
{
    private $yamlFileName;
    private $yamlData;

    public function __construct($yamlFileName)
    {
        $this->yamlFileName = $yamlFileName;
        $yamlFileLoc    = app_path("Yamls/generators/$yamlFileName.yaml");
        $this->yamlData = Yaml::parse(file_get_contents($yamlFileLoc));
    }

    public function clean()
    {
        $generator = \App\Models\Generator::where('yaml_filename', 'ilike', $this->yamlFileName)->first();
        if(!$generator){
            return "> unable to destroy! \n> this yaml hasn't been generated before";
        }

        $this->deleteFile($generator->file_migration, 'database_path');
        $this->deleteFile($generator->file_permission_seeder, 'database_path');
        $this->deleteFile($generator->file_model, 'app_path');
        $this->deleteFile($generator->file_request, 'app_path');
        $this->deleteFile($generator->file_form_request, 'app_path');
        $this->deleteFile($generator->file_controller, 'app_path');
        $this->deleteFile($generator->file_view_index, 'resource_path');
        $this->deleteFile($generator->file_view_create, 'resource_path');
        $this->deleteFile($generator->file_view_edit, 'resource_path');
        $this->deleteFile($generator->file_view_show, 'resource_path');
        $this->deleteRoute($generator->route_text);
        $this->deleteTable($this->yamlData['table'], $generator->file_migration);
        $this->deletePermission($this->yamlData['model']);
        $generator->delete();

        Artisan::call('dump-autoload');
        return '> ' . $this->yamlFileName . ".yaml successfully destroyed \n> all files & routes have been cleaned \n> (composer dump-autoload is also executed at the end of the process)";
    }

    private function deleteFile($filepath, $systemPathNameFunction)
    {
        if($filepath){
            try{
                unlink($systemPathNameFunction($filepath));
                return true;
            }catch (\Exception $e){
                return false;
            }
        }
    }

    private function deleteRoute($routeText)
    {
        //ambil text nya
        $string = file_get_contents(base_path('routes/web.php'));

        //string replace
        //$routeText = str_replace("\n" ,"\r\n", $routeText); //pas di save database kadang yang kesave hanya \n, makanya harus di tambah jadi \r\n
        $newString = str_replace($routeText,'', $string);

        //write lagi
        file_put_contents(base_path('routes/web.php'), $newString);
    }

    private function deleteTable($tableName, $migrationFilename)
    {
        $strReplace = str_replace(['migrations/', '.php'], ['', ''], $migrationFilename);
        $migration = DB::table('migrations')
            ->where('migration', 'ilike', $strReplace)
            ->first();
        if($migration){
            Schema::dropIfExists($tableName);
            DB::table('migrations')->delete($migration->id);
        }
    }

    private function deletePermission($modelData)
    {
        $permissionIds = DB::table('permissions')->where('group', $modelData['class_name'])->pluck('id');
        DB::table('role_has_permissions')->whereIn('permission_id', $permissionIds)->delete();
        DB::table('permissions')->whereIn('id', $permissionIds)->delete();
        app()->make(\Spatie\Permission\PermissionRegistrar::class)->forgetCachedPermissions();
    }
}
