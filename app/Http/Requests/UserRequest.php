<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class UserRequest extends FormRequest
{
    public $rules = [
        'username' => 'required|unique:users,username',
        'pegawai_id' => 'required|min:0',
        'pegawai_nama' => 'required',
        'roles' => 'required',
    ];

    public function getCreateRules()
    {
        return $this->rules;
    }

    public function getEditRules()
    {
        $this->rules['username'] = [
            Rule::unique('users')->ignore(array_values(request()->route()->originalParameters())[0]),
        ];
        return  $this->rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch (request()->route()->getActionMethod()) {
            case 'index':
                return auth('web')->user()->hasAnyPermission(['user - index']);
                break;
            case 'create':
                return auth('web')->user()->hasAnyPermission(['user - create']);
                break;
            case 'store':
                return auth('web')->user()->hasAnyPermission(['user - create']);
                break;
            case 'show':
                return auth('web')->user()->hasAnyPermission(['user - show']);
                break;
            case 'edit':
                return auth('web')->user()->hasAnyPermission(['user - edit']);
                break;
            case 'update':
                return auth('web')->user()->hasAnyPermission(['user - edit']);
                break;
            case 'destroy':
                return auth('web')->user()->hasAnyPermission(['user - delete']);
                break;
            case 'datatable':
                return auth('web')->user()->hasAnyPermission(['user - index']);
                break;
            default:
                return false;
                break;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $controllerMethod = request()->route()->getActionMethod();
        if ($controllerMethod == 'store') {
            return $this->getCreateRules();
        }
        if ($controllerMethod == 'update') {
            return $this->getEditRules();
        }
        return [];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //'title.required' => 'A title is required',
            //'body.required'  => 'A message is required',
        ];
    }
}
