@extends('templates.frontend.magenta.layout_nonhome')


@section('pageCss')
    <link rel="stylesheet" href="{{url('/')}}/templates/frontend/magenta/css/components/bs-select.css" type="text/css" />
@endsection

@section('content')
<div class="container">

    @if($errors->isNotEmpty())
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="style-msg2 errormsg">
                    <div class="msgtitle">Terjadi kesalahan pengisian form:</div>
                    <div class="sb-msg">
                        <ul>
                            @foreach($errors->all() as $message)
                                <li>{{ $message }}</li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    @endif

    <div class="row justify-content-center">
        <div class="col-md-8">
                <div class="card-body">
                    <form method="POST" action="{{ route('register') }}" autocomplete="off">
                        @csrf

                        <h3>Pembuatan user baru</h3>

                        <div class="col_full">
                            <label for="email"><span class="text-danger">*</span> Nama Lengkap</label>
                            <input id="nama_lengkap" type="text" class="form-control @error('nama_lengkap') is-invalid @enderror" name="nama_lengkap" value="{{ old('nama_lengkap') }}" required autocomplete="nama_lengkap" autofocus>

                            @error('nama_lengkap')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col_full">
                            <label for="email"><span class="text-danger">*</span> Alamat Email</label>
                            <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email">

                            @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col_full">
                            <label for="email"><span class="text-danger">*</span> No Handphone</label>
                            <input id="no_hp" type="text" class="form-control @error('no_hp') is-invalid @enderror" name="no_hp" value="{{ old('no_hp') }}" required autocomplete="no_hp">

                            @error('no_hp')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        {{--CHECKBOX--}}
                        <div class="col_full">
                            <label for="" style="display: block;"><span class="text-danger">*</span> Tingkat Pendidikan</label>
                            <div class="form-check">
                                <input class="form-check-input is_universitas_cb" type="radio" name="is_universitas" id="is_universitas_true" value="1" required>
                                <label class="form-check-label" for="is_universitas_true">
                                    Universitas
                                </label>
                            </div>
                            <div class="form-check">
                                <input class="form-check-input is_universitas_cb" type="radio" name="is_universitas" id="is_universitas_false" value="0">
                                <label class="form-check-label" for="is_universitas_false">
                                    SMA/SMK
                                </label>
                            </div>
                        </div>

                        {{--SEKOLAH--}}
                        <div class="col_full" id="rowSekolah" style="display: none;">
                            <label for="id_sekolah" style="display: block;"><span class="text-danger">*</span> <span id="labelSekolah">Universitas / Sekolah</span></label>
                            <select name="id_sekolah" id="id_sekolah" class="form-control form-control-lg" tabindex="-98" required></select>
                            <small id="" class="form-text text-muted">Jika pilihan tidak tersedia, maka pilih "lain-lain" lalu isikan sesuai dengan yang diinginkan</small>
                            <br>
                            <input style="@if(!old('sekolah'))
                                    display: none; @endif"
                                   id="sekolah" type="text"
                                   class="form-control @error('sekolah') is-invalid @enderror"
                                   name="sekolah" value="{{ old('sekolah') }}"
                                   @if(old('sekolah')) required @endif
                                   placeholder="isi nama sekolah/universitas">

                            @error('id_sekolah')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                            @error('sekolah')
                            <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>


                        <div class="col_full">
                            <label for="password" class=""><span class="text-danger">*</span> {{ __('Password') }}</label>
                            <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">

                            @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                            @enderror
                        </div>

                        <div class="col_full">
                            <label for="password-confirm" class=""><span class="text-danger">*</span> Konfirmasi Password</label>
                            <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                        </div>

                        <br>
                        <div class="col_full">
                            <em>
                                PS: Setelah tombol "Register" dibawah di-klik, sistem akan mengirim email untuk anda melakukan verifikasi.
                                Harap menunggu &plusmn; 5 menit karena ada jeda waktu dalam proses pengiriman email tersebut ke alamat email anda.
                            </em>
                        </div>
                        <div class="col_full nobottommargin">
                            <button type="submit" class="button button-3d button-rounded button-lime button-light" id="login-form-submit" name="login-form-submit" value="login"><i class="icon-check"></i> Register</button>
                        </div>
                    </form>
                </div>
        </div>
    </div>
</div>
@endsection


@section('pageScript')
    <!-- Bootstrap Select Plugin -->
    <script src="{{ url('/') }}/templates/frontend/magenta/js/components/bs-select.js"></script>

    <!-- Select Splitter Plugin -->
    <script src="{{ url('/') }}/templates/frontend/magenta/js/components/selectsplitter.js"></script>

    <script>
        $(document).ready(function(){
            //VARIABLES
            var isUniversitas = $(this).val();
            var select2Sekolah;

            initIdSekolah();

            // TOGGLE
            $('.is_universitas_cb').change(function(){
                //hide dulu setiap trigger
                $('#rowSekolah').hide('fast');

                //set values
                isUniversitas = $(this).val();
                let labelSekolah = (isUniversitas == 1) ? 'Nama Universitas' : 'Nama SMA/SMK';

                //execute
                $('#labelSekolah').text(labelSekolah);
                $('#rowSekolah').show('fast');
                destroySelect2Sekolah();
                initIdSekolah();
            });

            //DESTROY SELECT2 SEKOLAH
            function destroySelect2Sekolah(){
                select2Sekolah.val([]);
                select2Sekolah.select2('destroy');
                hideTextSekolah();
            }

            // SHOW TEXT SEKOLAH
            function showTextSekolah(){
                $('#sekolah').show('fast').attr('placeholder', $('#labelSekolah').text());
                $('#sekolah').prop('required', true);
            }
            // HIDE TEXT SEKOLAH
            function hideTextSekolah(){
                $('#sekolah').hide('fast');
                $('#sekolah').val('').text('').prop('required', false);
            }


            // INIT SELECT2 ID_SEKOLAH
            function initIdSekolah(){
                select2Sekolah = $('#id_sekolah').select2({
                    placeholder: 'Pilih',
                    allowClear: false,
                    ajax: {
                        delay: 1000,
                        url: '{{ url('pub_select2/sekolah_with_lain_lain') }}' + '/' + isUniversitas,
                        dataType: 'json',
                    },
                    cache: true,
                }).on('select2:select', function (event) {
                    let val = $(this).val();
                    if(val == 0){ // = 0 itu lain-lain
                        showTextSekolah();
                    }else{
                        hideTextSekolah();
                    }
                }).on('select2:unselect', function (event) {
                    hideTextSekolah();
                });
            }

            //SELECT2
            // $('#id_sekolah').select2();
            // $('#id_sekolah').change(function(){
            //     let val = $(this).val();
            //
            //     if(val > 0 || val === ''){
            //         $('#sekolah').val('').hide('fast').prop('required', false);
            //     }
            //     else{
            //         $('#sekolah').show('fast').prop('required', true);
            //     }
            // });
        });
    </script>
@endsection
