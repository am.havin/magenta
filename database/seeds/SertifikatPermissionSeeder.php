<?php

use Illuminate\Database\Seeder;

class SertifikatPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['group' => 'Sertifikat', 'name' => 'Sertifikat - index', 'guard_name' => 'web'],
            ['group' => 'Sertifikat', 'name' => 'Sertifikat - edit', 'guard_name' => 'web'],
        ];

        foreach($data as $val){
            \Spatie\Permission\Models\Permission::updateOrCreate($val, $val);
        }
    }
}
