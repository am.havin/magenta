<?php

use Illuminate\Database\Seeder;

class PenempatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'konten'     => '<p>Berikut adalah informasi pembagian penempatan seusai dengan jurusan peserta magang. Harap memilih unit penempatan yang sesuai dengan jurusannya, jika tidak admin akan merubah unit penempatan yang telah anda ajukan atau bahkan menolak pengajuan magang anda.</p><table class="table table-bordered"><tbody><tr><td><b>UNIT PENEMPATAN</b></td><td><b>JURUSAN YANG DITERIMA</b></td></tr><tr><td>Bidang Teknologi Informasi</td><td><p>SMA / SMK<br><span style="background-color: transparent;">- IPA<br></span><span style="background-color: transparent;">- Rekayasa Perangkat Lunak</span></p><p>Universitas<br>- Teknik Informatika<br>- Sistem Informasi<br>- Ilmu Komputer<br>- Teknik Informasi<br>- Sistem Informatika <br></p></td></tr></tbody></table><p><br></p>',
            ],
        ];

        foreach($data as $val){
            \App\Penempatan\Penempatan::updateOrCreate($val, $val);
        }
    }
}
