<?php

namespace App\Http\Requests;

use App\Alokasi\Alokasi;
use App\Helper;
use App\Magang\Magang;
use App\Peserta\Peserta;
use App\Rules\MaxDayDuration;
use App\Rules\MinDayDuration;
use App\Rules\TidakBolehMotongTglYgDisabled;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class MagangRequest extends FormRequest
{
    public $rules = [
        'id_peserta' => 'required',
        'id_unit' => 'required',
        'tgl_awal' => 'required|date_format:Y-m-d|after:yesterday',
        'tgl_akhir' => 'required|date_format:Y-m-d|after:yesterday',
    ];

    public function getEditRules()
    {
        $idMagang = array_values(request()->route()->originalParameters())[0];
        $magang = Magang::with('pengajuan')->findOrFail($idMagang);
        $rules =  $this->rules;

        //peserta
        $pesertas = Peserta::getCanBeSubmitted('', $magang->pengajuan->id_pendaftar);
        $rules['id_peserta'] = [
            Rule::in($pesertas->pluck('id'))
        ];

        //unit
        $alokasis = Alokasi::where('kuota', '>', 0)->get();
        $rules['id_unit'] = [
            'required',
            Rule::in($alokasis->pluck('id_unit'))
        ];

        //tgl_awal
        $tglAwal = Carbon::today();
        $unAvailableDates = Helper::getUnavailableDates($this->id_unit, $tglAwal->month, $tglAwal->year, $idMagang, false);
        $rules['tgl_awal'] = [
            'required',
            'date_format:Y-m-d',
            'after:yesterday',
            'before:+1 year',
            Rule::notIn($unAvailableDates),
        ];

        //tgl_akhir
        $tglAkhir = Carbon::today();
        $unAvailableDates = Helper::getUnavailableDates($this->id_unit, $tglAkhir->month, $tglAkhir->year, $idMagang, false);
        $rules['tgl_akhir'] = [
            'required',
            'date_format:Y-m-d',
            'after_or_equal:tgl_awal',
            'before:+1 year',
            Rule::notIn($unAvailableDates),
            new MinDayDuration(14),
            new MaxDayDuration(60),
        ];

        return $rules;
    }

    public function getUpdateRules()
    {
        $idMagang = array_values(request()->route()->originalParameters())[0];
        $magang = Magang::with('pengajuan')->findOrFail($idMagang);
        $rules =  $this->rules;

        //peserta
        $pesertas = Peserta::getCanBeSubmitted('', $magang->pengajuan->id_pendaftar);
        $rules['id_peserta'] = [
            Rule::in($pesertas->pluck('id'))
        ];

        //unit
        $alokasis = Alokasi::where('kuota', '>', 0)->get();
        $rules['id_unit'] = [
            'required',
            Rule::in($alokasis->pluck('id_unit'))
        ];

        //tgl_awal
        $tglAwal = Carbon::today();
        $unAvailableDates = Helper::getUnavailableDates($this->id_unit, $tglAwal->month, $tglAwal->year, $idMagang, false);
        $rules['tgl_awal'] = [
            'required',
            'date_format:Y-m-d',
            'after:yesterday',
            'before:+1 year',
            Rule::notIn($unAvailableDates),
        ];

        //tgl_akhir
        $tglAkhir = Carbon::today();
        $unAvailableDates = Helper::getUnavailableDates($this->id_unit, $tglAkhir->month, $tglAkhir->year, $idMagang, false);
        $rules['tgl_akhir'] = [
            'required',
            'date_format:Y-m-d',
            'after_or_equal:tgl_awal',
            'before:+1 year',
            Rule::notIn($unAvailableDates),
            new MinDayDuration(14),
            new MaxDayDuration(60),
            new TidakBolehMotongTglYgDisabled($idMagang, false),
        ];

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch (request()->route()->getActionMethod()) {
            case 'index':
                return auth('web')->user()->hasAnyPermission(['Magang - index']);
                break;
            case 'create':
                return auth('web')->user()->hasAnyPermission(['Magang - create']);
                break;
            case 'store':
                return auth('web')->user()->hasAnyPermission(['Magang - create']);
                break;
            case 'show':
                return auth('web')->user()->hasAnyPermission(['Magang - show']);
                break;
            case 'edit':
                return auth('web')->user()->hasAnyPermission(['Magang - edit']);
                break;
            case 'update':
                return auth('web')->user()->hasAnyPermission(['Magang - edit']);
                break;
            case 'destroy':
                return auth('web')->user()->hasAnyPermission(['Magang - delete']);
                break;
            case 'datatable':
                return auth('web')->user()->hasAnyPermission(['Magang - index']);
                break;
            default:
                return false;
                break;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $controllerMethod = request()->route()->getActionMethod();
        if ($controllerMethod == 'update') {
            return $this->getUpdateRules();
        }
        return [];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //'title.required' => 'A title is required',
            //'body.required'  => 'A message is required',
        ];
    }
}
