<?php

namespace App\Sertifikat;

use App\Magang\Magang;
use Carbon\Carbon;

class Sertifikat
{
    public static function getBadgeQty()
    {
        $in10Days = Carbon::now()->addHours(240);
        return Magang::query()->with('pengajuan.pendaftar')
            ->whereHas('pengajuan', function($q){
                $q->where('is_draft', false)->where('is_submitted', true)->where('is_deleted', false);
            })
            ->where('is_accepted', true)
            ->whereDate('tgl_akhir', '<=', $in10Days)
            ->whereNull('nilai')
            ->count();
    }
}
