@if(auth()->guard('pendaftar')->user() && !auth()->guard('pendaftar')->user()->hasVerifiedEmail())
    <div class="style-msg alertmsg">
        <div class="sb-msg text-center">
            <i class="icon-warning-sign"></i><strong>Warning!</strong> Email anda belum terverifikasi!
            Silahkan Cek email anda untuk melakukan verifikasi atau <a href="{{ route('verification.notice') }}">klik disini </a> jika ada kendala
        </div>
    </div>
@endif