@extends('templates.layout-blank')


@section('content')
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-body">
            <div class="modal-content">
                <form method="POST" action="{{ route('pendaftaran.step1.magang.update', [$pengajuan->id, $magang->id]) }}" id="editForm" autocomplete="off" enctype="multipart/form-data">
                    @csrf
                    @method('PUT')

                    <div class="modal-header">
                        <h5 class="modal-title" id="">{{ $title }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        {{--PESERTA & UNIT KERJA--}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="id_peserta" style="display: block;"><span class="text-danger">*</span> Peserta</label>
                                    <input type="text" class="form-control form-control-sm" value="{{ $magang->peserta }}" disabled>
                                    <small id="" class="form-text text-muted"></small>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="id_unit" style="display: block;"><span class="text-danger">*</span> Unit Kerja Tujuan</label>
                                    <select name="id_unit" id="id_unit" class="form-control-sm" tabindex="-98" required>
                                        <option value="{{ $magang->id_unit }}">{{ $magang->unit }}</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        {{--AWAL & AKHIR--}}
                        <div class="input-daterange">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="tgl_awal" style="display: block;"><span class="text-danger">*</span> Tanggal Awal Magang</label>
                                        <input id="tgl_awal" type="text"
                                               class="form-control"
                                               name="tgl_awal" value="{{ ($magang->carbon_tgl_awal) ? $magang->carbon_tgl_awal->format('Y-m-d') : '' }}" required placeholder="Pilih unit kerja terlebih dahulu">
                                        <small id="" class="form-text text-muted">
                                            Tanggal yang tidak dapat dipilih menandakan pada tanggal tersebut kuota untuk
                                            unit yang dituju sudah terpenuhi.<br>
                                        </small>
                                    </div>
                                </div>
                                <div class="col-md-6" style="display: block;">
                                    <div class="form-group">
                                        <label for="tgl_akhir" style="display: block;"><span class="text-danger">*</span> Tanggal Akhir Magang</label>
                                        <input id="tgl_akhir" type="text"
                                               class="form-control"
                                               name="tgl_akhir" value="{{ ($magang->carbon_tgl_akhir) ? $magang->carbon_tgl_akhir->format('Y-m-d') : '' }}" required placeholder="Pilih unit kerja terlebih dahulu">
                                        <small id="" class="form-text text-muted">
                                            Waktu magang minimal adalah 14 hari dan maksimal 60 hari
                                        </small>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <hr>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="alert alert-warning">
                                    <i class="icon-hand-up"></i><strong>Penting!</strong>
                                    Tanggal awal & tanggal akhir yang anda pilih diatas hanya akan disimpan selama 1x24 jam,
                                    jika anda belum menyelesaikan pendaftaran dalam kurun waktu tersebut, maka tanggal yang sudah anda pilih akan <i>expired</i>
                                    dan anda harus melakukan pemilihan tanggal kembali.
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer">
                            <button type="submit"
                                    class="button button-3d button-small button-rounded button-lime button-light"><i
                                        class="icon-check"></i> Simpan
                            </button>
                        </div>
                </form>
            </div>
        </div>
    </div>
@endsection


@section('pageScripts')
    {!! $validator->selector('#editForm') !!}
    <script>
        $.fn.datepicker.dates.id = {
            days: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"],
            daysShort: ["Mgu", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab"],
            daysMin: ["Mg", "Sn", "Sl", "Ra", "Ka", "Ju", "Sa"],
            months: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
            monthsShort: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Ags", "Sep", "Okt", "Nov", "Des"],
            today: "Hari Ini",
            clear: "Kosongkan"
        }
    </script>

    <script>
        jQuery(document).ready(function () {
            if (!(typeof datatable === "undefined")) {
                //pass instance datatable (optional) agar di-reload ketika submit formnya sukses
                initAjaxForm('#editForm', datatable); //datatable dapat dari index.blade.php
            }

            $('.input-daterange').datepicker({
                language: 'id',
                updateViewDate: false,
                format: 'yyyy-mm-dd',
                startDate: d = new Date(),
                endDate: new Date( (d.getFullYear()+1), d.getMonth(), d.getDate() ),
            });
            updateDisabledDates(
                '#tgl_awal, #tgl_akhir',
                $('#id_unit').val(),
                moment().format('MM'),
                moment().format('YYYY'),
                {{ $magang->id }}
            );

            //SELECT2 - UNIT KERJA
            $('#id_unit').select2({
                placeholder: 'Pilih',
                allowClear: false,
                ajax: {
                    url: '{{ url('front_select2/alokasi_unitkerjas') }}',
                    dataType: 'json',
                    delay: 3000
                },
            }).on('select2:select', function (event) {
                let unit = $(this).val();
                $('#tgl_awal').prop('disabled', false).text('').val('').attr('placeholder', 'Tanggal awal');
                $('#tgl_akhir').prop('disabled', false).text('').val('').attr('placeholder', 'Tanggal akhir');
                updateDisabledDates('#tgl_awal, #tgl_akhir', unit, moment().format('MM'), moment().format('YYYY'), {{ $magang->id }})
            });

            //SELECT2 - PESERTA
            $('#id_peserta').select2({
                placeholder: 'Pilih',
                allowClear: false,
                ajax: {
                    url: '{{ url('front_select2/ownpesertas') }}',
                    dataType: 'json',
                    delay: 3000
                },
            });
        });

        function updateDisabledDates(ele, unit, month, year, exceptId = null){
            let url = '{{ url('pendaftaran/step1/unavailable_dates') }}' + '/' + unit + '/' + month + '/' + year;
            url += (exceptId) ? '/' + exceptId : '';
            $.ajax({
                url: url,
                type: 'GET',
                contentType: false,
                processData: false,
                beforeSend: function() {
                    $('#editForm').parent().block({
                        message: '<small class="text-muted">Loading...</small>',
                        css: { border: 'none', backgroundColor: 'transparent' }
                    });
                },
                success: function (data) {
                    $(ele).datepicker('setDatesDisabled', data);
                    $('#editForm').parent().unblock();
                },
                error: function (error) {
                },
            });
        }
    </script>
@endsection


{{-- For Modal --}}
@if(Request::ajax())
    @yield('content')
    @yield('pageScripts')
@endif