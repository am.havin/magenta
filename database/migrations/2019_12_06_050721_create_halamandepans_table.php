<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHalamandepansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('halamandepans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('slogan');
            $table->string('introduksi');
            $table->string('teks_gambar');
            $table->string('step1_judul');
            $table->text('step1_teks');
            $table->string('step2_judul');
            $table->text('step2_teks');
            $table->string('step3_judul');
            $table->text('step3_teks');
            $table->string('gambar_depan')->nullable();

            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('halamandepans');
    }
}
