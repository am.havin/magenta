@extends((Request::ajax()) ? 'templates.layout-blank' : 'templates.'.config('genelator.template').'.layout')

@section('subheaderToolbar')
    <a href="#" class="btn invisible">
    </a>
@endsection

@section('content')
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title">{{ $title }}</h5>
                <button id="closeBtn" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                @if($errors->isNotEmpty())
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-warning" role="alert">
                                <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                <div class="alert-text">
                                    <ul>
                                        @foreach($errors->all() as $message)
                                            <li>{{ $message }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-lg-12">
                        <div class="kt-portlet">

                            <!--begin::Form-->
                            <form action="{{ url('pengumuman/') }}" id="createForm" method="post" class="kt-form"
                                  autocomplete="off" enctype="multipart/form-data">
                                @csrf

                                <div class="kt-portlet__body">
                                    <div class="form-group row">
                                        <div class="col-lg-12">
                                            <label> Image Pengumuman:</label>
                                            <input type="file" class="form-control-file" name="gambar" id="gambar">
                                            <span class="form-text text-muted">File .jpg .jpeg .png dengan ukuran maksimal 2MB</span>
                                        </div>
                                        <div class="w-100"></div>
                                        <br>
                                        <div class="col-lg-12">
                                            <label class="form-control-label"> Konten:</label>
                                            <div id="quillKonten" style="height: 200px"></div>
                                            <textarea class="form-control" style="display: none;" name="konten" id="konten" ></textarea>
                                            <span class="form-text text-muted">Jika pengumuman hanya ingin berupa gambar, konten ini tidak perlu diisi.</span>
                                        </div>
                                        <div class="w-100"></div>
                                        <br>
                                        <div class="col-lg-6">
                                            <label class="form-control-label">* Status:</label>
                                            <select name="is_active" id="is_active" class="form-control" required>
                                                <option value='0'>Tidak aktif</option>
                                                <option value='1'>Aktif</option>

                                            </select>
                                            <span class="form-text text-muted"></span>
                                        </div>

                                    </div>
                                </div>
                                <div class="kt-portlet__foot">
                                    <div class="kt-form__actions">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <button type="submit" class="btn btn-warning">Submit</button>
                                                {{-- <a href="{{ url('pengumuman/') }}" class="btn btn btn-secondary">Cancel</a> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!--end::Form-->
                        </div>
                        <!--end::Portlet-->
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('pageScripts')
    {!! $validator->selector('#createForm') !!}
    <script>
        jQuery(document).ready(function () {
            initSelect2();
            initDatepicker();
            if (!(typeof datatable === "undefined")) {
                //pass instance datatable (optional) agar di-reload ketika submit formnya sukses
                initAjaxForm('#createForm', datatable); //datatable dapat dari index.blade.php
            }

            //INIT QUILL
            initQuillLocal('#quillKonten', '#konten')
            function initQuillLocal(ele, textareaEle){
                var quill = new Quill(ele, {
                    modules: {
                        toolbar: [
                            ['bold', 'italic', 'underline'],
                            [{ 'list': 'ordered'}, { 'list': 'bullet' }],
                            [ 'link', 'clean' ],
                            [{ size: [ 'small', false, 'large', 'huge' ]}],
                            ['image']
                        ]
                    },
                    placeholder: '',
                    theme: 'snow' // or 'bubble'
                });
                quill.on('text-change', function() {
                    justHtml = quill.root.innerHTML;
                    $(textareaEle).html(justHtml);
                });

                return quill;
            }
        });
    </script>
@endsection

{{-- For Modal --}}
@if(Request::ajax())
    @yield('content')
    @yield('pageScripts')
@endif