@extends((Request::ajax()) ? 'templates.layout-blank' : 'templates.'.config('genelator.template').'.layout')

@section('subheaderToolbar')
    <a href="#" class="btn invisible">
    </a>
@endsection

@section('content')
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title">{{ $title }}</h5>
                <button id="closeBtn" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                @if($errors->isNotEmpty())
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-warning" role="alert">
                                <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                <div class="alert-text">
                                    <ul>
                                        @foreach($errors->all() as $message)
                                            <li>{{ $message }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-lg-12">
                        <div class="kt-portlet">

                            <!--begin::Form-->
                            <form action="{{ url('pendaftar/') }}" id="createForm" method="post" class="kt-form"
                                  autocomplete="off" enctype="multipart/form-data">
                                @csrf

                                <div class="kt-portlet__body">
                                    <div class="form-group row">
                                        <div class="col-lg-6">
                                            <label class="form-control-label">* Email:</label>
                                            <input type="text" name="email" id="email" class="form-control" value=""
                                                   required>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="form-control-label">* Password:</label>
                                            <input type="text" name="password" id="password" class="form-control" value=""
                                                   required>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="w-100"></div>
                                        <br>
                                        <div class="col-lg-6">
                                            <label class="form-control-label">* Nama lengkap:</label>
                                            <input type="text" name="nama_lengkap" id="nama_lengkap"
                                                   class="form-control" value="" required>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="form-control-label">* Nomor Handphone:</label>
                                            <input type="text" name="no_hp" id="no_hp" class="form-control" value=""
                                                   required>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="w-100"></div>
                                        <br>
                                        <div class="col-lg-6">
                                            <label class="form-control-label">* Sekolah/Universitas:</label>
                                            <select name="id_sekolah" id="id_sekolah" class="form-control select2ajax"
                                                    required data-ajax--url="{{ url('pub_select2/allsekolah') }}"
                                                    data-placeholder="Pilih" data-allow-clear="false">
                                                <option></option>
                                            </select>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="form-control-label" style="visibility: hidden;">Checkbox</label>
                                            <label class="kt-checkbox">
                                                <input type="checkbox" value="1" name="is_banned"> Blokir pendaftar?
                                                <span></span>
                                            </label>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="form-control-label" style="visibility: hidden;">Checkbox</label>
                                            <label class="kt-checkbox">
                                                <input type="checkbox" value="1" name="email_verified_at"> Email terverifikasi?
                                                <span></span>
                                            </label>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="w-100"></div>
                                        <br>
                                    </div>
                                </div>
                                <div class="kt-portlet__foot">
                                    <div class="kt-form__actions">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <button type="submit" class="btn btn-warning">Submit</button>
                                                {{-- <a href="{{ url('pendaftar/') }}" class="btn btn btn-secondary">Cancel</a> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!--end::Form-->
                        </div>
                        <!--end::Portlet-->
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('pageScripts')
    {!! $validator->selector('#createForm') !!}
    <script>
        jQuery(document).ready(function () {
            initSelect2();
            initDatepicker();
            if (!(typeof datatable === "undefined")) {
                //pass instance datatable (optional) agar di-reload ketika submit formnya sukses
                initAjaxForm('#createForm', datatable); //datatable dapat dari index.blade.php
            }
        });
    </script>
@endsection

{{-- For Modal --}}
@if(Request::ajax())
    @yield('content')
    @yield('pageScripts')
@endif