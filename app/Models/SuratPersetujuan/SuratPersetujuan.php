<?php

namespace App\SuratPersetujuan;

use App\Magang\Magang;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SuratPersetujuan
{
    public static function getBadgeQty()
    {
        return Magang::whereNull('file_surat_persetujuan_magang')
            ->where('is_accepted', true)
            ->whereRaw('CASE WHEN is_need_pendaftar_approval = true THEN is_approved_by_pendaftar = true ELSE 1=1 END')
            ->count();
    }
}
