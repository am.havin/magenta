<div class="kt-aside-menu-wrapper kt-grid__item kt-grid__item--fluid" id="kt_aside_menu_wrapper">
    <div id="kt_aside_menu" class="kt-aside-menu " data-ktmenu-vertical="1" data-ktmenu-scroll="1" data-ktmenu-dropdown-timeout="500">
        <ul class="kt-menu__nav ">

            @foreach($vcMainmenuItems as $menu)
                @php
                    if(!App\MainmenuHelper::isHavingThisMenu(array_values($menu)[0][2], $vcUserRoleMenus)){
                        continue;
                    }
                    $menuName = array_values($menu)[0][2];
                    $isActive = (in_array( array_values($menu)[0][2], $activeMenus)) ? 'kt-menu__item--open kt-menu__item--here' : '';
                    $url = (array_values($menu)[0][1]) ? url(array_values($menu)[0][1]) : 'javascript:;';
                    $icon = array_values($menu)[0][0];
                @endphp
                <li class="kt-menu__item  {{$isActive}} kt-menu__item--submenu kt-menu__item--rel kt-menu__item--open" @if(isset($menu['child']))data-ktmenu-submenu-toggle="hover" aria-haspopup="true" @endif>
                    <a href="{{ $url }}" class="kt-menu__link @if(isset($menu['child'])) kt-menu__toggle @endif">
                        <span class="kt-menu__link-icon"><i class="{{ $icon }}"></i></span>
                        <span class="kt-menu__link-text">{{ array_keys($menu)[0] }}</span>
                        @if(isset($menu['child']))
                            <i class="kt-menu__ver-arrow la la-angle-right"></i>
                        @endif
                        @if(isset($vcMainmenuBadges[$menuName]))
                            <span class="kt-menu__link-badge"><span class="kt-badge kt-badge--magenta kt-badge--inline">{{ $vcMainmenuBadges[$menuName] }}</span></span>
                        @endif
                    </a>
                    @php recursive($menu, 'left', $activeMenus, $vcUserRoleMenus, $vcMainmenuBadges) //$activeMenus didapat dari controller @endphp
                </li>
            @endforeach
        </ul>
    </div>
</div>



<?php function recursive($menu, $menuDirection, $activeMenus, $vcUserRoleMenus, $vcMainmenuBadges){ ?>

@if(isset($menu['child']))
    <div class="kt-menu__submenu ">
        <span class="kt-menu__arrow"></span>
        <ul class="kt-menu__subnav">
            <?php foreach($menu['child'] as $menu): ?>
            @php
                if(!App\MainmenuHelper::isHavingThisMenu(array_values($menu)[0][2], $vcUserRoleMenus)){
                    continue;
                }
                $menuName = array_values($menu)[0][2];
                $isActive = (in_array( array_values($menu)[0][2], $activeMenus)) ? 'kt-menu__item--active' : '';
                $url = (array_values($menu)[0][1]) ? url(array_values($menu)[0][1]) : 'javascript:;';
            @endphp

            <li class="kt-menu__item  {{$isActive}} kt-menu__item--submenu" @if(isset($menu['child']))data-ktmenu-submenu-toggle="hover" aria-haspopup="true" @endif>
                <a href="{{$url }}" class="kt-menu__link ">
                    <i class="kt-menu__link-bullet kt-menu__link-bullet--dot">
                        <span></span>
                    </i>
                    <span class="kt-menu__link-text">{{ array_keys($menu)[0] }}</span>
                    @if(isset($menu['child']))
                        <i class="kt-menu__ver-arrow la la-angle-right"></i>
                    @endif
                    @if(isset($vcMainmenuBadges[$menuName]))
                        <span class="kt-menu__link-badge"><span class="kt-badge kt-badge--magenta kt-badge--inline">{{ $vcMainmenuBadges[$menuName] }}</span></span>
                    @endif
                </a>
                @php recursive($menu, 'right', $activeMenus, $vcUserRoleMenus, $vcMainmenuBadges) @endphp
            </li>
            <?php endforeach; ?>
        </ul>
    </div>
@endif

<?php } ?>