<?php

namespace App\Halamandepan;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Halamandepan extends Model
{
    protected $table = 'halamandepans';
    protected $guarded = ['id'];

    public function doDelete()
    {
        $status = true;

        DB::beginTransaction();
        try{
            $this->delete();
            DB::commit();
        }catch(\Exception $e) {
            DB::rollback();
            $status = false;
        }

        return $status;
    }
}
