@if(session('alert'))
    <div class="alert alert-{{session('alert')->type}} alert-elevate" role="alert">
        <div class="alert-icon"><i class="{{session('alert')->icon}}"></i></div>
        <div class="alert-text">
            {{session('alert')->text}}
        </div>
        <div class="alert-close">
            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                <span aria-hidden="true"><i class="la la-close"></i></span>
            </button>
        </div>
    </div>
@endif

