<?php

namespace App\IndexNilai;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class IndexNilai extends Model
{
    protected $table = 'indexnilais';
    protected $guarded = ['id'];

    public function doDelete()
    {
        $status = true;

        DB::beginTransaction();
        try{
            $this->delete();
            DB::commit();
        }catch(\Exception $e) {
            DB::rollback();
            $status = false;
        }

        return $status;
    }
}
