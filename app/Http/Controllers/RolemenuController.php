<?php

namespace App\Http\Controllers;

use App\Role;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Spatie\Permission\Models\Permission;
use Symfony\Component\Yaml\Yaml;
use Yajra\DataTables\DataTables;

use App\Alert;
use App\Http\Controllers\Controller;
use App\Http\Requests\RolemenuRequest;
use App\Rolemenu;
use App\Toastr;


class RolemenuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Akses Menu';
        $activeMenus = ['menuSettings', 'menuAksesMenu'];
        $breadcrumbs = [
            'Settings' => 'javascript:;',
            'Akses Menu' => route('rolemenus.index'),
        ];
        $yamlFileLoc    = app_path('Yamls/menus/mainmenu.yaml');
        $menus = Yaml::parse(file_get_contents($yamlFileLoc));
        $roles = Role::all();
        $rolesMenus = DB::table('role_has_menus')->get();
        $viewData = compact('title', 'activeMenus', 'breadcrumbs', 'menus', 'roles', 'rolesMenus');

        return view('rolemenus.index', $viewData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return void
     */
    public function create()
    {
        abort(404);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param RolemenuRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(RolemenuRequest $request)
    {
        DB::table('role_has_menus')->truncate();
        if($request->has('inputs')){
            $data = [];
            foreach($request->input('inputs') as $menuId => $roles){
                foreach($roles as $roleId){
                    $data[] = [
                        'menu_id' => $menuId,
                        'role_id' => $roleId
                    ];
                }
            }
            DB::table('role_has_menus')->insert($data);
        }
        $status = true;

        //$alert = new Alert('success', 'Data baru berhasil disimpan!');
        //return redirect(route('rolemenus.index'))->with('alert', $alert);

        $toastr = new Toastr('success', 'Akses menu berhasil disimpan!');
        return redirect(route('rolemenus.index'))->with('toastr', $toastr);
    }

    /**
     * Display the specified resource.
     *
     * @param RolemenuRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(RolemenuRequest $request, $id)
    {
        abort(404);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param RolemenuRequest $request
     * @param  int $id
     * @return void
     */
    public function edit(RolemenuRequest $request, $id)
    {
        abort(404);
    }

    /**
     * Update the specified resource in storage.
     *
     * @return void
     */
    public function update()
    {
        abort(404);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param RolemenuRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(RolemenuRequest $request, $id)
    {
        return response()->json([
            'status' => Rolemenu::where('id', $id)->delete()
        ]);
    }
}
