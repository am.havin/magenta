<?php

namespace App\Http\Controllers\Select2;

use App\Alokasi\Alokasi;
use App\Magang\Magang;
use App\Peserta\Peserta;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;


class PesertaController extends Controller
{
    public function ownPesertas(Request $request)
    {
        $return = [];
        $data = Peserta::getCanBeSubmitted($request->q);
        foreach($data as $peserta){
            $return[] = ['id' => $peserta->id, 'text' => $peserta->nama ];
        }

        return response()->json(['results' => $return]);
    }
}
