<?php

use Illuminate\Database\Seeder;

class SekolahPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['group' => 'Sekolah', 'name' => 'Sekolah - index', 'guard_name' => 'web'],
            ['group' => 'Sekolah', 'name' => 'Sekolah - create', 'guard_name' => 'web'],
            ['group' => 'Sekolah', 'name' => 'Sekolah - show', 'guard_name' => 'web'],
            ['group' => 'Sekolah', 'name' => 'Sekolah - edit', 'guard_name' => 'web'],
            ['group' => 'Sekolah', 'name' => 'Sekolah - delete', 'guard_name' => 'web'],
        ];

        foreach($data as $val){
            \Spatie\Permission\Models\Permission::updateOrCreate($val, $val);
        }
    }
}
