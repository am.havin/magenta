<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class GeneratorMake extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'generator:make {yaml_filename}';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Run the generator';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $yamlFilename = $this->argument('yaml_filename');

        $this->validateFileExistance($yamlFilename);
        $generator = new \App\Generators\Generator($yamlFilename);
        $this->info($generator->generate());
    }

    private function validateFileExistance($yamlFilename)
    {
        if(file_exists(app_path("Yamls/generators/$yamlFilename.yaml"))){
            return true;
        }
        $this->info("File YAML $yamlFilename tidak ditemukan");
        die;
    }
}
