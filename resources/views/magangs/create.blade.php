@extends((Request::ajax()) ? 'templates.layout-blank' : 'templates.'.config('genelator.template').'.layout')

@section('subheaderToolbar')
    <a href="#" class="btn invisible">
    </a>
@endsection

@section('content')
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title">{{ $title }}</h5>
                <button id="closeBtn" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                @if($errors->isNotEmpty())
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-warning" role="alert">
                                <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                <div class="alert-text">
                                    <ul>
                                        @foreach($errors->all() as $message)
                                            <li>{{ $message }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-lg-12">
                        <div class="kt-portlet">

                            <!--begin::Form-->
                            <form action="{{ url('magang/') }}" id="createForm" method="post" class="kt-form" autocomplete="off" enctype="multipart/form-data">
                                @csrf

                                <div class="kt-portlet__body">
                                    <div class="form-group row">
                                                                    <div class="col-lg-6">
                                <label class="form-control-label">* Pengajuan:</label>
                                <select name="id_pengajuan" id="id_pengajuan" class="form-control select2ajax" required   data-ajax--url="{{ url('select2/roles') }}" data-placeholder="Pilih" data-allow-clear="true">
                                    <option></option>
                                </select>
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-6">
                                <label class="form-control-label">* Peserta:</label>
                                <select name="id_peserta" id="id_peserta" class="form-control select2ajax" required   data-ajax--url="{{ url('select2/roles') }}" data-placeholder="Pilih" data-allow-clear="true">
                                    <option></option>
                                </select>
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="w-100"></div><br>
                            <div class="col-lg-6">
                                <label class="form-control-label">* Peserta:</label>
                                <input type="text" name="peserta" id="peserta"  class="form-control" value="" required >
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-6">
                                <label class="form-control-label">* Email:</label>
                                <input type="text" name="email" id="email"  class="form-control" value="" required >
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="w-100"></div><br>
                            <div class="col-lg-6">
                                <label class="form-control-label">* Nomor HP:</label>
                                <input type="text" name="no_hp" id="no_hp"  class="form-control" value="" required >
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-6">
                                <label class="form-control-label">* Id Sekolah:</label>
                                <select name="id_sekolah" id="id_sekolah" class="form-control select2ajax" required   data-ajax--url="{{ url('select2/roles') }}" data-placeholder="Pilih" data-allow-clear="true">
                                    <option></option>
                                </select>
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="w-100"></div><br>
                            <div class="col-lg-6">
                                <label class="form-control-label">* Sekolah:</label>
                                <input type="text" name="sekolah" id="sekolah"  class="form-control" value="" required >
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-6">
                                <label class="form-control-label">* Id Program studi:</label>
                                <select name="id_programstudi" id="id_programstudi" class="form-control select2ajax" required   data-ajax--url="{{ url('select2/roles') }}" data-placeholder="Pilih" data-allow-clear="true">
                                    <option></option>
                                </select>
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="w-100"></div><br>
                            <div class="col-lg-6">
                                <label class="form-control-label">* Program Studi:</label>
                                <input type="text" name="programstudi" id="programstudi"  class="form-control" value="" required >
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-6">
                                <label class="form-control-label">* Id Unit:</label>
                                <select name="id_unit" id="id_unit" class="form-control select2ajax" required   data-ajax--url="{{ url('select2/roles') }}" data-placeholder="Pilih" data-allow-clear="true">
                                    <option></option>
                                </select>
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="w-100"></div><br>
                            <div class="col-lg-6">
                                <label class="form-control-label">* Unit:</label>
                                <input type="text" name="unit" id="unit"  class="form-control" value="" required >
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-6">
                                <label class="form-control-label">* Tanggal Mulai Magang:</label>
                                <input type="text" name="tgl_awal" id="tgl_awal"  class="form-control datepicker" value="" required >
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="w-100"></div><br>
                            <div class="col-lg-6">
                                <label class="form-control-label">* Tanggal Selesai Magang:</label>
                                <input type="text" name="tgl_akhir" id="tgl_akhir"  class="form-control datepicker" value="" required >
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-6">
                                <label class="form-control-label">* Is Accepted?:</label>
                                <input type="number" name="is_accepted" id="is_accepted"  class="form-control" value="" required >
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="w-100"></div><br>
                            <div class="col-lg-6">
                                <label> Surat Persetujuan Magang:</label>
                                <input type="file" class="form-control-file" name="file_surat_persetujuan_magang" id="file_surat_persetujuan_magang"   >
                                <span class="form-text text-muted"></span>
                                
                            </div>
                            <div class="col-lg-6">
                                <label class="form-control-label">* Nilai:</label>
                                <input type="number" name="nilai" id="nilai"  class="form-control" value="" required >
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="w-100"></div><br>
                            <div class="col-lg-6">
                                <label class="form-control-label"> Catatan:</label>
                                <textarea name="catatan_nilai" id="catatan_nilai" class="form-control"  ></textarea>
                                <span class="form-text text-muted">Catatan terhadap penilaian</span>
                            </div>

                                    </div>
                                </div>
                                <div class="kt-portlet__foot">
                                    <div class="kt-form__actions">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <button type="submit" class="btn btn-warning">Submit</button>
                                                {{-- <a href="{{ url('magang/') }}" class="btn btn btn-secondary">Cancel</a> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!--end::Form-->
                        </div>
                        <!--end::Portlet-->
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('pageScripts')
    {!! $validator->selector('#createForm') !!}
    <script>
            jQuery(document).ready(function () {
                initSelect2();
                initDatepicker();
                if (!(typeof datatable === "undefined")) {
                    //pass instance datatable (optional) agar di-reload ketika submit formnya sukses
                    initAjaxForm('#createForm', datatable); //datatable dapat dari index.blade.php
                }
            });
        </script>
@endsection

{{-- For Modal --}}
@if(Request::ajax())
    @yield('content')
    @yield('pageScripts')
@endif