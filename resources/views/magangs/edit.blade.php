@extends((Request::ajax()) ? 'templates.layout-blank' : 'templates.'.config('genelator.template').'.layout')

@section('subheaderToolbar')
    <a href="#" class="btn invisible">
    </a>
@endsection

@section('content')
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title">{{ $title }}</h5>
                <button id="closeBtn" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                @if($errors->isNotEmpty())
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-warning" role="alert">
                                <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                <div class="alert-text">
                                    <ul>
                                        @foreach($errors->all() as $message)
                                            <li>{{ $message }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-lg-12">
                        <div class="kt-portlet">

                            <!--begin::Form-->
                            <form action="{{ route('magang.update', $model->id) }}" id="editForm" method="post"
                                  class="kt-form" autocomplete="off" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="PUT">

                                <div class="kt-portlet__body">
                                    <div class="form-group row">
                                        <div class="col-lg-6">
                                            <label class="form-control-label">* Peserta:</label>
                                            <input type="text" class="form-control" value="{{ $model->peserta }}"
                                                   disabled>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="form-control-label">* Unit tujuan:</label>
                                            <select name="id_unit" id="id_unit" class="form-control" required>
                                                <option value="{{ $model->id_unit }}">{{ $model->unit }}</option>
                                            </select>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="w-100"></div>
                                        <br>
                                        <div class="input-daterange">
                                            <div class="row">
                                                <div class="col-lg-6">
                                                    <label class="form-control-label">* Tanggal Awal Magang:</label>
                                                    <input type="text" name="tgl_awal" id="tgl_awal"
                                                           class="form-control"
                                                           value="{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $model->tgl_awal)->format('Y-m-d') }}"
                                                           required>
                                                    <span class="form-text text-muted">
                                                    Tanggal yang tidak dapat dipilih menandakan pada tanggal tersebut kuota untuk
                                                    unit yang dituju sudah terpenuhi.
                                                </span>
                                                </div>
                                                <div class="col-lg-6">
                                                    <label class="form-control-label">* Tanggal Akhir Magang:</label>
                                                    <input type="text" name="tgl_akhir" id="tgl_akhir"
                                                           class="form-control"
                                                           value="{{ \Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $model->tgl_akhir)->format('Y-m-d') }}"
                                                           required>
                                                    <span class="form-text text-muted">
                                                    Waktu magang minimal adalah 14 hari dan maksimal 60 hari
                                                </span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="w-100"></div>
                                        <br>
                                    </div>
                                </div>
                                <div class="kt-portlet__foot">
                                    <div class="kt-form__actions">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <button type="submit" class="btn btn-warning">Submit</button>
                                                {{-- <a href="{{ url('magang/') }}" class="btn btn btn-secondary">Cancel</a> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!--end::Form-->
                        </div>
                        <!--end::Portlet-->
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection


@section('pageScripts')
    {!! $validator->selector('#editForm') !!}
    <script>
        jQuery(document).ready(function () {
            initDatepicker();
            if (!(typeof datatable === "undefined")) {
                //pass instance datatable (optional) agar di-reload ketika submit formnya sukses
                initAjaxForm('#editForm', datatable); //datatable dapat dari index.blade.php
            }

            $('.input-daterange').datepicker({
                language: 'id',
                updateViewDate: false,
                format: 'yyyy-mm-dd',
                startDate: d = new Date(),
                endDate: new Date( (d.getFullYear()+1), d.getMonth(), d.getDate() ),
            });
            updateDisabledDates(
                '#tgl_awal, #tgl_akhir',
                $('#id_unit').val(),
                moment().format('MM'),
                moment().format('YYYY'),
                {{ $model->id }}
            );

            //SELECT2 - UNIT KERJA
            $('#id_unit').select2({
                placeholder: 'Pilih',
                allowClear: false,
                ajax: {
                    url: '{{ url('select2/alokasi_unitkerjas') }}',
                    dataType: 'json',
                    delay: 3000
                },
            }).on('select2:select', function (event) {
                let unit = $(this).val();
                $('#tgl_awal').prop('disabled', false).text('').val('').attr('placeholder', 'Tanggal awal');
                $('#tgl_akhir').prop('disabled', false).text('').val('').attr('placeholder', 'Tanggal akhir');
                updateDisabledDates('#tgl_awal, #tgl_akhir', unit, moment().format('MM'), moment().format('YYYY'), {{ $model->id }})
            });
        });

        function updateDisabledDates(ele, unit, month, year, exceptId = null){
            let url = '{{ url('magang/unavailable_dates') }}' + '/' + unit + '/' + month + '/' + year + '/' + '{{ $model->id }}';
            $.ajax({
                url: url,
                type: 'GET',
                contentType: false,
                processData: false,
                beforeSend: function() {
                    KTApp.block('body', {
                        overlayColor: '#000000',
                        type: 'v2',
                        state: 'primary',
                        message: ''
                    });
                },
                success: function (data) {
                    $(ele).datepicker('setDatesDisabled', data);
                    KTApp.unblock('body');
                },
                error: function (error) {
                },
            });
        }
    </script>
        });
    </script>
@endsection


{{-- For Modal --}}
@if(Request::ajax())
    @yield('content')
    @yield('pageScripts')
@endif