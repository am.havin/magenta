<?php

namespace App\Rules;

use Carbon\Carbon;
use Illuminate\Contracts\Validation\Rule;

class MinDayDuration implements Rule
{
    private $minDuration;

    /**
     * Create a new rule instance.
     *
     * @param $minDuration
     */
    public function __construct($minDuration)
    {
        $this->minDuration = $minDuration;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $tglAwal = Carbon::createFromFormat('Y-m-d', request()->tgl_awal);
        $tglAkhir = Carbon::createFromFormat('Y-m-d', request()->tgl_akhir);
        if($tglAwal->diffInDays($tglAkhir) >= $this->minDuration){
            return true;
        }
        return false;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Minimal durasi magang adalah ' . $this->minDuration . ' hari!';
    }
}
