<?php

namespace App\Http\Middleware;

use App\User;
use Closure;
use GuzzleHttp\Client;

class IsSimanisUser
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!session('simanis_user')){
            if($user = $this->setSimanisUser()){
                session(['simanis_user' => $user]);
                return $next($request);
            }
            return redirect()->url('logout');
        }
        return $next($request);
    }

    private function setSimanisUser()
    {
        $user = null;

        # 1. cara pertama cek dulu udah ada di table users / belum
        # (karena kalau udah ada berarti dia sebelumnya sudah tervalidasi sebagai user simanis)
        $user = User::where('username', session('cas_user'))->first();

        # 2. kalau cara 1 nggak ada, baru ke cara kedua yaitu cek di simanis.bumn.go.id lewat API
        if(!$user){
            $data = $this->cekUserDiSimanis();
            if($data){
                $user = User::updateOrCreate(['username' => $data->name],[
                    'username' => $data->name,
                    'pegawai_id' => $data->pegawai->id,
                    'pegawai_nama' => $data->pegawai->nama,
                    'pegawai_pic' => $this->getUserPic($data->pegawai->foto),
                ]);
                if($user->roles->isEmpty()){
                    $user->assignRole('user');
                }
            }
        }
        return $user;
    }

    private function cekUserDiSimanis()
    {
        $urlSimanis = env('SIMANIS_URL', 'https://simanis.bumn.go.id');
        $username = session('cas_user');
        $url = "$urlSimanis/api/v1/users?filter[name]=$username&page[size]=1";

        $client = new Client();
        $respond = $client->request('GET', $url);
        $result = json_decode($respond->getBody());

        if(count($result->data)){
            return $result->data[0];
        }
        return false;
    }

    private function getUserPic($filename)
    {
        if($filename){
            return 'http://simanis.bumn.go.id/file/fopeg/'.$filename;
        }
        return url('images/male.png');
    }
}
