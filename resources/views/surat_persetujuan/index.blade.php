@extends((Request::ajax()) ? 'templates.layout-blank' : 'templates.'.config('genelator.template').'.layout')

@section('subheaderToolbar')
@endsection

@section('vendorPluginForCustomPagesStyles')
    @include('templates.metronic.demo5.includes.datatable_css')
@endsection

@section('content')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-list-2"></i>
            </span>
                <h3 class="kt-portlet__head-title">
                    Persetujuan Pengajuan Magang
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        {{--@if(auth()->user()->can('Pengajuan - create'))--}}
                            {{--<a href="{{ url('pengajuan/create') }}"--}}
                               {{--class="btn btn-sm btn-warning btn-elevate btn-icon-sm btnAdd">--}}
                                {{--<i class="la la-plus"></i>--}}
                                {{--Tambah {{ $title }}--}}
                            {{--</a>--}}
                        {{--@endif--}}
                        <button type="button" class="btn btn-sm btn-success btn-elevate btn-icon" id="btnToggleFilter"
                                data-toggle="kt-tooltip" data-placement="top" title=""
                                data-original-title="Search / Filter list data"
                                data-skin="brand">
                            <i class="la la-search"></i>
                        </button>
                        <button type="button" class="btn btn-sm btn-info btn-elevate btn-icon" id="btnReloadDt"
                                data-toggle="kt-tooltip" data-placement="top" title=""
                                data-original-title="Refresh list data"
                                data-skin="brand">
                            <i class="la la-refresh"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Filter for datatable-->
            <div id="sectionFilter" class="kt-section" style="display:none;">
                <div class="kt-section__content kt-section__content--solid">
                    <form id="filterForm" autocomplete="off">
                        <h5>Filter:</h5>
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label class="form-control-label">Akun pendaftar:</label>
                                <input type="text" name="filter_pendaftar"
                                       id="filter_pendaftar" class="form-control" value="">
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-3">
                                <label class="form-control-label"> Peserta magang:</label>
                                <input type="text" name="filter_peserta" id="filter_peserta" class="form-control"
                                       value="">
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-6">
                                <label class="form-control-label"> Unit kerja:</label>
                                <select name="filter_unit" id="filter_unit" class="form-control select2ajax"
                                        data-ajax--url="{{ url('select2/magang/unitkerja_magang_yg_diterima') }}" data-placeholder="Pilih"
                                        data-allow-clear="true">
                                    <option></option>
                                </select>
                                <span class="form-text text-muted"></span>
                            </div>
                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-12 text-right">
                                <button type="reset" class="btn btn-wide btn-sm btn-clean">Clear</button>
                                <button type="submit" class="btn btn-wide btn-sm btn-outline-success">Filter</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!--end: Filter for datatable-->

            <!--begin: Datatable -->
            <table class="table table-striped- table-hover table-checkable" id="datatable">
                <thead>
                <tr>
                    <th>Id Pendaftar</th>
                    <th>Id Pengajuan</th>
                    <th>Akun Pendaftar</th>
                    <th>id magang</th>
                    <th>Peserta magang</th>
                    <th>Unit kerja tujuan</th>
                    <th width="200px">Waktu magang</th>
                    <th width="100px">File yg telah diupload</th>
                    <th width="100px;">Action</th>
                </tr>
                </thead>
            </table>
            <!--end: Datatable -->

        </div>
    </div>

    <!-- Modal Container-->
    <div class="modal fade" id="modal" tabindex="-1"></div>
    <!--end: Modal Container-->
@endsection

@section('vendorPluginForCustomPagesScripts')
    @include('templates.metronic.demo5.includes.datatable_js')
@endsection

@section('pageScripts')
    <script src="{{ url('js/dataTables.rowsGroup.js') }}" type="text/javascript"></script>
    <script>
        jQuery(document).ready(function () {
            datatable = $('#datatable').DataTable({
                dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url: "{{ route('surat_persetujuan.datatable') }}",
                    data: function (d) {
                        d.filter_pendaftar = $('#filter_pendaftar').val();
                        d.filter_peserta = $('#filter_peserta').val();
                        d.filter_unit = $('#filter_unit').val();
                    }
                },
                columns: [
                    {data: 'id_pendaftar', name: 'id_pendaftar', visible: false},
                    {data: 'id_pengajuan', name: 'id_pengajuan', visible: false},
                    {data: 'pendaftar', name: 'pendaftar', orderable: false},
                    {data: 'id_magang', name: 'id_magang', visible: false},
                    {data: 'peserta', name: 'peserta', orderable: false},
                    {data: 'unit', name: 'unit', orderable: false},
                    {data: 'waktu_magang', name: 'waktu_magang', orderable: false},
                    {data: 'upload_file', name: 'upload_file', orderable: false},

                    {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'},
                ],
                order: [[1, "desc"],[ 2, "desc" ]],
                rowsGroup: [1,2],
                drawCallback: function (settings) {
                    KTApp.initTooltip($('[data-toggle="kt-tooltip"]'));
                }
            });

            //INIT DATATABLE'S BUTTONS
            initDatatableFilterAndRefreshButton(datatable); //check function in custom.js

            //INIT MODAL
            initAjaxModalButton('a.btnAdd, a.btnEdit');
        });
    </script>
@endsection