@extends((Request::ajax()) ? 'templates.layout-blank' : 'templates.'.config('genelator.template').'.layout')

@section('subheaderToolbar')
    <a href="#" class="btn invisible">
    </a>
@endsection

@section('content')
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title">{{ $title }}</h5>
                <button id="closeBtn" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="kt-portlet">
                            <div class="kt-portlet__body">
                                @if($model->pic)
                                    <img src="{{ route('pendaftar.foto', $model->id) }}?rand={{ rand(10,1000) }}" style="width: 150px; height: auto; top:10px; right: 36px; position: absolute; padding: 8px; background-color: #fff;">
                                @else
                                    <img src="{{ url('images/person.png')  }}" style="width: 150px; height: auto; top:10px; right: 36px; position: absolute; padding: 8px; background-color: #fff;">
                                @endif
                                <table class="table">
                                    <tbody>
                                    <tr>
                                        <th width="150px">Nama Lengkap</th>
                                        <td>{{ $model->nama_lengkap }}</td>
                                    </tr>
                                    <tr>
                                        <th>Email</th>
                                        <td>{{ $model->email }}</td>
                                    </tr>
                                    <tr>
                                        <th>No HP</th>
                                        <td>{{ $model->no_hp }}</td>
                                    </tr>
                                    <tr>
                                        <th>{{ ($model->is_universitas) ? 'Universitas' : 'SMA/SMK' }}</th>
                                        <td>{{ $model->sekolah }}</td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                        <!--end::Portlet-->
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection


@section('pageScripts')
@endsection


{{-- For Modal --}}
@if(Request::ajax())
    @yield('content')
    @yield('pageScripts')
@endif