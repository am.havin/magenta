<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolePermissionsSeeder extends Seeder
{
    private $userPermissions = [];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        #SIAPKAN ROLES
        $roleSuperadmin =  \Spatie\Permission\Models\Role::where('name', 'superadmin')->first();
        $roleAdmin =  \Spatie\Permission\Models\Role::where('name', 'admin')->first();
        $roleUser =  \Spatie\Permission\Models\Role::where('name', 'user')->first();

        #ISI PERMISSION SUPERADMIN
        $roleSuperadmin->givePermissionTo(\Spatie\Permission\Models\Permission::pluck('name')->toArray());

        #ISI PERMISSION ADMIN
        $adminPermissions = \Spatie\Permission\Models\Permission::whereNotIn('name', [
            'roles - index', 'roles - create', 'roles - edit', 'roles - delete',
            'menu - manage akses menu', 'permission - manage permission', 'generator - akses menu'
        ])->pluck('name');
        $roleAdmin->givePermissionTo($adminPermissions->toArray());

        #ISI PERMISSION USER
        $roleUser->givePermissionTo($this->userPermissions);
    }
}
