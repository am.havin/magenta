<?php

namespace App\Http\Controllers\SekolahProgramStudi;

use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

use App\Alert;
use App\Http\Controllers\Controller;
use App\Http\Requests\SekolahProgramStudiRequest;
use App\SekolahProgramStudi\SekolahProgramStudi;
use App\Sekolah\Sekolah;
use App\Toastr;


class SekolahProgramStudiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param SekolahProgramStudiRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(SekolahProgramStudiRequest $request)
    {
        $title = 'Program Studi';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Program Studi' => route('sekolahprogramstudi.index'),
        ];
        $url_get_name = $request->get( 'type' );
        $viewData = compact('title', 'activeMenus', 'breadcrumbs' ,'url_get_name');

        if($url_get_name == 'all')
        {
            return view('sekolahprogramstudis.index', $viewData);
        }
        else
        {
            return view('sekolahprogramstudis.approval', $viewData);
        }

       
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param SekolahProgramStudiRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(SekolahProgramStudiRequest $request)
    {
        $title = 'Tambah Program Studi';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Program Studi' => route('sekolahprogramstudi.index'),
            'Tambah' => route('sekolahprogramstudi.create'),
        ];
        $validator = \JsValidator::make($request->getCreateRules(), $request->messages());

        $viewData = compact('title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('sekolahprogramstudis.create', $viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SekolahProgramStudiRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(SekolahProgramStudiRequest $request)
    {
        try{
            $model = SekolahProgramStudi::create($request->all());
            $this->storeFiles($request, $model); //if there is any
            $json = ['status' => true, 'message' => 'Data berhasil disimpan!'];
        }catch(\Exception $e){
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data baru berhasil disimpan!');
        //return redirect(route('sekolahprogramstudi.index'))->with('alert', $alert);
        $toastr = new Toastr('success', 'Data baru berhasil disimpan!');
        return redirect(route('sekolahprogramstudi.index'))->with('toastr', $toastr);
    }

    /**
     * Display the specified resource.
     *
     * @param SekolahProgramStudiRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(SekolahProgramStudiRequest $request, $id)
    {
        $model = SekolahProgramStudi::findOrFail($id);
        $title = 'Detail Program Studi';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Program Studi' => route('sekolahprogramstudi.index'),
            'Detail' => route('sekolahprogramstudi.show', $model->id),
        ];
        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs');

        return view('sekolahprogramstudis.show', $viewData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param SekolahProgramStudiRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SekolahProgramStudiRequest $request, $id)
    {
        $model = SekolahProgramStudi::findOrFail($id);
        $title = 'Edit Program Studi';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Program Studi' => route('sekolahprogramstudi.index'),
            'Edit' => route('sekolahprogramstudi.edit', $model->id),
        ];
        $validator = \JsValidator::make($request->getEditRules(), $request->messages());

        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('sekolahprogramstudis.edit', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SekolahProgramStudiRequest $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(SekolahProgramStudiRequest $request, $id)
    {
        try{
            $is_approved = $_POST['is_approved'];
            $model =  SekolahProgramStudi::findOrFail($id);
            $this->storeFiles($request, $model); //if there is any
            if($is_approved == 0){
                $model->where('id', '=', $id)->delete();
            }else{
                $model->update($request->all());
            }
            $json = ['status' => true, 'message' => 'Data berhasil diupdate!'];
        }catch(\Exception $e){
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data berhasil diupdate!');
        //return redirect()->back()->with('alert', $alert);
        $toastr = new Toastr('success', 'Data berhasil diupdate!');
        return redirect()->back()->with('toastr', $toastr);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param SekolahProgramStudiRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SekolahProgramStudiRequest $request, $id)
    {
        $model = SekolahProgramStudi::findOrFail($id);
        return response()->json([
            'status' => $model->doDelete()
        ]);
    }

    /**
     * Handle Datatable request.
     *
     * @param SekolahProgramStudiRequest $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function datatable(SekolahProgramStudiRequest $request)
    {
        return Datatables::of(SekolahProgramStudi::with('pendaftar'))
            ->filter(function ($query) use ($request) {
                if (!empty($request->input('filter_nama'))) {
                    $query->where('nama', 'ilike', "%{$request->input('filter_nama')}%");
                }
                if (!empty($request->input('filter_id_sekolah'))) {
                    $query->where('id_sekolah', 'ilike', "%{$request->input('filter_id_sekolah')}%");
                }
                if (!empty($request->input('filter_id_pendaftar'))) {
                    $query->where('id_pendaftar', 'ilike', "%{$request->input('filter_id_pendaftar')}%");
                }
                if ($request->has('filter_is_approved')) {
                    $query->where('is_approved', $request->input('filter_is_approved'));
                }
            })
            ->editColumn('pendaftar', function ($data) {
                return ($data->pendaftar) ? $data->pendaftar->nama_lengkap : '';
            })
            ->editColumn('id_sekolah', function($data){
                $sekolah = Sekolah::find($data->id_sekolah);
                return ($sekolah) ? $sekolah->nama : '';
            })
            ->editColumn('is_approved', function($data){
                $status_caption = '';
                if($data->is_approved === 0)
                {
                    $status_caption = 'Rejected';
                }
                else if($data->is_approved === 1)
                {
                    $status_caption = 'Approved';
                }
                else
                {
                    $status_caption = '-';
                }
                return $status_caption;
            })
            ->addColumn('action', function ($data) {
                $show = $edit = $delete = '';
                if(auth()->user()->hasAnyPermission(['SekolahProgramStudi - show'])){
                    //$show = '<a href="'.route('sekolahprogramstudi.show', $data->id).'" class="btn btn-outline-hover-warning btn-icon btn-circle btn-sm" data-toggle="kt-tooltip" title="Show detail" data-skin="brand"><i class="la la-eye kt-font-warning"></i></a>';
                }
                if(auth()->user()->hasAnyPermission(['SekolahProgramStudi - edit'])){
                    $edit = '<a href="'.route('sekolahprogramstudi.edit', $data->id).'"class="btn btn-outline-hover-info btn-icon btn-circle btn-sm btnEdit" data-toggle="kt-tooltip" title="Edit" data-skin="brand"><i class="la la-pencil kt-font-info"></i></a>';
                }
                if(auth()->user()->hasAnyPermission(['SekolahProgramStudi - delete'])){
                    $delete = '<button type="button" class="btn btn-outline-hover-danger btn-icon btn-circle btn-sm btn-dt-delete" data-toggle="kt-tooltip" title="Delete" data-skin="brand" data-url="'.route('sekolahprogramstudi.destroy', $data->id).'" data-csrf="'.csrf_token().'"><i class="la la-close kt-font-danger"></i></button>';
                }
                return $show.$edit.$delete;
            })->make(true);
    }

    /**
     * Handle file store
     *
     * @param $request
     * @param $model
     */
    private function storeFiles($request, $model)
    {
        foreach($request->files as $key => $file){
            if ($request->file($key)->isValid()) {
                Storage::delete($model->$key);
                $model->$key = $request->file($key)->storeAs("SekolahProgramStudi/$key/$model->id", $file->getClientOriginalName(),'local');
                $model->save();
            }
        }
    }
}
