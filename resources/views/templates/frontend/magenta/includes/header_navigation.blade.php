<nav id="primary-menu">

    <ul>
        @foreach($vcFrontmenuItems as $item)
            @php
                $menuName = array_key_first($item);
                $isActive = (in_array( array_values($item)[0][2], $activeMenus)) ? 'current' : '';
                $url = (array_values($item)[0][1]) ? url(array_values($item)[0][1]) : 'javascript:;';
            @endphp

            <li class="{{ $isActive }}"><a href="{{ $url }}">{{ $menuName }}</a></li>
        @endforeach

        {{--LOGIN--}}
        @php
            $isActive = (in_array('menuLogin', $activeMenus)) ? 'current' : '';
        @endphp

        @if(auth()->guard('pendaftar')->check())
            <li><a href="{{ url('logout') }}"><div><i class="icon-sign-out-alt"></i> Logout</div></a></li>
        @else
            <li class="{{ $isActive }}"><a href="{{ url('login') }}"><div>Login</div></a></li>
        @endif

    </ul>

    @if(!auth()->guard('pendaftar')->check())
        <div id="top-account">
            <a href="{{ url('register') }}"><i class="icon-check"></i> <span>Register!</span></a>
        </div>
    @endif

</nav>