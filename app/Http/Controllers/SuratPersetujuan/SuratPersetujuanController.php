<?php

namespace App\Http\Controllers\SuratPersetujuan;

use App\Http\Requests\SuratPersetujuanRequest;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\Mail;
use Yajra\DataTables\DataTables;

use App\Alert;
use App\Http\Controllers\Controller;
use App\Http\Requests\PengajuanRequest;
use App\Pengajuan\Pengajuan;
use App\Magang\Magang;
use App\Toastr;
use App\Mail\MailSuratPersetujuanMagang;


class SuratPersetujuanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param SuratPersetujuanRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(SuratPersetujuanRequest $request)
    {
        $title = 'Surat Persetujuan Magang';
        $activeMenus = ['menuSuratPersetujuan'];
        $breadcrumbs = [
            'Surat Persetujuan' => route('surat_persetujuan.index'),
        ];
        $viewData = compact('title', 'activeMenus', 'breadcrumbs');

        return view('surat_persetujuan.index', $viewData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param PengajuanRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(PengajuanRequest $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PengajuanRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PengajuanRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param PengajuanRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(PengajuanRequest $request, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param SuratPersetujuanRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SuratPersetujuanRequest $request, $idMagang)
    {
        $model = Magang::findOrFail($idMagang);
        $title = 'Upload surat persetujuan magang';
        $activeMenus = ['menuSuratPersetujuan'];
        $breadcrumbs = [
            'Surat Persetujuan' => route('surat_persetujuan.index'),
            'Upload' => route('surat_persetujuan.edit', $model->id),
        ];
        $validator = \JsValidator::make($request->getEditRules(), $request->messages());
        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('surat_persetujuan.edit', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SuratPersetujuanRequest $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(SuratPersetujuanRequest $request, $idMagang)
    {
        try {
            $model = Magang::findOrFail($idMagang);
            //$model->update($request->all());
            $this->storeFiles($request, $model);
            $this->mail($idMagang);
            $json = ['status' => true, 'message' => 'File Surat Persetujuan Magang berhasil disimpan!'];
        } catch (\Exception $e) {
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data berhasil diupdate!');
        //return redirect()->back()->with('alert', $alert);
        $toastr = new Toastr('success', 'Data berhasil diupdate!');
        return redirect()->back()->with('toastr', $toastr);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param PengajuanRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PengajuanRequest $request, $id)
    {
        //
    }

    /**
     * Handle Datatable request.
     *
     * @param PengajuanRequest $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function datatable(PengajuanRequest $request)
    {
        $queryBuilder = Magang::query()
            ->with('pengajuan.pendaftar')
            ->whereHas('pengajuan', function($q){
                $q->where('is_draft', false)->where('is_submitted', true)->where('is_deleted', false);
            })
            ->where('is_accepted', true)
            ->whereRaw('CASE WHEN is_need_pendaftar_approval = true THEN is_approved_by_pendaftar = true ELSE 1=1 END')
            ->orderBy('id_pengajuan', 'desc');

        return Datatables::of($queryBuilder)
            ->filter(function ($query) use ($request) {
                if ($request->has('filter_pendaftar')) {
                    $query->whereHas('pengajuan.pendaftar', function($q) use($request){
                        $q->where('nama_lengkap', 'ilike', "%{$request->input('filter_pendaftar')}%");
                    });
                }
                if ($request->has('filter_peserta')) {
                    $query->where('peserta', 'ilike', "%{$request->input('filter_peserta')}%");
                }
                if ($request->has('filter_unit')) {
                    $query->where('unit', 'ilike', "%{$request->input('filter_unit')}%");
                }
            })
            ->editColumn('id_pendaftar', function($data){
                return $data->pengajuan->id_pendaftar;
            })
            ->editColumn('pendaftar', function($data){
                return $data->pengajuan->pendaftar->nama_lengkap;
            })
            ->editColumn('id_magang', function($data){
                return $data->id;
            })
            ->editColumn('unit', function($data){
                return $data->unit;
            })
            ->editColumn('waktu_magang', function($data){
                return "{$data->carbonTglAwal->format('d M Y')} s.d. {$data->carbonTglAkhir->format('d M Y')} <br>({$data->lamaHari} hari)";
            })
            ->editColumn('upload_file', function($data){
                if($data->file_surat_persetujuan_magang){
                    return '<a href="'. url("filedownload?p=$data->file_surat_persetujuan_magang") .'">
                                <i class="la la-file"></i> '.basename($data->file_surat_persetujuan_magang).'
                            </a>';
                }
                return '<span class="kt-badge kt-badge--danger kt-badge--inline">belum upload!</span>';
            })
            ->editColumn('tgl_pengajuan', function($data){
                return Carbon::createFromFormat('Y-m-d H:i:s', $data->pengajuan->submitted_at)->format('d M Y');
            })
            ->addColumn('action', function ($data) {
                if (auth()->user()->hasAnyPermission(['Surat persetujuan magang - edit'])) {
                    return '<a href="' . route('surat_persetujuan.edit', $data->id) . '"class="btn btn-outline-hover-info btn-icon btn-circle btn-sm btnEdit" data-toggle="kt-tooltip" title="Upload" data-skin="brand"><i class="la la-upload kt-font-info"></i></a>';

                }
                return '';
            })
            ->rawColumns(['action', 'waktu_magang', 'upload_file'])
            ->make(true);
    }

    public function mail($idMagang)
    {       
        $model = Magang::with('pengajuan.pendaftar')->findOrFail($idMagang);
        $data_pengajuan_sama = Magang::getPengajuanSama($model['id_pengajuan']);
        if($data_pengajuan_sama != false)
        {
            $pendaftar = $model->pengajuan->pendaftar;
            Mail::to($pendaftar->email)
                ->queue(new MailSuratPersetujuanMagang($data_pengajuan_sama->get()));
        }    
    }

    /**
     * Handle file store
     *
     * @param $request
     * @param $model
     */
    private function storeFiles($request, $model)
    {
        foreach ($request->files as $key => $file) {
            if ($request->file($key)->isValid()) {
                Storage::delete($model->$key);
                $model->$key = $request->file($key)->storeAs("Magang/$key/$model->id", $file->getClientOriginalName(), 'local');
                $model->save();
            }
        }
    }
}
