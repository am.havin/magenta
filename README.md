Genelator - Laravel 6 CRUD Generator & Starterkit with Metronic admin template.

This generator gives you:
1. CRUD with ajax modal & ajax form submit
2. Dhtml Form validation using jqueryValidation plugin
3. Ajax datatable on Index Page 
4. Datable filterable by field
5. Migration file
6. Permission Seeder file
7. Rollback capability of all generated files!!
8. Yaml based configuration

This starterkit also gives you:
1. User management 
2. Roles & Permission management
3. Menu access management
4. Yaml based menu

by Adla M Havin - 2019