<?php

namespace App\Http\View\Composers;

use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Symfony\Component\Yaml\Yaml;

class FrontmenuComposer
{
    private $yamlData;

    public function __construct()
    {
        $yamlFilename = (auth()->guard('pendaftar')->check()) ? 'frontmenu_loggedin' : 'frontmenu';
        $yamlFileLoc    = app_path("Yamls/menus/$yamlFilename.yaml");
        $this->yamlData = Yaml::parse(file_get_contents($yamlFileLoc));
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('vcFrontmenuItems', $this->yamlData);
    }
}
