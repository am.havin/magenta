<?php

use Illuminate\Database\Seeder;

class IndexNilaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['nilai_mutu' => 'A', 'nilai_awal' => 90, 'nilai_akhir' => 100, 'standar_kualitas' => 'Istimewa'],
            ['nilai_mutu' => 'B', 'nilai_awal' => 80, 'nilai_akhir' => 89, 'standar_kualitas' => 'Sangat Baik'],
            ['nilai_mutu' => 'C', 'nilai_awal' => 70, 'nilai_akhir' => 79, 'standar_kualitas' => 'Baik'],
            ['nilai_mutu' => 'D', 'nilai_awal' => 60, 'nilai_akhir' => 69, 'standar_kualitas' => 'Kurang'],
            ['nilai_mutu' => 'E', 'nilai_awal' => 0, 'nilai_akhir' => 59, 'standar_kualitas' => 'Sangat Kurang'],
        ];

        foreach($data as $val){
            \App\IndexNilai\IndexNilai::updateOrCreate($val, $val);
        }
    }
}
