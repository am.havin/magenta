<div class="kt-footer  kt-footer--extended  kt-grid__item" id="kt_footer">
    {{--<div class="kt-footer__top">--}}
        {{--<div class="kt-container ">--}}
            {{--<div class="row">--}}
                {{--<div class="col-lg-4">--}}
                    {{--<div class="kt-footer__section">--}}
                        {{--<h3 class="kt-footer__title">Tentang</h3>--}}
                        {{--<div class="kt-footer__content">--}}
                            {{--Whatever method you chosse,these nine will--}}
                            {{--make you re-think theway you craft healines--}}
                            {{--Why? Because they all have points in that are--}}
                            {{--worth from a point of view.--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-lg-4">--}}
                    {{--<div class="kt-footer__section">--}}
                        {{--<h3 class="kt-footer__title">Link Terkait</h3>--}}
                        {{--<div class="kt-footer__content">--}}
                            {{--<div class="kt-footer__nav">--}}
                                {{--<div class="kt-footer__nav-section">--}}
                                    {{--<a href="#">Portal Publik</a>--}}
                                    {{--<a href="#">SIMANIS</a>--}}
                                    {{--<a href="#">SILABA</a>--}}
                                {{--</div>--}}
                                {{--<div class="kt-footer__nav-section">--}}
                                    {{--<a href="#">PPID</a>--}}
                                    {{--<a href="#">SINADINE</a>--}}
                                    {{--<a href="#">SIBAGAS</a>--}}
                                {{--</div>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-lg-4">--}}
                    {{--<div class="kt-footer__section">--}}
                        {{--<h3 class="kt-footer__title">Saran & Masukan Singkat</h3>--}}
                        {{--<div class="kt-footer__content">--}}
                            {{--<form action="" class="kt-footer__subscribe">--}}
                                {{--<div class="input-group">--}}
                                    {{--<input type="text" class="form-control" placeholder="Saran & masukan anda">--}}
                                    {{--<div class="input-group-append">--}}
                                        {{--<button class="btn btn-brand" type="button">Kirim</button>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</form>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <div class="kt-footer__bottom">
        <div class="kt-container ">
            <div class="kt-footer__wrapper">
                <div class="kt-footer__copyright">
                    &copy; {{ date('Y') }} <a href="http://bumn.go.id" target="_blank">Kementerian Badan Usaha Milik Negara</a>
                </div>
                <div class="kt-footer__menu">
                    {{--<a href="mailto:adla.mohammad@bumn.go.id?subject=Pertanyaan tentang aplikasi Audit KBUMN" target="_blank">contact developer</a>--}}
                </div>
            </div>
        </div>
    </div>
</div>
