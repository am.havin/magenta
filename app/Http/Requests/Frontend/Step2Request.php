<?php

namespace App\Http\Requests\Frontend;

use App\Alokasi\Alokasi;
use App\Helper;
use App\Magang\Magang;
use App\Pengajuan\Pengajuan;
use App\Peserta\Peserta;
use App\Rules\MaxDayDuration;
use App\Rules\MinDayDuration;
use App\Rules\TidakBolehMotongTglYgDisabled;
use App\Toastr;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class Step2Request extends FormRequest
{
    public $rules = [
        'nama_pejabat_berwenang' => 'required|min:3',
        'jabatan' => 'required|min:4',
        'alamat_instansi' => 'required|min:6',
        'file_surat_pengantar_resmi' => 'required|max:512|mimes:pdf',
        'file_proposal' => 'max:512|mimes:pdf',
        'agreement' => 'required',
    ];

    public function getEditRules()
    {
        $rules = $this->rules;
        $rules['file_surat_pengantar_resmi'] = 'max:512|mimes:pdf';
        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //cek kepemilikan pengajuan & memang belum pernah disubmit sebelumnya
        $idPengajuan = array_values(request()->route()->originalParameters())[0];
        $pendaftar = auth('pendaftar')->user();
        $pengajuan = Pengajuan::where('id', $idPengajuan)
            ->with('magangs')
            ->where('id_pendaftar', $pendaftar->id)
            ->first();
        $return = ($pengajuan) ? true : false;

        return $return;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $controllerMethod = request()->route()->getActionMethod();
        if ($controllerMethod == 'update') {
            return $this->getEditRules();
        }
        return [];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [];
    }
}
