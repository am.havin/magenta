<?php

namespace App;

class RolemenuHelper
{
    public static function isChecked($rolesMenus, $menuId, $roleId)
    {
        $res = $rolesMenus->where('menu_id', $menuId)->where('role_id', $roleId);
        return ($res->count()) ? true : false;
    }
}
