@extends((Request::ajax()) ? 'templates.layout-blank' : 'templates.'.config('genelator.template').'.layout')

@section('subheaderToolbar')
@endsection

@section('vendorPluginForCustomPagesStyles')
    @include('templates.metronic.demo5.includes.datatable_css')
@endsection

@section('content')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-list-2"></i>
            </span>
                <h3 class="kt-portlet__head-title">
                    List data Akun Pendaftar
                </h3>
            </div>
            <div class="kt-portlet__head-toolbar">
                <div class="kt-portlet__head-wrapper">
                    <div class="kt-portlet__head-actions">
                        @if(auth()->user()->can('Pendaftar - create'))
                            <a href="{{ url('pendaftar/create') }}"
                               class="btn btn-sm btn-warning btn-elevate btn-icon-sm btnAdd">
                                <i class="la la-plus"></i>
                                Tambah {{ $title }}
                            </a>
                        @endif
                        <button type="button" class="btn btn-sm btn-success btn-elevate btn-icon" id="btnToggleFilter"
                                data-toggle="kt-tooltip" data-placement="top" title=""
                                data-original-title="Search / Filter list data"
                                data-skin="brand">
                            <i class="la la-search"></i>
                        </button>
                        <button type="button" class="btn btn-sm btn-info btn-elevate btn-icon" id="btnReloadDt"
                                data-toggle="kt-tooltip" data-placement="top" title=""
                                data-original-title="Refresh list data"
                                data-skin="brand">
                            <i class="la la-refresh"></i>
                        </button>
                    </div>
                </div>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Filter for datatable-->
            <div id="sectionFilter" class="kt-section" style="display: none;">
                <div class="kt-section__content kt-section__content--solid">
                    <form id="filterForm" autocomplete="off">
                        <h5>Filter:</h5>
                        <div class="form-group row">
                            <div class="col-lg-3">
                                <label class="form-control-label"> Email:</label>
                                <input type="text" name="email" id="filter_email" class="form-control" value="">
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-3">
                                <label class="form-control-label"> Nama lengkap:</label>
                                <input type="text" name="nama_lengkap" id="filter_nama_lengkap" class="form-control"
                                       value="">
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-3">
                                <label class="form-control-label"> Nomor Handphone:</label>
                                <input type="text" name="no_hp" id="filter_no_hp" class="form-control" value="">
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-3">
                                <label class="form-control-label"> Sekolah/Universitas:</label>
                                <select name="id_sekolah" id="filter_id_sekolah" class="form-control select2ajax"
                                        data-ajax--url="{{ url('select2/roles') }}" data-placeholder="Pilih"
                                        data-allow-clear="true">
                                    <option></option>
                                </select>
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="w-100"></div>
                            <br>

                        </div>
                        <hr>
                        <div class="row">
                            <div class="col-lg-12 text-right">
                                <button type="reset" class="btn btn-wide btn-sm btn-clean">Clear</button>
                                <button type="submit" class="btn btn-wide btn-sm btn-outline-success">Filter</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <!--end: Filter for datatable-->

            <!--begin: Datatable -->
            <table class="table table-striped- table-hover table-checkable" id="datatable">
                <thead>
                <tr>
                    <th>Email</th>
                    <th>Nama lengkap</th>
                    <th>Nomor Handphone</th>
                    <th>id_sekolah</th>
                    <th>Sekolah/Universitas</th>
                    <th width="80px">Email terverifikasi?</th>
                    <th width="80px">Blokir?</th>
                    <th width="100px;">Action</th>
                </tr>
                </thead>
            </table>
            <!--end: Datatable -->

        </div>
    </div>

    <!-- Modal Container-->
    <div class="modal fade" id="modal" tabindex="-1"></div>
    <!--end: Modal Container-->
@endsection

@section('vendorPluginForCustomPagesScripts')
    @include('templates.metronic.demo5.includes.datatable_js')
@endsection

@section('pageScripts')
    <script>
        jQuery(document).ready(function () {
            datatable = $('#datatable').DataTable({
                dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
                processing: true,
                serverSide: true,
                responsive: true,
                ajax: {
                    url: "{{ url('pendaftar/datatable') }}",
                    data: function (d) {
                        d.filter_email = $('#filter_email').val();
                        d.filter_nama_lengkap = $('#filter_nama_lengkap').val();
                        d.filter_no_hp = $('#filter_no_hp').val();
                        d.filter_id_sekolah = $('#filter_id_sekolah').val();
                    }
                },
                columns: [
                    {data: 'email', name: 'email'},
                    {data: 'nama_lengkap', name: 'nama_lengkap'},
                    {data: 'no_hp', name: 'no_hp'},
                    {data: 'id_sekolah', name: 'id_sekolah', visible: false},
                    {data: 'sekolah', name: 'sekolah'},
                    {data: 'email_terverifikasi', name: 'email_terverifikasi'},
                    {data: 'is_banned', name: 'is_banned'},

                    {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'},
                ],
                drawCallback: function (settings) {
                    KTApp.initTooltip($('[data-toggle="kt-tooltip"]'));
                }
            });

            //INIT DATATABLE'S BUTTONS
            initDatatableFilterAndRefreshButton(datatable); //check function in custom.js
            initDeleteButton(datatable); //check function in custom.js

            //INIT MODAL
            initAjaxModalButton('a.btnAdd, a.btnEdit');
        });
    </script>
@endsection