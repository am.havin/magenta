<?php

return [
    /*
    |--------------------------------------------------------------------------
    | Template
    |--------------------------------------------------------------------------
    |
    | template files are located in these 2 location:
    | 1. public/templates/*
    | 2. resource/views/templates/*
    |
    | that being said, here you can choose which template do you want to use:
    */

    'template' => env('GENELATOR_TEMPLATE', 'metronic.demo1'),
];
