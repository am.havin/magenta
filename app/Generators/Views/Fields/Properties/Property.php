<?php

namespace App\Generators\Views\Fields\Properties;

class Property
{
    public static function getRequired($nullable, $overrideRequired)
    {
        $required = false;
        if(!$nullable){
            $required = true;
        }
        $required = isset($overrideRequired) ? $overrideRequired : $required;

        return $required;
    }
}
