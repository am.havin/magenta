<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SuratPersetujuanRequest extends FormRequest
{
    public $rules = [
        'file_surat_persetujuan_magang' => 'required|max:512|mimes:pdf'
    ];

    public function getCreateRules()
    {
        return $this->rules;
    }

    public function getEditRules()
    {
        return  $this->rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch (request()->route()->getActionMethod()) {
            case 'index':
                return auth('web')->user()->hasAnyPermission(['Surat persetujuan magang - index']);
                break;
            case 'edit':
                return auth('web')->user()->hasAnyPermission(['Surat persetujuan magang - edit']);
                break;
            case 'update':
                return auth('web')->user()->hasAnyPermission(['Surat persetujuan magang - edit']);
                break;
            case 'datatable':
                return auth('web')->user()->hasAnyPermission(['Surat persetujuan magang - index']);
                break;
            default:
                return false;
                break;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $controllerMethod = request()->route()->getActionMethod();
        if ($controllerMethod == 'update') {
            return $this->getEditRules();
        }
        return [];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //'title.required' => 'A title is required',
            //'body.required'  => 'A message is required',
        ];
    }
}
