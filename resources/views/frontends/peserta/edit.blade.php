@extends('templates.layout-blank')


@section('content')
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-body">
            <div class="modal-content">
                <form method="POST" action="{{ route('peserta_magang.update', $peserta->id) }}" id="createForm" autocomplete="off" enctype="multipart/form-data">
                    @method('PUT')
                    @csrf

                    <div class="modal-header">
                        <h5 class="modal-title" id="">{{ $title }}</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <h3>Detail peserta</h3>

                        {{--NAMA LENGKAP & EMAIL--}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nama"><span class="text-danger">*</span> Nama Lengkap</label>
                                    <input id="nama" type="text" class="form-control" name="nama" value="{{ $peserta->nama }}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="email"><span class="text-danger">*</span> Email</label>
                                    <input id="email" type="email" class="form-control" name="email" value="{{ $peserta->email }}" required>
                                </div>
                            </div>
                        </div>

                        {{--KTP & NIM/NIS--}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="no_ktp"><span class="text-danger">*</span> Nomor KTP</label>
                                    <input id="no_ktp" min="0" type="number" class="form-control" name="no_ktp" value="{{ $peserta->no_ktp }}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nim_nis"><span class="text-danger">*</span> NIM / NIS</label>
                                    <input id="nim_nis" type="text" class="form-control" name="nim_nis" value="{{ $peserta->nim_nis }}" required>
                                </div>
                            </div>
                        </div>

                        {{--TEMPAT & TGL LAHIR--}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="no_ktp"><span class="text-danger">*</span> Kota Lahir</label>
                                    <input id="tempat_lahir" type="text" class="form-control" name="tempat_lahir" value="{{ $peserta->tempat_lahir }}" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="nim_nis"><span class="text-danger">*</span> Tgl Lahir</label>
                                    <input id="tgl_lahir" type="text" class="form-control datepicker" name="tgl_lahir" value="{{ $peserta->carbonTglLahir->format('Y-m-d') }}" required>
                                </div>
                            </div>
                        </div>

                        <hr>

                        {{--CHECKBOX--}}
                        <div class="row">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="" style="display: block;"><span class="text-danger">*</span> Tingkat Pendidikan</label>
                                    <div class="form-check">
                                        <input class="form-check-input is_universitas_cb" type="radio" name="is_universitas" id="is_universitas_true" value="1" @if($peserta->is_universitas) checked @endif>
                                        <label class="form-check-label" for="is_universitas_true">
                                            Universitas
                                        </label>
                                    </div>
                                    <div class="form-check">
                                        <input class="form-check-input is_universitas_cb" type="radio" name="is_universitas" id="is_universitas_false" value="0" @if(!$peserta->is_universitas) checked @endif>
                                        <label class="form-check-label" for="is_universitas_false">
                                            SMA-SMK
                                        </label>
                                    </div>
                                </div>
                            </div>
                        </div>

                        {{--SEKOLAH--}}
                        <div class="row" id="rowSekolah">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="id_sekolah" id="labelSekolah" style="display: block;"><span class="text-danger">*</span>{{ ($peserta->is_universitas) ? 'Nama Universitas' : 'Nama SMA-SMK' }}</label>
                                    <select name="id_sekolah" id="id_sekolah" class="form-control" tabindex="-98" required>
                                        @if($sekolah)
                                            <option value="{{ $sekolah->id }}" selected>{{ $sekolah->nama }}</option>
                                        @else
                                            <option value="0" selected>Lain-lain</option>
                                        @endif
                                    </select>
                                    <small id="" class="form-text text-muted">Jika nama sekolah tidak ada dalam pilihan,
                                        maka pilih "lain-lain" lalu isikan nama sekolah/universitas nya
                                    </small>
                                </div>
                            </div>
                            <div class="col-md-6" style="@if($sekolah) display: none; @endif">
                                <div class="form-group">
                                    <label for="sekolah" style="display: block; visibility: hidden;"><span class="text-danger">*</span> Nama sekolah/universitas</label>
                                    <input id="sekolah" type="text"
                                           class="form-control form-control-sm"
                                           name="sekolah" value="@if(!$sekolah) {{ $peserta->sekolah }} @endif"
                                           placeholder="isi nama sekolah/universitas">
                                </div>
                            </div>
                        </div>


                        {{--PROGRAM STUDI--}}
                        <div class="row" id="rowProgramStudi">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label for="id_programstudi" style="display: block;"><span class="text-danger">*</span> Program Studi</label>
                                    <select name="id_programstudi" id="id_programstudi" class="form-control-sm" style="width: 100%;" tabindex="-98" required>
                                        @if($programstudi)
                                            <option value="{{ $programstudi->id }}" selected>{{ $programstudi->nama }}</option>
                                        @else
                                            <option value="0" selected>Lain-lain</option>
                                        @endif
                                    </select>
                                    <small id="" class="form-text text-muted">Jika nama program studi tidak ada dalam pilihan,
                                        maka pilih "lain-lain" lalu isikan sesuai yang diinginkan
                                    </small>
                                </div>
                            </div>
                            <div class="col-md-6" style="@if($programstudi) display: none; @endif">
                                <div class="form-group">
                                    <label for="programstudi" style="display: block; visibility: hidden;"><span class="text-danger">*</span> Program Studi</label>
                                    <input id="programstudi" type="text"
                                           class="form-control form-control-sm"
                                           name="programstudi" value="@if(!$programstudi) {{ $peserta->programstudi }} @endif"
                                           placeholder="isi nama program studi">
                                    <small id="" class="form-text text-muted">
                                        Cth: S1 Sistem Informasi / IPA / Elektronika Industri
                                    </small>
                                </div>
                            </div>
                        </div>

                        {{--IPK--}}
                        <div class="row" id="rowIpk">
                            <div class="col-md-4">
                                <label for="ipk" style="display: block; ">
                                    <span class="text-danger">*</span>
                                    <span id="labelIpk">
                                        {{ ($peserta->is_universitas) ? 'IPK' : 'Nilai Rapot Terakhir' }}
                                    </span>
                                </label>
                                <input id="ipk" type="number"
                                       class="form-control form-control-sm"
                                       name="ipk" min="0" value="{{ $peserta->ipk }}">
                            </div>
                        </div>

                        <hr>

                        {{--CV--}}
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="foto">Foto</label>
                                    <input type="file" name="foto" class="form-control-file" id="foto">
                                    <small id="" class="form-text text-muted">
                                        File .jpg .jpeg .png berukuran maksimal 512KB <br>
                                        Upload ulang untuk menggati foto yang lama
                                    </small>
                                </div>
                            </div>
                            <div class="col-md-2">
                                <img src="{{ route('peserta_magang.foto', $peserta->id) }}?rand={{ rand(10,1000) }}" alt="">
                            </div>
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="file_cv">Curicullum Vitae</label>
                                    <input type="file" name="file_cv" class="form-control-file" id="file_cv">
                                    <small id="" class="form-text text-muted">
                                        File .pdf berukuran maksimal 2MB. <br>Upload ulang akan mengganti CV yang sebelumnya diupload.
                                    </small>
                                </div>
                            </div>
                            <div class="col-md-2">
                                @if($peserta->file_cv)
                                    <a href="{{ route('peserta_magang.cv', $peserta->id) }}">
                                        <i class="icon-file-pdf" style="font-size: 52px;"></i> <br>
                                        <small style="word-wrap: break-word;">{{ basename($peserta->file_cv) }}</small>
                                    </a>
                                @endif
                            </div>
                        </div>

                        {{--SKCK--}}
                        <div class="row">
                            <div class="col-md-4">
                                <div class="form-group">
                                    <label for="file_skck"><span class="text-danger">*</span> SKCK</label>
                                    <input type="file" name="file_skck" class="form-control-file" id="file_skck">
                                    <small id="" class="form-text text-muted">
                                        SKCK untuk mahasiswa / Surat Kelakuan Baik untuk siswa. <br>
                                        File .pdf berukuran maksimal 256KB
                                    </small>
                                </div>
                            </div>
                            <div class="col-md-2 text-center">
                                @if($peserta->file_skck)
                                    <a href="{{ route('peserta_magang.skck', $peserta->id) }}">
                                        <i class="icon-file-pdf" style="font-size: 52px;"></i> <br>
                                        <small style="word-wrap: break-word;">{{ basename($peserta->file_skck) }}</small>
                                    </a>
                                @endif
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="submit"
                                class="button button-3d button-small button-rounded button-lime button-light"><i
                                    class="icon-check"></i> Update
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection


@section('pageScripts')
    {!! $validator->selector('#createForm') !!}
    <script>
        $.fn.datepicker.dates.id = {
            days: ["Minggu", "Senin", "Selasa", "Rabu", "Kamis", "Jumat", "Sabtu"],
            daysShort: ["Mgu", "Sen", "Sel", "Rab", "Kam", "Jum", "Sab"],
            daysMin: ["Mg", "Sn", "Sl", "Ra", "Ka", "Ju", "Sa"],
            months: ["Januari", "Februari", "Maret", "April", "Mei", "Juni", "Juli", "Agustus", "September", "Oktober", "November", "Desember"],
            monthsShort: ["Jan", "Feb", "Mar", "Apr", "Mei", "Jun", "Jul", "Ags", "Sep", "Okt", "Nov", "Des"],
            today: "Hari Ini",
            clear: "Kosongkan"
        }
    </script>


    <script>
        jQuery(document).ready(function () {
            if (!(typeof datatable === "undefined")) {
                //pass instance datatable (optional) agar di-reload ketika submit formnya sukses
                initAjaxForm('#createForm', datatable); //datatable dapat dari index.blade.php
            }

            // LOGIC START HERE
            var select2Sekolah;
            var select2Prodi;
            var labelSekolah;
            var labelIpk;
            var isUniversitas = $(this).val();

            initIdSekolah();
            initIdProdi({{ ($sekolah) ? $sekolah->id : 0 }}, true);

            // TANGGAL LAHIR
            $('#tgl_lahir').datepicker({
                language: 'id',
                format: 'yyyy-mm-dd',
                endDate: new Date(),
            })

            // HIDE TEXT SEKOLAH
            function hideTextSekolah(){
                $('#sekolah').parent().parent().hide('fast');
                $('#sekolah').val('').text('').prop('required', false);
            }
            // HIDE TEXT PRODI
            function hideTextProdi(){
                $('#programstudi').parent().parent().hide('fast');
                $('#programstudi').val('').text('').prop('required', false);
            }

            // SHOW TEXT SEKOLAH
            function showTextSekolah(){
                $('#sekolah').parent().parent().show('fast');
                $('#sekolah').prop('required', true);
            }
            // SHOW TEXT PRODI
            function showTextProdi(){
                $('#programstudi').parent().parent().show('fast');
                $('#programstudi').prop('required', true);
            }

            // TOGGLE
            $('.is_universitas_cb').change(function(){
                //hide dulu setiap trigger
                $('#rowSekolah, #rowProgramStudi, #rowIpk').hide('fast');

                //set values
                isUniversitas = $(this).val();
                labelSekolah = (isUniversitas == 1) ? 'Nama Universitas' : 'Nama SMA/SMK';
                labelIpk = (isUniversitas == 1) ? 'IPK' : 'Nilai Rapot Terakhir';
                let willShowElements = '#rowSekolah, #rowProgramStudi, #rowIpk';

                //execute
                $('#labelSekolah').text(labelSekolah);
                $('#labelIpk').text(labelIpk);
                $(willShowElements).show('fast');
                destroySelect2Sekolah();
                destroySelect2Prodi();
                initIdSekolah();
            });

            // INIT SELECT2 ID_SEKOLAH
            function initIdSekolah(){
                select2Sekolah = $('#id_sekolah').select2({
                    placeholder: 'Pilih',
                    allowClear: false,
                    ajax: {
                        url: '{{ url('pub_select2/sekolah_with_lain_lain') }}',
                        dataType: 'json',
                        delay: 1000
                    },
                }).on('select2:select', function (event) {
                    let val = $(this).val();
                    if(val == 0){ // = 0 itu lain-lain
                        showTextSekolah();
                        initIdProdi(val, false);
                        hideTextProdi();
                    }else{
                        hideTextSekolah();
                        hideTextProdi();
                    }
                    initIdProdi(val, false);
                }).on('select2:unselect', function (event) {
                    hideTextSekolah();
                });
            }

            // INIT SELECT2 ID_PROGRAMSTUDI
            function initIdProdi(idSekolah, hasDefaulValue){
                if(!hasDefaulValue) {
                    $('#id_programstudi').val([]);
                }

                $('#id_programstudi').prop('disabled', false);
                select2Prodi = $('#id_programstudi').select2({
                    placeholder: 'Pilih',
                    allowClear: false,
                    ajax: {
                        url: '{{ url('pub_select2/programstudi_with_lain_lain') }}' + '?id_sekolah=' + idSekolah,
                        dataType: 'json',
                    },
                }).on('select2:select', function (event) {
                    let val = $(this).val();
                    if(val == 0){ // = 0 itu lain-lain
                        showTextProdi();
                    }else{
                        hideTextProdi();
                    }
                }).on('select2:unselect', function (event) {
                    hideTextProdi();
                });
            }

            //DESTROY SELECT2 SEKOLAH
            function destroySelect2Sekolah(){
                select2Sekolah.val([]);
                select2Sekolah.select2('destroy');
                hideTextSekolah();
            }

            //DESTROY SELECT2 ID_PROGRAMSTUDI
            function destroySelect2Prodi(){
                if ($('#id_programstudi').hasClass("select2-hidden-accessible")){
                    select2Prodi.val([]);
                    select2Prodi.select2('destroy');
                    $('#id_programstudi').prop('disabled', true);
                    hideTextProdi();
                }
            }
        });
    </script>
@endsection


{{-- For Modal --}}
@if(Request::ajax())
    @yield('content')
    @yield('pageScripts')
@endif