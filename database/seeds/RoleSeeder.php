<?php

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['name' => 'superadmin'],
            ['name' => 'admin'],
            ['name' => 'user']
        ];

        foreach($data as $val){
            \App\Role::updateOrCreate($val, $val);
        }
    }
}
