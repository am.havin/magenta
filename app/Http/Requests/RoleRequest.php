<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class RoleRequest extends FormRequest
{
    public $rules = [
        'name' => 'required|unique:roles',
        'guard_name' => 'required',
    ];

    public function getCreateRules()
    {
        return $this->rules;
    }

    public function getEditRules()
    {
        $this->rules['name'] = [
            'required',
            Rule::unique('roles')->ignore(array_values(request()->route()->originalParameters())[0]),
        ];
        return  $this->rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        switch (request()->route()->getActionMethod()) {
            case 'index':
                return auth('web')->user()->hasAnyPermission(['roles - index']);
                break;
            case 'create':
                return auth('web')->user()->hasAnyPermission(['roles - create']);
                break;
            case 'store':
                return auth('web')->user()->hasAnyPermission(['roles - create']);
                break;
            case 'show':
                return false;
                break;
            case 'edit':
                return auth('web')->user()->hasAnyPermission(['roles - edit']);
                break;
            case 'update':
                return auth('web')->user()->hasAnyPermission(['roles - edit']);
                break;
            case 'destroy':
                return auth('web')->user()->hasAnyPermission(['roles - delete']);
                break;
            case 'datatable':
                return auth('web')->user()->hasAnyPermission(['roles - index']);
                break;
            default:
                return false;
                break;
        }
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $controllerMethod = request()->route()->getActionMethod();
        if ($controllerMethod == 'store') {
            return $this->getCreateRules();
        }
        if ($controllerMethod == 'update') {
            return $this->getEditRules();
        }
        return [];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            //'title.required' => 'A title is required',
            //'body.required'  => 'A message is required',
        ];
    }
}
