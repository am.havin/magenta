@extends((Request::ajax()) ? 'templates.layout-blank' : 'templates.'.config('genelator.template').'.layout')

@section('subheaderToolbar')
    <a href="#" class="btn invisible">
    </a>
@endsection

@section('content')
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title">{{ $title }}</h5>
                <button id="closeBtn" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                @if($errors->isNotEmpty())
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-warning" role="alert">
                                <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                <div class="alert-text">
                                    <ul>
                                        @foreach($errors->all() as $message)
                                            <li>{{ $message }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-lg-12">
                        <div class="kt-portlet">

                            <!--begin::Form-->
                            <form action="{{ route('pendaftar.update', $model->id) }}" id="editForm" method="post"
                                  class="kt-form" autocomplete="off" enctype="multipart/form-data">
                                {{ csrf_field() }}
                                <input type="hidden" name="_method" value="PUT">

                                <div class="kt-portlet__body">
                                    <div class="form-group row">
                                        <div class="col-lg-6">
                                            <label class="form-control-label">* Email:</label>
                                            <input type="text" name="" id="" class="form-control"
                                                   value="{{ $model->email }}" disabled>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="form-control-label">* Password:</label>
                                            <input type="password" name="password" id="password" class="form-control" value="">
                                            <span class="form-text text-muted">Isi password baru untuk mengganti yang lama</span>
                                        </div>
                                        <div class="w-100"></div>
                                        <br>
                                        <div class="col-lg-6">
                                            <label class="form-control-label">* Nama lengkap:</label>
                                            <input type="text" name="nama_lengkap" id="nama_lengkap"
                                                   class="form-control" value="{{ $model->nama_lengkap }}" required>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="form-control-label">* Nomor Handphone:</label>
                                            <input type="text" name="no_hp" id="no_hp" class="form-control"
                                                   value="{{ $model->no_hp }}" required>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="w-100"></div>
                                        <br>
                                        <div class="col-lg-6">
                                            <label class="form-control-label">* Sekolah/Universitas:</label>
                                            <select name="id_sekolah" id="id_sekolah" class="form-control select2ajax"
                                                    required data-ajax--url="{{ url('pub_select2/allsekolah') }}"
                                                    data-placeholder="Pilih" data-allow-clear="false">
                                                <option value="{{ $model->id_sekolah }}">{{ $model->sekolah }}
                                                </option>
                                            </select>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="form-control-label" style="visibility: hidden;">Checkbox</label>
                                            <label class="kt-checkbox">
                                                <input type="checkbox" value="1" name="is_banned" {{ ($model->is_banned) ? 'checked' : '' }}> Blokir pendaftar?
                                                <span></span>
                                            </label>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="col-lg-3">
                                            <label class="form-control-label" style="visibility: hidden;">Checkbox</label>
                                            <label class="kt-checkbox">
                                                <input type="checkbox" value="1" name="email_verified_at" {{ ($model->email_verified_at) ? 'checked' : '' }}> Email terverifikasi?
                                                <span></span>
                                            </label>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="kt-portlet__foot">
                                    <div class="kt-form__actions">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <button type="submit" class="btn btn-warning">Submit</button>
                                                {{-- <a href="{{ url('pendaftar/') }}" class="btn btn btn-secondary">Cancel</a> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!--end::Form-->
                        </div>
                        <!--end::Portlet-->
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection


@section('pageScripts')
    {!! $validator->selector('#editForm') !!}
    <script>
        jQuery(document).ready(function () {
            initSelect2();
            initDatepicker();
            if (!(typeof datatable === "undefined")) {
                //pass instance datatable (optional) agar di-reload ketika submit formnya sukses
                initAjaxForm('#editForm', datatable); //datatable dapat dari index.blade.php
            }
        });
    </script>
@endsection


{{-- For Modal --}}
@if(Request::ajax())
    @yield('content')
    @yield('pageScripts')
@endif