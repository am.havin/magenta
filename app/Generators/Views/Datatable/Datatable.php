<?php

namespace App\Generators\Views\Datatable;

class Datatable
{
    private $stubString;
    private $fieldsData;

    /**
     * Create constructor.
     * @param $fieldsData
     */
    public function __construct($fieldsData)
    {
        $stubFileLoc        = resource_path('stubs/views/datatables/datatable.stub');
        $this->stubString   = file_get_contents($stubFileLoc);
        $this->fieldsData   = $fieldsData;
    }

    public function generate()
    {
        $fieldsString = $this->generateFields($this->fieldsData);
        return $this->replaceStubString($fieldsString);
    }

    private function generateFields($fields)
    {
        $return = '';
        foreach($fields as $key => $value){
            $return .= "                    <th>".$value['label']."</th>\r\n";
        }
        return $return;
    }

    /**
     * @param $fieldsString
     * @return mixed
     */
    private function replaceStubString($fieldsString)
    {
        $replacedString = str_replace(
            [
                '{{fields}}',
            ],
            [
                $fieldsString,
            ],
            $this->stubString
        );
        return $replacedString;
    }
}
