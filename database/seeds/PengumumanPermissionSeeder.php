<?php

use Illuminate\Database\Seeder;

class PengumumanPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['group' => 'Pengumuman', 'name' => 'Pengumuman - index', 'guard_name' => 'web'],
            ['group' => 'Pengumuman', 'name' => 'Pengumuman - create', 'guard_name' => 'web'],
            ['group' => 'Pengumuman', 'name' => 'Pengumuman - show', 'guard_name' => 'web'],
            ['group' => 'Pengumuman', 'name' => 'Pengumuman - edit', 'guard_name' => 'web'],
            ['group' => 'Pengumuman', 'name' => 'Pengumuman - delete', 'guard_name' => 'web'],
        ];

        foreach($data as $val){
            \Spatie\Permission\Models\Permission::updateOrCreate($val, $val);
        }
    }
}
