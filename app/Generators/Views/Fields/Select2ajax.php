<?php

namespace App\Generators\Views\Fields;

use App\Generators\Views\Fields\Properties\Property;

class Select2ajax
{
    private $stubString;
    private $name;
    private $options;
    private $colSize;
    private $overrideRequired;
    private $withValue;
    private $elementId;

    public function __construct($name, $options, $colSize = 6, $overrideRequired = null, $withValue = false, $elementId = null)
    {
        $this->stubString  = file_get_contents(resource_path('stubs/views/fields/select2ajax.stub'));
        $this->name = $name;
        $this->options = $options;
        $this->colSize = $colSize;
        $this->overrideRequired = $overrideRequired;
        $this->withValue = $withValue;
        $this->elementId = $elementId;
    }

    /**
     * @return string
     */
    public function generate()
    {
        $required = Property::getRequired($this->options['nullable'], $this->overrideRequired);

        return $replacedString = str_replace(
            [
                '{{colSize}}',
                '{{label}}',
                '{{name}}',
                '{{asterisk}}',
                '{{required}}',
                '{{attributes}}',
                '{{helptext}}',
                '{{multiple}}',
                '{{url}}',
                '{{value}}',
                '{{elementId}}'
            ],
            [
                $this->colSize,
                $this->options['label'],
                $this->name,
                ($required) ? '*' : '',
                ($required) ? 'required' : '',
                isset($this->options['html_attributes']) ? $this->options['html_attributes'] : '',
                isset($this->options['helptext']) ? $this->options['helptext'] : '',
                (isset($this->options['multiple']) && $this->options['multiple']) ? 'multiple' : '',
                isset($this->options['url']) ? $this->options['url'] : '',
                ($this->withValue) ? $this->generateValue() : '<option></option>',
                ($this->elementId) ? $this->elementId : $this->name,
            ],
            $this->stubString
        );
    }
    private function generateValue()
    {
        return '<option value="{{ $model->'.$this->name.' }}">Please manually set this text</option>';
    }
}
