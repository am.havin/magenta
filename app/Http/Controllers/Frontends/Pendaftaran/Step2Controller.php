<?php

namespace App\Http\Controllers\Frontends\Pendaftaran;

use App\Helper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Step2Request;
use App\Pengajuan\Pengajuan;
use App\Toastr;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;


class Step2Controller extends Controller
{
    protected $pendaftar;

    public function __construct()
    {
        $this->middleware('mustCompleteStep1');

        Helper::findThenHandleExpired(); //untuk check pengajuan magang yg udah expired

        //harus begini karena auth()->user() nge-return null kalau belum ngelewatin middleware
        $this->middleware(function ($request, $next) {
            $this->pendaftar= auth('pendaftar')->user();
            return $next($request);
        });
    }

    public function index(Step2Request $request, $pengajuanId)
    {
        $title = 'Proses Pengajuan - Kirim Pengajuan';
        $activeMenus = ['menuPendaftaran'];
        $pengajuan = Pengajuan::findOrFail($pengajuanId);
        $validator = \JsValidator::make($request->getEditRules(), $request->messages());

        $viewData = compact('title', 'activeMenus', 'pengajuan', 'validator');
        return view('frontends.pendaftaran.step2.index', $viewData);
    }

    public function update(Step2Request $request, $pengajuanId)
    {
        $pengajuan = Pengajuan::where('id', $pengajuanId)
            ->where('id_pendaftar', $this->pendaftar->id)
            ->where('is_submitted', false)
            ->first();
        if($pengajuan){
            $pengajuan->update([
                'nama_pejabat_berwenang' => $request->nama_pejabat_berwenang,
                'jabatan' => $request->jabatan,
                'alamat_instansi' => $request->alamat_instansi,
                'is_submitted' => true,
                'submitted_at' => Carbon::now()
            ]);
            $this->storeFiles($request, $pengajuan);
            $pengajuan->sendEmailAfterSubmit();
        }

        $toastr = new Toastr('success', 'Pengajuan telah disubmit!');
        return redirect(route('pendaftaran'))->with('toastr', $toastr);
    }

    /**
     * @param Step2Request $request
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function suratPengatarResmi(Step2Request $request, $pengajuanId)
    {
        $pengajuan = Pengajuan::findOrFail($pengajuanId);
        if($pengajuan){
            if (Storage::disk('local')->exists($pengajuan->file_surat_pengantar_resmi)) {
                return Storage::download($pengajuan->file_surat_pengantar_resmi);
            }
            abort(404);
        }
    }

    /**
     * @param Step2Request $request
     * @param $pengajuanId
     * @return \Illuminate\Http\JsonResponse|\Illuminate\Http\RedirectResponse|\Illuminate\Routing\Redirector
     */
    public function proposal(Step2Request $request, $pengajuanId)
    {
        $pengajuan = Pengajuan::findOrFail($pengajuanId);
        if($pengajuan){
            if (Storage::disk('local')->exists($pengajuan->file_proposal)) {
                return Storage::download($pengajuan->file_proposal);
            }
            abort(404);
        }
    }

    /**
     * Handle file store
     *
     * @param $request
     * @param $model
     */
    private function storeFiles($request, $model)
    {
        foreach($request->files as $key => $file){
            if ($request->file($key)->isValid()) {
                Storage::delete($model->$key);
                $model->$key = $request->file($key)->storeAs("Pengajuan/$key/$model->id", $file->getClientOriginalName(),'local');
                $model->save();
            }
        }
    }
}
