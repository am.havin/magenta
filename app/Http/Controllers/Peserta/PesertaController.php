<?php

namespace App\Http\Controllers\Peserta;

use App\Magang\Magang;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

use App\Alert;
use App\Http\Controllers\Controller;
use App\Http\Requests\PesertaRequest;
use App\Peserta\Peserta;
use App\Toastr;


class PesertaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param PesertaRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(PesertaRequest $request)
    {
        $title = 'Peserta Magang';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Peserta Magang' => route('peserta.index'),
        ];
        $viewData = compact('title', 'activeMenus', 'breadcrumbs');

        return view('pesertas.index', $viewData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param PesertaRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(PesertaRequest $request)
    {
        $title = 'Tambah Peserta Magang';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Peserta Magang' => route('peserta.index'),
            'Tambah' => route('peserta.create'),
        ];
        $validator = \JsValidator::make($request->getCreateRules(), $request->messages());

        $viewData = compact('title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('pesertas.create', $viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PesertaRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PesertaRequest $request)
    {
        try{
            $model = Peserta::create($request->all());
            $this->storeFiles($request, $model); //if there is any
            $json = ['status' => true, 'message' => 'Data berhasil disimpan!'];
        }catch(\Exception $e){
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data baru berhasil disimpan!');
        //return redirect(route('peserta.index'))->with('alert', $alert);
        $toastr = new Toastr('success', 'Data baru berhasil disimpan!');
        return redirect(route('peserta.index'))->with('toastr', $toastr);
    }

    /**
     * Display the specified resource.
     *
     * @param PesertaRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PesertaRequest $request, $id)
    {
        $model = Peserta::findOrFail($id);
        $title = 'Detail Peserta Magang';
        $activeMenus = [''];
        $breadcrumbs = [
            'Peserta Magang' => null,
            'Detail' => route('peserta.show', $model->id),
        ];
        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs');

        return view('pesertas.show', $viewData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param PesertaRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PesertaRequest $request, $id)
    {
        $model = Peserta::findOrFail($id);
        $title = 'Edit Peserta Magang';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Peserta Magang' => route('peserta.index'),
            'Edit' => route('peserta.edit', $model->id),
        ];
        $validator = \JsValidator::make($request->getEditRules(), $request->messages());

        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('pesertas.edit', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PesertaRequest $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(PesertaRequest $request, $id)
    {
        try{
            $model =  Peserta::findOrFail($id);
            $this->storeFiles($request, $model); //if there is any
            $model->update($request->all());
            $json = ['status' => true, 'message' => 'Data berhasil diupdate!'];
        }catch(\Exception $e){
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data berhasil diupdate!');
        //return redirect()->back()->with('alert', $alert);
        $toastr = new Toastr('success', 'Data berhasil diupdate!');
        return redirect()->back()->with('toastr', $toastr);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param PesertaRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PesertaRequest $request, $id)
    {
        $model = Peserta::findOrFail($id);
        return response()->json([
            'status' => $model->doDelete()
        ]);
    }

    /**
     * @param $id
     * @return \Symfony\Component\HttpFoundation\BinaryFileResponse
     */
    public function foto($id)
    {
        $model = Peserta::findOrFail($id);
        return response()->file(storage_path('app/'.$model->foto));
    }

    /**
     * Handle Datatable request.
     *
     * @param PesertaRequest $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function datatable(PesertaRequest $request)
    {
        return Datatables::of(Peserta::query())
            ->filter(function ($query) use ($request) {
                if (!empty($request->input('filter_id_pendaftar'))) {
                    $query->where('id_pendaftar', 'ilike', "%{$request->input('filter_id_pendaftar')}%");
                }
                if (!empty($request->input('filter_nama'))) {
                    $query->where('nama', 'ilike', "%{$request->input('filter_nama')}%");
                }
                if (!empty($request->input('filter_no_ktp'))) {
                    $query->where('no_ktp', 'ilike', "%{$request->input('filter_no_ktp')}%");
                }
                if (!empty($request->input('filter_nim_nis'))) {
                    $query->where('nim_nis', 'ilike', "%{$request->input('filter_nim_nis')}%");
                }
                if (!empty($request->input('filter_email'))) {
                    $query->where('email', 'ilike', "%{$request->input('filter_email')}%");
                }
                if (!empty($request->input('filter_id_sekolah'))) {
                    $query->where('id_sekolah', 'ilike', "%{$request->input('filter_id_sekolah')}%");
                }
                if (!empty($request->input('filter_sekolah'))) {
                    $query->where('sekolah', 'ilike', "%{$request->input('filter_sekolah')}%");
                }
                if (!empty($request->input('filter_id_programstudi'))) {
                    $query->where('id_programstudi', 'ilike', "%{$request->input('filter_id_programstudi')}%");
                }
                if (!empty($request->input('filter_programstudi'))) {
                    $query->where('programstudi', 'ilike', "%{$request->input('filter_programstudi')}%");
                }
                if (!empty($request->input('filter_file_cv'))) {
                    $query->where('file_cv', 'ilike', "%{$request->input('filter_file_cv')}%");
                }
            })
            ->addColumn('action', function ($data) {
                $show = $edit = $delete = '';
                if(auth()->user()->hasAnyPermission(['Peserta - show'])){
                    $show = '<a href="'.route('peserta.show', $data->id).'" class="btn btn-outline-hover-warning btn-icon btn-circle btn-sm" data-toggle="kt-tooltip" title="Show detail" data-skin="brand"><i class="la la-eye kt-font-warning"></i></a>';
                }
                if(auth()->user()->hasAnyPermission(['Peserta - edit'])){
                    $edit = '<a href="'.route('peserta.edit', $data->id).'"class="btn btn-outline-hover-info btn-icon btn-circle btn-sm btnEdit" data-toggle="kt-tooltip" title="Edit" data-skin="brand"><i class="la la-pencil kt-font-info"></i></a>';
                }
                if(auth()->user()->hasAnyPermission(['Peserta - delete'])){
                    $delete = '<button type="button" class="btn btn-outline-hover-danger btn-icon btn-circle btn-sm btn-dt-delete" data-toggle="kt-tooltip" title="Delete" data-skin="brand" data-url="'.route('peserta.destroy', $data->id).'" data-csrf="'.csrf_token().'"><i class="la la-close kt-font-danger"></i></button>';
                }
                return $show.$edit.$delete;
            })->make(true);
    }

    /**
     * Handle file store
     *
     * @param $request
     * @param $model
     */
    private function storeFiles($request, $model)
    {
        foreach($request->files as $key => $file){
            if ($request->file($key)->isValid()) {
                Storage::delete($model->$key);
                $model->$key = $request->file($key)->storeAs("Peserta/$key/$model->id", $file->getClientOriginalName(),'local');
                $model->save();
            }
        }
    }
}
