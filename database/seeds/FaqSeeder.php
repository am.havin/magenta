<?php

use Illuminate\Database\Seeder;

class FaqSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'nama'     => 'Lorem ipsum dolor sit amet, consectetur adipiscing elit?',
                'konten'    => 'Sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Urna et pharetra pharetra massa massa ultricies mi. Mauris rhoncus aenean vel elit scelerisque. Id velit ut tortor pretium viverra suspendisse potenti nullam. Suspendisse faucibus interdum posuere lorem ipsum dolor.',
                'is_aktif' => 1,
                'urutan' => 1,
            ],
            [
                'nama'     => 'Elementum sagittis vitae et leo. Eget nulla facilisi etiam dignissim diam quis enim lobortis?',
                'konten'    => 'Fermentum leo vel orci porta non pulvinar neque. Quis enim lobortis scelerisque fermentum dui faucibus in ornare quam.',
                'is_aktif' => 1,
                'urutan' => 2,
            ],
            [
                'nama'     => 'In mollis nunc sed id semper risus in hendrerit gravida. Purus faucibus ornare suspendisse sed nisi lacus sed?',
                'konten'    => 'Scelerisque fermentum dui faucibus in ornare quam viverra. Posuere urna nec tincidunt praesent semper. Arcu bibendum at varius vel pharetra vel turpis nunc. In aliquam sem fringilla ut morbi',
                'is_aktif' => 1,
                'urutan' => 3,
            ],
            [
                'nama'     => 'Ac tortor vitae purus faucibus ornare suspendisse sed nisi lacus?',
                'konten'    => 'Risus sed vulputate odio ut enim blandit. In fermentum posuere urna nec tincidunt praesent. Amet volutpat consequat mauris nunc. Sed velit dignissim sodales ut eu sem integer vitae',
                'is_aktif' => 1,
                'urutan' => 4,
            ],
            [
                'nama'     => 'Amet mauris commodo quis imperdiet massa tincidunt nunc pulvinar sapien.?',
                'konten'    => 'Nibh mauris cursus mattis molestie a iaculis. Feugiat scelerisque varius morbi enim nunc faucibus a pellentesque sit. Elementum eu facilisis sed odio morbi quis commodo odio aenean',
                'is_aktif' => 1,
                'urutan' => 5,
            ],
        ];

        foreach($data as $val){
            \App\Faq\Faq::updateOrCreate($val, $val);
        }
    }
}
