@extends((Request::ajax()) ? 'templates.layout-blank' : 'templates.'.config('genelator.template').'.layout')

@section('subheaderToolbar')
@endsection

@section('vendorPluginForCustomPagesStyles')
    @include('templates.metronic.demo5.includes.datatable_css')
@endsection

@section('content')
    <div class="kt-portlet kt-portlet--mobile" id="portletPermission">
        <div class="kt-portlet__head kt-portlet__head--lg">
            <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-list-2"></i>
            </span>
                <h3 class="kt-portlet__head-title">
                    Pengaturan Permission
                </h3>
            </div>
        </div>
        <div class="kt-portlet__body">

            <!--begin: Datatable -->
            <table class="table table-bordered- table-striped- table-hover table-checkable" id="form">
                <thead>
                <tr>
                    <th width="20%">Group</th>
                    <th width="">Permission</th>
                    @foreach($roles as $role)
                        <th width="2rem">{{ ucfirst($role->name) }}</th>
                    @endforeach
                </tr>
                </thead>
                <tbody>
                    @foreach($permissions as $permission)
                        <tr>
                            <td>{{ $permission->group }}</td>
                            <td>{{ $permission->name }}</td>
                            @foreach($roles as $role)
                                <td class="text-center">
                                    <label class="kt-checkbox kt-checkbox--solid">
                                        <input type="checkbox" name="{{ $permission->id }}" value="{{ $role->id }}"
                                               @if(\App\PermissionsHelper::isChecked($rolesPermissions, $permission->id, $role->id)) checked @endif>
                                        <span style="top: -12px;"></span>
                                    </label>
                                </td>
                            @endforeach
                        </tr>
                    @endforeach
                </tbody>
            </table>
            <!--end: Datatable -->

            <div class="row">
                <div class="col-lg-12 text-right">
                    <button id="permissionSubmit" type="button" class="btn btn-warning btn-elevate btn-widest" data-csrf="{{ csrf_token() }}" data-url="{{ route('permission.store') }}">
                        <i class="la la-check"></i> Simpan
                    </button>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('vendorPluginForCustomPagesScripts')
    @include('templates.metronic.demo5.includes.datatable_js')
@endsection

@section('pageScripts')
    <script>
        jQuery(document).ready(function () {
            // DATATABLE
            var table = $('#form').DataTable({
                responsive: true,
                paging: false,
                ordering: false,
                rowGroup: true,
                fixedHeader: {
                    headerOffset: 60
                },
                columnDefs: [
                    { "visible": false, "targets": 0 }
                ]
            });

            // SUBMIT EVENT
            $('#permissionSubmit').click( function() {
                let data = table.$('input').serializeArray();
                let url = $(this).attr('data-url');
                let csrf = $(this).attr('data-csrf');
                doSubmit(data, url, csrf);
            });
        });


        function doSubmit(serializedArray, url, csrf)
        {
            let promise = new Promise((resolve, reject) => {
                KTApp.block('body', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'primary',
                    message: 'Processing...'
                });

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        _token: csrf,
                        inputs: serializedArray
                    },
                    success: function (data) {
                        resolve(data)
                    },
                    error: function (error) {
                        reject(error)
                    },
                })
            }).then(data => {
                console.log(data);
                if(data.status){
                    KTApp.unblock('body');
                    toastr.success("Permission berhasil disimpan!");
                }else{
                    KTApp.unblock('body');
                    toastr.error("GAGAL, terjadi kesalahan! Coba refresh halaman");
                }
            }).catch(error => {
                console.log(error);
                toastr.error("ERROR, terjadi kesalahan! Coba refresh halaman");
            });
        }


        function doSwalSubmit(serializedArray, url, csrf)
        {
            let promise = new Promise((resolve, reject) => {
                swal.fire({
                    title: 'Menyimpan...',
                    onBeforeOpen: () => {
                        Swal.showLoading()
                    },
                    allowOutsideClick: false
                })

                $.ajax({
                    url: url,
                    type: 'POST',
                    data: {
                        _token: csrf,
                        inputs: serializedArray
                    },
                    success: function (data) {
                        resolve(data)
                    },
                    error: function (error) {
                        reject(error)
                    },
                })
            }).then(data => {
                console.log(data);
                if(data.status){
                    swal.fire(
                        'Berhasil!',
                        'Data berhasil disimpan.',
                        'success'
                    );
                }else{
                    swal.fire(
                        'Gagal!',
                        'Terjadi kesalahan, Coba refresh halaman.',
                        'error'
                    );
                }
            }).catch(error => {
                console.log(error);
                swal.fire(
                    'Error!',
                    'Terjadi kesalahan! Coba refresh halaman.',
                    'error'
                );
            });
        }
    </script>
@endsection
