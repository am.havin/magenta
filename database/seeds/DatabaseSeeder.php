<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        # USERS & ROLES
        $this->call(UserSeeder::class);
        $this->call(RoleSeeder::class);
        $this->call(UserRolesSeeder::class);
        $this->call(RoleMenusSeeder::class);

        # PERMISSIONS
        $this->call(PermissionSeeder::class);
        $this->call(PendaftarPermissionSeeder::class);
        $this->call(SekolahPermissionSeeder::class);
        $this->call(SekolahProgramStudiPermissionSeeder::class);
        $this->call(AlokasiPermissionSeeder::class);
        $this->call(FaqPermissionSeeder::class);
        $this->call(PesertaPermissionSeeder::class);
        $this->call(HalamandepanPermissionSeeder::class);
        $this->call(KontakkamiPermissionSeeder::class);
        $this->call(PengajuanPermissionSeeder::class);
        $this->call(MagangPermissionSeeder::class);
        $this->call(SuratPersetujuanMagangPermissionSeeder::class);
        $this->call(SertifikatPermissionSeeder::class);
        $this->call(PengumumanPermissionSeeder::class);
        $this->call(IndexNilaiPermissionSeeder::class);
        $this->call(PenempatanPermissionSeeder::class);
        $this->call(RolePermissionsSeeder::class); //always put this at the bottom

        # DATA
        $this->call(AlokasiSeeder::class);
        $this->call(PendaftarSeeder::class);
        $this->call(SekolahSeeder::class);
        $this->call(HalamandepanSeeder::class);
        $this->call(KontakkamiSeeder::class);
        $this->call(FaqSeeder::class);
        $this->call(MagangSeeder::class);
        $this->call(IndexNilaiSeeder::class);
        $this->call(PenempatanSeeder::class);
    }
}
