@extends((Request::ajax()) ? 'templates.layout-blank' : 'templates.'.config('genelator.template').'.layout')

@section('subheaderToolbar')
    <a href="#" class="btn invisible">
    </a>
@endsection

@section('content')
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title">{{ $title }}</h5>
                <button id="closeBtn" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                @if($errors->isNotEmpty())
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-warning" role="alert">
                                <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                <div class="alert-text">
                                    <ul>
                                        @foreach($errors->all() as $message)
                                            <li>{{ $message }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-lg-12">
                        <div class="kt-portlet">

                            <!--begin::Form-->
                            <form action="{{ url('pengajuan/') }}" id="createForm" method="post" class="kt-form" autocomplete="off" enctype="multipart/form-data">
                                @csrf

                                <div class="kt-portlet__body">
                                    <div class="form-group row">
                                                                    <div class="col-lg-6">
                                <label class="form-control-label">* Pendaftar:</label>
                                <select name="id_pendaftar" id="id_pendaftar" class="form-control select2ajax" required   data-ajax--url="{{ url('select2/roles') }}" data-placeholder="Pilih" data-allow-clear="true">
                                    <option></option>
                                </select>
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="col-lg-6">
                                <label> Surat Pengantar Resmi:</label>
                                <input type="file" class="form-control-file" name="file_surat_pengantar_resmi" id="file_surat_pengantar_resmi"   >
                                <span class="form-text text-muted"></span>
                                
                            </div>
                            <div class="w-100"></div><br>
                            <div class="col-lg-6">
                                <label> Proposal:</label>
                                <input type="file" class="form-control-file" name="file_proposal" id="file_proposal"   >
                                <span class="form-text text-muted"></span>
                                
                            </div>
                            <div class="col-lg-6">
                                <label class="form-control-label">* Is New?:</label>
                                <input type="number" name="is_new" id="is_new"  class="form-control" value="" required >
                                <span class="form-text text-muted"></span>
                            </div>
                            <div class="w-100"></div><br>
                            <div class="col-lg-6">
                                <label class="form-control-label">* Is Draft?:</label>
                                <input type="number" name="is_draft" id="is_draft"  class="form-control" value="" required >
                                <span class="form-text text-muted"></span>
                            </div>

                                    </div>
                                </div>
                                <div class="kt-portlet__foot">
                                    <div class="kt-form__actions">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <button type="submit" class="btn btn-warning">Submit</button>
                                                {{-- <a href="{{ url('pengajuan/') }}" class="btn btn btn-secondary">Cancel</a> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!--end::Form-->
                        </div>
                        <!--end::Portlet-->
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection

@section('pageScripts')
    {!! $validator->selector('#createForm') !!}
    <script>
            jQuery(document).ready(function () {
                initSelect2();
                initDatepicker();
                if (!(typeof datatable === "undefined")) {
                    //pass instance datatable (optional) agar di-reload ketika submit formnya sukses
                    initAjaxForm('#createForm', datatable); //datatable dapat dari index.blade.php
                }
            });
        </script>
@endsection

{{-- For Modal --}}
@if(Request::ajax())
    @yield('content')
    @yield('pageScripts')
@endif