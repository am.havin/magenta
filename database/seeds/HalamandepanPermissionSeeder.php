<?php

use Illuminate\Database\Seeder;

class HalamandepanPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['group' => 'Halamandepan', 'name' => 'Halamandepan - index', 'guard_name' => 'web'],
            ['group' => 'Halamandepan', 'name' => 'Halamandepan - create', 'guard_name' => 'web'],
            ['group' => 'Halamandepan', 'name' => 'Halamandepan - show', 'guard_name' => 'web'],
            ['group' => 'Halamandepan', 'name' => 'Halamandepan - edit', 'guard_name' => 'web'],
            ['group' => 'Halamandepan', 'name' => 'Halamandepan - delete', 'guard_name' => 'web'],
        ];

        foreach($data as $val){
            \Spatie\Permission\Models\Permission::updateOrCreate($val, $val);
        }
    }
}
