<div id="copyrights" class="nobg dark pt-0">

    <div class="line mt-0"></div>

    <div class="container clearfix">

        <div class="row">
            <div class="col-12">
                <p class="mb-2 text-white-50">Copyrights &copy; {{ date('Y') }} All Rights Reserved by Kementerian Badan Usaha Milik Negara.</p>
                <div class="copyright-links text-white-50">
                    <a target="_blank" href="https://www.facebook.com/KementerianBUMNRI" class="text-white-50"><i class="icon-facebook"></i> facebook</a> /
                    <a target="_blank" href="https://www.instagram.com/kementerianbumn/?hl=en" class="text-white-50"><i class="icon-instagram"></i> instagram</a> /
                    <a target="_blank" href="https://twitter.com/kemenbumn?lang=en" class="text-white-50"><i class="icon-twitter"></i> twitter</a>
                </div>
            </div>
        </div>

    </div>

</div>