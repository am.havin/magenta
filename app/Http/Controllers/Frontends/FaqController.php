<?php

namespace App\Http\Controllers\Frontends;

use App\Http\Controllers\Controller;
use App\Faq\Faq;


class FaqController extends Controller
{

    /**
     * Display a listing of the resource.
     *
     * @param FaqRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = 'Frequently Aksed Questions';
        $activeMenus = ['menuFaq'];
        $faqs = Faq::where('is_aktif', 1)->orderBy('urutan', 'asc')->get();
        $viewData = compact('title', 'activeMenus', 'faqs');

        return view('frontends.faq.index', $viewData);
    }

}


