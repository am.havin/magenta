<?php

use Illuminate\Database\Seeder;

class SuratPersetujuanMagangPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['group' => 'Surat Persetujuan', 'name' => 'Surat persetujuan magang - index', 'guard_name' => 'web'],
            ['group' => 'Surat Persetujuan', 'name' => 'Surat persetujuan magang - edit', 'guard_name' => 'web'],
        ];

        foreach($data as $val){
            \Spatie\Permission\Models\Permission::updateOrCreate($val, $val);
        }
    }
}
