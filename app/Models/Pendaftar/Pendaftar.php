<?php

namespace App\Pendaftar;

use App\Magang\Magang;
use App\Notification\Notifications\ResetPassword;
use App\Notifications\VerifyEmail;
use App\Notifications\VerifyEmailQueued;
use App\Pengajuan\Pengajuan;
use App\Peserta\Peserta;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Traits\HasRoles;

class Pendaftar extends Authenticatable implements MustVerifyEmail
{
    use HasRoles;
    use Notifiable;

    protected $table = 'pendaftars';
    protected $guarded = ['id'];


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'email', 'nama_lengkap', 'no_hp', 'id_sekolah', 'sekolah', 'password', 'is_banned', 'email_verified_at', 'is_universitas'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function pesertas()
    {
        return $this->hasMany('App\Peserta\Peserta', 'id_pendaftar');
    }

    public function pengajuans()
    {
        return $this->hasMany('App\Pengajuan\Pengajuan', 'id_pendaftar');
    }

    public function modelSekolah()
    {
        return $this->belongsTo('App\Sekolah\Sekolah', 'id_sekolah');
    }

    public function getHasNeedYourApprovalMagangsAttribute()
    {
        $pengajuanIds = $this->pengajuans->pluck('id');
        $magangs = Magang::whereIn('id_pengajuan', $pengajuanIds)
            ->where('is_accepted', true)
            ->where('is_need_pendaftar_approval', true)
            ->whereNull('is_approved_by_pendaftar')
            ->get();

        return $magangs->count() ? true : false;
    }

    public function generateDraftPengajuan()
    {
        return Pengajuan::create([
            'id_pendaftar' => $this->id,
            'is_new' => true,
            'is_draft' => true,
            'is_submitted' => false
        ]);
    }

    public function clearDraftPengajuans()
    {
        $pengajuans = Pengajuan::where('is_draft', true)->where('id_pendaftar', $this->id)->get();
        $pengajuans->each(function($pengajuan, $key){
            $pengajuan->doDelete();
        });
    }

    public function doDelete()
    {
        $status = true;

        DB::beginTransaction();
        try{
            $this->is_deleted = true;
            $this->save();
            DB::commit();
        }catch(\Exception $e) {
            DB::rollback();
            $status = false;
        }

        return $status;
    }

    /**
     * Send the email verification notification.
     *
     * @return void
     */
    public function sendEmailVerificationNotification()
    {
        $this->notify(new VerifyEmailQueued($this));
    }

    /**
     * Send the password reset notification.
     *
     * @param  string  $token
     * @return void
     */
    public function sendPasswordResetNotification($token)
    {
        $this->notify(new ResetPassword($token, $this));
    }
}
