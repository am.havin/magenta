<?php

namespace App\Faq;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Faq extends Model
{
    protected $table = 'faqs';
    protected $guarded = ['id'];

    public function doDelete()
    {
        $status = true;

        DB::beginTransaction();
        try{
            $this->delete();
            DB::commit();
        }catch(\Exception $e) {
            DB::rollback();
            $status = false;
        }

        return $status;
    }
}
