<?php

namespace App\Http\Controllers\Faq;

use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

use App\Alert;
use App\Http\Controllers\Controller;
use App\Http\Requests\FaqRequest;
use App\Faq\Faq;
use App\Toastr;


class FaqController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param FaqRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(FaqRequest $request)
    {
        $title = 'Faq';
        $activeMenus = ['menuFaq'];
        $breadcrumbs = [
            'Faq' => route('faq.index'),
        ];
        $viewData = compact('title', 'activeMenus', 'breadcrumbs');

        return view('faq.index', $viewData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param FaqRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(FaqRequest $request)
    {
        $title = 'Tambah Faq';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Faq' => route('faq.index'),
            'Tambah' => route('faq.create'),
        ];
        $validator = \JsValidator::make($request->getCreateRules(), $request->messages());

        $viewData = compact('title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('faq.create', $viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param FaqRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(FaqRequest $request)
    {
        try{
            $model = Faq::create($request->all());
            $this->storeFiles($request, $model); //if there is any
            $json = ['status' => true, 'message' => 'Data berhasil disimpan!'];
        }catch(\Exception $e){
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data baru berhasil disimpan!');
        //return redirect(route('faq.index'))->with('alert', $alert);
        $toastr = new Toastr('success', 'Data baru berhasil disimpan!');
        return redirect(route('faq.index'))->with('toastr', $toastr);
    }

    /**
     * Display the specified resource.
     *
     * @param FaqRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(FaqRequest $request, $id)
    {
        $model = Faq::findOrFail($id);
        $title = 'Detail FAQ';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Faq' => route('faq.index'),
            'Detail' => route('faq.show', $model->id),
        ];
        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs');

        return view('faq.show', $viewData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param FaqRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(FaqRequest $request, $id)
    {
        $model = Faq::findOrFail($id);
        $title = 'Edit Faq';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Faq' => route('faq.index'),
            'Edit' => route('faq.edit', $model->id),
        ];
        $validator = \JsValidator::make($request->getEditRules(), $request->messages());

        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('faq.edit', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param FaqRequest $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(FaqRequest $request, $id)
    {
        try{
            $model =  Faq::findOrFail($id);
            $this->storeFiles($request, $model); //if there is any
            $model->update($request->all());
            $json = ['status' => true, 'message' => 'Data berhasil diupdate!'];
        }catch(\Exception $e){
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data berhasil diupdate!');
        //return redirect()->back()->with('alert', $alert);
        $toastr = new Toastr('success', 'Data berhasil diupdate!');
        return redirect()->back()->with('toastr', $toastr);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param FaqRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(FaqRequest $request, $id)
    {
        $model = Faq::findOrFail($id);
        return response()->json([
            'status' => $model->doDelete()
        ]);
    }

    /**
     * Handle Datatable request.
     *
     * @param FaqRequest $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function datatable(FaqRequest $request)
    {
        return Datatables::of(Faq::query())
            ->filter(function ($query) use ($request) {
                if (!empty($request->input('filter_nama'))) {
                    $query->where('nama', 'ilike', "%{$request->input('filter_nama')}%");
                }
                if (!empty($request->input('filter_konten'))) {
                    $query->where('konten', 'ilike', "%{$request->input('filter_konten')}%");
                }
                if (!empty($request->input('filter_is_aktif'))) {
                    $query->where('is_aktif', 'ilike', "%{$request->input('filter_is_aktif')}%");
                }
            })
            ->editColumn('is_aktif', function($data){
                return ($data->is_aktif) ? '<i class="fa fa-check"></i>' : '';
            })
            ->addColumn('action', function ($data) {
                $show = $edit = $delete = '';
                //if(auth()->user()->hasAnyPermission(['Faq - show'])){
                //    $show = '<a href="'.route('faq.show', $data->id).'" class="btn btn-outline-hover-warning btn-icon btn-circle btn-sm" data-toggle="kt-tooltip" title="Show detail" data-skin="brand"><i class="la la-eye kt-font-warning"></i></a>';
                //}
                if(auth()->user()->hasAnyPermission(['Faq - edit'])){
                    $edit = '<a href="'.route('faq.edit', $data->id).'"class="btn btn-outline-hover-info btn-icon btn-circle btn-sm btnEdit" data-toggle="kt-tooltip" title="Edit" data-skin="brand"><i class="la la-pencil kt-font-info"></i></a>';
                }
                if(auth()->user()->hasAnyPermission(['Faq - delete'])){
                    $delete = '<button type="button" class="btn btn-outline-hover-danger btn-icon btn-circle btn-sm btn-dt-delete" data-toggle="kt-tooltip" title="Delete" data-skin="brand" data-url="'.route('faq.destroy', $data->id).'" data-csrf="'.csrf_token().'"><i class="la la-close kt-font-danger"></i></button>';
                }
                return $show.$edit.$delete;
            })
            ->rawColumns(['action', 'is_aktif'])
            ->make(true);
    }

    /**
     * Handle file store
     *
     * @param $request
     * @param $model
     */
    private function storeFiles($request, $model)
    {
        foreach($request->files as $key => $file){
            if ($request->file($key)->isValid()) {
                Storage::delete($model->$key);
                $model->$key = $request->file($key)->storeAs("Faq/$key/$model->id", $file->getClientOriginalName(),'local');
                $model->save();
            }
        }
    }
}
