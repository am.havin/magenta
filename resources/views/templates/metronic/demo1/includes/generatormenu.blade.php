@can('generator - akses menu')
    <div class="dropdown">
        <button type="button" class="btn btn-pill btn-secondary dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true">
            Generator Menu
        </button>
        <div class="dropdown-menu dropdown-menu-right dropdown-menu-fit dropdown-menu-md">
            <ul class="kt-nav kt-nav--bold kt-nav--md-space kt-margin-t-20 kt-margin-b-20">

                @foreach($vcGeneratorMenus as $menu)
                    <li class="kt-nav__item">
                        <a class="kt-nav__link active" href="{{ $menu->url }}">
                            <span class="kt-nav__link-text">{{ $menu->name }}</span>
                        </a>
                    </li>
                @endforeach

            </ul>
        </div>
    </div>
@endcan