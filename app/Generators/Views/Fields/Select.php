<?php

namespace App\Generators\Views\Fields;

use App\Generators\Views\Fields\Properties\Property;

class Select
{
    private $stubString;
    private $name;
    private $options;
    private $colSize;
    private $overrideRequired;
    private $withValue;
    private $elementId;

    public function __construct($name, $options, $colSize = 6, $overrideRequired = null, $withValue = false, $elementId = null)
    {
        $this->stubString  = file_get_contents(resource_path('stubs/views/fields/select.stub'));
        $this->name = $name;
        $this->options = $options;
        $this->colSize = $colSize;
        $this->overrideRequired = $overrideRequired;
        $this->withValue = $withValue;
        $this->elementId = $elementId;
    }

    /**
     * @return string
     */
    public function generate()
    {
        $required = Property::getRequired($this->options['nullable'], $this->overrideRequired);

        return $replacedString = str_replace(
            [
                '{{colSize}}',
                '{{label}}',
                '{{name}}',
                '{{asterisk}}',
                '{{required}}',
                '{{attributes}}',
                '{{helptext}}',
                '{{options}}',
                '{{multiple}}',
                '{{elementId}}'
            ],
            [
                $this->colSize,
                $this->options['label'],
                $this->name,
                ($required) ? '*' : '',
                ($required) ? 'required' : '',
                isset($this->options['html_attributes']) ? $this->options['html_attributes'] : '',
                isset($this->options['helptext']) ? $this->options['helptext'] : '',
                $this->generateOptions($this->options['options']),
                (isset($this->options['multiple']) && $this->options['multiple']) ? 'multiple' : '',
                ($this->elementId) ? $this->elementId : $this->name,
            ],
            $this->stubString
        );
    }

    public function generateOptions($selectOptions)
    {
        $return = '';
        if($selectOptions){
            foreach($selectOptions as $value => $label){
                $selected = ($this->withValue) ? '@if($model->'.$this->name.' == \''.$value.'\') selected @endif' : '';
                $return .= "                                    <option value='$value' $selected>$label</option>\r\n";
            }
        }
        return $return;
    }

}
