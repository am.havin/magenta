<?php

namespace App\Http\Controllers\Pengajuan;

use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

use App\Alert;
use App\Http\Controllers\Controller;
use App\Http\Requests\PengajuanRequest;
use App\Pengajuan\Pengajuan;
use App\Magang\Magang;
use App\Toastr;


class PengajuanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param PengajuanRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(PengajuanRequest $request)
    {
        $title = 'Approval';
        $activeMenus = ['menuApproval'];
        $breadcrumbs = [
            'Approval' => route('pengajuan.index'),
        ];
        $viewData = compact('title', 'activeMenus', 'breadcrumbs');

        return view('pengajuans.index', $viewData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param PengajuanRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(PengajuanRequest $request)
    {
        $title = 'Tambah Pendaftaran Magang';
        $activeMenus = ['menuApproval'];
        $breadcrumbs = [
            'Pendaftaran Magang' => route('pengajuan.index'),
            'Tambah' => route('pengajuan.create'),
        ];
        $validator = \JsValidator::make($request->getCreateRules(), $request->messages());

        $viewData = compact('title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('pengajuans.create', $viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PengajuanRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PengajuanRequest $request)
    {
        try {
            $model = Pengajuan::create($request->all());
            $this->storeFiles($request, $model); //if there is any
            $json = ['status' => true, 'message' => 'Data berhasil disimpan!'];
        } catch (\Exception $e) {
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data baru berhasil disimpan!');
        //return redirect(route('pengajuan.index'))->with('alert', $alert);
        $toastr = new Toastr('success', 'Data baru berhasil disimpan!');
        return redirect(route('pengajuan.index'))->with('toastr', $toastr);
    }

    /**
     * Display the specified resource.
     *
     * @param PengajuanRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(PengajuanRequest $request, $id)
    {
        $model = Pengajuan::with('pendaftar')->findOrFail($id);
        $title = 'Detail Pendaftaran Magang';
        $activeMenus = ['menuApproval'];
        $breadcrumbs = [
            'Pendaftaran Magang' => route('pengajuan.index'),
            'Detail' => route('pengajuan.show', $model->id),
        ];
        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs');

        return view('pengajuans.show', $viewData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param PengajuanRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PengajuanRequest $request, $id)
    {
        $model = Pengajuan::findOrFail($id);
        $title = 'Edit pengajuan Magang';
        $activeMenus = ['menuApproval'];
        $breadcrumbs = [
            'Approval' => route('pengajuan.index'),
            'Edit pengajuan Magang' => route('pengajuan.edit', $model->id),
        ];
        $validator = \JsValidator::make($request->getEditRules(), $request->messages());

        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('pengajuans.edit', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PengajuanRequest $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(PengajuanRequest $request, $id)
    {
        try {
            $model = Pengajuan::findOrFail($id);
            $model->update($request->all());
            $this->storeFiles($request, $model); //if there is any
            $json = ['status' => true, 'message' => 'Data berhasil diupdate!'];
        } catch (\Exception $e) {
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data berhasil diupdate!');
        //return redirect()->back()->with('alert', $alert);
        $toastr = new Toastr('success', 'Data berhasil diupdate!');
        return redirect()->back()->with('toastr', $toastr);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param PengajuanRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PengajuanRequest $request, $id)
    {
        $model = Pengajuan::findOrFail($id);
        return response()->json([
            'status' => $model->doDelete()
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param PengajuanRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function approval(PengajuanRequest $request, $id)
    {
        try{
            //update magang
            $model = Magang::with('pengajuan')->findOrFail($id);
            $model->is_accepted = ((int) $request->nilai) ? true : false;
            $model->is_approved_by_pendaftar = null;
            $model->accepted_at = Carbon::now();
            $model->save();

            //send notifikasi approval
            $model->pengajuan->sendEmailAfterApproval();

            $status = true;
        }catch(\Exception $e){
            $status = false;
        }

        return response()->json([
            'status' => $status
        ]);
    }

    /**
     * Handle Datatable request.
     *
     * @param PengajuanRequest $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function datatable(PengajuanRequest $request)
    {
        $queryBuilder = Magang::query()
            ->with('pengajuan.pendaftar')
            ->whereHas('pengajuan', function($q){
                $q->where('is_draft', false)->where('is_submitted', true)->where('is_deleted', false);
            })
            ->orderBy('id_pengajuan', 'desc');

        return Datatables::of($queryBuilder)
            ->filter(function ($query) use ($request) {
                if ($request->has('filter_pendaftar')) {
                    $query->whereHas('pengajuan.pendaftar', function($q) use($request){
                        $q->where('nama_lengkap', 'ilike', "%{$request->input('filter_pendaftar')}%");
                    });
                }
                if ($request->has('filter_peserta')) {
                    $query->where('peserta', 'ilike', "%{$request->input('filter_peserta')}%");
                }
                if ($request->has('filter_unit')) {
                    $query->where('unit', 'ilike', "%{$request->input('filter_unit')}%");
                }
            })
            ->editColumn('id_pendaftar', function($data){
                return $data->pengajuan->id_pendaftar;
            })
            ->editColumn('no_pengajuan', function($data){
                return '<a href="'.route('pengajuan.show', $data->id_pengajuan).'" class="btnShow">' . $data->pengajuan->nomorPengajuan . '</a>';
            })
            ->editColumn('pendaftar', function($data){
                return '<a href="'.route('pendaftar.show', $data->pengajuan->id_pendaftar).'" class="btnShow">' . $data->pengajuan->pendaftar->nama_lengkap . '</a>';
            })
            ->editColumn('id_magang', function($data){
                return $data->id;
            })
            ->editColumn('peserta', function($data){
                return '<a href="'.route('magang.show', $data->id).'" class="btnShow">' . $data->peserta . '</a>';
            })
            ->editColumn('unit', function($data){
                $text = $data->unit;
                $oldText = ($data->old_unit) ? "<s>$data->old_unit</s><br>" : '';
                return $oldText.$text;
            })
            ->editColumn('waktu_magang', function($data){
                $oldText = '';
                $text = "{$data->carbonTglAwal->format('d M Y')} s.d. {$data->carbonTglAkhir->format('d M Y')} <br>({$data->lamaHari} hari)";
                if($data->old_tgl_awal){
                    $oldText =  "<s>{$data->carbonOldTglAwal->format('d M Y')} s.d. {$data->carbonOldTglAkhir->format('d M Y')} <br>({$data->oldLamaHari} hari)</s><br>";
                }
                return $oldText.$text;
            })
            ->editColumn('tgl_pengajuan', function($data){
                return Carbon::createFromFormat('Y-m-d H:i:s', $data->pengajuan->submitted_at)->format('d M Y');
            })
            ->editColumn('status', function($data){
                $return = '';
                if($data->is_accepted === null){
                    $return = '<span class="kt-badge kt-badge--brand kt-badge--inline">new</span>';
                }
                if($data->is_accepted === true){
                    $return = '<span class="kt-badge kt-badge--warning kt-badge--inline">Disetujui</span>';
                    if($data->is_need_pendaftar_approval && $data->is_approved_by_pendaftar == null){
                        $return = '<span class="kt-badge kt-badge--success kt-badge--inline">Disetujui Bersyarat</span>';
                    }
                    if($data->is_need_pendaftar_approval && $data->is_approved_by_pendaftar){
                        $return = '<span class="kt-badge kt-badge--warning kt-badge--inline">Pendaftar Setuju</span>';
                    }
                }
                if($data->is_accepted === false){
                    $return = '<span class="kt-badge kt-badge--dark kt-badge--inline">ditolak</span>';
                    if($data->is_need_pendaftar_approval && $data->is_approved_by_pendaftar === false){
                        $return = '<span class="kt-badge kt-badge--dark kt-badge--inline">Pendaftar Menolak</span>';
                    }
                }
                return $return;
            })
            ->addColumn('action', function ($data) {
                $edit = '';
                if (auth()->user()->hasAnyPermission(['Pengajuan - edit'])) {
                    $edit = '<a href="' . route('magang.edit', $data->id) . '"class="btn btn-outline-hover-warning btn-icon btn-circle btn-sm btnEdit" data-toggle="kt-tooltip" title="Edit waktu magang" data-skin="brand"><i class="la la-calendar kt-font-warning"></i></a>';
                }
                return $edit;
            })
            ->addColumn('approval', function ($data) {
                $setuju = $tolak = '';
                if (auth()->user()->hasAnyPermission(['Pengajuan - edit'])) {
                    $setuju = '<button type="button" class="btn btn-outline-hover-primary btn-icon btn-circle btn-sm btnApproval" data-toggle="kt-tooltip" title="Setuju" data-skin="brand" data-url="' . route('pengajuan.approval', $data->id) . '" data-redirect-url="' . route('pengajuan.index') . '" data-csrf="' . csrf_token() . '" data-nilai="1"><i class="la la-thumbs-up kt-font-primary"></i></button>';
                    $tolak = '<button type="button" class="btn btn-outline-hover-danger btn-icon btn-circle btn-sm btnApproval" data-toggle="kt-tooltip" title="Tolak" data-skin="brand" data-url="' . route('pengajuan.approval', $data->id) . '" data-redirect-url="' . route('pengajuan.index') . '" data-csrf="' . csrf_token() . '" data-nilai="0"><i class="la la-thumbs-down kt-font-danger"></i></button>';
                }
                return $setuju . $tolak;
            })
            ->rawColumns(['unit', 'action', 'approval', 'waktu_magang', 'status', 'no_pengajuan', 'pendaftar', 'peserta'])
            ->make(true);
    }

    /**
     * Handle file store
     *
     * @param $request
     * @param $model
     */
    private function storeFiles($request, $model)
    {
        foreach ($request->files as $key => $file) {
            if ($request->file($key)->isValid()) {
                Storage::delete($model->$key);
                $model->$key = $request->file($key)->storeAs("Pengajuan/$key/$model->id", $file->getClientOriginalName(), 'local');
                $model->save();
            }
        }
    }
}
