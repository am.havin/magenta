<?php

namespace App\Generators;

use Carbon\Carbon;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Symfony\Component\Yaml\Yaml;

class Migration
{
    private $stubString;

    private $fieldTypesMapForDB = [
        'int'           => 'integer',
        'integer'       => 'integer',
        'string'        => 'string',
        'text'          => 'text',
        'date'          => 'timestamp',
        'select'        => 'integer',
        'select2'       => 'integer',
        'select2ajax'   => 'integer',
        'file'          => 'string',
    ];

    /**
     * Migration constructor.
     */
    public function __construct()
    {
        $stubFileLoc        = resource_path('stubs/migration.stub');
        $this->stubString   = file_get_contents($stubFileLoc);
    }

    /**
     * @param $tableName
     * @param $fields
     * @return string
     */
    public function generate($tableName, $fields)
    {
        if($this->validate($fields)){
            dd('Terjadi kesalahan! pastikan penulisan type field sudah benar.');
            return null; //todo: bagusnya throw exception
        }

        $fieldString     = $this->generateFieldsString($fields);
        $migrationString = $this->replaceStubString($tableName, $fieldString);
        return $this->createFile($tableName, $migrationString);
    }

    /**
     * @param $fields
     * @return string
     */
    private function generateFieldsString($fields)
    {
        $fieldString = '';
        foreach($fields as $fieldName => $value){
            $fieldString .= "            "
                . '$table->'
                .  $this->fieldTypesMapForDB[$value['type']]
                . '(\''.$fieldName.'\')';

            if($value['nullable']){
                $fieldString .= '->nullable()';
            }

            $fieldString .= ";\r\n";
        }
        return $fieldString;
    }

    /**
     * @param $tableName
     * @param $fieldString
     * @return mixed
     */
    private function replaceStubString($tableName, $fieldString)
    {
        $replacedString = str_replace(
            [
                '{{className}}',
                '{{tableName}}',
                '{{migrationFields}}'
            ],
            [
                Str::studly('create_'.$tableName.'_table'),
                $tableName,
                $fieldString
            ],
            $this->stubString
        );
        return $replacedString;
    }

    /**
     * @param $tableName
     * @param $migrationString
     * @return string
     */
    private function createFile($tableName, $migrationString)
    {
        $datetime = date('Y_m_d_his');
        $filepath = "migrations/".$datetime."_create_".$tableName."_table.php";
        file_put_contents(database_path($filepath), $migrationString);
        return $filepath;
    }

    /**
     * @param $fields
     * @return bool
     */
    private function validate($fields)
    {
        $unknownTypes = Arr::where($fields, function ($value, $key) {
            return !in_array($value['type'], Generator::$availableTypes);
        });

        return ($unknownTypes) ? true : false;
    }
}
