<?php

namespace App\Http\Controllers\Select2;

use App\Sekolah\Sekolah;
use App\SekolahProgramStudi\SekolahProgramStudi;
use Illuminate\Http\Request;

use App\Http\Controllers\Controller;


class ProgramStudiController extends Controller
{
    public function index(Request $request)
    {
        $return = [];
        $data = SekolahProgramStudi::where('nama', 'ilike', "%$request->q%")->where('is_approved', '=', '1')->pluck('nama', 'id');
        foreach($data as $id => $text){
            $return[] = ['id' => $id, 'text' => $text ];
        }

        return response()->json(['results' => $return]);
    }

    public function programstudi_with_lain_lain(Request $request)
    {
        $return[] = ['id' => 0, 'text' => 'Lain-lain' ];
        if($request->id_sekolah != 0){
            $sekolah = Sekolah::findOrFail($request->id_sekolah);
            $data = SekolahProgramStudi::where('nama', 'ilike', "%$request->q%")
                ->where('id_sekolah', $sekolah->id)
                ->where('is_approved', '=', '1')->pluck('nama', 'id');
            foreach($data as $id => $text){
                $return[] = ['id' => $id, 'text' => $text ];
            }
        }
        return response()->json(['results' => $return]);
    }
}
