<div class="section nobg clearfix">
    <div class="container">
        <div class="heading-block nobottomborder center">
            <div class="before-heading">See What's Up in Latest</div>
            <h2 class="nott t600">News &amp; Events</h2>
        </div>

        <div class="clear"></div>

        <div class="row mb-5">
            <div class="col-md-4 mt-5">
                <article class="ipost">
                    <div class="entry-title">
                        <h3><a href="#">A Meetup for People Currently Running an e-Commerce Business</a></h3>
                    </div>
                    <div class="entry-content clearfix">
                        <p>Synergistically expedite focused experiences through orthogonal "outside the box" thinking. Collaboratively reconceptualize e-commerce via effective applications. Enthusiastically conceptualize go forward functionalities vis-a-vis sticky partnerships. Distinctively underwhelm premier scenarios without synergistic best practices. Globally target cross-platform.</p>
                    </div>
                    <div class="author-meta d-flex align-items-center">
                        <div class="author-image">
                            <img src="{{url('/')}}/templates/frontend/magenta/demos/articles/images/authors/1.jpg" alt="Author Image" class="rounded-circle">
                        </div>
                        <ul class="entry-meta m-0 clearfix">
                            <li><span>By</span> <a href="#">Hanson Deck</a></li>
                            <li class="mb-0"><a href="#">Mar 11, 2016</a></li>
                        </ul>
                    </div>
                </article>
            </div>

            <div class="col-md-4 mt-5">
                <article class="ipost">
                    <div class="entry-title">
                        <h3><a href="#">Creating Your Own Demand as a Digital Nomad</a></h3>
                    </div>
                    <div class="entry-content clearfix">
                        <p>Pellentesque hic illo beatae rhoncus sint, quis, fugiat imperdiet unde architecto magna dui hymenaeos autem lorem eligendi.</p>
                    </div>
                    <div class="author-meta d-flex align-items-center">
                        <div class="author-image">
                            <img src="{{url('/')}}/templates/frontend/magenta/demos/articles/images/authors/2.jpg" alt="Author Image" class="rounded-circle">
                        </div>
                        <ul class="entry-meta m-0 clearfix">
                            <li><span>By</span> <a href="#">Hanson Deck</a></li>
                            <li class="mb-0"><a href="#">Mar 11, 2016</a></li>
                        </ul>
                    </div>
                </article>
            </div>

            <div class="col-md-4 mt-5">
                <article class="ipost">
                    <div class="entry-title">
                        <h3><a href="#">Succeeding Remotely Within A Stateside Team</a></h3>
                    </div>
                    <div class="entry-content clearfix">
                        <p>Pellentesque hic illo beatae rhoncus sint, quis, fugiat imperdiet unde architecto magna dui hymenaeos autem lorem eligendi.</p>
                    </div>
                    <div class="author-meta d-flex align-items-center">
                        <div class="author-image">
                            <img src="{{url('/')}}/templates/frontend/magenta/demos/articles/images/authors/3.jpg" alt="Author Image" class="rounded-circle">
                        </div>
                        <ul class="entry-meta m-0 clearfix">
                            <li><span>By</span> <a href="#">Hanson Deck</a></li>
                            <li class="mb-0"><a href="#">Mar 11, 2016</a></li>
                        </ul>
                    </div>
                </article>
            </div>

        </div>
    </div>

</div>
