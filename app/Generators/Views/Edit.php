<?php

namespace App\Generators\Views;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Symfony\Component\Yaml\Yaml;

class Edit
{

    private $stubString;

    /**
     * Create constructor.
     */
    public function __construct()
    {
        $stubFileLoc        = resource_path('stubs/views/edit.stub');
        $this->stubString   = file_get_contents($stubFileLoc);
    }

    /**
     * @param $fieldsData
     * @param $routeData
     * @param $viewsData
     * @param $requestData
     * @return string
     */
    public function generate($fieldsData, $routeData, $viewsData, $requestData)
    {
        $fieldsString = $this->generateFields($fieldsData);
        $createString = $this->replaceStubString($routeData['url'], $fieldsString, $requestData);
        return $this->createFile($viewsData['file_dir'], $createString);
    }

    private function generateFields($fields)
    {
        $formFields = new EditFormFields($fields);
        return $formFields->generate();
    }

    /**
     * @param $route
     * @param $fieldString
     * @param $requestData
     * @return mixed
     */
    private function replaceStubString($route, $fieldString, $requestData)
    {
        $replacedString = str_replace(
            [
                '{{routeUrl}}',
                '{{routeName}}',
                '{{fields}}',
                '{{requestNamespace}}',
                '{{requestClassName}}',
            ],
            [
                $routeUrl = Str::finish($route, '/'),
                str_replace('/', '.', $routeUrl),
                $fieldString,
                $requestData['namespace'],
                $requestData['class_name'],
            ],
            $this->stubString
        );
        return $replacedString;
    }

    /**
     * @param $file_dir
     * @param $string
     * @return string
     */
    private function createFile($file_dir, $string)
    {
        $fileDir = 'views/'.$file_dir;
        if(!is_dir(resource_path($fileDir))){ //bikin direktorinya  kalau belum ada
            mkdir(resource_path($fileDir), 0755, true);
        }

        $filepath = Str::finish($fileDir , '/') . "edit.blade.php";
        file_put_contents(resource_path($filepath), $string);

        return $filepath;
    }
}
