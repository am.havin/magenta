<?php

namespace App\Http\Controllers\Frontends;

use App\Http\Requests\Frontend\AkunRequest;
use App\Sekolah\Sekolah;
use Illuminate\Support\Facades\Storage;
//use Illuminate\Support\Facades\Request;
use Illuminate\Http\Request;
//use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Hash;

use App\Http\Controllers\Controller;
use App\Pendaftar\Pendaftar;
use App\Toastr;



class AkunController extends Controller
{
    public function index(AkunRequest $request)
    {
        $model = auth('pendaftar')->user();
        $sekolah = ($model->id_sekolah) ? Sekolah::find($model->id_sekolah) : null;

        $title = 'Akun';
        $activeMenus = ['menuAkun'];
        if($model->pic){
            $photo = explode("/", $model->pic);
            $photo = $photo[2];
        }else{
            $photo = 'male.png';
        }
        $validator = \JsValidator::make($request->getEditRules(), $request->messages());

        $viewData = compact('model', 'sekolah', 'title', 'activeMenus', 'photo', 'validator');
        return view('frontends.akun.index', $viewData);
    }  
    
    function updatedata(AkunRequest $request)
    {
        // prepare data
        $data = $request->all();

        //prepare sekolah
        if(isset($data['id_sekolah'])){
            $sekolah = Sekolah::find($data['id_sekolah']);
        }

        // update
        $model = auth('pendaftar')->user();
        $data['is_universitas'] = ($request->is_universitas == '1') ? true : false;
        $data['sekolah'] = isset($sekolah) ? $sekolah->nama : $data['sekolah'];
        $model->update($data);
        $this->storeFiles($request, $model); //if there is any

        // return
        $toastr = new Toastr('success', 'BERHASIL! Data akun berhasil diupdate.');
        return redirect()->back()->with('toastr', $toastr);
    }

    function updatePassword(Request $request)
    {
        $password = $request->password;
        $password_new = $request->password_new;
        $password_new_retype = $request->password_new_retype;

        if( $password_new == $password_new_retype ){ // check if retype well
            $model =  auth('pendaftar')->user();
            if( Hash::check($password, $model->password) ){ // check if old pass same
                $model->password = Hash::make($password_new);
                $model->save();

                //return
                $toastr = new Toastr('success', 'BERHASIL! Password berhasil diganti.');
                return redirect()->back()->with('toastr', $toastr);
            }
            //return
            $toastr = new Toastr('error', 'GAGAL! Password lama salah.');
            return redirect()->back()->with('toastr', $toastr);
        }

        //return
        $toastr = new Toastr('error', 'GAGAL! password baru & ulangi password baru tidak sama');
        return redirect()->back()->with('toastr', $toastr);
    }

    public function fotoPendaftar($id, $file) {
        $pendaftar = auth('pendaftar')->user();
        if($pendaftar->id != $id){
            abort(404); //hanya boleh lihat foto dirinya sendiri.
        }

        $path = storage_path('app/pendaftar/'.$id . '/' . $file);
        if (file_exists($path)) {
            return response()->file($path, array('Content-Type' =>'image/jpeg', 'Content-Type' =>'image/png'));
        }
        abort(404);
    }

    private function storeFiles($request, $model)
    {
        foreach($request->files as $key => $file){
            if ($request->file($key)->isValid()) {
                Storage::delete($model->$key);
                $model->$key = $request->file($key)->storeAs("pendaftar/$model->id", $file->getClientOriginalName(),'local');
                $model->save();
            }
        }
    }
}
