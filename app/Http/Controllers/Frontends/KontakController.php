<?php

namespace App\Http\Controllers\Frontends;

use App\Http\Controllers\Controller;
use App\Kontakkami\Kontakkami;

class KontakController extends Controller
{
    public function index()
    {
        $title = 'Kontak Kami';
        $activeMenus = ['menuKontak'];
        $kontakkami = kontakkami::findOrFail(1);
        $viewData = compact('title', 'activeMenus', 'kontakkami');

        return view('frontends.kontak.index', $viewData);


    }
}
