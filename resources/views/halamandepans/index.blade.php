@extends((Request::ajax()) ? 'templates.layout-blank' : 'templates.'.config('genelator.template').'.layout')

@section('subheaderToolbar')
@endsection

@section('vendorPluginForCustomPagesStyles')
    @include('templates.metronic.demo5.includes.datatable_css')
@endsection

@section('content')
<div class="kt-portlet kt-portlet--mobile">
    <div class="kt-portlet__head kt-portlet__head--lg">
        <div class="kt-portlet__head-label">
            <span class="kt-portlet__head-icon">
                <i class="kt-font-brand flaticon2-list-2"></i>
            </span>
            <h3 class="kt-portlet__head-title">
                List data Halamandepan
            </h3>
        </div>
        <div class="kt-portlet__head-toolbar">
            <div class="kt-portlet__head-wrapper">
                <div class="kt-portlet__head-actions">
                    @if(auth()->user()->can('Halamandepan - create'))
                        <a href="{{ url('halamandepan/create') }}" class="btn btn-sm btn-warning btn-elevate btn-icon-sm btnAdd">
                            <i class="la la-plus"></i>
                            Tambah {{ $title }}
                        </a>
                    @endif
                    <button type="button" class="btn btn-sm btn-success btn-elevate btn-icon" id="btnToggleFilter"
                            data-toggle="kt-tooltip" data-placement="top" title="" data-original-title="Search / Filter list data"
                            data-skin="brand">
                        <i class="la la-search"></i>
                    </button>
                    <button type="button" class="btn btn-sm btn-info btn-elevate btn-icon" id="btnReloadDt"
                            data-toggle="kt-tooltip" data-placement="top" title="" data-original-title="Refresh list data"
                            data-skin="brand">
                        <i class="la la-refresh"></i>
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div class="kt-portlet__body">

        <!--begin: Filter for datatable-->
        <div id="sectionFilter" class="kt-section">
    <div class="kt-section__content kt-section__content--solid">
        <form id="filterForm" autocomplete="off">
            <h5>Filter:</h5>
            <div class="form-group row">
                                            <div class="col-lg-3">
                                <label class="form-control-label"> Slogan:</label>
                                <input type="text" name="slogan" id="filter_slogan"  class="form-control" value=""  >
                                <span class="form-text text-muted">Slogan</span>
                            </div>
                            <div class="col-lg-3">
                                <label class="form-control-label"> Introduksi:</label>
                                <input type="text" name="introduksi" id="filter_introduksi"  class="form-control" value=""  >
                                <span class="form-text text-muted">Introduksi</span>
                            </div>
                            <div class="col-lg-3">
                                <label class="form-control-label"> Teks Gambar:</label>
                                <input type="text" name="teks_gambar" id="filter_teks_gambar"  class="form-control" value=""  >
                                <span class="form-text text-muted">Teks Gambar</span>
                            </div>
                            <div class="col-lg-3">
                                <label class="form-control-label"> Step1 Judul:</label>
                                <input type="text" name="step1_judul" id="filter_step1_judul"  class="form-control" value=""  >
                                <span class="form-text text-muted">Step1 Judul</span>
                            </div>
                            <div class="w-100"></div><br>
                            <div class="col-lg-3">
                                <label class="form-control-label"> Step1 Teks:</label>
                                <textarea name="step1_teks" id="filter_step1_teks" class="form-control"  ></textarea>
                                <span class="form-text text-muted">Step1 Teks</span>
                            </div>
                            <div class="col-lg-3">
                                <label class="form-control-label"> Step2 Judul:</label>
                                <input type="text" name="step2_judul" id="filter_step2_judul"  class="form-control" value=""  >
                                <span class="form-text text-muted">Step2 Judul</span>
                            </div>
                            <div class="col-lg-3">
                                <label class="form-control-label"> Step2 Teks:</label>
                                <textarea name="step2_teks" id="filter_step2_teks" class="form-control"  ></textarea>
                                <span class="form-text text-muted">Step2 Teks</span>
                            </div>
                            <div class="col-lg-3">
                                <label class="form-control-label"> Step2 Judul:</label>
                                <input type="text" name="step3_judul" id="filter_step3_judul"  class="form-control" value=""  >
                                <span class="form-text text-muted">Step2 Judul</span>
                            </div>
                            <div class="w-100"></div><br>
                            <div class="col-lg-3">
                                <label class="form-control-label"> Step3 Teks:</label>
                                <textarea name="step3_teks" id="filter_step3_teks" class="form-control"  ></textarea>
                                <span class="form-text text-muted">Step3 Teks</span>
                            </div>
                            <div class="col-lg-3">
                                <label class="form-control-label"> Gambar Depan:</label>
                                <input type="text" name="gambar_depan" id="filter_gambar_depan"  class="form-control" value=""  >
                                <span class="form-text text-muted"></span>
                            </div>

            </div>
            <hr>
            <div class="row">
                <div class="col-lg-12 text-right">
                    <button type="reset" class="btn btn-wide btn-sm btn-clean">Clear</button>
                    <button type="submit" class="btn btn-wide btn-sm btn-outline-success">Filter</button>
                </div>
            </div>
        </form>
    </div>
</div>
        <!--end: Filter for datatable-->

        <!--begin: Datatable -->
        <table class="table table-striped- table-hover table-checkable" id="datatable">
    <thead>
        <tr>
                                <th>Slogan</th>
                    <th>Introduksi</th>
                    <th>Teks Gambar</th>
                    <th>Step1 Judul</th>
                    <th>Step1 Teks</th>
                    <th>Step2 Judul</th>
                    <th>Step2 Teks</th>
                    <th>Step2 Judul</th>
                    <th>Step3 Teks</th>
                    <th>Gambar Depan</th>

            <th  width="100px;">Action</th>
        </tr>
    </thead>
</table>
        <!--end: Datatable -->

    </div>
</div>

<!-- Modal Container-->
<div class="modal fade" id="modal" tabindex="-1"></div>
<!--end: Modal Container-->
@endsection

@section('vendorPluginForCustomPagesScripts')
    @include('templates.metronic.demo5.includes.datatable_js')
@endsection

@section('pageScripts')
<script>
        jQuery(document).ready(function() {
            datatable = $('#datatable').DataTable({
    dom: `<'row'<'col-sm-12'tr>><'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7 dataTables_pager'lp>>`,
    processing: true,
    serverSide: true,
    responsive: true,
    ajax: {
        url: "{{ url('halamandepan/datatable') }}",
        data: function (d) {
                        d.filter_slogan = $('#filter_slogan').val();
            d.filter_introduksi = $('#filter_introduksi').val();
            d.filter_teks_gambar = $('#filter_teks_gambar').val();
            d.filter_step1_judul = $('#filter_step1_judul').val();
            d.filter_step1_teks = $('#filter_step1_teks').val();
            d.filter_step2_judul = $('#filter_step2_judul').val();
            d.filter_step2_teks = $('#filter_step2_teks').val();
            d.filter_step3_judul = $('#filter_step3_judul').val();
            d.filter_step3_teks = $('#filter_step3_teks').val();
            d.filter_gambar_depan = $('#filter_gambar_depan').val();

        }
    },
    columns: [
                    {data: 'slogan', name: 'slogan'},
            {data: 'introduksi', name: 'introduksi'},
            {data: 'teks_gambar', name: 'teks_gambar'},
            {data: 'step1_judul', name: 'step1_judul'},
            {data: 'step1_teks', name: 'step1_teks'},
            {data: 'step2_judul', name: 'step2_judul'},
            {data: 'step2_teks', name: 'step2_teks'},
            {data: 'step3_judul', name: 'step3_judul'},
            {data: 'step3_teks', name: 'step3_teks'},
            {data: 'gambar_depan', name: 'gambar_depan'},

        {data: 'action', name: 'action', orderable: false, searchable: false, className: 'text-center'},
    ],
    drawCallback: function( settings ) {
        KTApp.initTooltip($('[data-toggle="kt-tooltip"]'));
    }
});

            //INIT DATATABLE'S BUTTONS
            initDatatableFilterAndRefreshButton(datatable); //check function in custom.js
            initDeleteButton(datatable); //check function in custom.js

            //INIT MODAL
            initAjaxModalButton('a.btnAdd, a.btnEdit');
        });
    </script>
@endsection