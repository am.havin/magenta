@extends('templates.frontend.magenta.layout_nonhome')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-header">{{ __('Lakukan verifikasi email!') }}</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            <i class="icon-check"></i> {{ __('Link verifikasi baru telah dikirim ke alamat email anda') }}
                        </div>
                    @endif

                    {!!  __('Untuk dapat melakukan aktifitas lebih lanjut, silahkan cek email anda <b>'.auth()->user()->email.'</b> untuk melakukan proses verifikasi.') !!}
                        <br><br>
                    {{ __('Jika email verifikasi tidak diterima') }},
                    <form class="d-inline" method="POST" action="{{ route('verification.resend') }}">
                        @csrf
                        <button type="submit" class="btn btn-link p-0 m-0 align-baseline">{{ __('klik disini untuk mengirim ulang') }}</button>.
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
