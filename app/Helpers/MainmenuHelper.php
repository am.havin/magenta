<?php

namespace App;

class MainmenuHelper
{
    public static function isHavingThisMenu($menuId, $userRoleMenus)
    {
        $res = $userRoleMenus->where('menu_id', $menuId);
        return ($res->count()) ? true : false;
    }
}