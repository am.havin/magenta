<?php

namespace App\Generators\Views;

use App\Generators\Views\Fields\Date;
use App\Generators\Views\Fields\File;
use App\Generators\Views\Fields\Integer;
use App\Generators\Views\Fields\Select;
use App\Generators\Views\Fields\Select2ajax;
use App\Generators\Views\Fields\Text;
use App\Generators\Views\Fields\Textarea;

class EditFormFields
{
    private $fields;

    public function __construct($yamlFields)
    {
        $this->fields = $yamlFields;
    }

    public function generate()
    {
        $string = '';
        $i = 1;
        foreach($this->fields as $name => $val) {
            $funcName = 'generate' . ucfirst($val['type']);
            $string .= $this->{$funcName}($name, $val);
            $string .= "\r\n";
            $string .= ($i%2==0) ? '                            <div class="w-100"></div><br>'."\r\n" : '' ;
            $i++;
        }
        return "" . $string;
    }

    private function generateInt($name, $options)
    {
        $field = new Integer($name, $options, 6, null, true);
        return $field->generate();
    }

    private function generateInteger($name, $options)
    {
        $this->generateInt($name, $options);
    }

    private function generateString($name, $options)
    {
        $field = new Text($name, $options, 6, null, true);
        return $field->generate();
    }

    private function generateText($name, $options)
    {
        $field = new Textarea($name, $options, 6, null, true);
        return $field->generate();
    }

    private function generateRte($name, $options)
    {
        return '';
    }

    private function generateDate($name, $options)
    {
        $field = new Date($name, $options, 6, null, true);
        return $field->generate();
    }

    private function generateSelect($name, $options)
    {
        $field = new Select($name, $options, 6, null, true);
        return $field->generate();
    }

    private function generateSelect2($name, $options)
    {
        //
    }

    private function generateSelect2ajax($name, $options)
    {
        $field = new Select2ajax($name, $options, 6, null, true);
        return $field->generate();
    }

    private function generateFile($name, $options)
    {
        $field = new File($name, $options, 6, null, true);
        return $field->generate();
    }
}
