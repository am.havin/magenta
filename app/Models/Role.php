<?php

namespace App;

use Illuminate\Support\Facades\DB;

class Role extends \Spatie\Permission\Models\Role
{
    public function doDelete()
    {
        $status = true;

        DB::beginTransaction();
        try{
            $this->syncPermissions(); //clear role_has_permissions
            DB::table('model_has_roles')->where('role_id', $this->id)->delete();
            DB::table('role_has_menus')->where('role_id', $this->id)->delete();
            $this->delete();

            DB::commit();
        }catch(\Exception $e) {
            DB::rollback();
            $status =  false;
        }
        return $status;
    }
}
