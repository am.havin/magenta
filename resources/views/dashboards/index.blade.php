@extends((Request::ajax()) ? 'templates.layout-blank' : 'templates.'.config('genelator.template').'.layout')

@section('subheaderToolbar')
    {{--<a href="#" class="btn kt-subheader__btn-daterange" id="" data-toggle="kt-tooltip" title="Header toolbar"--}}
    {{--data-skin="brand" data-placement="left">--}}
    {{--<i class="flaticon2-add"></i>--}}
    {{--<span>Header toolbar</span>--}}
    {{--</a>--}}
@endsection

@section('content')
    <div class="row">
        <div class="kt-portlet">
            <div class="kt-form kt-form--label-right">
                <div class="kt-portlet__body">
                    <div class="row">
                        <div class="col-3">
                            <a href="{{ url('pengajuan') }}" target="_blank"
                               title="Klik untuk link ke list data pengajuan baru">
                                <div class="kt-portlet kt-portlet--solid-warning h-100">
                                    <div class="kt-portlet__head kt-ribbon kt-ribbon--info">
                                        <div class="kt-ribbon__target"
                                             style="top: 15px; right: -14px; font-size: 22px;"><b>{{ $magang_new }}</b>
                                        </div>

                                        <div class="kt-portlet__head-label">
                                            <h3 class="kt-portlet__head-title widget-judul">
                                                Pengajuan Baru
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__body">
                                        Jumlah Pengajuan yang Butuh Approval Data
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-3">
                            <a href="javascript:;" class="" data-toggle="modal" data-target="#mahasiswa-magang-now">
                                <div class="kt-portlet kt-portlet--solid-info h-100">
                                    <div class="kt-portlet__head kt-ribbon kt-ribbon--success">
                                        <div class="kt-ribbon__target"
                                             style="top: 15px; right: -14px; font-size: 22px;">
                                            <b>{{ $magang_aktif_count }}</b></div>

                                        <div class="kt-portlet__head-label">
                                            <h3 class="kt-portlet__head-title widget-judul">Saat Ini Magang</h3>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__body text-light">
                                        Jumlah peserta yang sedang Magang pada hari ini
                                    </div>
                                </div>
                            </a>
                            <div class="modal fade" id="mahasiswa-magang-now" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog modal-lg" role="document">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Saat ini magang</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <p>
                                                Berikut adalah list peserta yang sedang magang pada hari ini.
                                            </p>
                                            <table class="table table-sm table-bordered">
                                                <thead>
                                                <th>Nama</th>
                                                <th>Unit</th>
                                                <th>Waktu
                                                    <magang></magang>
                                                </th>
                                                </thead>
                                                <tbody>
                                                @foreach($magang_saat_ini_aktif as $magang)
                                                    <tr>
                                                        <td>{{ $magang->peserta }}</td>
                                                        <td>{{ $magang->unit }}</td>
                                                        <td>{{ $magang->carbonTglAwal->format('d/m/Y') }}
                                                            - {{ $magang->carbonTglAkhir->format('d/m/Y') }}</td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-primary" data-dismiss="modal">Close
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-3">
                            <a href="{{ url('sertifikat_10hari') }}" target="_blank">
                                <div class="kt-portlet bg-secondary h-100">
                                    <div class="kt-portlet__head kt-ribbon kt-ribbon--warning">
                                        <div class="kt-ribbon__target"
                                             style="top: 15px; right: -14px; font-size: 22px;">
                                            <b>{{ $cetak_sertifikat }}</b></div>

                                        <div class="kt-portlet__head-label">
                                            <h3 class="kt-portlet__head-title text-dark widget-judul">
                                                Cetak Sertifikat
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__body text-dark">
                                        Jumlah Sertifikat yang Sudah dicetak
                                    </div>
                                </div>
                            </a>
                        </div>
                        <div class="col-3">
                            <div class="kt-portlet bg-warning h-100">
                                <a href="{{ url('sekolah') }}" target="_blank">
                                    <div class="kt-portlet__head kt-ribbon kt-ribbon--success">
                                        <div class="kt-ribbon__target bg-dark"
                                             style="top: 15px; right: -14px; font-size: 22px;"><b>{{ $sekolah_new }}</b>
                                        </div>

                                        <div class="kt-portlet__head-label">
                                            <h3 class="kt-portlet__head-title text-dark widget-judul">
                                                Sekolah / Universitas Baru
                                            </h3>
                                        </div>
                                    </div>
                                    <div class="kt-portlet__body text-dark">
                                        Sekolah atau Universitas Baru yang Butuh Approval Data
                                    </div>
                                </a>
                            </div>
                        </div>
                    </div>

                    <br><br><br>
                    <h2 class="text-center">Kalender Magang</h2>

                    <div id="calendar-magang">
                        <v-app>
                            <template>
                                <v-row class="fill-height">
                                    <v-col>
                                        <v-sheet height="64">
                                            <v-toolbar flat color="white">
                                                <v-btn outlined class="mr-4" v-on:click="setToday">
                                                    Today
                                                </v-btn>
                                                <v-btn fab text small v-on:click="prev">
                                                    <v-icon small>mdi-chevron-left</v-icon>
                                                </v-btn>
                                                <v-btn fab text small v-on:click="next">
                                                    <v-icon small>mdi-chevron-right</v-icon>
                                                </v-btn>
                                                <v-toolbar-title>@{{ title }}</v-toolbar-title>
                                                <v-spacer></v-spacer>
                                                <v-menu bottom right>
                                                    <template v-slot:activator="{ on }">
                                                        <v-btn
                                                                outlined
                                                                v-on="on"
                                                        >
                                                            <span>@{{ typeToLabel[type] }}</span>
                                                            <v-icon right>mdi-menu-down</v-icon>
                                                        </v-btn>
                                                    </template>
                                                    <v-list>
                                                        <v-list-item v-on:click="type = 'day'">
                                                            <v-list-item-title>Day</v-list-item-title>
                                                        </v-list-item>
                                                        <v-list-item v-on:click="type = 'week'">
                                                            <v-list-item-title>Week</v-list-item-title>
                                                        </v-list-item>
                                                        <v-list-item v-on:click="type = 'month'">
                                                            <v-list-item-title>Month</v-list-item-title>
                                                        </v-list-item>
                                                        <v-list-item v-on:click="type = '4day'">
                                                            <v-list-item-title>4 days</v-list-item-title>
                                                        </v-list-item>
                                                    </v-list>
                                                </v-menu>
                                            </v-toolbar>
                                        </v-sheet>
                                        <v-sheet height="600">
                                            <v-calendar
                                                    ref="calendar"
                                                    v-model="focus"
                                                    color="primary"
                                                    v-bind:events="events"
                                                    v-bind:event-color="getEventColor"
                                                    v-bind:event-margin-bottom="3"
                                                    v-bind:now="today"
                                                    v-bind:type="type"
                                                    v-on:click:event="showEvent"
                                                    v-on:click:more="viewDay"
                                                    v-on:click:date="viewDay"
                                                    v-on:change="updateRange"
                                            ></v-calendar>
                                            <v-menu
                                                    v-model="selectedOpen"
                                                    v-bind:close-on-content-click="false"
                                                    v-bind:activator="selectedElement"
                                                    offset-x
                                            >
                                                <v-card
                                                        color="grey lighten-4"
                                                        min-width="350px"
                                                        flat
                                                >
                                                    <v-toolbar
                                                            v-bind:color="selectedEvent.color"
                                                            dark
                                                    >
                                                        <v-toolbar-title v-html="selectedEvent.name"></v-toolbar-title>
                                                        <v-spacer></v-spacer>
                                                    </v-toolbar>
                                                    <v-card-text>
                                                        <span v-html="selectedEvent.details"></span>
                                                    </v-card-text>
                                                    <v-card-actions>
                                                        <v-btn
                                                                text
                                                                color="secondary"
                                                                v-on:click="selectedOpen = false"
                                                        >
                                                            Close
                                                        </v-btn>
                                                    </v-card-actions>
                                                </v-card>
                                            </v-menu>
                                        </v-sheet>
                                    </v-col>
                                </v-row>
                            </template>
                        </v-app>
                    </div>
                </div>
                <div class="kt-portlet__foot">
                <!-- {{ $magang_aktif_json }} -->
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pageScripts')
    <!-- <script src="https://keenthemes.com/metronic/themes/metronic/theme/default/demo1/dist/assets/plugins/global/plugins.bundle.js"></script>
<script src="https://keenthemes.com/metronic/themes/metronic/theme/default/demo1/dist/assets/js/scripts.bundle.js"></script> -->
    <link href="https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css" rel="stylesheet">
    <link href="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.min.css" rel="stylesheet">
    <script src="https://cdn.jsdelivr.net/npm/vue@2.x/dist/vue.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vuetify@2.x/dist/vuetify.js"></script>
    <style>
        .kt-menu__link-text {
            font-size: 0.8rem !important;
        }

        .fa {
            font-size: 1rem !important;
        }

        .v-application--wrap {
            min-height: 10vh !important;
        }

        .kt-footer__copyright {
            font-size: 0.8rem !important;
        }

        .btn {
            font-size: 0.8rem !important;
        }

        .kt-subheader__breadcrumbs-link.kt-subheader__breadcrumbs-link--active {
            font-size: 0.8rem !important;
        }

        .kt-header__topbar-welcome {
            font-size: 0.75rem !important;
        }

        #genelator-main_title {
            font-size: 1.65rem !important;
        }

        .widget-judul {
            font-size: 1rem !important;
        }
    </style>
    <script>
        var calendar_magang = new Vue({
            el: '#calendar-magang',
            vuetify: new Vuetify(),
            data: function () {
                return {
                    today: '<?php echo $tanggal_now; ?>',
                    focus: '<?php echo $tanggal_now; ?>',
                    type: 'month',
                    typeToLabel: {
                        month: 'Month',
                        week: 'Week',
                        day: 'Day',
                        '4day': '4 Days',
                    },
                    start: null,
                    end: null,
                    selectedEvent: {},
                    selectedElement: null,
                    selectedOpen: false,
                    events: <?php echo $magang_aktif_json; ?>
                };
            },
            computed: {
                title() {
                    const {start, end} = this
                    if (!start || !end) {
                        return ''
                    }

                    const startMonth = this.monthFormatter(start)
                    const endMonth = this.monthFormatter(end)
                    const suffixMonth = startMonth === endMonth ? '' : endMonth

                    const startYear = start.year
                    const endYear = end.year
                    const suffixYear = startYear === endYear ? '' : endYear

                    const startDay = start.day + this.nth(start.day)
                    const endDay = end.day + this.nth(end.day)

                    switch (this.type) {
                        case 'month':
                            return `${startMonth} ${startYear}`
                        case 'week':
                        case '4day':
                            return `${startMonth} ${startDay} ${startYear} - ${suffixMonth} ${endDay} ${suffixYear}`
                        case 'day':
                            return `${startMonth} ${startDay} ${startYear}`
                    }
                    return ''
                },
                monthFormatter() {
                    return this.$refs.calendar.getFormatter({
                        timeZone: 'UTC', month: 'long',
                    })
                },
            },
            mounted() {
                this.$refs.calendar.checkChange()
            },
            methods: {
                viewDay({date}) {
                    this.focus = date
                    this.type = 'day'
                },
                getEventColor(event) {
                    return event.color
                },
                setToday() {
                    this.focus = this.today
                },
                prev() {
                    this.$refs.calendar.prev()
                },
                next() {
                    this.$refs.calendar.next()
                },
                showEvent({nativeEvent, event}) {
                    const open = () => {
                        this.selectedEvent = event
                        this.selectedElement = nativeEvent.target
                        setTimeout(() => this.selectedOpen = true, 10)
                    }

                    if (this.selectedOpen) {
                        this.selectedOpen = false
                        setTimeout(open, 10)
                    } else {
                        open()
                    }

                    nativeEvent.stopPropagation()
                },
                updateRange({start, end}) {
                    // You could load events from an outside source (like database) now that we have the start and end dates on the calendar
                    this.start = start
                    this.end = end
                },
                nth(d) {
                    return d > 3 && d < 21
                        ? 'th'
                        : ['th', 'st', 'nd', 'rd', 'th', 'th', 'th', 'th', 'th', 'th'][d % 10]
                },
            },
        });
    </script>
    <script>
        jQuery(document).ready(function () {

        });
    </script>
@endsection
