<style>
    body,
    body *{
        font-family: "times";
        color: #333;
        margin: 0px;
    }

    .text-center {text-align: center;}
    .text-right {text-align: right;}
    .text-left {text-align: left;}
    .text-bold {font-weight: bold;}

    table {
        width: 100%;
        border-collapse: collapse;
    }
    th, td {
        vertical-align: top;
        padding: 5px;
        border: 1px solid #666666;
        text-align: left;
    }
    .table-borderless td {
        border: none;
    }

</style>

<head>
    <title>{{ $title }}</title>
</head>

<body>
    @yield('body')
</body>
