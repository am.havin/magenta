jQuery(document).ready(function () {
    initDatepicker();
    initSelect2();
});



//DATE PICKER
function initDatepicker(datepickerEle = '.datepicker') {
    let arrows;
    if (KTUtil.isRTL()) {
        arrows = {
            leftArrow: '<i class="la la-angle-right"></i>',
            rightArrow: '<i class="la la-angle-left"></i>'
        }
    } else {
        arrows = {
            leftArrow: '<i class="la la-angle-left"></i>',
            rightArrow: '<i class="la la-angle-right"></i>'
        }
    }

    $(datepickerEle).datepicker({
        format: 'yyyy-mm-dd',
        rtl: KTUtil.isRTL(),
        todayHighlight: true,
        orientation: "bottom left",
        templates: arrows,
        language: 'id',
        weekStart: 1,
    });

    
}

//SELECT2
function initSelect2(ele = '.select2ajax') {
    $(document).ready(function(){
        $(ele).each(function(i, obj) {
            if (!$(obj).hasClass("select2-hidden-accessible"))
            {
                $(ele).select2();
            }
        });
    });
}

//QUILL RTE
function initQuill(ele, textareaEle){
    var quill = new Quill(ele, {
        modules: {
            toolbar: [
                ['bold', 'italic', 'underline'],
                [{ 'list': 'ordered'}, { 'list': 'bullet' }],
                [ 'link', 'clean' ],
            ]
        },
        placeholder: '',
        theme: 'snow' // or 'bubble'
    });
    quill.on('text-change', function() {
        justHtml = quill.root.innerHTML;
        $(textareaEle).html(justHtml);
    });

    return quill;
}

//DATATABLE FILTER & REFRESH BUTTON
function initDatatableFilterAndRefreshButton(datatable) {
    $('#btnReloadDt').click(function (e) {
        datatable.ajax.reload();
    });

    $('#btnToggleFilter').click(function (e) {
        $('#sectionFilter').toggle('fast');
    });

    $('#filterForm').on('submit', function(e) {
        datatable.draw();
        e.preventDefault();
    });
}

//AJAX DELETE BUTTON
//TODO: harusnya elementnya dibuat dinamis!
function initDeleteButton(datatable = null, modalEle = null, successFunction = () => {}) {
    $('body').on('click', '.btn-dt-delete', function() {
        let url = $(this).attr('data-url');
        let csrf = $(this).attr('data-csrf');
        let redirectUrl = $(this).attr('data-redirect-url');

        //SWEET ALERT
        swal.fire({
            title: 'Yakin ingin menghapus?',
            text: "Data yang sudah dihapus tidak dapat dikembalikan!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonText: 'Ya, hapus!',
        }).then(function (result) {
            if (result.value) {
                KTApp.block('body', {
                    overlayColor: '#000000',
                    type: 'v2',
                    state: 'primary',
                    message: 'Processing...'
                });
                doDelete(url, csrf)
                .then(data => {
                    if(data.status){
                        KTApp.unblock('body');
                        swal.fire(
                            'Berhasil!',
                            'Data berhasil dihapus.',
                            'success'
                        ).then(function() {
                            if(redirectUrl != null){
                                window.location.replace(redirectUrl);
                            }
                        });
                        if(modalEle != null){
                            $(modalEle).modal('hide');
                        }
                        if(datatable != null){
                            datatable.ajax.reload();
                        }
                        successFunction();
                    }else{
                        KTApp.unblock('body');
                        swal.fire(
                            'Gagal!',
                            'Terjadi kesalahan, data anda belum dapat dihapus.',
                            'error'
                        );
                    }
                })
                .catch(error => {
                    console.log(error);
                    KTApp.unblock('body');
                    swal.fire(
                        'Error!',
                        'Terjadi kesalahan, data anda belum dapat dihapus.',
                        'error'
                    );
                })
            }
        });
    });

    function doDelete(url, csrf) {
        return new Promise((resolve, reject) => {
            $.ajax({
                url: url,
                type: 'POST',
                data: {
                    _token: csrf,
                    _method: 'DELETE'
                },
                success: function (data) {
                    resolve(data)
                },
                error: function (error) {
                    reject(error)
                },
            })
        });
    }
}

//AJAX MODAL
function initAjaxModalButton(linkEle){
    $(document).on('click', linkEle, function(){
        ajaxModal({
            url: $(this).attr('href'),
            modalContainerEle: '#modal',
            options: {
                backdrop: 'static',
                keyboard: false
            }
        });
        return false;
    });
}
function ajaxModal({url, modalContainerEle, options = {}}){
    KTApp.block('body', {
        overlayColor: '#000000',
        type: 'v2',
        state: 'primary',
        message: 'Processing...'
    });

    let modalPromise = new Promise((resolve, reject) => {
        $.ajax({
            url: url,
            type: 'GET',
            success: function (data) {
                resolve(data)
            },
            error: function (error) {
                reject(error)
            },
        }).then(data => {
            KTApp.unblock('body');
            $(modalContainerEle).html(data);
            $(modalContainerEle).modal(options);
        }).catch(error => {
            KTApp.unblock('body');
            console.log(error);
            toastr.error("GAGAL, terjadi kesalahan! Coba refresh halaman");
        })
    });
}

//AJAX FORM
function initAjaxForm(formEle, datatable = null){
    $(formEle).on('submit', function(event){
        event.preventDefault();
        if($(this).valid()){
            ajaxFormSubmit({
                form: $(this),
                initialFunction: function(){
                    // FUNCTION YANG DIJALANKAN SAAT SI FORM DISUBMIT
                    KTApp.block(formEle, {
                        overlayColor: '#000000',
                        type: 'v2',
                        state: 'primary',
                        message: ''
                    });
                },
                successFunction: function () {
                    // FUNCTION YANG DIJALANKAN KETIKA RETURN DARI CONTROLLER STATUS = TRUE
                    KTApp.unblock(formEle);
                    $('#modal').modal('hide'); //#modal ada di layout.blade.php
                    if(datatable != null){
                        datatable.ajax.reload();
                    }
                },
                failFunction: function(){
                    // FUNCTION YANG DIJALANKAN KETIKA RETURN DARI CONTROLLER STATUS = FALSE
                    KTApp.unblock(formEle);
                },
                errorFunction: function(){
                    // FUNCTION YANG DI JALANKAN KETIKA ADA ERROR DILUAR RETURN YANG DARI CONTROLLER
                    KTApp.unblock(formEle);
                }
            });
        }
    });
}
function ajaxFormSubmit({form, initialFunction = () => {}, successFunction = () => {}, failFunction = () => {}, errorFunction = () => {} }) {
    let promise = new Promise((resolve, reject) => {
        initialFunction();
        $.ajax({
            url: form.prop('action'),
            type: 'POST',
            data: new FormData(form[0]),
            contentType: false,
            processData: false,
            success: function (data) {
                resolve(data)
            },
            error: function (error) {
                reject(error)
            },
        })
    }).then(data => {
        if(data.status){
            successFunction();
            toastr.success(data.message);
        }else{
            failFunction();
            toastr.error(data.message);
        }
    }).catch(error => {
        console.log(error);
        errorFunction();
        toastr.error(helperReformatAjaxErrorMessage(error));
    });
}

// HELPER
function isset (accessor) {
    //https://stackoverflow.com/questions/2281633/javascript-isset-equivalent
    try {
        // Note we're seeing if the returned value of our function is not
        // undefined
        return typeof accessor() !== 'undefined'
    } catch (e) {
        // And we're able to catch the Error it would normally throw for
        // referencing a property of undefined
        return false
    }
}
function helperReformatAjaxErrorMessage(error, defaultMsg = 'ERROR, terjadi kesalahan! Coba refresh halaman'){
    //1. error jqueryvalidation.js
    let newErrorMsg = '';
    if(isset(() => error.responseJSON.errors)){
        Object.entries(error.responseJSON.errors).forEach(function([key, value]){
            newErrorMsg += '<li>'+value+'</li>';
        });
        return 'ERROR!<br><ul>' + newErrorMsg + '</ul>';
    }

    //2. siapkan untuk error2 lainnya

    return defaultMsg;
}