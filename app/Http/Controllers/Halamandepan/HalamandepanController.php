<?php

namespace App\Http\Controllers\Halamandepan;

use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

use App\Alert;
use App\Http\Controllers\Controller;
use App\Http\Requests\HalamandepanRequest;
use App\Halamandepan\Halamandepan;
use App\Toastr;


class HalamandepanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param HalamandepanRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(HalamandepanRequest $request)
    {
        //redirect ke halaman edit id = 1
        return redirect(route('halamandepan.edit', 1));

        //normal logic
        //$title = 'Halamandepan';
        //$activeMenus = ['menuDashboard'];
        //$breadcrumbs = [
        //    'Halamandepan' => route('halamandepan.index'),
        //];
        //$viewData = compact('title', 'activeMenus', 'breadcrumbs');

        //return view('halamandepans.index', $viewData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param HalamandepanRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(HalamandepanRequest $request)
    {
        $title = 'Tambah Halamandepan';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Halamandepan' => route('halamandepan.index'),
            'Tambah' => route('halamandepan.create'),
        ];
        $validator = \JsValidator::make($request->getCreateRules(), $request->messages());

        $viewData = compact('title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('halamandepans.create', $viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param HalamandepanRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(HalamandepanRequest $request)
    {
        try{
            $model = Halamandepan::create($request->all());
            $this->storeFiles($request, $model); //if there is any
            $json = ['status' => true, 'message' => 'Data berhasil disimpan!'];
        }catch(\Exception $e){
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data baru berhasil disimpan!');
        //return redirect(route('halamandepan.index'))->with('alert', $alert);
        $toastr = new Toastr('success', 'Data baru berhasil disimpan!');
        return redirect(route('halamandepan.index'))->with('toastr', $toastr);
    }

    /**
     * Display the specified resource.
     *
     * @param HalamandepanRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(HalamandepanRequest $request, $id)
    {
        $model = Halamandepan::findOrFail($id);
        $title = 'Detail Halamandepan';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Halamandepan' => route('halamandepan.index'),
            'Detail' => route('halamandepan.show', $model->id),
        ];
        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs');

        return view('halamandepans.show', $viewData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param HalamandepanRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(HalamandepanRequest $request, $id)
    {
        //hanya boleh id == 1
        if($id != 1){
            abort(404);
        }

        $model = Halamandepan::findOrFail($id);
        $title = 'Edit Halaman Depan';
        $activeMenus = ['menuHalamanDepan'];
        $breadcrumbs = [
            'Halaman Depan' => route('halamandepan.index'),
            'Edit' => route('halamandepan.edit', $model->id),
        ];
        $validator = \JsValidator::make($request->getEditRules(), $request->messages());

        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('halamandepans.edit', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param HalamandepanRequest $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(HalamandepanRequest $request, $id)
    {
        try{
            $data = $request->all();
            $data['slogan'] = ($request->slogan) ? $request->slogan : '';
            $data['introduksi'] = ($request->introduksi) ? $request->introduksi : '';
            $data['teks_gambar'] = ($request->teks_gambar) ? $request->teks_gambar : '';
            $data['step1_judul'] = ($request->step1_judul) ? $request->step1_judul : '';
            $data['step2_judul'] = ($request->step2_judul) ? $request->step2_judul : '';
            $data['step3_judul'] = ($request->step3_judul) ? $request->step3_judul : '';
            $data['step1_teks'] = ($request->step1_teks) ? $request->step1_teks : '';
            $data['step2_teks'] = ($request->step2_teks) ? $request->step2_teks : '';
            $data['step3_teks'] = ($request->step3_teks) ? $request->step3_teks : '';

            $model =  Halamandepan::findOrFail($id);
            $model->update($data);
            $this->storeFiles($request, $model); //if there is any
            $json = ['status' => true, 'message' => 'Data berhasil diupdate!'];
        }catch(\Exception $e){
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data berhasil diupdate!');
        //return redirect()->back()->with('alert', $alert);
        $toastr = new Toastr('success', 'Data berhasil diupdate!');
        return redirect()->back()->with('toastr', $toastr);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param HalamandepanRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(HalamandepanRequest $request, $id)
    {
        $model = Halamandepan::findOrFail($id);
        return response()->json([
            'status' => $model->doDelete()
        ]);
    }

    /**
     * Handle Datatable request.
     *
     * @param HalamandepanRequest $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function datatable(HalamandepanRequest $request)
    {
        return Datatables::of(Halamandepan::query())
            ->filter(function ($query) use ($request) {
                
                if (!empty($request->input('filter_slogan'))) {
                    $query->where('slogan', 'ilike', "%{$request->input('filter_slogan')}%");
                }
                if (!empty($request->input('filter_introduksi'))) {
                    $query->where('introduksi', 'ilike', "%{$request->input('filter_introduksi')}%");
                }
                if (!empty($request->input('filter_teks_gambar'))) {
                    $query->where('teks_gambar', 'ilike', "%{$request->input('filter_teks_gambar')}%");
                }
                if (!empty($request->input('filter_step1_judul'))) {
                    $query->where('step1_judul', 'ilike', "%{$request->input('filter_step1_judul')}%");
                }
                if (!empty($request->input('filter_step1_teks'))) {
                    $query->where('step1_teks', 'ilike', "%{$request->input('filter_step1_teks')}%");
                }
                if (!empty($request->input('filter_step2_judul'))) {
                    $query->where('step2_judul', 'ilike', "%{$request->input('filter_step2_judul')}%");
                }
                if (!empty($request->input('filter_step2_teks'))) {
                    $query->where('step2_teks', 'ilike', "%{$request->input('filter_step2_teks')}%");
                }
                if (!empty($request->input('filter_step3_judul'))) {
                    $query->where('step3_judul', 'ilike', "%{$request->input('filter_step3_judul')}%");
                }
                if (!empty($request->input('filter_step3_teks'))) {
                    $query->where('step3_teks', 'ilike', "%{$request->input('filter_step3_teks')}%");
                }
                if (!empty($request->input('filter_gambar_depan'))) {
                    $query->where('gambar_depan', 'ilike', "%{$request->input('filter_gambar_depan')}%");
                }
            })
            ->addColumn('action', function ($data) {
                $show = $edit = $delete = '';
                if(auth()->user()->hasAnyPermission(['Halamandepan - show'])){
                    $show = '<a href="'.route('halamandepan.show', $data->id).'" class="btn btn-outline-hover-warning btn-icon btn-circle btn-sm" data-toggle="kt-tooltip" title="Show detail" data-skin="brand"><i class="la la-eye kt-font-warning"></i></a>';
                }
                if(auth()->user()->hasAnyPermission(['Halamandepan - edit'])){
                    $edit = '<a href="'.route('halamandepan.edit', $data->id).'"class="btn btn-outline-hover-info btn-icon btn-circle btn-sm btnEdit" data-toggle="kt-tooltip" title="Edit" data-skin="brand"><i class="la la-pencil kt-font-info"></i></a>';
                }
                if(auth()->user()->hasAnyPermission(['Halamandepan - delete'])){
                    $delete = '<button type="button" class="btn btn-outline-hover-danger btn-icon btn-circle btn-sm btn-dt-delete" data-toggle="kt-tooltip" title="Delete" data-skin="brand" data-url="'.route('halamandepan.destroy', $data->id).'" data-csrf="'.csrf_token().'"><i class="la la-close kt-font-danger"></i></button>';
                }
                return $show.$edit.$delete;
            })->make(true);
    }

    /**
     * Handle file store
     *
     * @param $request
     * @param $model
     */
    private function storeFiles($request, $model)
    {
        foreach($request->files as $key => $file){
            if ($request->file($key)->isValid()) {
                Storage::delete($model->$key);
                $model->$key = $request->file($key)->storeAs("public/$key/$model->id", $file->getClientOriginalName(),'local');
                $model->save();
            }
        }
    }
}
