<?php

/*
|--------------------------------------------------------------------------
| ROUTE FRONTEND:
|--------------------------------------------------------------------------
*/
Auth::routes(['verify' => true]);
Route::get('logout', 'Auth\LoginController@logout');

Route::get('/', 'Frontends\HomeController@index');
Route::get('kontak', 'Frontends\KontakController@index');
Route::get('faqfront', 'Frontends\FaqController@index');
Route::get('gambarpengumuman/{pengumuman_id}', 'Frontends\PengumumanController@gambar');

# PUBLIC SELECT2
Route::group(['prefix' => 'pub_select2'], function () {
    #Sekolah
    Route::get('allsekolah', '\App\Http\Controllers\Select2\SekolahController@all');
    Route::get('sekolah/{is_universitas?}', '\App\Http\Controllers\Select2\SekolahController@index');
    Route::get('sekolah_with_lain_lain/{is_universitas?}', '\App\Http\Controllers\Select2\SekolahController@sekolah_with_lain_lain');

    #Program Studi
    Route::get('programstudi', '\App\Http\Controllers\Select2\ProgramStudiController@index');
    Route::get('programstudi_with_lain_lain', '\App\Http\Controllers\Select2\ProgramStudiController@programstudi_with_lain_lain');
});

# HARUS SUDAH LOGIN SEBAGAI PENDAFTAR
Route::middleware(['web', 'auth:pendaftar', 'verified'])->group(function () {
    # FRONTEND SELECT2
    Route::group(['prefix' => 'front_select2'], function () {
        #PESERTA
        Route::get('ownpesertas', '\App\Http\Controllers\Select2\PesertaController@ownPesertas');

        #ALOKASI
        Route::get('alokasi_unitkerjas', '\App\Http\Controllers\Select2\AlokasiController@unitkerjas');
    });

    # PESERTA MAGANG
    Route::get('peserta_magang/datatable', 'Frontends\PesertaController@datatable');
    Route::get('peserta_magang/foto/{peserta_id}', 'Frontends\PesertaController@foto')->name('peserta_magang.foto');
    Route::get('peserta_magang/cv/{peserta_id}', 'Frontends\PesertaController@cv')->name('peserta_magang.cv');
    Route::get('peserta_magang/skck/{peserta_id}', 'Frontends\PesertaController@skck')->name('peserta_magang.skck');
    Route::resource('peserta_magang', 'Frontends\PesertaController');

    # STEP 1
    Route::get('pendaftaran/step1', 'Frontends\Pendaftaran\Step1Controller@index')->name('pendaftaran.step1.index');
    Route::get('pendaftaran/step1/{pengajuanId}/edit', 'Frontends\Pendaftaran\Step1Controller@edit')->name('pendaftaran.step1.edit');
    Route::get('pendaftaran/step1/unavailable_dates/{unit}/{bulan}/{tahun}/{exceptMagangId?}', 'Frontends\Pendaftaran\Step1Controller@unavailableDates')->name('pendaftaran.step1.unavailable_dates');

    # PENDAFTARAN MAGANG
    Route::get('pendaftaran', 'Frontends\Pendaftaran\PendaftaranController@index')->name('pendaftaran');
    Route::post('pendaftaran/{magangId}/approve', 'Frontends\Pendaftaran\PendaftaranController@approve')->name('pendaftaran.magang.approve');
    Route::get('pendaftaran/{pendaftaranId}', 'Frontends\Pendaftaran\PendaftaranController@show')->name('pendaftaran.show');
    Route::get('pendaftaran/{pendaftaranId}/surat_pengantar_resmi', 'Frontends\Pendaftaran\PendaftaranController@suratPengantarResmi')->name('pendaftaran.surat_pengantar_resmi');
    Route::get('pendaftaran/{pendaftaranId}/proposal', 'Frontends\Pendaftaran\PendaftaranController@proposal')->name('pendaftaran.proposal');
    Route::delete('pendaftaran/{pengajuanId}', 'Frontends\Pendaftaran\PendaftaranController@destroy')->name('pendaftaran.destroy');

    # MAGANG
    Route::get('pendaftaran/step1/{pengajuanId}/magang/datatable', 'Frontends\Pendaftaran\MagangController@datatable')->name('pendaftaran.step1.magang.datatable');
    Route::get('pendaftaran/step1/{pengajuanId}/magang/create', 'Frontends\Pendaftaran\MagangController@create')->name('pendaftaran.step1.magang.create');
    Route::post('pendaftaran/step1/{pengajuanId}/magang/store', 'Frontends\Pendaftaran\MagangController@store')->name('pendaftaran.step1.magang.store');
    Route::get('pendaftaran/step1/{pengajuanId}/magang/{magangId}/edit', 'Frontends\Pendaftaran\MagangController@edit')->name('pendaftaran.step1.magang.edit');
    Route::put('pendaftaran/step1/{pengajuanId}/magang/{magangId}/update', 'Frontends\Pendaftaran\MagangController@update')->name('pendaftaran.step1.magang.update');
    Route::delete('pendaftaran/step1/{pengajuanId}/magang/{magangId}/delete', 'Frontends\Pendaftaran\MagangController@destroy')->name('pendaftaran.step1.magang.delete');

    # STEP 2
    Route::get('pendaftaran/step2/{pengajuanId}', 'Frontends\Pendaftaran\Step2Controller@index')->name('pendaftaran.step2.index');
    Route::put('pendaftaran/step2/{pengajuanId}', 'Frontends\Pendaftaran\Step2Controller@update')->name('pendaftaran.step2.update');
    Route::get('pendaftaran/step2/{pengajuanId}/surat_pengantar_resmi', 'Frontends\Pendaftaran\Step2Controller@suratPengatarResmi')->name('pendaftaran.step2.surat_pengantar_resmi');
    Route::get('pendaftaran/step2/{pengajuanId}/proposal', 'Frontends\Pendaftaran\Step2Controller@proposal')->name('pendaftaran.step2.proposal');

    #INFO PENEMPATAN
    Route::get('info_penempatan', 'Frontends\PenempatanController@index');

    # AKUN
    Route::get('akun', 'Frontends\AkunController@index');
    Route::get('akun/images/{id}/{file}', 'Frontends\AkunController@fotopendaftar');
    Route::post('akundataupdate', 'Frontends\AkunController@updatedata');
    Route::post('akunpasswordupdate', 'Frontends\AkunController@updatepassword');
});


/*
|--------------------------------------------------------------------------
| ROUTE BACKEND:
|--------------------------------------------------------------------------
*/

Route::middleware(['cas', 'isSimanisUser', 'manualLogin'])->group(function () {
    # ADMIN
    Route::redirect('admin', 'dashboard');

    # LOGOUT
    Route::get('adminlogout', 'AuthController@logout');

    # ROLES
    Route::get('roles/datatable', 'RoleController@datatable');
    Route::resource('roles', 'RoleController');

    # USERS
    Route::get('users/datatable', 'UserController@datatable');
    Route::resource('users', 'UserController');

    # PERMISSIONS
    Route::resource('permission', 'PermissionController');

    # ROLE MENUS
    Route::resource('rolemenus', 'RolemenuController');

    # FILE DOWNLOAD
    Route::get('filedownload', function (\Illuminate\Http\Request $request) {
        if (Storage::disk('local')->exists($request->input('p'))) {
            return Storage::download($request->input('p'));
        }
        abort(404);
    });

    # SELECT2
    Route::group(['prefix' => 'select2'], function () {
        #Roles
        Route::get('roles', '\App\Http\Controllers\Select2\RolesController@index');

        #Sekolah
        Route::get('sekolah', '\App\Http\Controllers\Select2\SekolahController@index');

        #Program Studi
        Route::get('programstudi', '\App\Http\Controllers\Select2\ProgramStudiController@index');

        #Unit
        Route::get('magang/unitkerja_magang_yg_diterima', '\App\Http\Controllers\Select2\MagangController@unitkerja_magang_yg_diterima');

        #Alokasi
        Route::get('alokasi_unitkerjas', '\App\Http\Controllers\Select2\AlokasiController@unitkerjas');
    });

    # DASHBOARD
    Route::get('dashboard', 'Dashboards\DashboardController@index');

    # PENDAFTAR
    Route::get('pendaftar/datatable', 'Pendaftar\PendaftarController@datatable');
    Route::get('pendaftar/foto/{pendaftar_id}', 'Pendaftar\PendaftarController@foto')->name('pendaftar.foto');
    Route::resource('pendaftar', 'Pendaftar\PendaftarController');

    # SEKOLAH
    Route::get('sekolah/datatable', 'Sekolah\SekolahController@datatable');
    Route::resource('sekolah', 'Sekolah\SekolahController');

    # PROGRAM STUDI
    Route::get('sekolahprogramstudi/datatable', 'SekolahProgramStudi\SekolahProgramStudiController@datatable');
    Route::resource('sekolahprogramstudi', 'SekolahProgramStudi\SekolahProgramStudiController');

    # FAQ
    Route::get('faq/datatable', 'Faq\FaqController@datatable');
    Route::resource('faq', 'Faq\FaqController');

    # KONTAK KAMI
    Route::get('kontakkami/datatable', 'Kontakkami\KontakkamiController@datatable');
    Route::resource('kontakkami', 'Kontakkami\KontakkamiController');

    # HALAMAN DEPAN
    Route::get('halamandepan/datatable', 'Halamandepan\HalamandepanController@datatable');
    Route::resource('halamandepan', 'Halamandepan\HalamandepanController');

    # ALOKASI
    Route::get('alokasi/datatable', 'Alokasi\AlokasiController@datatable');
    Route::resource('alokasi', 'Alokasi\AlokasiController');

    # DATA PESERTA
    Route::get('peserta/datatable', 'Peserta\PesertaController@datatable');
    Route::get('peserta/foto/{peserta_id}', 'Peserta\PesertaController@foto')->name('peserta.foto');
    Route::resource('peserta', 'Peserta\PesertaController');

    # DATA MAGANG
    Route::get('magang/datatable', 'Magang\MagangController@datatable');
    Route::get('magang/unavailable_dates/{unit}/{bulan}/{tahun}/{exceptMagangId?}', 'Magang\MagangController@unavailableDates')->name('magang.unavailable_dates');
    Route::resource('magang', 'Magang\MagangController');

    # APPROVAL
    Route::get('pengajuan/datatable', 'Pengajuan\PengajuanController@datatable');
    Route::post('pengajuan/approval/{idMagang}', 'Pengajuan\PengajuanController@approval')->name('pengajuan.approval');
    Route::resource('pengajuan', 'Pengajuan\PengajuanController');

    # SURAT PERSETUJUAN
    Route::get('surat_persetujuan/datatable', 'SuratPersetujuan\SuratPersetujuanController@datatable')->name('surat_persetujuan.datatable');
    Route::get('surat_persetujuan', 'SuratPersetujuan\SuratPersetujuanController@index')->name('surat_persetujuan.index');
    Route::get('surat_persetujuan/{idMagang}/edit', 'SuratPersetujuan\SuratPersetujuanController@edit')->name('surat_persetujuan.edit');
    Route::put('surat_persetujuan/{idMagang}', 'SuratPersetujuan\SuratPersetujuanController@update')->name('surat_persetujuan.update');

    # PENGUMUMAN
    Route::get('pengumuman/datatable', 'Pengumuman\PengumumanController@datatable');
    Route::get('pengumuman/gambar/{pengumuman_id}', 'Pengumuman\PengumumanController@gambar');
    Route::resource('pengumuman', 'Pengumuman\PengumumanController');

    # SERTIFIKAT
    Route::get('sertifikat/datatable', 'Sertifikat\SertifikatController@datatable')->name('sertifikat.datatable');
    Route::get('sertifikat/datatable_10hari', 'Sertifikat\SertifikatController@datatable10hari')->name('sertifikat.datatable10hari');
    Route::get('sertifikat', 'Sertifikat\SertifikatController@index')->name('sertifikat.index');
    Route::get('sertifikat_10hari', 'Sertifikat\SertifikatController@index10hari')->name('sertifikat.index10hari');
    Route::get('sertifikat/{idMagang}/edit', 'Sertifikat\SertifikatController@edit')->name('sertifikat.edit');
    Route::put('sertifikat/{idMagang}', 'Sertifikat\SertifikatController@update')->name('sertifikat.update');
    Route::get('sertifikat/{idMagang}/cetak', 'Sertifikat\SertifikatController@formCetak')->name('sertifikat.form_cetak');
    Route::post('sertifikat/{idMagang}/cetak', 'Sertifikat\SertifikatController@cetak')->name('sertifikat.cetak');
    Route::get('sertifikat/get_nilai/{nilai}', 'Sertifikat\SertifikatController@getNilai')->name('sertifikat.get_nilai');

    # INDEX NILAI
    Route::get('indexnilai/datatable', 'IndexNilai\IndexNilaiController@datatable');
    Route::resource('indexnilai', 'IndexNilai\IndexNilaiController');

    # TEST
//    Route::get('test', function(){
//        dd(auth('pendaftar')->user()->hasNeedYourApprovalMagangs);
//    });
});

Route::get('penempatan/datatable', 'Penempatan\PenempatanController@datatable');
Route::resource('penempatan', 'Penempatan\PenempatanController');
