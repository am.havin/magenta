<?php

namespace App\Http\Controllers\Frontends\Pendaftaran;

use App\Alokasi\Alokasi;
use App\Generators\Request;
use App\Helper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Step1Request;
use App\Magang\Magang;
use App\Pengajuan\Pengajuan;
use App\Peserta\Peserta;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;


class Step1Controller extends Controller
{
    protected $pendaftar;

    /**
     * Step1Controller constructor.
     */
    public function __construct()
    {
        Helper::findThenHandleExpired(); //untuk check pengajuan magang yg udah expired

        //harus begini karena auth()->user() nge-return null kalau belum ngelewatin middleware
        $this->middleware(function ($request, $next) {
            $this->pendaftar= auth('pendaftar')->user();
            return $next($request);
        });
    }

    public function index()
    {
        $title = 'Proses Pengajuan - Pilih Peserta';
        $activeMenus = ['menuPendaftaran'];
        $this->pendaftar->clearDraftPengajuans();
        $pengajuan = $this->pendaftar->generateDraftPengajuan();

        $viewData = compact('title', 'activeMenus', 'pengajuan');
        return view('frontends.pendaftaran.step1.index', $viewData);
    }

    public function edit(Step1Request $request, $pengajuanId)
    {
        $title = 'Proses Pengajuan - Pilih Peserta';
        $activeMenus = ['menuPendaftaran'];
        $pengajuan = Pengajuan::findOrFail($pengajuanId);

        $viewData = compact('title', 'activeMenus', 'pengajuan');
        return view('frontends.pendaftaran.step1.index', $viewData);
    }

    public function unavailableDates(Request $request, $idUnit, $bulan, $tahun, $exceptMagangId = null)
    {
        $dates = Helper::getUnavailableDates($idUnit, $bulan, $tahun, $exceptMagangId);
        return response()->json($dates);
    }
}
