<?php

namespace App\Generators;

use Carbon\Carbon;
use Illuminate\Http\File;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Symfony\Component\Yaml\Yaml;

class Model
{
    private $stubString;

    /**
     * Model constructor.
     */
    public function __construct()
    {
        $stubFileLoc        = resource_path('stubs/model.stub');
        $this->stubString   = file_get_contents($stubFileLoc);
    }

    /**
     * @param $tableName
     * @param $modelData
     * @return string
     */
    public function generate($tableName, $modelData)
    {
        $modelString = $this->replaceStubString($modelData['class_name'], $modelData['namespace'], $tableName);
        return $this->createFile($modelData['class_name'], $modelData['file_dir'], $modelString);
    }

    /**
     * @param $className
     * @param $nameSpace
     * @param $tableName
     * @return mixed
     */
    private function replaceStubString($className, $namespace, $tableName)
    {
        $replacedString = str_replace(
            [
                '{{namespace}}',
                '{{className}}',
                '{{tableName}}'
            ],
            [
                $namespace,
                $className,
                $tableName
            ],
            $this->stubString
        );
        return $replacedString;
    }

    /**
     * @param $className
     * @param $file_dir
     * @param $modelString
     * @return string
     */
    private function createFile($className, $file_dir, $modelString)
    {
        $fileDir = 'Models/'.$file_dir;
        if(!is_dir(app_path($fileDir))){ //bikin direktorinya  kalau belum ada
            mkdir(app_path($fileDir), 0755, true);
        }

        $filepath = Str::finish($fileDir , '/') . "$className.php";
        file_put_contents(app_path($filepath), $modelString);

        return $filepath;
    }
}
