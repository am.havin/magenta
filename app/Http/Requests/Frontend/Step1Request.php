<?php

namespace App\Http\Requests\Frontend;

use App\Alokasi\Alokasi;
use App\Helper;
use App\Magang\Magang;
use App\Pengajuan\Pengajuan;
use App\Peserta\Peserta;
use App\Rules\MaxDayDuration;
use App\Rules\MinDayDuration;
use App\Rules\TidakBolehMotongTglYgDisabled;
use Carbon\Carbon;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class Step1Request extends FormRequest
{
    public $rules = [
        'id_peserta' => 'required',
        'id_unit' => 'required',
        'tgl_awal' => 'required|date_format:Y-m-d|after:yesterday',
        'tgl_akhir' => 'required|date_format:Y-m-d|after:yesterday',
    ];

    public function getCreateRules()
    {
        $rules =  $this->rules;

        //peserta
        $pesertas = Peserta::getCanBeSubmitted();
        $rules['id_peserta'] = [
            'required',
            Rule::in($pesertas->pluck('id'))
        ];

        //unit
        $alokasis = Alokasi::where('kuota', '>', 0)->get();
        $rules['id_unit'] = [
            'required',
            Rule::in($alokasis->pluck('id_unit'))
        ];

        //tgl_awal
        $tglAwal = Carbon::today();
        $unAvailableDates = Helper::getUnavailableDates($this->id_unit, $tglAwal->month, $tglAwal->year);
        $rules['tgl_awal'] = [
            'required',
            'date_format:Y-m-d',
            'after:yesterday',
            'before:+1 year',
            Rule::notIn($unAvailableDates),
        ];

        //tgl_akhir
        $tglAkhir = Carbon::today();
        $unAvailableDates = Helper::getUnavailableDates($this->id_unit, $tglAkhir->month, $tglAkhir->year);
        $rules['tgl_akhir'] = [
            'required',
            'date_format:Y-m-d',
            'after_or_equal:tgl_awal',
            'before:+1 year',
            Rule::notIn($unAvailableDates),
            new MinDayDuration(14),
            new MaxDayDuration(60),
        ];

        return $rules;
    }

    public function getStoreRules()
    {
        $rules =  $this->rules;

        //peserta
        $pesertas = Peserta::getCanBeSubmitted();
        $rules['id_peserta'] = [
            'required',
            Rule::in($pesertas->pluck('id'))
        ];

        //unit
        $alokasis = Alokasi::where('kuota', '>', 0)->get();
        $rules['id_unit'] = [
            'required',
            Rule::in($alokasis->pluck('id_unit'))
        ];

        //tgl_awal
        $tglAwal = Carbon::createFromFormat('Y-m-d', $this->tgl_awal);
        $unAvailableDates = Helper::getUnavailableDates($this->id_unit, $tglAwal->month, $tglAwal->year);
        $rules['tgl_awal'] = [
            'required',
            'date_format:Y-m-d',
            'after:yesterday',
            'before:+1 year',
            Rule::notIn($unAvailableDates),
        ];

        //tgl_akhir
        $tglAkhir = Carbon::createFromFormat('Y-m-d', $this->tgl_akhir);
        $unAvailableDates = Helper::getUnavailableDates($this->id_unit, $tglAkhir->month, $tglAkhir->year);
        $rules['tgl_akhir'] = [
            'required',
            'date_format:Y-m-d',
            'after_or_equal:tgl_awal',
            'before:+1 year',
            Rule::notIn($unAvailableDates),
            new MinDayDuration(14),
            new MaxDayDuration(60),
            new TidakBolehMotongTglYgDisabled(),
        ];

        return $rules;
    }

    public function getEditRules()
    {
        $rules =  $this->rules;

        //peserta
        $pesertas = Peserta::getCanBeSubmitted();
        $rules['id_peserta'] = [
            Rule::in($pesertas->pluck('id'))
        ];

        //unit
        $alokasis = Alokasi::where('kuota', '>', 0)->get();
        $rules['id_unit'] = [
            'required',
            Rule::in($alokasis->pluck('id_unit'))
        ];

        //tgl_awal
        $idExceptMagang = array_values(request()->route()->originalParameters())[1];
        $tglAwal = Carbon::today();
        $unAvailableDates = Helper::getUnavailableDates($this->id_unit, $tglAwal->month, $tglAwal->year, $idExceptMagang);
        $rules['tgl_awal'] = [
            'required',
            'date_format:Y-m-d',
            'after:yesterday',
            'before:+1 year',
            Rule::notIn($unAvailableDates),
        ];

        //tgl_akhir
        $idExceptMagang = array_values(request()->route()->originalParameters())[1];
        $tglAkhir = Carbon::today();
        $unAvailableDates = Helper::getUnavailableDates($this->id_unit, $tglAkhir->month, $tglAkhir->year, $idExceptMagang);
        $rules['tgl_akhir'] = [
            'required',
            'date_format:Y-m-d',
            'after_or_equal:tgl_awal',
            'before:+1 year',
            Rule::notIn($unAvailableDates),
            new MinDayDuration(14),
            new MaxDayDuration(60),
        ];

        return $rules;
    }

    public function getUpdateRules()
    {
        $rules =  $this->rules;

        //peserta
        $pesertas = Peserta::getCanBeSubmitted();
        $rules['id_peserta'] = [
            Rule::in($pesertas->pluck('id'))
        ];

        //unit
        $alokasis = Alokasi::where('kuota', '>', 0)->get();
        $rules['id_unit'] = [
            'required',
            Rule::in($alokasis->pluck('id_unit'))
        ];

        //tgl_awal
        $idExceptMagang = array_values(request()->route()->originalParameters())[1];
        $tglAwal = Carbon::today();
        $unAvailableDates = Helper::getUnavailableDates($this->id_unit, $tglAwal->month, $tglAwal->year, $idExceptMagang);
        $rules['tgl_awal'] = [
            'required',
            'date_format:Y-m-d',
            'after:yesterday',
            'before:+1 year',
            Rule::notIn($unAvailableDates),
        ];

        //tgl_akhir
        $idExceptMagang = array_values(request()->route()->originalParameters())[1];
        $tglAkhir = Carbon::today();
        $unAvailableDates = Helper::getUnavailableDates($this->id_unit, $tglAkhir->month, $tglAkhir->year, $idExceptMagang);
        $rules['tgl_akhir'] = [
            'required',
            'date_format:Y-m-d',
            'after_or_equal:tgl_awal',
            'before:+1 year',
            Rule::notIn($unAvailableDates),
            new MinDayDuration(14),
            new MaxDayDuration(60),
            new TidakBolehMotongTglYgDisabled($idExceptMagang),
        ];

        return $rules;
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        //cek kepemilikan pengajuan
        $idPengajuan = array_values(request()->route()->originalParameters())[0];
        $pendaftar = auth('pendaftar')->user();
        $pengajuan = Pengajuan::where('id', $idPengajuan)->where('id_pendaftar', $pendaftar->id)->first();
        $return = ($pengajuan) ? true : false;

        //cek juga apakah sudah disubmit / belum? (klo sudah nggak boleh di akses)
        $return = ($pengajuan->is_submitted) ? false : true;

        //cek magang yang dipilih apakah benar milik pengajuan ini
        if($return){
            $idMagang = isset(array_values(request()->route()->originalParameters())[1])
                ? array_values(request()->route()->originalParameters())[1]
                : null;

            if($idMagang){
                $magangs = $pengajuan->magangs()->where('id', $idMagang)->whereNull('is_accepted')->get();
                $return = ($magangs->isNotEmpty()) ? true : false;
            }
        }

        return $return;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $controllerMethod = request()->route()->getActionMethod();
        if ($controllerMethod == 'store') {
            return $this->getStoreRules();
        }
        if ($controllerMethod == 'update') {
            return $this->getUpdateRules();
        }
        return [];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'id_peserta.required' => 'Peserta harus dipilih',
            'id_unit.required'  => 'Unit kerja tujuan harus dipilih',
            'id_unit.in'  => 'Unit kerja tujuan tidak tersedia',

            'tgl_awal.required'  => 'Tanggal awal magang harus diisi',
            'tgl_awal.before'  => 'Tanggal pengajuan magang hanya dapat dilakukan dalam lingkup waktu 1 tahun dari hari ini',

            'tgl_akhir.required'  => 'Tanggal akhir magang harus diisi',
            'tgl_akhir.after_or_equal'  => 'Tanggal akhir magang harus lebih, atau sama dengan tanggal awal magang',
            'tgl_akhir.before'  => 'Tanggal pengajuan magang hanya dapat dilakukan dalam lingkup waktu 1 tahun dari hari ini',
        ];
    }
}
