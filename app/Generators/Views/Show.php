<?php

namespace App\Generators\Views;

use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Symfony\Component\Yaml\Yaml;

class Show
{

    private $stubString;

    /**
     * Create constructor.
     */
    public function __construct()
    {
        $stubFileLoc        = resource_path('stubs/views/show.stub');
        $this->stubString   = file_get_contents($stubFileLoc);
    }

    /**
     * @param $fieldsData
     * @param $routeData
     * @param $viewsData
     * @return string
     */
    public function generate($fieldsData, $routeData, $viewsData)
    {
        $fieldsString = $this->generateData($fieldsData);
        $showString = $this->replaceStubString($routeData['url'], $viewsData['title'], $fieldsString);
        return $this->createFile($viewsData['file_dir'], $showString);
    }

    private function generateData($fields)
    {
        $return = '';
        $i = 0;
        foreach($fields as $key => $field){
            if($i == 0 || $i%2 == 0){
                $return .= "<div class=\"kt-widget12__item\">";
            }

            $value = '{{ $model->'.$key.' }} ';
            $return .= "
                <div class=\"kt-widget12__info\">
                    <span class=\"kt-widget12__desc\">".$field['label']."</span>
                    <span class=\"kt-widget12__value\">$value</span>
                </div>
            ";

            if($i%2 != 0){
                $return .= "</div>";
            }
            $i++;
        }

        return $return;
    }

    /**
     * @param $route
     * @param $title
     * @param $fieldString
     * @return mixed
     */
    private function replaceStubString($route, $title, $fieldString)
    {
        $replacedString = str_replace(
            [
                '{{title}}',
                '{{routeUrl}}',
                '{{routeName}}',
                '{{fields}}',
            ],
            [
                $title,
                $routeUrl = Str::finish($route, '/'),
                str_replace('/', '.', $routeUrl),
                $fieldString,
            ],
            $this->stubString
        );
        return $replacedString;
    }

    /**
     * @param $file_dir
     * @param $string
     * @return string
     */
    private function createFile($file_dir, $string)
    {
        $fileDir = 'views/'.$file_dir;
        if(!is_dir(resource_path($fileDir))){ //bikin direktorinya  kalau belum ada
            mkdir(resource_path($fileDir), 0755, true);
        }

        $filepath = Str::finish($fileDir , '/') . "show.blade.php";
        file_put_contents(resource_path($filepath), $string);

        return $filepath;
    }
}
