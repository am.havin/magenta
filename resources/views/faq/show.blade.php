@extends((Request::ajax()) ? 'templates.layout-blank' : 'templates.'.config('genelator.template').'.layout')


@section('subheaderToolbar')
    <a href="#" class="btn invisible">
    </a>
@endsection

@section('content')
    <div class="row">
        <div class="col-sm-12">
            <!--begin:: Widgets -->
            <div class="kt-portlet kt-portlet--height-fluid">
                <div class="kt-portlet__head">
                    <div class="kt-portlet__head-label">
                        <h3 class="kt-portlet__head-title">
                            Detail Faq :
                        </h3>
                    </div>
                    <div class="kt-portlet__head-toolbar">
                        <div class="kt-portlet__head-toolbar">
                            <div class="btn-group">
                                <button type="button" class="btn btn-warning dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Actions</button>
                                <div class="dropdown-menu dropdown-menu-right" x-placement="bottom-end" style="position: absolute; will-change: transform; top: 0px; left: 0px; transform: translate3d(-149px, 33px, 0px);">
                                    <a class="dropdown-item" href="{{ route('faq.edit', $model->id) }}">
                                        <i class="la la-pencil"></i>
                                        Edit
                                    </a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item kt-font-danger btn-dt-delete" href="javascript:;"
                                       data-url="{{ route('faq.destroy', $model->id) }}" data-csrf="{{ csrf_token() }}"
                                       data-redirect-url="{{ route('faq.index') }}">
                                        <i class="la la-close kt-font-danger"></i>
                                        Delete
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="kt-portlet__body">
                    <div class="kt-widget12">
                        <div class="kt-widget12__content">
                            <div class="kt-widget12__item">
                <div class="kt-widget12__info">
                    <span class="kt-widget12__desc">Judul FAQ</span>
                    <span class="kt-widget12__value">{{ $model->nama }}</span>
                </div>
            
                <div class="kt-widget12__info">
                    <span class="kt-widget12__desc">Konten</span>
                    <span class="kt-widget12__value">{{ $model->konten }} </span>
                </div>
            </div><div class="kt-widget12__item">
                <div class="kt-widget12__info">
                    <span class="kt-widget12__desc">Status</span>
                    <span class="kt-widget12__value">{{ $model->is_aktif }} </span>
                </div>
            
                        </div>
                    </div>
                </div>
            </div>
            <!--end:: Widgets-->
        </div>
    </div>
@endsection

@section('pageScripts')
    <script>
        jQuery(document).ready(function () {
            initSelect2('.select2ajax');
            initDeleteButton();
        });
    </script>
@endsection