@if(session('frontAlert'))
    <div class="style-msg successmsg">
        <div class="sb-msg text-center">
            <i class="icon-check"></i><strong>Berhasil!</strong>
            {{ session('frontAlert') }}
        </div>
    </div>
@endif

@if(session('errorAlert'))
    <div class="style-msg successmsg">
        <div class="sb-msg text-center">
            <i class="icon-times-circle"></i>
            <strong>
                GAGAL!
            </strong>
            {{ session('errorAlert') }}
        </div>
    </div>
@endif

@if(auth('pendaftar')->check() && auth('pendaftar')->user()->hasNeedYourApprovalMagangs)
    <div class="style-msg alertmsg">
        <div class="sb-msg text-center">
            <i class="icon-warning-sign"></i><strong>Penting!</strong>
            Ada pengajuan magang anda yang <b>disetujui bersyarat</b> dan membutuhkan approval anda!
        </div>
    </div>
@endif