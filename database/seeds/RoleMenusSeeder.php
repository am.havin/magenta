<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RoleMenusSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        #SIAPKAN ROLES
        $roleSuperadmin =  DB::table('roles')->where('name', 'superadmin')->first();
        $roleAdmin =  DB::table('roles')->where('name', 'admin')->first();
        $roleUser =  DB::table('roles')->where('name', 'user')->first();

        #SIAPKAN MENU
        $superadminMenus = ['menuHomepage', 'menuSettings', 'menuRoles', 'menuPermissions', 'menuAksesMenu', 'menuUsers',
            'menuVerifikasi', 'menuApproval', 'menuSuratPersetujuan', 'menuSertifikat', 'menuDataMagang', 'menuKontenWeb',
            'menuPengumuman', 'menuHalamanDepan', 'menuFaq', 'menuKontakKami', 'menuSettings', 'menuPendaftar', 'menuReferensi',
            'menuSekolah', 'menuSekolahProgramStudi', 'menuAlokasi', 'menuIndexNilai', 'menuPenempatan'];
        $adminMenus = ['menuHomepage', 'menuSettings', 'menuUsers',
            'menuVerifikasi', 'menuApproval', 'menuSuratPersetujuan', 'menuSertifikat', 'menuDataMagang', 'menuKontenWeb',
            'menuPengumuman', 'menuHalamanDepan', 'menuFaq', 'menuKontakKami', 'menuSettings', 'menuPendaftar', 'menuReferensi',
            'menuSekolah', 'menuSekolahProgramStudi', 'menuAlokasi', 'menuIndexNilai', 'menuPenempatan'];
        $userMenus = ['menuHomepage'];

        #ISI MENU SUPERADMIN
        $data = [];
        foreach($superadminMenus as $menu){
            $data[] = ['role_id' => $roleSuperadmin->id, 'menu_id' => $menu];
        }

        #ISI MENU ADMIN
        foreach($adminMenus as $menu){
            $data[] = ['role_id' => $roleAdmin->id, 'menu_id' => $menu];
        }

        #ISI MENU USER
        foreach($userMenus as $menu){
            $data[] = ['role_id' => $roleUser->id, 'menu_id' => $menu];
        }

        foreach($data as $val){
            DB::table('role_has_menus')
                ->updateOrInsert($val, $val);
        }
    }
}
