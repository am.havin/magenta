<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserRolesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $roleSuperadmin =  DB::table('roles')->where('name', 'superadmin')->first();
        $roleAdmin =  DB::table('roles')->where('name', 'admin')->first();

        $superadminlIds = \App\User::whereIn('username', ['annas.abdillah', 'admin.adla'])->pluck('id');
        $adminIds = \App\User::whereIn('username', ['adla.mohammad', 'widia.jessti'])->pluck('id');

        $data = [];
        foreach($superadminlIds as $modelId){
            $data[] = ['role_id' => $roleSuperadmin->id, 'model_type' => 'App\User', 'model_id' => $modelId];
        }
        foreach($adminIds as $modelId){
            $data[] = ['role_id' => $roleAdmin->id, 'model_type' => 'App\User', 'model_id' => $modelId];
        }

        foreach($data as $val){
            DB::table('model_has_roles')
                ->updateOrInsert($val, $val);
        }
    }
}
