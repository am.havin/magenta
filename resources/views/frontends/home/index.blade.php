@extends('templates.frontend.magenta.layout')

@section('content')
    @if($pengumuman)
        <div class="modal fade" id="modalPengumuman" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" style="border: none; border-radius: 0;">
                    <div class="modal-header" style="border-bottom: none;">
                        <h4 class="modal-title" id="exampleModalLabel" style="font-weight: lighter;">PENGUMUMAN</h4>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true" style="font-family: arial;">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        @if($pengumuman->gambar)
                            <img src="{{ url('gambarpengumuman/' . $pengumuman->id) }}" alt="">
                        @endif
                        <br><br>
                        <div>
                            {!! $pengumuman->konten !!}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endif
@endsection

@section('pageScript')
    @if($pengumuman)
        <script>
            $(document).ready(function(){
                $('#modalPengumuman').modal({
                    backdrop: 'static',
                    keyboard: false
                })
            });
        </script>
    @endif
@endsection