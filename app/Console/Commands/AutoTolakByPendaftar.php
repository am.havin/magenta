<?php

namespace App\Console\Commands;

use App\Magang\Magang;
use Carbon\Carbon;
use Illuminate\Console\Command;

class AutoTolakByPendaftar extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'magang:auto_tolak_by_pendaftar';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Command description';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $status = Magang::where('is_need_pendaftar_approval', true)
            ->where('is_accepted', true)
            ->whereNull('is_approved_by_pendaftar')
            ->where('accepted_at', '<', Carbon::now()->subDays(3))
            ->update([
                'is_approved_by_pendaftar' => false,
                'is_accepted' => false,
                'accepted_at' => Carbon::now()
            ]);

        if($status){
            $this->info("Pengajuan yang Diterima Bersyarat sudah otomatis ditolak");
        }else{
            $this->info("Tidak ada pengajuan Diterima Bersyarat yang expired");
        }
    }
}
