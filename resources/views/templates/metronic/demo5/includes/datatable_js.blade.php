<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>
<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>
<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/js/global/integration/plugins/datatables.init.js" type="text/javascript"></script>
<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-autofill/js/dataTables.autoFill.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-autofill-bs4/js/autoFill.bootstrap4.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-buttons/js/dataTables.buttons.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-buttons/js/buttons.colVis.js" type="text/javascript"></script>
<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-buttons/js/buttons.flash.js" type="text/javascript"></script>
<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-buttons/js/buttons.html5.js" type="text/javascript"></script>
<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-buttons/js/buttons.print.js" type="text/javascript"></script>
<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-colreorder/js/dataTables.colReorder.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-fixedcolumns/js/dataTables.fixedColumns.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-keytable/js/dataTables.keyTable.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-rowgroup/js/dataTables.rowGroup.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-rowreorder/js/dataTables.rowReorder.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-scroller/js/dataTables.scroller.min.js" type="text/javascript"></script>
<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-select/js/dataTables.select.min.js" type="text/javascript"></script>