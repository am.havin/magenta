<?php

namespace App\Rules;

use App\Helper;
use Carbon\Carbon;
use Carbon\CarbonPeriod;
use Illuminate\Contracts\Validation\Rule;

class TidakBolehMotongTglYgDisabled implements Rule
{
    private $exceptIdMagang;
    private $checkIsOwnUnapprovedMagang;

    /**
     * Create a new rule instance.
     *
     * @param $exceptIdMagang
     * @param bool $checkIsOwnUnapprovedMagang
     */
    public function __construct($exceptIdMagang = null, $checkIsOwnUnapprovedMagang = true)
    {
        $this->exceptIdMagang = $exceptIdMagang;
        $this->checkIsOwnUnapprovedMagang = $checkIsOwnUnapprovedMagang;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        $unit = request()->id_unit;
        $tglAwal = Carbon::createFromFormat('Y-m-d', request()->tgl_awal);
        $tglAkhir = Carbon::createFromFormat('Y-m-d', request()->tgl_akhir);
        $disabledDates = collect(Helper::getUnavailableDates($unit, $tglAwal->month, $tglAkhir->year, $this->exceptIdMagang, $this->checkIsOwnUnapprovedMagang));

        //ambil semua tanggal saat magang
        $tglMagangs = collect([]);
        $period = CarbonPeriod::create($tglAwal, '1 day', $tglAkhir);
        foreach($period as $key => $value){
            $tglMagangs->push($value->format('Y-m-d'));
        }

        //cek yang beririsannya
        $intersect = $disabledDates->intersect($tglMagangs);

        return ($intersect->isNotEmpty()) ? false : true;
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        return 'Tanggal magang anda tidak boleh beririsan dengan tanggal yang kuotanya sudah penuh! ';
    }
}
