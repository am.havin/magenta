@extends('templates.frontend.magenta.layout_nonhome')

@section('content')
    <div class="container">

        @if($errors->isNotEmpty())
            <div class="row justify-content-center">
                <div class="col-md-10">
                    <div class="style-msg2 errormsg">
                        <div class="msgtitle">Terjadi kesalahan pengisian form:</div>
                        <div class="sb-msg">
                            <ul>
                                @foreach($errors->all() as $message)
                                    <li>{{ $message }}</li>
                                @endforeach
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        @endif

        <form action="{{ url('akundataupdate') }}" method="post" id="dataAkunForm" class="kt-form"
              autocomplete="off" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row d-flex justify-content-center">
                <div class="col-md-10 rounded">
                    <table class="table w-100">
                        <thead>
                        <tr>
                            <th colspan="2">Update Data Login</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td width="20%">Email</td>
                            <td width="400px"><input type="text" name="" id="" class="form-control rounded"
                                                     value="{{ $model->email }}" disabled></td>
                        </tr>
                        <tr>
                            <td>Nama Lengkap</td>
                            <td><input type="text" name="nama_lengkap" id="nama_lengkap" class="form-control rounded"
                                       value="{{ $model->nama_lengkap }}"></td>
                        </tr>
                        <tr>
                            <td>Sekolah / Universitas</td>
{{--                            <td>--}}
{{--                                <input type="text" class="form-control" value="{{ $model->sekolah }}" disabled>--}}
{{--                            </td>--}}
                            <td>
                                <div class="form-check-inline">
                                    <input class="form-check-input is_universitas_cb" type="radio" name="is_universitas" id="is_universitas_true" value="1" {{ ($model->is_universitas) ? 'checked' : '' }} >
                                    <label class="form-check-label" for="is_universitas_true">
                                        Universitas
                                    </label>
                                </div>
                                <div class="form-check-inline">
                                    <input class="form-check-input is_universitas_cb" type="radio" name="is_universitas" id="is_universitas_false" value="0" {{ (!$model->is_universitas) ? 'checked' : '' }}>
                                    <label class="form-check-label" for="is_universitas_false">
                                        SMA/SMK
                                    </label>
                                </div>
                                <br><br>
                                <select name="id_sekolah" id="id_sekolah" class="form-control" tabindex="-98" required>
                                    @if($sekolah)
                                        <option value="{{ $sekolah->id }}" selected>{{ $sekolah->nama }}</option>
                                    @else
                                        <option value="0" selected>Lain-lain</option>
                                    @endif
                                </select>
                                <br><br>
                                <input id="sekolah" type="text"
                                       class="form-control form-control-sm"
                                       name="sekolah" value="@if(!$sekolah) {{ $model->sekolah }} @endif"
                                       placeholder="isi nama sekolah/universitas"
                                       style="@if($sekolah) display: none; @endif">
                                <br>
                            </td>
                        </tr>
                        <tr>
                            <td>Nomor Handphone</td>
                            <td><input type="text" name="no_hp" id="no_hp" class="form-control rounded"
                                       value="@if(old('no_hp')){{ old('no_hp') }}@else{{ $model->no_hp }}@endif"></td>
                        </tr>
                        <tr>
                            <td>Foto</td>
                            <td>
                                @if($model->pic)
                                    <img height="200" width="200"
                                         src="{{url('akun/images/'. $model->id . '/'. $photo )}}"
                                         alt="foto profile" class="img-thumbnail">
                                @else
                                    <img height="200" width="200" src="{{url('images/male.png')}}"
                                         alt="foto profile empty" class="img-thumbnail">
                                @endif

                                <br>
                                <input type="file" name="pic" id="pic" accept="image/jpg,image/jpeg,image/png"
                                       style="width: 100%;">
                                <small class="form-text text-muted">
                                    Maksimal ukuran 500KB dengan format, jpg, jpeg atau png.
                                    Upload ulang akan mengganti foto yang laman.
                                </small>
                            </td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td colspan="2" align="left">
                                <button type="submit"
                                        class="button button-3d button-small button-rounded button-lime button-light"
                                        name="button-info-akun">
                                    <i class="icon-check"></i> Update
                                </button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
        <form action="{{ url('akunpasswordupdate') }}" method="post" id="passwordAkunForm"
              class="kt-form" autocomplete="off" enctype="multipart/form-data">
            {{ csrf_field() }}
            <div class="row d-flex justify-content-center">
                <div class="col-md-10 rounded">
                    <table class="table w-100">
                        <thead>
                        <tr>
                            <th colspan="2">Ubah Password</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td class="" width="20%">Password Lama</td>
                            <td class=""><input type="password" name="password" id="password"
                                                class="form-control rounded"></td>
                        </tr>
                        <tr>
                            <td class="">Password Baru</td>
                            <td class=""><input type="password" name="password_new" id="password_new"
                                                class="form-control rounded"></td>
                        </tr>
                        <tr>
                            <td class="">Ulangi Password Baru</td>
                            <td class="" align="left"><input type="password" name="password_new_retype"
                                                             id="password_new_retype" class="form-control rounded"></td>
                        </tr>
                        <tr>
                            <td>&nbsp;</td>
                            <td class="" align="left">
                                <button type="submit"
                                        class="button button-3d button-small button-rounded button-lime button-light"
                                        name="button-password-akun">
                                    <i class="icon-check"></i> Ganti Password
                                </button>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </form>
    </div>
@endsection


@section('pageScript')
    {!! $validator->selector('#dataAkunForm') !!}
    <script>
        $(document).ready(function(){
            //VARIABLES
            var isUniversitas = {{ ($model->is_universitas) ? 'true' : 'false' }};
            var labelSekolah = '';
            var select2Sekolah;

            initIdSekolah();

            // TOGGLE
            $('.is_universitas_cb').change(function(){
                //hide dulu setiap trigger
                $('#rowSekolah').hide('fast');

                //set values
                isUniversitas = $(this).val();
                labelSekolah = (isUniversitas == 1) ? 'Nama Universitas' : 'Nama SMA/SMK';

                //execute
                $('#rowSekolah').show('fast');
                destroySelect2Sekolah();
                initIdSekolah();
            });

            //DESTROY SELECT2 SEKOLAH
            function destroySelect2Sekolah(){
                select2Sekolah.val([]);
                select2Sekolah.select2('destroy');
                hideTextSekolah();
            }

            // SHOW TEXT SEKOLAH
            function showTextSekolah(){
                console.log('muncul');
                $('#sekolah').show('fast').attr('placeholder', labelSekolah);
                $('#sekolah').prop('required', true);
            }
            // HIDE TEXT SEKOLAH
            function hideTextSekolah(){
                $('#sekolah').hide('fast');
                $('#sekolah').val('').text('').prop('required', false);
            }


            // INIT SELECT2 ID_SEKOLAH
            function initIdSekolah(){
                select2Sekolah = $('#id_sekolah').select2({
                    placeholder: 'Pilih',
                    allowClear: false,
                    ajax: {
                        delay: 1000,
                        url: '{{ url('pub_select2/sekolah_with_lain_lain') }}' + '/' + isUniversitas,
                        dataType: 'json',
                    },
                    cache: true,
                }).on('select2:select', function (event) {
                    let val = $(this).val();
                    if(val == 0){ // = 0 itu lain-lain
                        showTextSekolah();
                    }else{
                        hideTextSekolah();
                    }
                }).on('select2:unselect', function (event) {
                    hideTextSekolah();
                });
            }

            //SELECT2
            // $('#id_sekolah').select2();
            // $('#id_sekolah').change(function(){
            //     let val = $(this).val();
            //
            //     if(val > 0 || val === ''){
            //         $('#sekolah').val('').hide('fast').prop('required', false);
            //     }
            //     else{
            //         $('#sekolah').show('fast').prop('required', true);
            //     }
            // });
        });
    </script>
@endsection


{{-- For Modal --}}
@if(Request::ajax())
    @yield('content')
    @yield('pageScripts')
@endif