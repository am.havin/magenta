<?php

namespace App\Http\Controllers\Frontends\Pendaftaran;

use App\Alokasi\Alokasi;
use App\Helper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Frontend\Step1Request;
use App\Magang\Magang;
use App\Pengajuan\Pengajuan;
use App\Peserta\Peserta;
use Carbon\Carbon;
use Yajra\DataTables\DataTables;


class MagangController extends Controller
{
    protected $pendaftar;

    public function __construct()
    {
        //harus begini karena auth()->user() nge-return null kalau belum ngelewatin middleware
        $this->middleware(function ($request, $next) {
            $this->pendaftar= auth('pendaftar')->user();
            return $next($request);
        });
    }

    public function index()
    {
        $title = 'Proses Pengajuan - Pilih Peserta';
        $activeMenus = ['menuPendaftaran'];
        $this->pendaftar->clearDraftPengajuans();
        $draftPengajuan = $this->pendaftar->generateDraftPengajuan();

        $viewData = compact('title', 'activeMenus', 'draftPengajuan');
        return view('frontends.pendaftaran.step1', $viewData);
    }

    public function create(Step1Request $request, $pengajuanId)
    {
        $title = 'Tambah Data Peserta';
        $activeMenus = [''];
        $validator = \JsValidator::make($request->getCreateRules(), $request->messages());

        $pengajuan = Pengajuan::findOrFail($pengajuanId);
        $viewData = compact('title', 'activeMenus', 'validator', 'pengajuan');
        return view('frontends.pendaftaran.step1.magangs.create', $viewData);
    }

    public function store(Step1Request $request, $pengajuanId)
    {
        $peserta = Peserta::findOrFail($request->id_peserta);
        $alokasi = Alokasi::where('id_unit', $request->id_unit)->first();
        $pengajuan = Pengajuan::findOrFail($pengajuanId);

        try{
            $curMagangs = ($pengajuan->magangs) ? $pengajuan->magangs->count() : 0;
            $canCreate = ($curMagangs >= Pengajuan::$maxPengajuanPeserta) ? false : true;

            if ($canCreate) {
                $magang = Magang::UpdateOrCreate([
                    'id_pengajuan' => $pengajuanId,
                    'id_peserta' => $peserta->id,
                ],[
                    'id_pengajuan' => $pengajuanId,
                    'id_peserta' => $peserta->id,
                    'peserta' => $peserta->nama,
                    'email' => $peserta->email,
                    'no_ktp' => $peserta->no_ktp,
                    'nim_nis' => $peserta->nim_nis,
                    'id_sekolah' => $peserta->id_sekolah,
                    'sekolah' => $peserta->sekolah,
                    'is_universitas' => $peserta->is_universitas,
                    'id_programstudi' => $peserta->id_programstudi,
                    'programstudi' => $peserta->programstudi,
                    'ipk' => $peserta->ipk,
                    'id_unit' => $alokasi->id_unit,
                    'unit' => $alokasi->unit,
                    'tgl_awal' => $request->tgl_awal,
                    'tgl_akhir' => $request->tgl_akhir,
                    'created_by' => $this->pendaftar->id
                ]);
                $pengajuan->is_draft = false;
                $pengajuan->save();
                $magang->purgeExpired();

                $json = ['status' => true, 'message' => 'SUKSES! Peserta berhasil dipilih!'];
            }else{
                $json = ['status' => false, 'message' => '<i class="icon-exclamation"></i> &nbsp; Jumlah maksimal peserta magang yang anda ajukan sudah mencapai batas maksimal'];
            }
        }catch(\Exception $e){
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data baru berhasil disimpan!');
        //return redirect(route('pendaftar.index'))->with('alert', $alert);
        $toastr = new Toastr('success', 'Data magang berhasil disimpan!');
        return redirect(route('pendaftar.index'))->with('toastr', $toastr);
    }

    public function edit(Step1Request $request, $pengajuanId, $magangId)
    {
        $title = 'Edit Data Peserta';
        $activeMenus = [''];
        $validator = \JsValidator::make($request->getEditRules(), $request->messages());

        $pengajuan = Pengajuan::findOrFail($pengajuanId);
        $magang = Magang::findOrFail($magangId);
        $viewData = compact('title', 'activeMenus', 'validator', 'pengajuan', 'magang');
        return view('frontends.pendaftaran.step1.magangs.edit', $viewData);
    }

    public function update(Step1Request $request, $pengajuanId, $magangId)
    {
        $magang = Magang::findOrFail($magangId);
        $peserta = Peserta::findOrFail($magang->id_peserta);
        $alokasi = Alokasi::where('id_unit', $request->id_unit)->first();
        $pengajuan = Pengajuan::findOrFail($pengajuanId);

        try{
            $magang->update([
                'id_peserta' => $peserta->id,
                'peserta' => $peserta->nama,
                'email' => $peserta->email,
                'no_ktp' => $peserta->no_ktp,
                'nim_nis' => $peserta->nim_nis,
                'id_sekolah' => $peserta->id_sekolah,
                'sekolah' => $peserta->sekolah,
                'is_universitas' => $peserta->is_universitas,
                'id_programstudi' => $peserta->id_programstudi,
                'programstudi' => $peserta->programstudi,
                'ipk' => $peserta->ipk,
                'id_unit' => $alokasi->id_unit,
                'unit' => $alokasi->unit,
                'tgl_awal' => $request->tgl_awal,
                'tgl_akhir' => $request->tgl_akhir,
                'updated_by' => $this->pendaftar->id
            ]);
            $magang->purgeExpired();
            $json = ['status' => true, 'message' => 'SUKSES! data telah diupdate!'];
        }catch(\Exception $e){
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data baru berhasil disimpan!');
        //return redirect(route('pendaftar.index'))->with('alert', $alert);
        $toastr = new Toastr('success', 'Data magang berhasil disimpan!');
        return redirect(route('pendaftar.index'))->with('toastr', $toastr);
    }

    public function destroy(Step1Request $request, $pengajuanId, $magangId)
    {
        $status = false;

        $magang = Magang::findOrFail($magangId);
        $status = $magang->doDelete();

        return response()->json([
            'status' => $status
        ]);
    }

    public function datatable(Step1Request $request, $pengajuanId)
    {
        $datatable =  Datatables::of(Magang::where('id_pengajuan', $pengajuanId))
            ->editColumn('foto', function($data){
                return '<img src="'.route('peserta_magang.foto', $data->id_peserta).'?rand='.rand(10,1000).'">';
            })
            ->editColumn('tgl_awal', function($data){
                if($data->tgl_awal){
                    $tgl = Carbon::createFromFormat('Y-m-d h:i:s', $data->tgl_awal);
                    return $tgl->format('d/m/Y');
                }
                return '<span class="badge badge-pill badge-danger"><i>expired</i></span>';
            })
            ->editColumn('tgl_akhir', function($data){
                if($data->tgl_akhir){
                    $tgl = Carbon::createFromFormat('Y-m-d h:i:s', $data->tgl_akhir);
                    return $tgl->format('d/m/Y');
                }
                return '<span class="badge badge-pill badge-danger"><i>expired</i></span>';
            })
            ->editColumn('lama', function($data){
                if($data->lama_hari){
                    return $data->lama_hari .' hari';
                }
                return '<span class="badge badge-pill badge-danger"><i>expired</i></span>';
            })
            ->addColumn('action', function ($data) {
                $actions = '';
                $actions .= '<a href="' . route('pendaftaran.step1.magang.edit', [$data->id_pengajuan, $data->id]) . '"class="btn btn-info btn-sm btnEdit" data-toggle="tooltip" title="Edit"><i class="icon-pencil"></i></a> ';
                $actions .= '<button type="button" class="btn btn-danger btn-sm btn-dt-delete" data-toggle="tooltip" title="Delete" data-url="' . route('pendaftaran.step1.magang.delete', [$data->id_pengajuan, $data->id]) . '" data-csrf="' . csrf_token() . '"><i class="icon-remove"></i></button>';
                return $actions;
            })
            ->rawColumns(['action', 'nama', 'no_ktp', 'sekolah', 'file_cv', 'tgl_awal', 'tgl_akhir', 'lama', 'foto']);

        return $datatable->make(true);
    }
}
