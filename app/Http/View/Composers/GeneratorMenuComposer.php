<?php

namespace App\Http\View\Composers;

use App\Generators\Generator;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Symfony\Component\Yaml\Yaml;

class GeneratorMenuComposer
{
    public function __construct()
    {

    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $urls = DB::table('generators')->pluck('route_url');
        $menus = $urls->map(function($value){
            $value = Str::finish($value, '/');
            return (object) [
                'name'  => $value,
                'url'   => url(Str::finish($value, '/'))
            ];
        });
        $view->with('vcGeneratorMenus', $menus);
    }
}
