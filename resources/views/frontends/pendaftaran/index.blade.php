@extends('templates.frontend.magenta.layout_nonhome')

@section('content')
    <div class="container">
        <div class="row">
            <div class="col-md-10 offset-1">
                <div style="" class="kt-portlet__head-actions">
                    <a href="{{ route('pendaftaran.step1.index') }}"
                       class="button button-3d button-small button-rounded button-lime button-light btnAdd">
                        <i class="icon-plus"></i>
                        Proses pengajuan
                    </a>
                    <br>
                    <br>
                    <br>
                </div>
            </div>

            <div class="col-md-10 offset-1">

                @if(auth('pendaftar')->user()->hasNeedYourApprovalMagangs)
                    <div class="alert alert-info">
                        <i class="icon-warning-sign"></i><strong>PENTING!</strong> <br>
                        Anda memiliki pengajuan magang yang telah disetujui <b>namun</b> terjadi penyesuaian Unit Penempatan / Tanggal oleh SDM.
                        <br>
                        Oleh sebab itu anda harus melakukan persetujuan kembali atas berubahan ini. Apabila dalam waktu 3 hari anda tidak meng-klik icon persetujuan,
                        maka pengajuan anda akan otomatis Ditolak. <br><br>
                        > Untuk menyetujui cukup dengan mengklik pada tombol "butuh persetujuan anda" lalu pilih setuju.
                    </div>
                    <br>
                @endif

                <div class="table-responsive">
                    <table class="table table-bordered table-sm">
                        <thead>
                        <tr>
                            <th class="text-right" width="20px">No</th>
                            <th class="text-right" width="100px">No Pengajuan</th>
                            <th>Peserta Magang</th>
                            <th>Unit Kerja Tujuan</th>
                            <th>Status</th>
                            <th width="100px" class="text-center">Butuh Persetujuan Anda</th>
                            <th class="text-center">Action</th>
                        </tr>
                        </thead>
                        <tbody>

                        @if($pengajuans->isNotEmpty())
                            @php $no = 1; @endphp
                            @foreach($pengajuans as $pengajuan)
                                @foreach($pengajuan->magangs->sortBy('peserta') as $magang)
                                    <tr>
                                        @if($loop->first)
                                            <td rowspan="{{ $pengajuan->magangs->count() }}" class="text-right">{{ $no }}</td>
                                            <td rowspan="{{ $pengajuan->magangs->count() }}" class="text-right">{{ $pengajuan->nomorPengajuan }}</td>
                                        @endif

                                        <td>{{ $magang->peserta }}</td>
                                        <td>
                                            {{--UNIT--}}
                                            @if($magang->old_unit)
                                                <s>{{ $magang->old_unit }}</s><br>
                                            @endif
                                            {{ $magang->unit }}<br>

                                            {{--TANGGAL--}}
                                            @if($magang->old_tgl_awal && $magang->old_tgl_akhir)
                                                <small class="text-muted">
                                                    <s>
                                                        {{ $magang->carbon_old_tgl_awal->format('d M Y') }} -
                                                        {{ $magang->carbon_old_tgl_akhir->format('d M Y') }}
                                                        ({{ $magang->old_lama_hari }} hari)
                                                    </s>
                                                </small><br>
                                            @endif
                                            <small class="text-muted">
                                                @if($magang->carbon_tgl_awal && $magang->carbon_tgl_akhir)
                                                    {{ $magang->carbon_tgl_awal->format('d M Y') }} -
                                                    {{ $magang->carbon_tgl_akhir->format('d M Y') }}
                                                    ({{ $magang->lama_hari }} hari)
                                                @endif
                                            </small>
                                        </td>
                                        <td>
                                            @if(!$pengajuan->is_submitted)
                                                @if($magang->is_expired)
                                                    <span class="badge badge-pill badge-danger">Expired</span>
                                                @else
                                                    <span class="badge badge-pill badge-dark">Belum disubmit</span>
                                                @endif
                                            @else
                                                {{--BELUM APA2--}}
                                                @if($magang->is_accepted === null) <span class="badge badge-primary">Belum disetujui</span> @endif

                                                {{--SETUJU--}}
                                                @if($magang->is_accepted === true)
                                                    @if($magang->is_need_pendaftar_approval && $magang->is_approved_by_pendaftar === null)
                                                        <span class="badge badge-warning"> <i class="icon-exclamation"></i> &nbsp; Disetujui Bersyarat</span>
                                                    @elseif($magang->is_need_pendaftar_approval && $magang->is_approved_by_pendaftar)
                                                        <span class="badge badge-warning"> <i class="icon-check"></i> &nbsp; Telah Anda Setujui</span>
                                                    @else
                                                        <span class="badge badge-warning"> <i class="icon-check"></i> &nbsp; Disetujui</span>
                                                    @endif
                                                @endif

                                                {{--TOLAK--}}
                                                @if($magang->is_accepted === false)
                                                    @if($magang->is_need_pendaftar_approval && $magang->is_approved_by_pendaftar === false)
                                                        <span class="badge badge-dark">Telah Anda Tolak</span>
                                                    @else
                                                        <span class="badge badge-danger">Tidak disetujui</span>
                                                    @endif
                                                @endif
                                            @endif
                                        </td>

                                        <td class="text-center">
                                            @if($magang->is_need_pendaftar_approval && $magang->is_approved_by_pendaftar == null)
                                                <a href="javascript:;"
                                                   class="btn btn-info btn-sm btnApprove"
                                                   data-toggle="tooltip"
                                                   data-url="{{ route('pendaftaran.magang.approve', $magang->id) }}"
                                                   data-csrf="{{ csrf_token() }}"
                                                   data-redirect-url="{{ route('pendaftaran') }}"
                                                   title="Klik untuk melakukan approval. jika tidak akan ditolak otomatis tgl: {{ $magang->carbonAcceptedAt->addDays(3)->format('d/m/Y H:i') }}"
                                                >
                                                    <i class="icon-handshake"></i>
                                                </a>
                                            @else
                                                -
                                            @endif
                                        </td>

                                        @if($loop->first)
                                            <td rowspan="{{ $pengajuan->magangs->count() }}" class="text-center">
                                                @if($pengajuan->is_submitted)
                                                    <a href="{{ route('pendaftaran.show', $pengajuan->id) }}" data-toggle="tooltip" class="btn btn-primary btn-sm btn-show" title="Lihat detail pengajuan yang sudah disubmit">
                                                        <i class="icon-line2-magnifier"></i>
                                                    </a>
                                                    {{--<span data-toggle="tooltip" title="Pengajuan ini sudah disubmit sehingga tidak dapat di edit lagi">--}}
                                                    {{--<button class="btn btn-info btn-sm disabled"><i class="icon-pencil"></i></button>--}}
                                                    {{--</span>--}}
                                                @else
                                                    <a href="{{ route('pendaftaran.step1.edit', $pengajuan->id) }}" data-toggle="tooltip" class="btn btn-info btn-sm" title="Edit">
                                                        <i class="icon-pencil"></i>
                                                    </a>
                                                @endif

                                                @if($pengajuan->hasAtLeast1CheckedMagang())
                                                    {{--<span data-toggle="tooltip" title="Pengajuan ini memiliki peserta yang telah disetujui sehingga tidak dapat didelete">--}}
                                                    {{--<button class="btn btn-danger btn-sm disabled"><i class="icon-times"></i></button>--}}
                                                    {{--</span>--}}
                                                @else
                                                    <button
                                                            type="button"
                                                            class="btn btn-danger btn-dt-delete btn-sm"
                                                            data-toggle="tooltip"
                                                            title="Delete"
                                                            data-url="{{ route('pendaftaran.destroy', $pengajuan->id) }}" data-csrf="{{ csrf_token() }}"
                                                            data-redirect-url="{{ route('pendaftaran') }}">
                                                        <i class="icon-times"></i>
                                                    </button>
                                                @endif
                                            </td>
                                        @endif
                                    </tr>
                                @endforeach
                                @php $no++; @endphp
                            @endforeach
                        @else
                            <tr>
                                <td colspan="7" class="text-center"><i>Belum ada pengajuan pendaftaran magang</i></td>
                            </tr>
                        @endif
                        </tbody>
                    </table>
                </div>
            </div>

            <!-- Modal -->
            <div class="modal fade" id="modal" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true"></div>
        </div>
    </div>
@endsection

@section('pageScript')
    <script>
        $(document).ready(function(){
            $('[data-toggle="tooltip"]').tooltip();
            initAjaxModalButton('a.btn-show');
            initDeleteButton(); //check function in custom.js
        });
    </script>

    <script>
        $(document).ready(function(){
            $('body').on('click', '.btnApprove', function() {
                let url = $(this).attr('data-url');
                let csrf = $(this).attr('data-csrf');
                let redirectUrl = $(this).attr('data-redirect-url');
                let isSetuju = null;

                //SWEET ALERT
                swal.fire({
                    title: 'Silahkan pilih apakah anda setuju atau tidak?',
                    type: 'warning',
                    showCancelButton: true,
                    showCloseButton: true,
                    confirmButtonText: 'Ya, Setuju!',
                    cancelButtonText: 'Tidak Setuju'
                }).then(function (result) {
                    if (result.value) {
                        isSetuju = 1;
                    }else if (result.dismiss === Swal.DismissReason.cancel) {
                        isSetuju = 0;
                    }

                    if(isSetuju != null){
                        $('body').block({
                            message: '<small class="text-muted">Loading...</small>',
                            css: { border: 'none', backgroundColor: 'transparent' }
                        });
                        doApprove(url, csrf, isSetuju)
                            .then(data => {
                                if(data.status){
                                    $('body').unblock();
                                    swal.fire(
                                        'Berhasil!',
                                        'Terimakasih atas perhatian anda.',
                                        'success'
                                    ).then(function() {
                                        if(redirectUrl != null){
                                            window.location.replace(redirectUrl);
                                        }
                                    });
                                }else{
                                    $('body').unblock();
                                    swal.fire(
                                        'Gagal!',
                                        'Terjadi kesalahan, approval gagal.',
                                        'error'
                                    );
                                }
                            })
                            .catch(error => {
                                console.log(error);
                                $('body').unblock();
                                swal.fire(
                                    'Error!',
                                    'Terjadi kesalahan, approval gagal.',
                                    'error'
                                );
                            })
                    }
                });
            });

            function doApprove(url, csrf, val) {
                return new Promise((resolve, reject) => {
                    $.ajax({
                        url: url,
                        type: 'POST',
                        data: {
                            _token: csrf,
                            approval: val
                        },
                        success: function (data) {
                            resolve(data)
                        },
                        error: function (error) {
                            reject(error)
                        },
                    })
                });
            }
        });
    </script>
@endsection
