<!DOCTYPE html>

<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 4 & Angular 8
Author: KeenThemes
Website: http://www.keenthemes.com/
Contact: support@keenthemes.com
Follow: www.twitter.com/keenthemes
Dribbble: www.dribbble.com/keenthemes
Like: www.facebook.com/keenthemes
Purchase: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
Renew Support: http://themeforest.net/item/metronic-responsive-admin-dashboard-template/4021469?ref=keenthemes
License: You must have a valid license purchased only from themeforest(the above link) in order to legally use the theme for your project.
-->
<html lang="en">

	<!-- begin::Head -->
	<head>
		<base href="">
		<meta charset="utf-8" />
		<title>{{ $title }}</title>
		<meta name="description" content="Latest updates and statistic charts">
		<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

		<!--begin::Fonts -->
		<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Poppins:300,400,500,600,700">

		<!--end::Fonts -->

		<!--begin::Page Vendors Styles(used by this page) -->
		<!--end::Page Vendors Styles -->

		<!--begin::Global Theme Styles(used by all pages) -->

		<!--begin:: Vendor Plugins -->
        @yield('vendorPluginStyles')

        <link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/perfect-scrollbar/css/perfect-scrollbar.css" rel="stylesheet" type="text/css" />
		<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/tether/dist/css/tether.css" rel="stylesheet" type="text/css" />
		<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/bootstrap-datepicker/dist/css/bootstrap-datepicker3.css" rel="stylesheet" type="text/css" />
		<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/bootstrap-datetime-picker/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
		<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/bootstrap-timepicker/css/bootstrap-timepicker.css" rel="stylesheet" type="text/css" />
		<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" type="text/css" />
        <link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.css" rel="stylesheet" type="text/css" />
		<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/bootstrap-select/dist/css/bootstrap-select.css" rel="stylesheet" type="text/css" />
		<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.css" rel="stylesheet" type="text/css" />
		<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/select2/dist/css/select2.css" rel="stylesheet" type="text/css" />
		<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/ion-rangeslider/css/ion.rangeSlider.css" rel="stylesheet" type="text/css" />
		<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/nouislider/distribute/nouislider.css" rel="stylesheet" type="text/css" />
		<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/owl.carousel/dist/assets/owl.carousel.css" rel="stylesheet" type="text/css" />
		<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/owl.carousel/dist/assets/owl.theme.default.css" rel="stylesheet" type="text/css" />
		<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/dropzone/dist/dropzone.css" rel="stylesheet" type="text/css" />
		<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/quill/dist/quill.snow.css" rel="stylesheet" type="text/css" />
		<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/@yaireo/tagify/dist/tagify.css" rel="stylesheet" type="text/css" />
		<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/summernote/dist/summernote.css" rel="stylesheet" type="text/css" />
		<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
		<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/animate.css/animate.css" rel="stylesheet" type="text/css" />
		<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/toastr/build/toastr.css" rel="stylesheet" type="text/css" />
		<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/dual-listbox/dist/dual-listbox.css" rel="stylesheet" type="text/css" />
		<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/morris.js/morris.css" rel="stylesheet" type="text/css" />
		<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/sweetalert2/dist/sweetalert2.css" rel="stylesheet" type="text/css" />
		<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/socicon/css/socicon.css" rel="stylesheet" type="text/css" />
		<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/plugins/line-awesome/css/line-awesome.css" rel="stylesheet" type="text/css" />
		<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/plugins/flaticon/flaticon.css" rel="stylesheet" type="text/css" />
		<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/plugins/flaticon2/flaticon.css" rel="stylesheet" type="text/css" />
		<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/@fortawesome/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css" />

		<!--end:: Vendor Plugins -->
		<link href="{{ url('/') }}/templates/metronic/demo5/assets/css/style.bundle.css" rel="stylesheet" type="text/css" />

		<!--begin:: Vendor Plugins for custom pages -->
        @yield('vendorPluginForCustomPagesStyles')

        {{--<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" type="text/css" />--}}
		{{--<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/@fullcalendar/core/main.css" rel="stylesheet" type="text/css" />--}}
		{{--<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/@fullcalendar/daygrid/main.css" rel="stylesheet" type="text/css" />--}}
		{{--<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/@fullcalendar/list/main.css" rel="stylesheet" type="text/css" />--}}
		{{--<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/@fullcalendar/timegrid/main.css" rel="stylesheet" type="text/css" />--}}
		{{--<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet" type="text/css" />--}}
		{{--<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-buttons-bs4/css/buttons.bootstrap4.min.css" rel="stylesheet" type="text/css" />--}}
		{{--<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-autofill-bs4/css/autoFill.bootstrap4.min.css" rel="stylesheet" type="text/css" />--}}
		{{--<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-colreorder-bs4/css/colReorder.bootstrap4.min.css" rel="stylesheet" type="text/css" />--}}
		{{--<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-fixedcolumns-bs4/css/fixedColumns.bootstrap4.min.css" rel="stylesheet" type="text/css" />--}}
		{{--<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-fixedheader-bs4/css/fixedHeader.bootstrap4.min.css" rel="stylesheet" type="text/css" />--}}
		{{--<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-keytable-bs4/css/keyTable.bootstrap4.min.css" rel="stylesheet" type="text/css" />--}}
		{{--<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-responsive-bs4/css/responsive.bootstrap4.min.css" rel="stylesheet" type="text/css" />--}}
		{{--<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-rowgroup-bs4/css/rowGroup.bootstrap4.min.css" rel="stylesheet" type="text/css" />--}}
		{{--<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-rowreorder-bs4/css/rowReorder.bootstrap4.min.css" rel="stylesheet" type="text/css" />--}}
		{{--<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-scroller-bs4/css/scroller.bootstrap4.min.css" rel="stylesheet" type="text/css" />--}}
		{{--<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-select-bs4/css/select.bootstrap4.min.css" rel="stylesheet" type="text/css" />--}}
		{{--<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/jstree/dist/themes/default/style.css" rel="stylesheet" type="text/css" />--}}
		{{--<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/jqvmap/dist/jqvmap.css" rel="stylesheet" type="text/css" />--}}
		{{--<link href="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/uppy/dist/uppy.min.css" rel="stylesheet" type="text/css" />--}}

		<!--end:: Vendor Plugins for custom pages -->

		<link href="{{ url('/') }}/templates/metronic/demo5/assets/css/custom.css" rel="stylesheet" type="text/css" />
		<!--end::Global Theme Styles -->

		<!--begin::Layout Skins(used by all pages) -->

		<!--end::Layout Skins -->
		<link rel="shortcut icon" href="{{ url('/') }}/icon.ico" />
	</head>

	<!-- end::Head -->

	<!-- begin::Body -->
	<body class="kt-page--loading-enabled kt-page--loading kt-header--fixed kt-header--minimize-topbar kt-header-mobile--fixed kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-subheader--enabled kt-subheader--transparent kt-page--loading">

		<!-- begin::Page loader -->

		<!-- end::Page Loader -->

		<!-- begin:: Page -->

		<!-- begin:: Header Mobile -->
		@include('templates.metronic.demo5.includes.headermobile')
		<!-- end:: Header Mobile -->
		<div class="kt-grid kt-grid--hor kt-grid--root">
			<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--ver kt-page">
				<div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper " id="kt_wrapper">

					<!-- begin:: Header -->
					<div id="kt_header" class="kt-header kt-grid__item  kt-header--fixed " data-ktheader-minimize="on">
						<div class="kt-header__top">
							<div class="kt-container ">

								<!-- begin:: Brand -->
								<div class="kt-header__brand   kt-grid__item" id="kt_header_brand">
									<div class="kt-header__brand-logo">
										<a href="{{ url('/') }}">
											<img alt="Logo" src="{{ url('/logo.png') }}" />
										</a>
									</div>
                                    {{--@include('templates.metronic.demo5.includes.brand-nav')--}}
								</div>

								<!-- end:: Brand -->

								<!-- begin:: Header Topbar -->
								<div class="kt-header__topbar">

									<!--begin: Search -->
									<div class="kt-header__topbar-item kt-header__topbar-item--search dropdown kt-hidden-desktop" id="kt_quick_search_toggle">
										<div class="kt-header__topbar-wrapper" data-toggle="dropdown" data-offset="10px,10px">
											<span class="kt-header__topbar-icon kt-header__topbar-icon--success">
												<svg xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="24px" height="24px" viewBox="0 0 24 24" version="1.1" class="kt-svg-icon">
													<g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
														<rect x="0" y="0" width="24" height="24" />
														<path d="M14.2928932,16.7071068 C13.9023689,16.3165825 13.9023689,15.6834175 14.2928932,15.2928932 C14.6834175,14.9023689 15.3165825,14.9023689 15.7071068,15.2928932 L19.7071068,19.2928932 C20.0976311,19.6834175 20.0976311,20.3165825 19.7071068,20.7071068 C19.3165825,21.0976311 18.6834175,21.0976311 18.2928932,20.7071068 L14.2928932,16.7071068 Z" fill="#000000" fill-rule="nonzero" opacity="0.3" />
														<path d="M11,16 C13.7614237,16 16,13.7614237 16,11 C16,8.23857625 13.7614237,6 11,6 C8.23857625,6 6,8.23857625 6,11 C6,13.7614237 8.23857625,16 11,16 Z M11,18 C7.13400675,18 4,14.8659932 4,11 C4,7.13400675 7.13400675,4 11,4 C14.8659932,4 18,7.13400675 18,11 C18,14.8659932 14.8659932,18 11,18 Z" fill="#000000" fill-rule="nonzero" />
													</g>
												</svg>

												<!--<i class="flaticon2-search-1"></i>-->
											</span>
										</div>
										<div class="dropdown-menu dropdown-menu-fit dropdown-menu-right dropdown-menu-anim dropdown-menu-lg">
											<div class="kt-quick-search kt-quick-search--dropdown kt-quick-search--result-compact" id="kt_quick_search_dropdown">
												<form method="get" class="kt-quick-search__form">
													<div class="input-group">
														<div class="input-group-prepend"><span class="input-group-text"><i class="flaticon2-search-1"></i></span></div>
														<input type="text" class="form-control kt-quick-search__input" placeholder="Search...">
														<div class="input-group-append"><span class="input-group-text"><i class="la la-close kt-quick-search__close"></i></span></div>
													</div>
												</form>
												<div class="kt-quick-search__wrapper kt-scroll" data-scroll="true" data-height="325" data-mobile-height="200">
												</div>
											</div>
										</div>
									</div>
									<!--end: Search -->

									<!--begin: Notifications -->
									@include('templates.metronic.demo5.includes.notifications')
									<!--end: Notifications -->

									<!--begin: User bar -->
									{{--@include('templates.metronic.demo5.includes.userbar')--}}
									<!--end: User bar -->
								</div>

								<!-- end:: Header Topbar -->
							</div>
						</div>
						<div class="kt-header__bottom">
							<div class="kt-container ">

								<!-- begin: Header Menu -->
								@include('templates.metronic.demo5.includes.mainmenu')
								<!-- end: Header Menu -->
							</div>
						</div>
					</div>

					<!-- end:: Header -->
					<div class="kt-body kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-grid--stretch" id="kt_body">
						<div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">

							<!-- begin:: Subheader -->
							<div class="kt-subheader   kt-grid__item" id="kt_subheader">
								<div class="kt-container ">
									<div class="kt-subheader__main">
										<h3 class="kt-subheader__title">
											{{ $title }} </h3>
										<span class="kt-subheader__separator kt-hidden"></span>
										@include('templates.metronic.demo5.includes.breadcrumbs')
									</div>
									<div class="kt-subheader__toolbar">
                                        <div class="kt-subheader__wrapper">
                                            @yield('subheaderToolbar')
                                        </div>
									</div>
								</div>
							</div>

							<!-- end:: Subheader -->

							<!-- begin:: Content -->
							<div class="kt-container kt-grid__item kt-grid__item--fluid">
                                @include('templates.metronic.demo5.includes.alert')
                                @yield('content')
							</div>
							<!-- end:: Content -->
						</div>
					</div>

					<!-- begin:: Footer -->
					@include('templates.metronic.demo5.includes.footer')
					<!-- end:: Footer -->
				</div>
			</div>
		</div>

		<!-- end:: Page -->

		<!-- begin::Quick Panel -->
		{{--@include('templates.metronic.demo5.includes.quickpanel')--}}
		<!-- end::Quick Panel -->

		<!-- begin::Scrolltop -->
		<div id="kt_scrolltop" class="kt-scrolltop">
			<i class="fa fa-arrow-up"></i>
		</div>
		<!-- end::Scrolltop -->

		<!-- begin::Sticky Toolbar -->
		{{--@include('templates.metronic.demo5.includes.stickytoolbar')--}}
		<!-- end::Sticky Toolbar -->

		<!-- begin::Demo Panel -->
		{{--@include('templates.metronic.demo5.includes.demopanel')--}}
		<!-- end::Demo Panel -->

		<!--Begin:: Chat-->
		{{--@include('templates.metronic.demo5.includes.chat')--}}
		<!--ENd:: Chat-->

		<!-- begin::Global Config(global config for global JS sciprts) -->
		<script>
			var KTAppOptions = {
				"colors": {
					"state": {
						"brand": "#3d94fb",
						"light": "#ffffff",
						"dark": "#282a3c",
						"primary": "#5867dd",
						"success": "#34bfa3",
						"info": "#3d94fb",
						"warning": "#ffb822",
						"danger": "#fd27eb"
					},
					"base": {
						"label": ["#c5cbe3", "#a1a8c3", "#3d4465", "#3e4466"],
						"shape": ["#f0f3ff", "#d9dffa", "#afb4d4", "#646c9a"]
					}
				}
			};
		</script>

		<!-- end::Global Config -->

		<!--begin::Global Theme Bundle(used by all pages) -->

		<!--begin:: Vendor Plugins -->
        <script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/jquery/dist/jquery.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/popper.js/dist/umd/popper.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/js-cookie/src/js.cookie.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/moment/min/moment.min.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/tooltip.js/dist/umd/tooltip.min.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/perfect-scrollbar/dist/perfect-scrollbar.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/sticky-js/dist/sticky.min.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/wnumb/wNumb.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/jquery-form/dist/jquery.form.min.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/block-ui/jquery.blockUI.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/bootstrap-datepicker/dist/locales/bootstrap-datepicker.id.min.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/js/global/integration/plugins/bootstrap-datepicker.init.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/bootstrap-datetime-picker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/js/global/integration/plugins/bootstrap-timepicker.init.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/bootstrap-daterangepicker/daterangepicker.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/bootstrap-touchspin/dist/jquery.bootstrap-touchspin.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/bootstrap-maxlength/src/bootstrap-maxlength.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/plugins/bootstrap-multiselectsplitter/bootstrap-multiselectsplitter.min.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/bootstrap-select/dist/js/bootstrap-select.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/bootstrap-switch/dist/js/bootstrap-switch.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/js/global/integration/plugins/bootstrap-switch.init.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/select2/dist/js/select2.full.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/ion-rangeslider/js/ion.rangeSlider.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/typeahead.js/dist/typeahead.bundle.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/handlebars/dist/handlebars.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/inputmask/dist/jquery.inputmask.bundle.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/inputmask/dist/inputmask/inputmask.date.extensions.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/inputmask/dist/inputmask/inputmask.numeric.extensions.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/nouislider/distribute/nouislider.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/owl.carousel/dist/owl.carousel.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/autosize/dist/autosize.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/clipboard/dist/clipboard.min.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/dropzone/dist/dropzone.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/js/global/integration/plugins/dropzone.init.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/quill/dist/quill.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/@yaireo/tagify/dist/tagify.polyfills.min.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/@yaireo/tagify/dist/tagify.min.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/summernote/dist/summernote.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/markdown/lib/markdown.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/js/global/integration/plugins/bootstrap-markdown.init.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/bootstrap-notify/bootstrap-notify.min.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/js/global/integration/plugins/bootstrap-notify.init.js" type="text/javascript"></script>
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/jquery-validation/dist/jquery.validate.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/jquery-validation/dist/additional-methods.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/js/global/integration/plugins/jquery-validation.init.js" type="text/javascript"></script>--}}
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/toastr/build/toastr.min.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/dual-listbox/dist/dual-listbox.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/raphael/raphael.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/morris.js/morris.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/chart.js/dist/Chart.bundle.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/plugins/bootstrap-session-timeout/dist/bootstrap-session-timeout.min.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/plugins/jquery-idletimer/idle-timer.min.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/waypoints/lib/jquery.waypoints.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/counterup/jquery.counterup.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/es6-promise-polyfill/promise.min.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/sweetalert2/dist/sweetalert2.min.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/js/global/integration/plugins/sweetalert2.init.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/jquery.repeater/src/lib.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/jquery.repeater/src/jquery.input.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/jquery.repeater/src/repeater.js" type="text/javascript"></script>
		<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/general/dompurify/dist/purify.js" type="text/javascript"></script>
		<!--end:: Vendor Plugins -->

		<script src="{{ url('/') }}/templates/metronic/demo5/assets/js/scripts.bundle.js" type="text/javascript"></script>
		<script src="{{ url('/js/custom.js') }}" type="text/javascript"></script>
    	<script src="{{ asset('assets/global/plugins/treegrid/js/jquery.treegrid.js') }}" type="text/javascript"></script>

		<!--begin:: Vendor Plugins for custom pages -->
        {{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/@fullcalendar/core/main.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/@fullcalendar/daygrid/main.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/@fullcalendar/google-calendar/main.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/@fullcalendar/interaction/main.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/@fullcalendar/list/main.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/@fullcalendar/timegrid/main.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/gmaps/gmaps.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/flot/dist/es5/jquery.flot.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/flot/source/jquery.flot.resize.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/flot/source/jquery.flot.categories.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/flot/source/jquery.flot.pie.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/flot/source/jquery.flot.stack.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/flot/source/jquery.flot.crosshair.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/flot/source/jquery.flot.axislabels.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net/js/jquery.dataTables.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-bs4/js/dataTables.bootstrap4.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/js/global/integration/plugins/datatables.init.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-autofill/js/dataTables.autoFill.min.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-autofill-bs4/js/autoFill.bootstrap4.min.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/jszip/dist/jszip.min.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/pdfmake/build/pdfmake.min.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/pdfmake/build/vfs_fonts.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-buttons/js/dataTables.buttons.min.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-buttons-bs4/js/buttons.bootstrap4.min.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-buttons/js/buttons.colVis.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-buttons/js/buttons.flash.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-buttons/js/buttons.html5.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-buttons/js/buttons.print.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-colreorder/js/dataTables.colReorder.min.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-fixedcolumns/js/dataTables.fixedColumns.min.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-fixedheader/js/dataTables.fixedHeader.min.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-keytable/js/dataTables.keyTable.min.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-responsive/js/dataTables.responsive.min.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-responsive-bs4/js/responsive.bootstrap4.min.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-rowgroup/js/dataTables.rowGroup.min.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-rowreorder/js/dataTables.rowReorder.min.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-scroller/js/dataTables.scroller.min.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/datatables.net-select/js/dataTables.select.min.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/jstree/dist/jstree.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/jqvmap/dist/jquery.vmap.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/jqvmap/dist/maps/jquery.vmap.world.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/jqvmap/dist/maps/jquery.vmap.russia.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/jqvmap/dist/maps/jquery.vmap.usa.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/jqvmap/dist/maps/jquery.vmap.germany.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/jqvmap/dist/maps/jquery.vmap.europe.js" type="text/javascript"></script>--}}
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/plugins/custom/uppy/dist/uppy.min.js" type="text/javascript"></script>--}}
        @yield('vendorPluginForCustomPagesScripts')
		<!--end:: Vendor Plugins for custom pages -->

		<!--end::Global Theme Bundle -->

		<!--begin::Page Vendors(used by this page) -->
		{{--<script src="//maps.google.com/maps/api/js?key=AIzaSyBTGnKT7dt597vo9QgeQ7BFhvSRP4eiMSM" type="text/javascript"></script>--}}
		<!--end::Page Vendors -->

		<!--begin::Page Scripts(used by this page) -->
		{{--<script src="{{ url('/') }}/templates/metronic/demo5/assets/js/pages/dashboard.js" type="text/javascript"></script>--}}
        @yield('pageScripts')
		<!--end::Page Scripts -->

		{{--script yg berjalan global--}}
		<script type="text/javascript" src="{{ asset('vendor/jsvalidation/js/jsvalidation.js')}}"></script>
		<script>
            jQuery(document).ready(function() {
                toastr.options = {
                    "closeButton": true,
                    "debug": false,
                    "newestOnTop": true,
                    "progressBar": false,
                    "positionClass": "toast-top-right",
                    "preventDuplicates": false,
                    "onclick": null,
                    "showDuration": "10",
                    "hideDuration": "10",
                    "timeOut": "10000",
                    "extendedTimeOut": "10",
                    "showEasing": "swing",
                    "hideEasing": "linear",
                    "showMethod": "fadeIn",
                    "hideMethod": "fadeOut",
                };

				@if(session('toastr'))
                	toastr.{{ session('toastr')->type }}("{{ session('toastr')->text }}");
				@endif
            });
		</script>
	</body>

	<!-- end::Body -->
</html>
