<?php

namespace App\Http\Controllers\Sertifikat;

use App\Http\Requests\SertifikatRequest;
use App\IndexNilai\IndexNilai;
use Carbon\Carbon;
use Illuminate\Support\Facades\Storage;
use niklasravnsborg\LaravelPdf\Facades\Pdf;
use Yajra\DataTables\DataTables;

use App\Alert;
use App\Http\Controllers\Controller;
use App\Magang\Magang;
use App\Toastr;


class SertifikatController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param SertifikatRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(SertifikatRequest $request)
    {
        $title = 'Sertifikat';
        $activeMenus = ['menuSertifikat'];
        $breadcrumbs = [
            'Sertifikat' => route('sertifikat.index'),
        ];
        $viewData = compact('title', 'activeMenus', 'breadcrumbs');

        return view('sertifikat.index', $viewData);
    }

    /**
     * Display a listing of the resource.
     *
     * @param SertifikatRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index10hari(SertifikatRequest $request)
    {
        $title = 'Sertifikat';
        $activeMenus = ['menuSertifikat'];
        $breadcrumbs = [
            'Sertifikat' => route('sertifikat.index'),
        ];
        $viewData = compact('title', 'activeMenus', 'breadcrumbs');

        return view('sertifikat.index10hari', $viewData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param SertifikatRequest $request
     * @return void
     */
    public function create(SertifikatRequest $request)
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SertifikatRequest $request
     * @return void
     */
    public function store(SertifikatRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param SertifikatRequest $request
     * @param  int $id
     * @return void
     */
    public function show(SertifikatRequest $request, $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param SertifikatRequest $request
     * @param $idMagang
     * @return \Illuminate\Http\Response
     */
    public function edit(SertifikatRequest $request, $idMagang)
    {
        $model = Magang::findOrFail($idMagang);
        $title = 'Form input nilai magang';
        $activeMenus = ['menuSertifikat'];
        $breadcrumbs = [
            'Sertifikat' => route('sertifikat.index'),
            'Nilai' => route('sertifikat.edit', $model->id),
        ];
        $validator = \JsValidator::make($request->getEditRules(), $request->messages());
        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('sertifikat.edit', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SertifikatRequest $request
     * @param $idMagang
     * @return \Illuminate\Http\Response
     */
    public function update(SertifikatRequest $request, $idMagang)
    {
        try {
            $model = Magang::findOrFail($idMagang);
            $model->update($request->all());
            $this->storeFiles($request, $model); //if there is any
            $json = ['status' => true, 'message' => 'Nilai berhasil disimpan!'];
        } catch (\Exception $e) {
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data berhasil diupdate!');
        //return redirect()->back()->with('alert', $alert);
        $toastr = new Toastr('success', 'Data berhasil diupdate!');
        return redirect()->back()->with('toastr', $toastr);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param SertifikatRequest $request
     * @param  int $id
     * @return void
     */
    public function destroy(SertifikatRequest $request, $id)
    {
        //
    }

    /**
     * Menampilkan form cetak PDF
     *
     * @param SertifikatRequest $request
     * @param $idMagang
     * @return \Illuminate\Http\Response
     */
    public function formCetak(SertifikatRequest $request, $idMagang)
    {
        $model = Magang::with('pengajuan.pendaftar', 'modelPeserta')->findOrFail($idMagang);
        $title = 'Cetak Sertifikat';
        $activeMenus = ['menuSertifikat'];
        $breadcrumbs = [
            'Sertifikat' => route('sertifikat.index'),
            'Cetak' => route('sertifikat.form_cetak', $model->id),
        ];
        $validator = \JsValidator::make($request->getCetakRules(), $request->messages());
        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('sertifikat.cetak', $viewData);
    }

    /**
     * Cetak sertfikat ke PDF
     *
     * @param SertifikatRequest $request
     * @param $idMagang
     * @return \Illuminate\Http\Response
     */
    public function cetak(SertifikatRequest $request, $idMagang)
    {
        $model = Magang::with('pengajuan.pendaftar', 'modelPeserta')->findOrFail($idMagang);
        $model->update($request->all());

        $data = [
            'title' => 'Sertifikat Magang',
            'model' => $model
        ];

        $pdf = PDF::loadView('sertifikat.pdf', $data, [], [
            'format' => 'Letter-L',
            'author' => 'Magang Generasi Bertalenta',
            'creator' => 'Kementerian BUMN',
            'margin_top' => 0,
            'margin_left' => 0,
            'margin_bottom' => 0,
            'margin_right' => 0,
            'margin_footer' => 0,
        ]);

        //return view('sertifikat.pdf', $data);
        return $pdf->stream('PKA.pdf');

    }

    public function getNilai($nilai)
    {
        $index = IndexNilai::where('nilai_awal', '<=', $nilai)
            ->where('nilai_akhir', '>=', $nilai)
            ->first();
        return response()->json($index);
    }

    /**
     * Handle Datatable request.
     *
     * @param SertifikatRequest $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function datatable(SertifikatRequest $request)
    {
        $queryBuilder = Magang::query()
            ->with('pengajuan.pendaftar')
            ->whereHas('pengajuan', function($q){
                $q->where('is_draft', false)->where('is_submitted', true)->where('is_deleted', false);
            })
            ->where('is_accepted', true)
            ->whereRaw('CASE WHEN is_need_pendaftar_approval = true THEN is_approved_by_pendaftar = true ELSE 1=1 END')
            ->orderBy('id_pengajuan', 'desc');

        return Datatables::of($queryBuilder)
            ->filter(function ($query) use ($request) {
                if (!empty($request->input('filter_id_pendaftar'))) {
                    $query->where('id_pendaftar', 'ilike', "%{$request->input('filter_id_pendaftar')}%");
                }
                if (!empty($request->input('filter_file_surat_pengantar_resmi'))) {
                    $query->where('file_surat_pengantar_resmi', 'ilike', "%{$request->input('filter_file_surat_pengantar_resmi')}%");
                }
                if (!empty($request->input('filter_file_proposal'))) {
                    $query->where('file_proposal', 'ilike', "%{$request->input('filter_file_proposal')}%");
                }
                if (!empty($request->input('filter_is_new'))) {
                    $query->where('is_new', 'ilike', "%{$request->input('filter_is_new')}%");
                }
                if (!empty($request->input('filter_is_draft'))) {
                    $query->where('is_draft', 'ilike', "%{$request->input('filter_is_draft')}%");
                }
            })
            ->editColumn('id_pendaftar', function($data){
                return $data->pengajuan->id_pendaftar;
            })
            ->editColumn('pendaftar', function($data){
                return $data->pengajuan->pendaftar->nama_lengkap;
            })
            ->editColumn('id_magang', function($data){
                return $data->id;
            })
            ->editColumn('unit', function($data){
                return $data->unit;
            })
            ->editColumn('waktu_magang', function($data){
                return "{$data->carbonTglAwal->format('d M Y')} s.d. {$data->carbonTglAkhir->format('d M Y')} <br>({$data->lamaHari} hari)";
            })
            ->editColumn('nilai', function($data){
                if($data->nilai){
                    return '<span class="kt-badge kt-badge--warning kt-badge--lg">'.$data->nilai.'</span>';
                }
                return '<span class="kt-badge kt-badge--unified-danger kt-badge--lg" data-toggle="kt-tooltip" title="Belum ada nilai" data-skin="brand">?</span>';
            })
            ->editColumn('tgl_pengajuan', function($data){
                return Carbon::createFromFormat('Y-m-d H:i:s', $data->pengajuan->submitted_at)->format('d M Y');
            })
            ->addColumn('action', function ($data) {
                $edit = $cetak = '';
                if (auth()->user()->hasAnyPermission(['Sertifikat - edit'])) {
                    #edit
                    $edit =  '<a href="' . route('sertifikat.edit', $data->id) . '" class="btn btn-outline-hover-info btn-icon btn-circle btn-sm btnEdit" data-toggle="kt-tooltip" title="Isi nilai" data-skin="brand"><i class="la la-pencil kt-font-info"></i></a>';

                    #nilai
                    $cetak =  '<div data-toggle="kt-tooltip" title="Cetak tidak dapat dilakukan sebelum isi nilai" data-skin="brand" style="cursor: not-allowed; display: inline;"><a href="javascript:;" class="btn btn-outline-hover-info btn-icon btn-circle btn-sm disabled"><i class="la la-print kt-font-warning"></i></a></div>';
                    if($data->nilai){
                        $cetak =  '<a href="'.route('sertifikat.form_cetak', $data->id).'" class="btn btn-outline-hover-info btn-icon btn-circle btn-sm btnEdit" data-toggle="kt-tooltip" title="Cetak Sertifikat" data-skin="brand"><i class="la la-print kt-font-warning"></i></a>';
                    }
                }
                return $edit . $cetak;
            })
            ->rawColumns(['action', 'waktu_magang', 'nilai'])
            ->make(true);
    }

    /**
     * Handle Datatable request.
     *
     * @param SertifikatRequest $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function datatable10hari(SertifikatRequest $request)
    {
        $in10Days = Carbon::now()->addHours(240);
        $queryBuilder = Magang::query()
            ->with('pengajuan.pendaftar')
            ->whereHas('pengajuan', function($q){
                $q->where('is_draft', false)->where('is_submitted', true)->where('is_deleted', false);
            })
            ->where('is_accepted', true)
            ->whereDate('tgl_akhir', '<=', $in10Days)
            ->whereNull('nilai')
            ->orderBy('id_pengajuan', 'desc');

        return Datatables::of($queryBuilder)
            ->filter(function ($query) use ($request) {
                if (!empty($request->input('filter_id_pendaftar'))) {
                    $query->where('id_pendaftar', 'ilike', "%{$request->input('filter_id_pendaftar')}%");
                }
                if (!empty($request->input('filter_file_surat_pengantar_resmi'))) {
                    $query->where('file_surat_pengantar_resmi', 'ilike', "%{$request->input('filter_file_surat_pengantar_resmi')}%");
                }
                if (!empty($request->input('filter_file_proposal'))) {
                    $query->where('file_proposal', 'ilike', "%{$request->input('filter_file_proposal')}%");
                }
                if (!empty($request->input('filter_is_new'))) {
                    $query->where('is_new', 'ilike', "%{$request->input('filter_is_new')}%");
                }
                if (!empty($request->input('filter_is_draft'))) {
                    $query->where('is_draft', 'ilike', "%{$request->input('filter_is_draft')}%");
                }
            })
            ->editColumn('id_pendaftar', function($data){
                return $data->pengajuan->id_pendaftar;
            })
            ->editColumn('pendaftar', function($data){
                return $data->pengajuan->pendaftar->nama_lengkap;
            })
            ->editColumn('id_magang', function($data){
                return $data->id;
            })
            ->editColumn('unit', function($data){
                return $data->unit;
            })
            ->editColumn('waktu_magang', function($data){
                return "{$data->carbonTglAwal->format('d M Y')} s.d. {$data->carbonTglAkhir->format('d M Y')} <br>({$data->lamaHari} hari)";
            })
            ->editColumn('nilai', function($data){
                if($data->nilai){
                    return '<span class="kt-badge kt-badge--warning kt-badge--lg">'.$data->nilai.'</span>';
                }
                return '<span class="kt-badge kt-badge--unified-danger kt-badge--lg" data-toggle="kt-tooltip" title="Belum ada nilai" data-skin="brand">?</span>';
            })
            ->editColumn('tgl_pengajuan', function($data){
                return Carbon::createFromFormat('Y-m-d H:i:s', $data->pengajuan->submitted_at)->format('d M Y');
            })
            ->addColumn('action', function ($data) {
                $edit = $cetak = '';
                if (auth()->user()->hasAnyPermission(['Sertifikat - edit'])) {
                    $edit =  '<a href="' . route('sertifikat.edit', $data->id) . '" class="btn btn-outline-hover-info btn-icon btn-circle btn-sm btnEdit" data-toggle="kt-tooltip" title="Isi nilai" data-skin="brand"><i class="la la-pencil kt-font-info"></i></a>';
                    //$cetak =  '<a href="" class="btn btn-outline-hover-info btn-icon btn-circle btn-sm btnEdit" data-toggle="kt-tooltip" title="Cetak Sertifikat" data-skin="brand"><i class="la la-print kt-font-warning"></i></a>';
                }
                return $edit . $cetak;
            })
            ->rawColumns(['action', 'waktu_magang', 'nilai'])
            ->make(true);
    }

    /**
     * Handle file store
     *
     * @param $request
     * @param $model
     */
    private function storeFiles($request, $model)
    {
        foreach ($request->files as $key => $file) {
            if ($request->file($key)->isValid()) {
                Storage::delete($model->$key);
                $model->$key = $request->file($key)->storeAs("Magang/$key/$model->id", $file->getClientOriginalName(), 'local');
                $model->save();
            }
        }
    }
}
