<?php

namespace App\Http\View\Composers;

use App\Sekolah\Sekolah;
use App\SekolahProgramStudi\SekolahProgramStudi;
use App\Pengajuan\Pengajuan;
use App\Sertifikat\Sertifikat;
use App\SuratPersetujuan\SuratPersetujuan;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\View\View;
use Symfony\Component\Yaml\Yaml;

class MainMenuComposer
{
    private $yamlData;

    public function __construct()
    {
        $yamlFileLoc    = app_path('Yamls/menus/mainmenu.yaml');
        $this->yamlData = Yaml::parse(file_get_contents($yamlFileLoc));
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        //all menu items
        $view->with('vcMainmenuItems', $this->yamlData);

        //all user's menu items
        $userRoles = auth('web')->user()->roles()->pluck('id');
        $userRoleMenus = DB::table('role_has_menus')->whereIn('role_id', $userRoles)->get();
        $view->with('vcUserRoleMenus', $userRoleMenus);

        //badges
        $badges = [
            'menuTest' => 'new!',
            'menuSettings' => null,
            'menuSekolah' => Sekolah::needApproval()->count(),
            'menuSekolahProgramStudi' => SekolahProgramStudi::needApproval()->count(),
            'menuAlokasi' => null,
            'menuApproval' => Pengajuan::getAprrovalBadgeQty(),
            'menuSuratPersetujuan' => SuratPersetujuan::getBadgeQty(),
            'menuSertifikat' => Sertifikat::getBadgeQty(),
        ];
        $view->with('vcMainmenuBadges', $badges);
    }
}
