<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        /*
        |--------------------------------------------------------------------------
        | FRONTEND
        |--------------------------------------------------------------------------
        */
        View::composer(
            "templates.frontend.magenta.includes.header_navigation", 'App\Http\View\Composers\FrontmenuComposer'
        );


        /*
        |--------------------------------------------------------------------------
        | BACKEND
        |--------------------------------------------------------------------------
        */
        $activeTemplate = config('genelator.template');

        // Main menu
        View::composer(
            "templates.$activeTemplate.includes.mainmenu", 'App\Http\View\Composers\MainMenuComposer'
        );

        // Generator menu
        View::composer(
            "templates.$activeTemplate.includes.generatormenu", 'App\Http\View\Composers\GeneratorMenuComposer'
        );
    }
}
