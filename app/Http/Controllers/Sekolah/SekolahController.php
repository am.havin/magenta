<?php

namespace App\Http\Controllers\Sekolah;

use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

use App\Alert;
use App\Http\Controllers\Controller;
use App\Http\Requests\SekolahRequest;
use App\Sekolah\Sekolah;
use App\Toastr;


class SekolahController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param SekolahRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(SekolahRequest $request)
    {

        $title = 'SMA-SMK/Universitas';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'SMA-SMK/Universitas' => route('sekolah.index'),
        ];

        $url_get_name = $request->get('type');
        $viewData = compact('title', 'activeMenus', 'breadcrumbs', 'url_get_name');

        if ($url_get_name == 'all') {
            return view('sekolahs.index', $viewData);
        } else {
            return view('sekolahs.approval', $viewData);
        }


    }

    /**
     * Show the form for creating a new resource.
     *
     * @param SekolahRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(SekolahRequest $request)
    {
        $title = 'Tambah Sekolah';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Sekolah' => route('sekolah.index'),
            'Tambah' => route('sekolah.create'),
        ];
        $validator = \JsValidator::make($request->getCreateRules(), $request->messages());

        $viewData = compact('title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('sekolahs.create', $viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param SekolahRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(SekolahRequest $request)
    {
        try {
            $model = Sekolah::create($request->all());
            $this->storeFiles($request, $model); //if there is any
            $json = ['status' => true, 'message' => 'Data berhasil disimpan!'];
        } catch (\Exception $e) {
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data baru berhasil disimpan!');
        //return redirect(route('sekolah.index'))->with('alert', $alert);
        $toastr = new Toastr('success', 'Data baru berhasil disimpan!');
        return redirect(route('sekolah.index'))->with('toastr', $toastr);
    }

    /**
     * Display the specified resource.
     *
     * @param SekolahRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(SekolahRequest $request, $id)
    {
        $model = Sekolah::findOrFail($id);
        $title = 'Detail Sekolah';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Sekolah' => route('sekolah.index'),
            'Detail' => route('sekolah.show', $model->id),
        ];
        $approval_caption = Sekolah::approvalCaption($model->is_approved);
        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs', 'approval_caption');

        return view('sekolahs.show', $viewData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param SekolahRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(SekolahRequest $request, $id)
    {
        $model = Sekolah::findOrFail($id);
        $title = 'Edit Sekolah';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Sekolah' => route('sekolah.index'),
            'Edit' => route('sekolah.edit', $model->id),
        ];
        $validator = \JsValidator::make($request->getEditRules(), $request->messages());

        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('sekolahs.edit', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param SekolahRequest $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(SekolahRequest $request, $id)
    {
        try {
            $is_approved = $_POST['is_approved'];
            $model = Sekolah::findOrFail($id);
            $this->storeFiles($request, $model); //if there is any
            if ($is_approved === 0) {
                $model->where('id', '=', $id)->delete();
            } else {
                $model->update($request->all());
            }
            $json = ['status' => true, 'message' => 'Data berhasil diupdate!'];
        } catch (\Exception $e) {
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data berhasil diupdate!');
        //return redirect()->back()->with('alert', $alert);
        $toastr = new Toastr('success', 'Data berhasil diupdate!');
        return redirect()->back()->with('toastr', $toastr);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param SekolahRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(SekolahRequest $request, $id)
    {
        $model = Sekolah::findOrFail($id);
        return response()->json([
            'status' => $model->doDelete()
        ]);
    }

    /**
     * Handle Datatable request.
     *
     * @param SekolahRequest $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function datatable(SekolahRequest $request)
    {
        return Datatables::of(Sekolah::with('pendaftar'))
            ->filter(function ($query) use ($request) {
                if (!empty($request->input('filter_nama'))) {
                    $query->where('nama', 'ilike', "%{$request->input('filter_nama')}%");
                }
                if (!empty($request->input('filter_id_pendaftar'))) {
                    $query->where('id_pendaftar', 'ilike', "%{$request->input('filter_id_pendaftar')}%");
                }
                if ($request->has('filter_is_universitas') && $request->filter_is_universitas != null) {
                    $val = ($request->filter_is_universitas) ? true : false;
                    $query->where('is_universitas', $val);
                }
                if ($request->has('filter_is_approved')) {
                    $query->where('is_approved', $request->input('filter_is_approved'));
                }
            })
            ->editColumn('pendaftar', function ($data) {
                return ($data->pendaftar) ? $data->pendaftar->nama_lengkap : '';
            })
            ->editColumn('is_universitas', function ($data) {
                return ($data->is_universitas) ? 'Universitas' : 'SMA-SMK';
            })
            ->editColumn('is_approved', function ($data) {
                $status_caption = Sekolah::approvalCaption($data->is_approved);
                return $status_caption;
            })
            ->addColumn('action', function ($data) {
                $show = $edit = $delete = '';
                if (auth()->user()->hasAnyPermission(['Sekolah - show'])) {
                    //$show = '<a href="' . route('sekolah.show', $data->id) . '" class="btn btn-outline-hover-warning btn-icon btn-circle btn-sm" data-toggle="kt-tooltip" title="Show detail" data-skin="brand"><i class="la la-eye kt-font-warning"></i></a>';
                }
                if (auth()->user()->hasAnyPermission(['Sekolah - edit'])) {
                    $edit = '<a href="' . route('sekolah.edit', $data->id) . '"class="btn btn-outline-hover-info btn-icon btn-circle btn-sm btnEdit" data-toggle="kt-tooltip" title="Edit" data-skin="brand"><i class="la la-pencil kt-font-info"></i></a>';
                }
                if (auth()->user()->hasAnyPermission(['Sekolah - delete'])) {
                    $delete = '<button type="button" class="btn btn-outline-hover-danger btn-icon btn-circle btn-sm btn-dt-delete" data-toggle="kt-tooltip" title="Delete" data-skin="brand" data-url="' . route('sekolah.destroy', $data->id) . '" data-csrf="' . csrf_token() . '"><i class="la la-close kt-font-danger"></i></button>';
                }
                return $show . $edit . $delete;
            })->make(true);
    }

    /**
     * Handle file store
     *
     * @param $request
     * @param $model
     */
    private function storeFiles($request, $model)
    {
        foreach ($request->files as $key => $file) {
            if ($request->file($key)->isValid()) {
                Storage::delete($model->$key);
                $model->$key = $request->file($key)->storeAs("Sekolah/$key/$model->id", $file->getClientOriginalName(), 'local');
                $model->save();
            }
        }
    }
}
