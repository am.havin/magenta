<?php

use Illuminate\Database\Seeder;

class SekolahProgramStudiPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['group' => 'SekolahProgramStudi', 'name' => 'SekolahProgramStudi - index', 'guard_name' => 'web'],
            ['group' => 'SekolahProgramStudi', 'name' => 'SekolahProgramStudi - create', 'guard_name' => 'web'],
            ['group' => 'SekolahProgramStudi', 'name' => 'SekolahProgramStudi - show', 'guard_name' => 'web'],
            ['group' => 'SekolahProgramStudi', 'name' => 'SekolahProgramStudi - edit', 'guard_name' => 'web'],
            ['group' => 'SekolahProgramStudi', 'name' => 'SekolahProgramStudi - delete', 'guard_name' => 'web'],
        ];

        foreach($data as $val){
            \Spatie\Permission\Models\Permission::updateOrCreate($val, $val);
        }
    }
}
