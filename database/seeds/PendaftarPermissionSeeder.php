<?php

use Illuminate\Database\Seeder;

class PendaftarPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['group' => 'Pendaftar', 'name' => 'Pendaftar - index', 'guard_name' => 'web'],
            ['group' => 'Pendaftar', 'name' => 'Pendaftar - create', 'guard_name' => 'web'],
            ['group' => 'Pendaftar', 'name' => 'Pendaftar - show', 'guard_name' => 'web'],
            ['group' => 'Pendaftar', 'name' => 'Pendaftar - edit', 'guard_name' => 'web'],
            ['group' => 'Pendaftar', 'name' => 'Pendaftar - delete', 'guard_name' => 'web'],
        ];

        foreach($data as $val){
            \Spatie\Permission\Models\Permission::updateOrCreate($val, $val);
        }
    }
}
