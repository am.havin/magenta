<?php

namespace App\Notifications;

use App\Pengajuan\Pengajuan;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;
use Illuminate\Support\Facades\Lang;


class SubmitPengajuanPendaftar extends Notification implements ShouldQueue
{
    use Queueable;

    private $pengajuan;

    /**
     * Create a new notification instance.
     *
     * @param Pengajuan $pengajuan
     */
    public function __construct(Pengajuan $pengajuan)
    {
        $this->pengajuan = $pengajuan;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail($notifiable)
    {
        return (new MailMessage)
            ->subject(Lang::get("Notifikasi - Submit Pengajuan Magang"))
            ->greeting("Pengajuan magang anda telah berhasil di-submit!")
            ->line(Lang::get("Hai {$this->pengajuan->pendaftar->nama_lengkap}, pengajuan magang anda telah berhasil di-submit dengan nomor pengajuan {$this->pengajuan->nomorPengajuan}! Selanjutnya silahkan menunggu proses verifikasi dari kami."))
            ->line(Lang::get('Jika anda tidak merasa melakukan pengajuan magang di Kementerian BUMN, harap abaikan saja email ini.'));
    }

    /**
     * Get the array representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function toArray($notifiable)
    {
        return [
            //
        ];
    }
}
