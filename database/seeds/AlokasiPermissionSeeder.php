<?php

use Illuminate\Database\Seeder;

class AlokasiPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['group' => 'Alokasi', 'name' => 'Alokasi - index', 'guard_name' => 'web'],
            ['group' => 'Alokasi', 'name' => 'Alokasi - create', 'guard_name' => 'web'],
            ['group' => 'Alokasi', 'name' => 'Alokasi - show', 'guard_name' => 'web'],
            ['group' => 'Alokasi', 'name' => 'Alokasi - edit', 'guard_name' => 'web'],
            ['group' => 'Alokasi', 'name' => 'Alokasi - delete', 'guard_name' => 'web'],
        ];

        foreach($data as $val){
            \Spatie\Permission\Models\Permission::updateOrCreate($val, $val);
        }
    }
}
