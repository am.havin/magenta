<?php

namespace App\Http\Controllers\Pengumuman;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Str;
use Yajra\DataTables\DataTables;

use App\Alert;
use App\Http\Controllers\Controller;
use App\Http\Requests\PengumumanRequest;
use App\Pengumuman\Pengumuman;
use App\Toastr;


class PengumumanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param PengumumanRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(PengumumanRequest $request)
    {
        $title = 'Pengumuman';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Pengumuman' => route('pengumuman.index'),
        ];
        $viewData = compact('title', 'activeMenus', 'breadcrumbs');

        return view('pengumumans.index', $viewData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param PengumumanRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(PengumumanRequest $request)
    {
        $title = 'Tambah Pengumuman';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Pengumuman' => route('pengumuman.index'),
            'Tambah' => route('pengumuman.create'),
        ];
        $validator = \JsValidator::make($request->getCreateRules(), $request->messages());

        $viewData = compact('title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('pengumumans.create', $viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param PengumumanRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(PengumumanRequest $request)
    {
        try{
            if($request->is_active){
                //karena hanya boleh 1 yang aktif, maka inactive-kan yang lain
                Pengumuman::query()->update(['is_active' => 0]);
            }
            $model = Pengumuman::create($request->all());

            $this->storeFiles($request, $model); //if there is any
            $json = ['status' => true, 'message' => 'Data berhasil disimpan!'];
        }catch(\Exception $e){
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data baru berhasil disimpan!');
        //return redirect(route('pengumuman.index'))->with('alert', $alert);
        $toastr = new Toastr('success', 'Data baru berhasil disimpan!');
        return redirect(route('pengumuman.index'))->with('toastr', $toastr);
    }

    /**
     * Display the specified resource.
     *
     * @param PengumumanRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(PengumumanRequest $request, $id)
    {
        $model = Pengumuman::findOrFail($id);
        $title = 'Detail Pengumuman';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Pengumuman' => route('pengumuman.index'),
            'Detail' => route('pengumuman.show', $model->id),
        ];
        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs');

        return view('pengumumans.show', $viewData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param PengumumanRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(PengumumanRequest $request, $id)
    {
        $model = Pengumuman::findOrFail($id);
        $title = 'Edit Pengumuman';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Pengumuman' => route('pengumuman.index'),
            'Edit' => route('pengumuman.edit', $model->id),
        ];
        $validator = \JsValidator::make($request->getEditRules(), $request->messages());

        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('pengumumans.edit', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param PengumumanRequest $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(PengumumanRequest $request, $id)
    {
        try{
            $model =  Pengumuman::findOrFail($id);
            if($request->is_active){
                //karena hanya boleh 1 yang aktif, maka inactive-kan yang lain
                Pengumuman::query()->update(['is_active' => 0]);
            }
            $model->update($request->all());
            $this->storeFiles($request, $model); //if there is any
            $json = ['status' => true, 'message' => 'Data berhasil diupdate!'];
        }catch(\Exception $e){
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data berhasil diupdate!');
        //return redirect()->back()->with('alert', $alert);
        $toastr = new Toastr('success', 'Data berhasil diupdate!');
        return redirect()->back()->with('toastr', $toastr);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param PengumumanRequest $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(PengumumanRequest $request, $id)
    {
        $model = Pengumuman::findOrFail($id);
        return response()->json([
            'status' => $model->doDelete()
        ]);
    }

    public function gambar($idPengumuman)
    {
        $model = Pengumuman::findOrFail($idPengumuman);
        return response()->file(storage_path('app/'.$model->gambar));
    }

    /**
     * Handle Datatable request.
     *
     * @param PengumumanRequest $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function datatable(PengumumanRequest $request)
    {
        return Datatables::of(Pengumuman::query())
            ->filter(function ($query) use ($request) {
                if (!empty($request->input('filter_gambar'))) {
                    $query->where('gambar', 'ilike', "%{$request->input('filter_gambar')}%");
                }
                if (!empty($request->input('filter_konten'))) {
                    $query->where('konten', 'ilike', "%{$request->input('filter_konten')}%");
                }
                if (!empty($request->input('filter_is_active'))) {
                    $query->where('is_active', 'ilike', "%{$request->input('filter_is_active')}%");
                }
            })
            ->editColumn('gambar', function($data){
                if($data->gambar){
                    return '<img src="'.url('pengumuman/gambar/' . $data->id).'" alt="" style="max-width: 200px;">';
                }
                return '-';
            })
            ->editColumn('konten', function($data){
                if($data->konten){
                    return Str::limit(strip_tags($data->konten), 200);
                }
                return '';
            })
            ->editColumn('is_active', function($data){
                if($data->is_active){
                    return 'aktif';
                }
                return 'tidak aktif';
            })
            ->addColumn('action', function ($data) {
                $show = $edit = $delete = '';
                if(auth()->user()->hasAnyPermission(['Pengumuman - show'])){
                    //$show = '<a href="'.route('pengumuman.show', $data->id).'" class="btn btn-outline-hover-warning btn-icon btn-circle btn-sm" data-toggle="kt-tooltip" title="Show detail" data-skin="brand"><i class="la la-eye kt-font-warning"></i></a>';
                }
                if(auth()->user()->hasAnyPermission(['Pengumuman - edit'])){
                    $edit = '<a href="'.route('pengumuman.edit', $data->id).'"class="btn btn-outline-hover-info btn-icon btn-circle btn-sm btnEdit" data-toggle="kt-tooltip" title="Edit" data-skin="brand"><i class="la la-pencil kt-font-info"></i></a>';
                }
                if(auth()->user()->hasAnyPermission(['Pengumuman - delete'])){
                    $delete = '<button type="button" class="btn btn-outline-hover-danger btn-icon btn-circle btn-sm btn-dt-delete" data-toggle="kt-tooltip" title="Delete" data-skin="brand" data-url="'.route('pengumuman.destroy', $data->id).'" data-csrf="'.csrf_token().'"><i class="la la-close kt-font-danger"></i></button>';
                }
                return $show.$edit.$delete;
            })
            ->rawColumns(['gambar', 'action'])
            ->make(true);
    }

    /**
     * Handle file store
     *
     * @param $request
     * @param $model
     */
    private function storeFiles($request, $model)
    {
        foreach($request->files as $key => $file){
            if ($request->file($key)->isValid()) {
                Storage::delete($model->$key);
                $model->$key = $request->file($key)->storeAs("Pengumuman/$key/$model->id", $file->getClientOriginalName(),'local');
                $model->save();
            }
        }
    }
}
