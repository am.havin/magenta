<?php

namespace App\Http\Controllers\Alokasi;

use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\DataTables;

use App\Alert;
use App\Http\Controllers\Controller;
use App\Http\Requests\AlokasiRequest;
use App\Alokasi\Alokasi;
use App\Sekolah\Sekolah;
use App\Toastr;


class AlokasiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @param AlokasiRequest $request
     * @return \Illuminate\Http\Response
     */
    public function index(AlokasiRequest $request)
    {
        $title = 'Alokasi Magang';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Alokasi' => route('alokasi.index'),
        ];
        $viewData = compact('title', 'activeMenus', 'breadcrumbs');

        return view('alokasis.index', $viewData);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param AlokasiRequest $request
     * @return \Illuminate\Http\Response
     */
    public function create(AlokasiRequest $request)
    {
        $title = 'Tambah Alokasi';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Alokasi' => route('alokasi.index'),
            'Tambah' => route('alokasi.create'),
        ];
        $validator = \JsValidator::make($request->getCreateRules(), $request->messages());

        $viewData = compact('title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('alokasis.create', $viewData);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param AlokasiRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(AlokasiRequest $request)
    {
        try {
            $model = Alokasi::create($request->all());
            $this->storeFiles($request, $model); //if there is any
            $json = ['status' => true, 'message' => 'Data berhasil disimpan!'];
        } catch (\Exception $e) {
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data baru berhasil disimpan!');
        //return redirect(route('alokasi.index'))->with('alert', $alert);
        $toastr = new Toastr('success', 'Data baru berhasil disimpan!');
        return redirect(route('alokasi.index'))->with('toastr', $toastr);
    }

    /**
     * Display the specified resource.
     *
     * @param AlokasiRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show(AlokasiRequest $request, $id)
    {
        $model = Alokasi::findOrFail($id);
        $title = 'Detail Alokasi';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Alokasi' => route('alokasi.index'),
            'Detail' => route('alokasi.show', $model->id),
        ];
        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs');

        return view('alokasis.show', $viewData);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param AlokasiRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(AlokasiRequest $request, $id)
    {
        $model = Alokasi::findOrFail($id);
        $title = 'Edit Alokasi';
        $activeMenus = ['menuDashboard'];
        $breadcrumbs = [
            'Alokasi' => route('alokasi.index'),
            'Edit' => route('alokasi.edit', $model->id),
        ];
        $validator = \JsValidator::make($request->getEditRules(), $request->messages());

        $viewData = compact('model', 'title', 'activeMenus', 'breadcrumbs', 'validator');
        return view('alokasis.edit', $viewData);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param AlokasiRequest $request
     * @param $id
     * @return \Illuminate\Http\Response
     */
    public function update(AlokasiRequest $request, $id)
    {
        try {
            $model = Alokasi::findOrFail($id);
            $this->storeFiles($request, $model); //if there is any
            $model->update($request->all());
            $json = ['status' => true, 'message' => 'Data berhasil diupdate!'];
        } catch (\Exception $e) {
            $json = ['status' => false, 'message' => $e->getMessage()];
        }

        # JSON RETURN
        if ($request->ajax()) {
            return response()->json($json);
        }

        # STANDARD RETURN
        //$alert = new Alert('success', 'Data berhasil diupdate!');
        //return redirect()->back()->with('alert', $alert);
        $toastr = new Toastr('success', 'Data berhasil diupdate!');
        return redirect()->back()->with('toastr', $toastr);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param AlokasiRequest $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(AlokasiRequest $request, $id)
    {
        $model = Alokasi::findOrFail($id);
        return response()->json([
            'status' => $model->doDelete()
        ]);
    }

    /**
     * Handle Datatable request.
     *
     * @param AlokasiRequest $request
     * @return \Illuminate\Http\Response
     * @throws \Exception
     */
    public function datatable(AlokasiRequest $request)
    {
        return Datatables::of(Alokasi::query())
            ->filter(function ($query) use ($request) {
                if (!empty($request->input('filter_id_unit'))) {
                    $query->where('id_unit', 'ilike', "%{$request->input('filter_id_unit')}%");
                }

                if (!empty($request->input('filter_unit'))) {
                    $query->where('unit', 'ilike', "%{$request->input('filter_unit')}%");
                }

                if (!empty($request->input('filter_kuota'))) {
                    $query->where('kuota', 'ilike', "%{$request->input('filter_kuota')}%");
                }
            })
            ->addColumn('action', function ($data) {
                $show = $edit = $delete = '';
                if (auth()->user()->hasAnyPermission(['Alokasi - show'])) {
                    //$show = '<a href="' . route('alokasi.show', $data->id) . '" class="btn btn-outline-hover-warning btn-icon btn-circle btn-sm" data-toggle="kt-tooltip" title="Show detail" data-skin="brand"><i class="la la-eye kt-font-warning"></i></a>';
                }
                if (auth()->user()->hasAnyPermission(['Alokasi - edit'])) {
                    $edit = '<a href="' . route('alokasi.edit', $data->id) . '"class="btn btn-outline-hover-info btn-icon btn-circle btn-sm btnEdit" data-toggle="kt-tooltip" title="Edit" data-skin="brand"><i class="la la-pencil kt-font-info"></i></a>';
                }
                if (auth()->user()->hasAnyPermission(['Alokasi - delete'])) {
                    $delete = '<button type="button" class="btn btn-outline-hover-danger btn-icon btn-circle btn-sm btn-dt-delete" data-toggle="kt-tooltip" title="Delete" data-skin="brand" data-url="' . route('alokasi.destroy', $data->id) . '" data-csrf="' . csrf_token() . '"><i class="la la-close kt-font-danger"></i></button>';
                }
                return $show . $edit . $delete;
            })->make(true);
    }

    /**
     * Handle file store
     *
     * @param $request
     * @param $model
     */
    private function storeFiles($request, $model)
    {
        foreach ($request->files as $key => $file) {
            if ($request->file($key)->isValid()) {
                Storage::delete($model->$key);
                $model->$key = $request->file($key)->storeAs("Alokasi/$key/$model->id", $file->getClientOriginalName(), 'local');
                $model->save();
            }
        }
    }
}
