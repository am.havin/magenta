<?php

use Illuminate\Database\Seeder;

class PesertaPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['group' => 'Peserta', 'name' => 'Peserta - index', 'guard_name' => 'web'],
            ['group' => 'Peserta', 'name' => 'Peserta - create', 'guard_name' => 'web'],
            ['group' => 'Peserta', 'name' => 'Peserta - show', 'guard_name' => 'web'],
            ['group' => 'Peserta', 'name' => 'Peserta - edit', 'guard_name' => 'web'],
            ['group' => 'Peserta', 'name' => 'Peserta - delete', 'guard_name' => 'web'],
        ];

        foreach($data as $val){
            \Spatie\Permission\Models\Permission::updateOrCreate($val, $val);
        }
    }
}
