<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePendaftarsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pendaftars', function (Blueprint $table) {
            $table->increments('id');
            $table->string('email');
            $table->string('password');
            $table->string('nama_lengkap');
            $table->string('no_hp');
            $table->integer('id_sekolah')->nullable();
            $table->string('sekolah')->nullable();
            $table->string('pic')->nullable();
            $table->boolean('is_banned')->default(false);
            $table->boolean('is_deleted')->default(false);

            $table->string('created_by')->nullable();
            $table->string('updated_by')->nullable();
            $table->string('remember_token')->nullable();
            $table->timestamp('email_verified_at')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pendaftars');
    }
}
