<div class="section mb-0 pb-0 bg-theme-light clearfix" style="padding-top: 120px;">
    <div class="container">
        <div class="row">
            <div class="col-md-8 offset-1">
                <div class="before-heading">Don't Hesitate to Reach out to Us</div>
                <h2 class="nott t600 display-4">Want to Talk?</h2>
                <h4 class="t300 mb-4">Call us at <a href="tel:+(61)8-234-532-45">+(61)8-234-532-45</a></h4>
                <a href="#" class="button button-rounded button-xlarge m-0 nott t600 ls0 px-5">Email Us</a>
            </div>
        </div>
    </div>
    <img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/footer-bg.jpg" alt="Footer Image" class="footer-img">
</div>
