@if(isset($approval) && $approval)
    <table>
        <tbody>
        <tr>
            <td>Nomor pengajuan</td>
            <td>:</td>
            <td>{{ $pengajuan->nomorPengajuan }}</td>
        </tr>
        <tr>
            <td>Nama Pendaftar</td>
            <td>:</td>
            <td>{{ $pengajuan->pendaftar->nama_lengkap }}</td>
        </tr>
        </tbody>
    </table>

<style>
    .table-bordered td,
    .table-bordered th{
        border: 1px solid #aaa !important;
        border-collapse: collapse;
        border-spacing: 0;
        padding: 5px;
    }
</style>

<table class="table-bordered" cellpadding="0" cellspacing="0" valign="top">
    <thead>
        <tr>
            <th>Peserta</th>
            <th>Unit tujuan</th>
            <th>Tanggal magang</th>
            <th>Status</th>
        </tr>
    </thead>
    <tbody>
    @foreach($pengajuan->magangs as $magang)
        <tr>
            <td>{{ $magang->peserta }}</td>
            <td>{{ $magang->unit }}</td>
            <td>{{ $magang->carbonTglAwal->format('d/m/Y') }} - {{ $magang->carbonTglAkhir->format('d/m/Y') }}</td>
            <td>{{ $magang->textStatusForEmail }}</td>
        </tr>
    @endforeach
    </tbody>
</table>

@if($pengajuan->hasAtLeast1UnapprovedMagangBersyarat())
<em>
    PS: Ada peserta magang anda yang diterima bersyarat sehingga membutuhkan approval anda.
    Khusus peserta magang yang "Disetujui Bersyarat", silahkan login untuk melakukan Approval, jika anda tidak melakukan approval dalam waktu 3 hari
    maka pengajuan anda untuk peserta tersebut akan otomatis Ditolak.
</em>
<br><br>
@endif

@endif