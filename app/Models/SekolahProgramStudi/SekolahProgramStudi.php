<?php

namespace App\SekolahProgramStudi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class SekolahProgramStudi extends Model
{
    protected $table = 'sekolah_programstudis';
    protected $guarded = ['id'];

    public function pendaftar()
    {
        return $this->belongsTo('App\Pendaftar\Pendaftar', 'id_pendaftar');
    }

    public function sekolah()
    {
        return $this->belongsTo('App\Sekolah\Sekolah', 'id_sekolah');
    }

    public function scopeNeedApproval($query)
    {
        return $query->whereNull('is_approved');
    }

    public function doDelete()
    {
        $status = true;

        DB::beginTransaction();
        try{
            $this->delete();
            DB::commit();
        }catch(\Exception $e) {
            DB::rollback();
            $status = false;
        }

        return $status;
    }
}
