<?php

namespace App\Generators;

use Carbon\Carbon;
use Illuminate\Http\File;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Symfony\Component\Yaml\Yaml;

class Controller
{
    private $stubString;

    /**
     * Migration constructor.
     */
    public function __construct()
    {
        $stubFileLoc        = resource_path('stubs/controller.stub');
        $this->stubString   = file_get_contents($stubFileLoc);
    }

    /**
     * @param $controllerData
     * @param $requestData
     * @param $modelData
     * @param $viewData
     * @param $routeData
     * @param $fieldsData
     * @return string
     */
    public function generate($controllerData, $requestData, $modelData, $viewData, $routeData, $fieldsData)
    {
        $controllerString = $this->replaceStubString(
            $controllerData['class_name'],
            $controllerData['namespace'],
            $requestData['class_name'],
            $requestData['namespace'],
            $modelData['class_name'],
            $modelData['namespace'],
            $viewData['file_dir'],
            $viewData['title'],
            $routeData['url'],
            $this->generateFilterCode($fieldsData)
        );
        return $this->createFile($controllerData['class_name'], $controllerData['file_dir'], $controllerString);
    }

    private function generateFilterCode($fields)
    {
        $return = '';
        foreach($fields as $key => $value){
            $return .= '
                if (!empty($request->input(\'filter_'.$key.'\'))) {
                    $query->where(\''.$key.'\', \'ilike\', "%{$request->input(\'filter_'.$key.'\')}%");
                }
            ';
            $return .= "\r\n";
        }
        return $return;
    }

    /**
     * @param $className
     * @param $namespace
     * @param $requestClassName
     * @param $requestNamespace
     * @param $modelClassName
     * @param $modelNamespace
     * @param $viewFileDir
     * @param $title
     * @param $route
     * @param $filterCode
     * @return String mixed
     */
    private function replaceStubString($className, $namespace, $requestClassName, $requestNamespace, $modelClassName, $modelNamespace, $viewFileDir, $title, $route, $filterCode)
    {
        $viewFileDir = str_replace('/', '.', Str::finish($viewFileDir, '/'));
        $route = str_replace('/', '.', Str::finish($route, '/'));

        $replacedString = str_replace(
            [
                '{{namespace}}',
                '{{className}}',
                '{{modelNamespace}}',
                '{{modelClassName}}',
                '{{requestNamespace}}',
                '{{requestClassName}}',
                '{{viewFileDir}}',
                '{{title}}',
                '{{route}}',
                '{{filterCodes}}'
            ],
            [
                $namespace,
                $className,
                $modelNamespace,
                $modelClassName,
                $requestNamespace,
                $requestClassName,
                $viewFileDir,
                $title,
                $route,
                $filterCode
            ],
            $this->stubString
        );
        return $replacedString;
    }

    /**
     * @param $className
     * @param $file_dir
     * @param $controllerString
     * @return string
     */
    private function createFile($className, $file_dir, $controllerString)
    {
        $fileDir = 'Http/Controllers/'.$file_dir;
        if(!is_dir(app_path($fileDir))){ //bikin direktorinya  kalau belum ada
            mkdir(app_path($fileDir), 0755, true);
        }

        $filepath = Str::finish($fileDir , '/') . "$className.php";
        file_put_contents(app_path($filepath), $controllerString);

        return $filepath;
    }
}
