<?php

namespace App\Alokasi;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Alokasi extends Model
{
    protected $table = 'alokasis';
    protected $guarded = ['id'];

    public function doDelete()
    {
        $status = true;

        DB::beginTransaction();
        try{
            $this->delete();
            DB::commit();
        }catch(\Exception $e) {
            DB::rollback();
            $status = false;
        }

        return $status;
    }
}
