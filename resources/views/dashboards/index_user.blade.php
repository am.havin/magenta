@extends((Request::ajax()) ? 'templates.layout-blank' : 'templates.'.config('genelator.template').'.layout')

@section('subheaderToolbar')
    {{--<a href="#" class="btn kt-subheader__btn-daterange" id="" data-toggle="kt-tooltip" title="Header toolbar"--}}
    {{--data-skin="brand" data-placement="left">--}}
    {{--<i class="flaticon2-add"></i>--}}
    {{--<span>Header toolbar</span>--}}
    {{--</a>--}}
@endsection

@section('content')
    <div class="kt-portlet kt-portlet--mobile">
        <div class="kt-portlet__body text-center">
            <br>
            <div class="alert alert-light alert-elevate fade show" role="alert">
                <div class="alert-icon"><i class="flaticon-warning"></i></div>
                <div class="alert-text">
                    Anda tidak memiliki hak akses lebih lanjut untuk melakukan operasi admin di website Magenta ini! <br>
                    silahkan <a href="{{ url('adminlogout') }}">logout</a> dari website ini.
                </div>
            </div>
        </div>
    </div>
@endsection

@section('pageScripts')
@endsection
