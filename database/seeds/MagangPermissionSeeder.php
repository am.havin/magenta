<?php

use Illuminate\Database\Seeder;

class MagangPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            ['group' => 'Magang', 'name' => 'Magang - index', 'guard_name' => 'web'],
            ['group' => 'Magang', 'name' => 'Magang - create', 'guard_name' => 'web'],
            ['group' => 'Magang', 'name' => 'Magang - show', 'guard_name' => 'web'],
            ['group' => 'Magang', 'name' => 'Magang - edit', 'guard_name' => 'web'],
            ['group' => 'Magang', 'name' => 'Magang - delete', 'guard_name' => 'web'],
        ];

        foreach($data as $val){
            \Spatie\Permission\Models\Permission::updateOrCreate($val, $val);
        }
    }
}
