<?php

namespace App\Http\Requests\Frontend;

use App\Peserta\Peserta;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\Rule;

class PesertaRequest extends FormRequest
{
    public $rules = [
        'nama' => 'required',
        'no_ktp' => 'required|size:16|unique:pesertas',
        'nim_nis' => 'required',
        'email' => 'required|email|unique:pesertas',
        'tempat_lahir' => 'required',
        'tgl_lahir' => 'required|date_format:Y-m-d|before:today',
        'is_universitas' => 'required',
        'id_sekolah' => 'required_without:sekolah',
        'sekolah' => 'required_if:id_sekolah,0',
        'id_programstudi' => 'required_without:programstudi',
        'programstudi' => 'required_if:id_programstudi,0',
        'ipk' => 'required|min:0|max:100',
        'file_cv' => 'max:2048|mimes:pdf',
        'file_skck' => 'required|max:256|mimes:pdf',
        'foto' => 'required|max:512|mimes:jpg,png,jpeg'
    ];

    public function getCreateRules()
    {
        return $this->rules;
    }

    public function getEditRules()
    {
        $pesertaId= array_values(request()->route()->originalParameters())[0];
        $pendaftar = auth('pendaftar')->user();
        $curPeserta = Peserta::where('id', $pesertaId)->where('id_pendaftar', $pendaftar->id)->first();
        if($curPeserta){
            $uniqueRule = Rule::unique('pesertas')->ignore($pesertaId);
            $this->rules['no_ktp'] = ['required', 'min:12', $uniqueRule];
            $this->rules['email'] = ['required', 'min:6', $uniqueRule];
            $this->rules['foto'] = 'max:512|mimes:jpg,png,jpeg';
            $this->rules['file_cv'] = 'max:2048|mimes:pdf';
            $this->rules['file_skck'] = 'max:512|mimes:pdf';
            return $this->rules;
        }
        return abort(404);
    }

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $controllerMethod = request()->route()->getActionMethod();
        if ($controllerMethod == 'store') {
            return $this->getCreateRules();
        }
        if ($controllerMethod == 'update') {
            return $this->getEditRules();
        }
        return [];
    }

    /**
     * Get the error messages for the defined validation rules.
     *
     * @return array
     */
    public function messages()
    {
        return [
            'is_universitas.required' => 'Tingkat pendidikan harus dipilih',
            'ipk.required' => 'Harus diisi!',
            'no_ktp.size' => 'Nomor KTP harus tepat 16 angka'
            //'body.required'  => 'A message is required',
        ];
    }
}
