@extends((Request::ajax()) ? 'templates.layout-blank' : 'templates.'.config('genelator.template').'.layout')

@section('subheaderToolbar')
    <a href="#" class="btn invisible">
    </a>
@endsection

@section('content')
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">

            <div class="modal-header">
                <h5 class="modal-title">{{ $title }}</h5>
                <button id="closeBtn" type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <div class="modal-body">
                @if($errors->isNotEmpty())
                    <div class="row">
                        <div class="col-lg-12">
                            <div class="alert alert-warning" role="alert">
                                <div class="alert-icon"><i class="flaticon-warning"></i></div>
                                <div class="alert-text">
                                    <ul>
                                        @foreach($errors->all() as $message)
                                            <li>{{ $message }}</li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                <div class="row">
                    <div class="col-lg-12">
                        <div class="kt-portlet">
                            <!--begin::Form-->
                            <form action="{{ url('users/') }}" id="createForm" method="post" class="kt-form" autocomplete="off"
                                  enctype="multipart/form-data">
                                @csrf
                                <input type="hidden" name="pegawai_pic" id ="pegawai_pic">

                                <div class="kt-portlet__body">
                                    <div class="form-group row">
                                        <div class="col-lg-6">
                                            <label class="form-control-label">* Username:</label>
                                            <select name="username" id="username" class="form-control" required>
                                                <option></option>
                                            </select>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="form-control-label">* Id Pegawai:</label>
                                            <input type="number" name="pegawai_id" id="pegawai_id" class="form-control" value=""
                                                   required readonly>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="w-100"></div>
                                        <br>
                                        <div class="col-lg-6">
                                            <label class="form-control-label">* Nama Pegawai:</label>
                                            <input type="text" name="pegawai_nama" id="pegawai_nama" class="form-control" value=""
                                                   required readonly>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="col-lg-6">
                                            <label class="form-control-label">* Role:</label>
                                            <select name="roles[]" id="roles" class="form-control select2ajax" required multiple
                                                    data-ajax--url="{{ url('select2/roles') }}" data-placeholder="Pilih"
                                                    data-allow-clear="true">
                                                <option></option>
                                            </select>
                                            <span class="form-text text-muted"></span>
                                        </div>
                                        <div class="w-100"></div>
                                        <br>
                                    </div>
                                </div>
                                <div class="kt-portlet__foot">
                                    <div class="kt-form__actions">
                                        <div class="row">
                                            <div class="col-lg-12">
                                                <button type="submit" class="btn btn-warning">Submit</button>
                                                {{-- <a href="{{ url('users/') }}" class="btn btn btn-secondary">Cancel</a> --}}
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </form>
                            <!--end::Form-->
                        </div>
                        <!--end::Portlet-->
                    </div>
                </div>
            </div>

        </div>
    </div>
@endsection



@section('pageScripts')
    {!! $validator->selector('#createForm') !!}

    <script>
        jQuery(document).ready(function () {
            initSelect2();
            initDatepicker();
            if (!(typeof datatable === "undefined")) {
                //pass instance datatable (optional) agar di-reload ketika submit formnya sukses
                initAjaxForm('#createForm', datatable); //datatable dapat dari index.blade.php
            }

            let select2username = $('#username').select2({
                placeholder: 'Pilih',
                allowClear: true,
                minimumInputLength: 2,
                ajax: {
                    url: 'https://simanis.bumn.go.id/api/v1/select2/users',
                    dataType: 'json',
                    delay: 250,
                    processResults: function (data, params) {
                        return {
                            results: data.results,
                        };
                    },
                    cache: true
                },
            }).on('select2:select', function (event) {
                assignData({
                    username: event.params.data.complete_data.name,
                    id: event.params.data.complete_data.pegawai.id,
                    nama: event.params.data.complete_data.pegawai.nama,
                    pic: event.params.data.complete_data.pegawai.foto,
                })
            }).on('select2:unselect', function (event) {
                clearData();
            })

            function assignData({'username': username, 'id': id, 'nama': nama, 'pic': pic}) {
                $('#username').val(username);
                $('#pegawai_id').val(id);
                $('#pegawai_nama').val(nama);
                $('#pegawai_pic').val('http://simanis.bumn.go.id/file/fopeg/' + pic);
            }

            function clearData() {
                $('#pegawai_id').val(null);
                $('#pegawai_nama').val(null);
                $('#pegawai_pic').val(null);
            }
        });
    </script>
@endsection

{{-- For Modal --}}
@if(Request::ajax())
    @yield('content')
    @yield('pageScripts')
@endif
