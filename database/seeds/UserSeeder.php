<?php

use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'username' => 'adla.mohammad',
                'pegawai_id' => 844,
                'pegawai_nama' => 'Adla Mohammad Havin',
                'pegawai_pic' => 'http://simanis.bumn.go.id/file/fopeg/PNP-964.jpg',
            ],
            [
                'username' => 'annas.abdillah',
                'pegawai_id' => 406,
                'pegawai_nama' => 'Annas Abdillah Marta',
                'pegawai_pic' => 'http://simanis.bumn.go.id/file/fopeg/Annas Abdillah Marta.jpg',
            ],
            [
                'username' => 'admin.adla',
                'pegawai_id' => 844,
                'pegawai_nama' => 'Adla Mohammad Havin',
                'pegawai_pic' => 'http://simanis.bumn.go.id/file/fopeg/PNP-964.jpg',
            ],
            [
                'username' => 'widia.jessti',
                'pegawai_id' => 940,
                'pegawai_nama' => 'Widia Jessti',
                'pegawai_pic' => 'http://simanis.bumn.go.id/file/fopeg/Widia Jessti.jpg',
            ],
        ];

        foreach($data as $val){
            \App\User::updateOrCreate($val, $val);
        }
    }
}
