<div class="section p-0 nobg clearfix">
    <div class="container">
        <div class="row mb-2">
            <div class="col-lg-6 col-md-4">
                <div class="heading-block nobottomborder mb-4">
                    <div class="before-heading">How do you Contact Us</div>
                    <h2 class="nott t600">Locations</h2>
                </div>
            </div>

            <div class="col-lg-6 col-md-8">
                <div class="row">
                    <div class="col-sm-4 col-3">
                        <div class="counter color t600"><span data-from="1" data-to="3" data-refresh-interval="2" data-speed="600"></span>+</div>
                        <h5 class="mt-0 t500">Branches</h5>
                    </div>

                    <div class="col-sm-4 col-6">
                        <div class="counter color t600"><span data-from="100" data-to="4500" data-refresh-interval="100" data-speed="1500"></span>+</div>
                        <h5 class="mt-0 t500">Active Members</h5>
                    </div>

                    <div class="col-sm-4 col-3">
                        <div class="counter color t600"><span data-from="2" data-to="100" data-refresh-interval="30" data-speed="1500"></span>%</div>
                        <h5 class="mt-0 t500">Secured</h5>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="locations-carousel" class="owl-carousel carousel-widget owl-carousel-full clearfix" data-animate-in="fadeIn" data-animate-out="fadeOut" data-speed="200" data-margin="0" data-nav="true" data-pagi="true" data-items="1">
        <div>
            <div class="masonry-thumbs grid-5" data-big="2" data-lightbox="gallery">
                <a href="demos/coworking/images/location/thumbs/full/1.jpg" data-lightbox="gallery-item"><img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/location/thumbs/1.jpg" alt="Gallery Thumb 1"></a>
                <a href="#"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3151.835253572242!2d144.95373531531297!3d-37.817327679751735!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad65d4c2b349649%3A0xb6899234e561db11!2sEnvato!5e0!3m2!1sen!2sin!4v1554124945593!5m2!1sen!2sin" width="500" height="334" allowfullscreen  style="border:0;"></iframe></a>
                <a href="demos/coworking/images/location/thumbs/full/2.jpg" data-lightbox="gallery-item"><img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/location/thumbs/2.jpg" alt="Gallery Thumb 12"></a>
                <a href="demos/coworking/images/location/thumbs/full/3.jpg" data-lightbox="gallery-item"><img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/location/thumbs/3.jpg" alt="Gallery Thumb 3"></a>
                <a href="demos/coworking/images/location/thumbs/full/4.jpg" data-lightbox="gallery-item"><img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/location/thumbs/4.jpg" alt="Gallery Thumb 4"></a>
                <a href="demos/coworking/images/location/thumbs/full/5.jpg" data-lightbox="gallery-item"><img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/location/thumbs/5.jpg" alt="Gallery Thumb 5"></a>
                <a href="demos/coworking/images/location/thumbs/full/6.jpg" data-lightbox="gallery-item"><img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/location/thumbs/6.jpg" alt="Gallery Thumb 6"></a>
                <a href="demos/coworking/images/location/thumbs/full/7.jpg" data-lightbox="gallery-item"><img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/location/thumbs/7.jpg" alt="Gallery Thumb 7"></a>
                <a href="demos/coworking/images/location/thumbs/full/8.jpg" data-lightbox="gallery-item"><img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/location/thumbs/8.jpg" alt="Gallery Thumb 13"></a>
                <a href="demos/coworking/images/location/thumbs/full/9.jpg" data-lightbox="gallery-item"><img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/location/thumbs/9.jpg" alt="Gallery Thumb 6"></a>
                <a href="demos/coworking/images/location/thumbs/full/10.jpg" data-lightbox="gallery-item"><img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/location/thumbs/10.jpg" alt="Gallery Thumb 7"></a>
                <a href="demos/coworking/images/location/thumbs/full/11.jpg" data-lightbox="gallery-item"><img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/location/thumbs/11.jpg" alt="Gallery Thumb 12"></a>
            </div>
            <div class="container">
                <div class="card shadow bg-white">
                    <div class="card-body">
                        <h3 class="mb-4">Melbourne</h3>
                        <div style="font-size: 16px;">
                            <address>
                                121 King St,<br>Melbourne VIC 3000,<br>Australia
                            </address>
                            <div class="mb-4" style="font-size: 15px;">
                                <small class="text-black-50 d-block mb-2">Phone Number:</small>
                                <a href="tel:+(61)3-222-333-44" class="text-dark d-block mb-1">+(61)3-222-333-44</a>
                                <a href="tel:+(61)3-234-532-45" class="text-dark">+(61)3-234-532-45</a>
                            </div>

                            <a href="#" class="button button-rounded m-0 nott t600 ls0">Email Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div>
            <div class="masonry-thumbs grid-5" data-big="1,6" data-lightbox="gallery">
                <a href="#"><iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3151.835253572242!2d144.95373531531297!3d-37.817327679751735!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x6ad65d4c2b349649%3A0xb6899234e561db11!2sEnvato!5e0!3m2!1sen!2sin!4v1554124945593!5m2!1sen!2sin" width="500" height="334" allowfullscreen  style="border:0;"></iframe></a>
                <a href="demos/coworking/images/location/thumbs/full/5.jpg" data-lightbox="gallery-item"><img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/location/thumbs/5.jpg" alt="Gallery Thumb 5"></a>
                <a href="demos/coworking/images/location/thumbs/full/2.jpg" data-lightbox="gallery-item"><img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/location/thumbs/2.jpg" alt="Gallery Thumb 12"></a>
                <a href="demos/coworking/images/location/thumbs/full/4.jpg" data-lightbox="gallery-item"><img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/location/thumbs/4.jpg" alt="Gallery Thumb 4"></a>
                <a href="demos/coworking/images/location/thumbs/full/1.jpg" data-lightbox="gallery-item"><img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/location/thumbs/1.jpg" alt="Gallery Thumb 1"></a>
                <a href="demos/coworking/images/location/thumbs/full/9.jpg" data-lightbox="gallery-item"><img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/location/thumbs/9.jpg" alt="Gallery Thumb 6"></a>
                <a href="demos/coworking/images/location/thumbs/full/3.jpg" data-lightbox="gallery-item"><img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/location/thumbs/3.jpg" alt="Gallery Thumb 3"></a>
                <a href="demos/coworking/images/location/thumbs/full/6.jpg" data-lightbox="gallery-item"><img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/location/thumbs/6.jpg" alt="Gallery Thumb 6"></a>
                <a href="demos/coworking/images/location/thumbs/full/7.jpg" data-lightbox="gallery-item"><img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/location/thumbs/7.jpg" alt="Gallery Thumb 7"></a>
            </div>
            <div class="container">
                <div class="card shadow bg-white">
                    <div class="card-body">
                        <h3 class="mb-4">Sydney</h3>
                        <div style="font-size: 16px;">
                            <address>
                                Bennelong Point,<br>Sydney NSW 2000,<br>Australia.
                            </address>
                            <div class="mb-4" style="font-size: 15px;">
                                <small class="text-black-50 d-block mb-2">Phone Number:</small>
                                <a href="tel:+(61)2-222-333-44" class="text-dark d-block mb-1">+(61)2-222-333-44</a>
                                <a href="tel:+(61)2-234-532-45" class="text-dark">+(61)2-234-532-45</a>
                            </div>

                            <a href="#" class="button button-rounded m-0 nott t600 ls0">Email Us</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div>
            <img src="{{url('/')}}/templates/frontend/magenta/demos/coworking/images/location/perth.jpg" alt="Perth">
            <div class="container">
                <div class="card shadow bg-white">
                    <div class="card-body">
                        <h3 class="mb-4">Perth</h3>
                        <div style="font-size: 16px;">
                            <address>
                                Fraser Ave,<br>Perth WA 6005,<br>Australia
                            </address>
                            <div class="mb-4" style="font-size: 15px;">
                                <small class="text-black-50 d-block mb-2">Phone Number:</small>
                                <a href="tel:+(61)8-222-333-44" class="text-dark d-block mb-1">+(61)8-222-333-44</a>
                                <a href="tel:+(61)8-234-532-45" class="text-dark">+(61)8-234-532-45</a>
                            </div>

                            <div class="d-flex">
                                <a href="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d54159.94533455547!2d115.81934154447853!3d-31.96098939372422!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2a32a525098d1f2f%3A0xec4a7b90626730e7!2sKings+Park+and+Botanic+Garden!5e0!3m2!1sen!2sin!4v1554279269646!5m2!1sen!2sin" data-lightbox="iframe" class="button button-rounded button-border m-0 nott t500 ls0">View Map</a>
                                <a href="#" class="button button-rounded m-0 nott t600 ls0 ml-2">Email Us</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
